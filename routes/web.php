<?php

use App\Blog;
use App\Event;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sitemap', 'SitemapController@index');
Route::get('/sitemap/posts', 'SitemapController@blogs');
Route::get('/sitemap/cards', 'SitemapController@cards');
Route::get('/sitemap/items', 'SitemapController@items');
Route::get('/sitemap/quests', 'SitemapController@quests');
Route::get('/sitemap/guides', 'SitemapController@guides');
Route::get('/sitemap/monsters', 'SitemapController@monsters');
Route::get('/sitemap-news', 'SitemapController@news');

Route::get('/', 'FrontendController@index');

Route::get('/privacy-policy', 'FrontendController@privacy');

Route::get('guide/ragnarok-mobile-all-cooking-recipes', function () {
    return redirect('guide/cooking-recipes');
});

Route::get('jobs', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.index', compact('guides', 'events'));
});

Route::get('thief', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.thief', compact('guides', 'events'));
});

Route::get('assassin', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.assassin', compact('guides', 'events'));
});

Route::get('assassin-cross', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.assassin-cross', compact('guides', 'events'));
});

Route::get('merchant', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.merchant', compact('guides', 'events'));
});

Route::get('blacksmith', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.blacksmith', compact('guides', 'events'));
});

Route::get('whitesmith', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.whitesmith', compact('guides', 'events'));
});

Route::get('rogue', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.rogue', compact('guides', 'events'));
});

Route::get('stalker', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.stalker', compact('guides', 'events'));
});

Route::get('acolyte', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.acolyte', compact('guides', 'events'));
});

Route::get('priest', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.priest', compact('guides', 'events'));
});

Route::get('high-priest', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.high-priest', compact('guides', 'events'));
});

Route::get('monk', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.monk', compact('guides', 'events'));
});

Route::get('champion', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.champion', compact('guides', 'events'));
});

Route::get('archer', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.archer', compact('guides', 'events'));
});

Route::get('hunter', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.hunter', compact('guides', 'events'));
});

Route::get('sniper', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.sniper', compact('guides', 'events'));
});

Route::get('alchemist', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.alchemist', compact('guides', 'events'));
});

Route::get('creator', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.creator', compact('guides', 'events'));
});

Route::get('mage', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.magician', compact('guides', 'events'));
});

Route::get('wizard', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.wizard', compact('guides', 'events'));
});

Route::get('high-wizard', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.high-wizard', compact('guides', 'events'));
});

Route::get('swordsman', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.swordsman', compact('guides', 'events'));
});

Route::get('knight', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.knight', compact('guides', 'events'));
});

Route::get('lord-knight', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.lord-knight', compact('guides', 'events'));
});

Route::get('crusader', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.crusader', compact('guides', 'events'));
});

Route::get('paladin', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.jobs.paladin', compact('guides', 'events'));
});

Route::get('skill-simulator', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.pages.skill-simulator', compact('guides', 'events'));
});

Route::get('priest-skill-simulator', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.pages.priest-skill-simulator', compact('guides', 'events'));
});

Route::get('monk-skill-simulator', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.pages.monk-skill-simulator', compact('guides', 'events'));
});

Route::get('knight-skill-simulator', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.pages.knight-skill-simulator', compact('guides', 'events'));
});

Route::get('crusader-skill-simulator', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.pages.crusader-skill-simulator', compact('guides', 'events'));
});

Route::get('valhalla-ruins', function () {
	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    $events = Event::all();
    return view('frontend.pages.valhalla-ruins', compact('guides', 'events'));
});

Route::get('/endless-tower', 'PageController@endlessTower');

Route::get('/monsters', 'FrontendController@monsters');

Route::get('/monster/{slug}', 'FrontendController@showMonster');

Route::get('/monsters/{name}', 'FrontendController@monsterFilter');

Route::get('/items', 'FrontendController@items');

Route::get('/item/{slug}', 'FrontendController@showItem');

Route::get('/access/{slug}', 'FrontendController@dropItem');

Route::get('/cards', 'FrontendController@cards');

Route::get('/card/{slug}', 'FrontendController@showCard');

Route::get('/map/{slug}', 'FrontendController@showMap');

Route::get('/maps', 'FrontendController@allMaps');

Route::get('/guides', 'FrontendController@guides');

Route::get('/guide/{slug}', 'FrontendController@showGuide');

Route::get('/quests', 'FrontendController@quests');

Route::get('/quest/{slug}', 'FrontendController@showQuest');

Route::get('/search', 'FrontendController@search');

Auth::routes();

Route::group(['middleware' => ['auth']], function() {

	Route::get('admin', 'HomeController@index');

	Route::resource('admin/blogs', 'BlogController');
	Route::get('/admin-blogs', 'BlogController@search');

	Route::resource('admin/users', 'UserController');

	Route::resource('admin/monsters', 'MonsterController');
	Route::get('/admin-monsters', 'MonsterController@search');

	Route::resource('admin/items', 'ItemController');
	Route::get('/admin-items', 'ItemController@search');
    Route::get('admin/items/tier/{id}', 'ItemController@tier');
    Route::post('admin/items/tier/save/{id}', 'ItemController@saveTier');

	Route::resource('admin/cards', 'CardController');
	Route::get('/admin-cards', 'CardController@search');

	Route::resource('admin/events', 'EventController');

	Route::resource('admin/drops', 'DropController');

	Route::resource('admin/types', 'TypeController');

	Route::resource('admin/zones', 'ZoneController');

	Route::resource('admin/characters', 'CharacterController');

	Route::resource('admin/races', 'RaceController');

	Route::resource('admin/item-types', 'ItemTypeController');

	Route::resource('admin/sizes', 'SizeController');

	Route::resource('admin/maps', 'MapController');

	Route::resource('admin/elements', 'ElementController');

    Route::resource('admin/jobs', 'JobController');

    Route::resource('admin/skills', 'SkillController');

    Route::resource('admin/levels', 'SkillLevelController');

});