@extends('layouts.blog')

@section('title', 'Search results for ' . $query  . ' - ' . config('app.name'))
@section('description', str_limit(strip_tags('search results for '. $query . ' in Ragnarok Mobile Eternal Love. Ragnarok Eternal Love Database. ROM Database'), 160, ''))
@section('canonical', asset('search?q='.$query))

@section('content')
<div style="padding: 20px" class="col-md-9 pad25 whitebg">

	@if (count($monsters) == 0 && count($cards) == 0 && count($items) == 0 && count($blogs) == 0 || $query == null)
	<p style="padding-top: 10px"><h1>No result found</h1></p>
	@else
	<h1>Search results for <span>{{ $query }}</span></h1>
	<h3>We found<h3>
		<ul>
			@if(count($monsters) > 0)
			<li>{{ count($monsters) }} result in <a href="#monsters">Monster section</a></li>
			@endif

			@if(count($cards) > 0)
			<li>{{ count($cards) }} result in <a href="#cards">Card section</a></li>
			@endif

			@if(count($items) > 0)
			<li>{{ count($items) }} result in <a href="#items">Item and Equipment section</a></li>
			@endif

			@if(count($blogs) > 0)
			<li>{{ count($blogs) }} result in <a href="#blogs">Guide section</a></li>
			@endif
		</ul>
		@if (count($monsters) > 0)
		<h2 id="monsters">Monsters</h2>
		@desktop
		<div class="table-responsive">
			<table class="table">
				<tbody>
					@foreach ($monsters as $monster)
					<tr>
						<td><a href="{{ url('monster/' . $monster->slug) }}"><img src="{{ ($monster->big_image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/monsters/large/' . $monster->big_image) }}"></a></td>
						<td style="vertical-align: middle;"><a href="{{ url('monster/' . $monster->slug) }}">{{ $monster->name }}</a> @auth <br><a href="{{ url('admin/monsters').'/'.$monster->id.'/edit' }}"><i class="fa fa-pencil"></i> Edit</a> @endauth</td>
						<td style="vertical-align: middle;">{{ $monster->element['name'] }}</td>
						<td style="vertical-align: middle;">{{ $monster->race['name'] }}</td>
						<td style="vertical-align: middle;">{{ $monster->size['name'] }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@elsedesktop
		@foreach ($monsters as $monster)
		<div class="whitebg-2">

			<a class="pull-left" href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">
				<img class="is75px75px" src="{{ $monster->big_image == null ? asset('frontend/img/noimage.png') : url('uploads/images/monsters/large').'/'.$monster->big_image }}">
			</a>
			<div class="is-pulled-left">
				<a href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">{{ $monster == null ? '' : $monster->name }}</a>
				<p>{{ $monster == null ? '' : 'Level' }}
					{{ $monster == null ? '' : $monster->level }} / Base: {{ $monster->base_exp }} / Job: {{ $monster->job_exp }}<br>
					{{ $monster->element['name'] }} / {{ $monster->race['name'] }} / {{ $monster->size['name'] }}
				</p>
			</div>


		</div>
		@endforeach
		@enddesktop
		@endif

		@if (count($cards) > 0)
		<h2 id="cards">Cards</h2>
		@desktop
		<div class="table-responsive">
			<table class="table">
				<tbody>
					@foreach ($cards as $card)
					<tr>
						<td style="width: 200px"><a href="{{ url('card/' . $card->slug) }}"><img class="card-image" src="{{ ($card->image == null) ? asset('frontend/img/default-no-mons-image.jpg') : asset('uploads/images/cards/small/' . $card->image) }}" width="88px" height="88px"></a></td>
						<td style="vertical-align: middle; width: 200px"><a href="{{ url('card/' . $card->slug) }}">{{ $card->name }}</a> @auth <br><a href="{{ url('admin/cards').'/'.$card->id.'/edit' }}"><i class="fa fa-pencil"></i> Edit</a> @endauth</td>
						<td style="vertical-align: middle;">{!! $card->content !!}</td>
						<td style="vertical-align: middle;">{!! $card->equipable !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		@elsedesktop
		@foreach ($cards as $card)
		<div class="whitebg-2">
			<a class="pull-left" href="{{ $card == null ? '' : url('card').'/'.$card->slug }}">
				<img class="is115px90px" src="{{ $card->image == null ? asset('frontend/img/default-no-mons-image.jpg') : url('uploads/images/cards/small').'/'.$card->image }}">
			</a>
			<div class="is-pulled-left">
				<a href="{{ $card == null ? '' : url('card').'/'.$card->slug }}">{{ $card == null ? '' : $card->name }}</a>
				<br>
				{!! $card->equipable !!} / {!! $card->type !!}
				<br>
				{!! $card->content !!}
			</p>
		</div>


	</div>
	@endforeach

	@enddesktop
	@endif

	@if (count($items) > 0)
	<h2 id="items">Items/Equipments</h2>
	@desktop
	<div class="table-responsive">
		<table class="table">
			<tbody>
				@foreach ($items as $item)
				<tr>
					<td class="td-image" style="width: 150px"><a href="{{ url('item/' . $item->slug) }}"><img src="{{ ($item->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $item->image) }}" {{ ($item->image == null) ? "style=width:50%" : "" }}></a></td>
					<td style="vertical-align: middle; width: 200px"><a href="{{ url('item/' . $item->slug) }}">{{ $item->name }}</a></td>
					<td style="vertical-align: middle;">{!! $item->content == null ? $item->description : nl2br(e($item->content)) !!}</td>
					<td style="vertical-align: middle;">{{ $item->type }} @auth <br><a href="{{ url('admin/items').'/'.$item->id.'/edit' }}"><i class="fa fa-pencil"></i> Edit</a>          {!! Form::model($item ,['route' => ['items.destroy', $item->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
						<button type="submit" class="btn btn-danger btn-xs">Delete</button>
					{!! Form::close() !!} @endauth</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@elsedesktop
   @foreach ($items as $item)
              <div class="whitebg-2">

              <a style="padding-top: 15px" class="pull-left" href="{{ $item->image == null ? '' : url('item').'/'.$item->slug }}">
                <img class="is75px75px" src="{{ ($item->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $item->image) }}">
              </a>
              <div class="is-pulled-left">
                <a href="{{ $item == null ? '' : url('item').'/'.$item->slug }}">{{ $item == null ? '' : $item->name }}</a>
                <p>{{ $item->type }}<br>{!! str_limit($item->description, 50, '...') !!}
                </p>
              </div>
 

</div>
 @endforeach
	@enddesktop
	@endif

	@if (count($blogs) > 0)
	<h2 id="blogs">Blogs</h2>
	<ul>
		@foreach ($blogs as $blog)
		<li><a href="{{ url('').'/guide/'.$blog->slug }}">{{ $blog->title }}</a></li>
		@endforeach
	</ul>
	@endif

	@endif
</div>



@endsection