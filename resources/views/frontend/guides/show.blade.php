@extends('layouts.blog')

@section('title', $blog->title . ' - ' . config('app.name'))
@section('description', str_limit(strip_tags($blog->excerpt), 160, ''))
@section('image', asset('uploads/images/blogs/'.$blog->image))
@section('canonical', asset('guide/'.$blog->slug))

@section('content')
		<div style="padding: 20px" class="col-md-9 pad25 whitebg">
			<div class="">
				<img src="{{ asset('uploads/images/blogs/'.$blog->image) }}" alt="">
			</div>
			<div class="row">	
					<!--date--><!-- 
				<div class="col-md-1 hidden-xs hidden-sm pad25">	
					<div class="btn btn-medium btn-rounded btn-blog1">
						{{ \Carbon\Carbon::parse($blog->created_at)->format('d') }}<br>{{ \Carbon\Carbon::parse($blog->created_at)->format('M') }}<br>
						<i class="fa fa-comments fa-2x"></i><br>
						<a class="com_no" href="#">0</a>
					</div>
				</div> --><!-- 
				<div class="col-md-1 hidden-lg hidden-md pad25">	
					<div class="btn btn-medium btn-rounded btn-blog2">
						{{ \Carbon\Carbon::parse($blog->created_at)->format('d') }} {{ \Carbon\Carbon::parse($blog->created_at)->format('M') }} <i class="fa fa-comments fa-2x"></i> <a class="com_no" href="#">0</a>
					</div>
				</div> -->
					
				<div class="col-md-11 contents">
					<h1 class="post_link" style="margin-bottom: 0px;">{{ $blog->title }}</h1>
					<div class="post-meta muted">
					    <div class="fb-like" style="margin-bottom: 5px;" data-href="{{ url('guide'.'/'.$blog->slug) }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
						<ul>
							<li>Date <a href="#">{{ \Carbon\Carbon::parse($blog->created_at)->format('M d, Y') }}</a></li>
							<li>Category <a href="{{ url(strtolower($blog->category).'s/') }}">{{ $blog->category }}</a></li>
							<!-- <li>Tags <a href="#">prints</a>, <a href="">design</a> </li> -->
						</ul>
					</div>
						
					{!! $blog->content !!}
					
					<div class="fb-like" style="margin-bottom: 5px;" data-href="{{ url('guide'.'/'.$blog->slug) }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>  
					<div class="pad30"></div>
<div class="fb-comments" data-href="{{ url('guide/'.$blog->slug) }}" data-numposts="5" data-width="100%"></div>
				</div>
			</div>
		</div>

		
@endsection