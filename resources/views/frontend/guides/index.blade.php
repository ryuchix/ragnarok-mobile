@extends('layouts.blog')

@section('title', 'Ragnarok Guides, Quests for Ragnarok Mobile: Eternal Love' . ' - ' . config('app.name'))
@section('description', str_limit(strip_tags('Ultimate guide, updates, quests, refines, requirements for ragnarok mobile eternal love. Read it first at Ragnarok Mobile - Ultimate Guide for Ragnarok M: Eternal Love'), 160, ''))
@section('canonical', asset('guides/'))

@section('content')
		<div class="col-md-9 pad25">
			@foreach ($blogs as $blog)
			<div class="">
				<img src="{{ asset('uploads/images/blogs/'.$blog->image) }}" alt=""></a>
			</div>
			<div class="row">	
					<!--date-->
				<div class="col-md-1 hidden-xs hidden-sm pad25">	
					<div class="btn btn-medium btn-rounded btn-blog1">
						{{ \Carbon\Carbon::parse($blog->created_at)->format('d') }}<br>{{ \Carbon\Carbon::parse($blog->created_at)->format('M') }}<br>
						<i class="fa fa-comments fa-2x"></i><br>
						<a class="com_no" href="#">0</a>
					</div>
				</div>
				<div class="col-md-1 hidden-lg hidden-md pad25">	
					<div class="btn btn-medium btn-rounded btn-blog2">
						{{ \Carbon\Carbon::parse($blog->created_at)->format('d') }} {{ \Carbon\Carbon::parse($blog->created_at)->format('M') }} <i class="fa fa-comments fa-2x"></i> <a class="com_no" href="#">0</a>
					</div>
				</div>
					
				<div class="col-md-11">
					<h1 class="post_link"><a href="{{ url(strtolower($blog->category).'/'.$blog->slug) }}">{{ $blog->title }} </a></h1>
					
					<div class="post-meta muted">
						<ul>
							<li>Category <a href="{{ url(strtolower($blog->category).'s/') }}">{{ $blog->category }}</a></li>
							<!-- <li>Tags <a href="#">prints</a>, <a href="">design</a> </li> -->
						</ul>
					</div>
					{!! $blog->excerpt !!}		
				
					<div class="pad30"></div>
				</div>
			</div>
			@endforeach
			<div style="text-align: center"> {{ $blogs->links('vendor.pagination.default') }}</div>
		</div>



@endsection