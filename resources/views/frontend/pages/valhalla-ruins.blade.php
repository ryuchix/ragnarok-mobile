@extends('layouts.pages')

@section('title', 'Valhalla Ruins Maps Updated Weekly in Ragnarok Mobile Eternal Love')
@section('description', str_limit(strip_tags('Valhalla Ruins Maps List, Valhalla Ruins Maps MVP, Valhalla Ruins Maps Mini. Valhalla Ruins Maps resets every Monday at 6PM GMT+8 on SEA Server. Selected maps consists of MVPS and Mini bosses'), 160, ''))
@section('canonical', asset('valhalla-ruins/'))
@section('image', asset('frontend/img/valhall-ruins.jpg'))

@section('header')
    <style>
        img.alignnone {
            max-width: 100%; 
            display:block; 
            height: auto;
        }
        
    </style>
@endsection

@section('content')

    <h1>Valhalla Ruins Maps </h1>
    <p>This page is updated weekly. Valhalla Ruins or Guild Ruins VR/GR is a dungeon quest for all members of the guild. The ruins consist of 4 doors, by default the portals are closed and you need Silver Medal to open it.
    Your need to reach a certain level to enter the dungeon, level 40, 60, 80 and 100. Each door needs 40 silver medals.
    Unlike Endless Tower the VR/GR maps are the same across all Guilds in all channels.</p>
    
    @include('frontend.pages.simple_dom_php')
    
    
    <?php
    
     //base url
    $base = 'https://ragnamobileguide.com/valhalla-ruins-maps-sea/';
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_URL, $base);
    curl_setopt($curl, CURLOPT_REFERER, $base);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $str = curl_exec($curl);
    curl_close($curl);
    
    // Create a DOM object
    $html_base = new simple_html_dom();
    // Load HTML from a string
    $html_base->load($str);
    
    //get all category links
    foreach($html_base->find('.entry-content') as $element) {
        echo $element;
    }
    
    $html_base->clear(); 
    unset($html_base);
    
    ?>
    
    <em>Thanks to <a href="https://www.facebook.com/Biscusz">Biscusztv</a> and <a href="https://ragnamobileguide.com/valhalla-ruins-maps-sea/">ragnamobileguide</a></em>
    
@endsection

@section('script')
    <script>
        $( document ).ready(function() {

            $(".entry-content").find('p').first().hide();
            $(".entry-content").find('p:nth-child(2)').hide();
            $(".entry-content").find('p:nth-child(3)').hide();
            $(".entry-content").find('p:nth-child(4)').hide();
            $(".entry-content").find('p:nth-child(5)').hide();
            $(".entry-content").find('p').last().hide();
            $('.the_champ_sharing_container').hide();
        
        });
    </script>

@endsection