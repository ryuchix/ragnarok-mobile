@extends('layouts.pages')

@section('title', 'Skill Simulator')
@section('description', str_limit(strip_tags('Endless Tower Floor Monsters List, Endless Tower MVP, Endless Tower Mini. Endless Tower resets every Monday at 6PM GMT+8 on SEA Server. Selected floors consists of MVPS and Mini bosses and they are based on the last digit of the channel you are in. Example, if you are in PH25, all channels that ends in 5 like 5, 15, 25, 35 etc will have the same monsters.'), 160, ''))
@section('canonical', asset('skill-simulator'))
@section('image', asset('frontend/img/endless-tower-min.jpg'))

@section('header')
<style>

    .img {
        position: relative;
        z-index: 9999;
        padding-top: 20px;
        display: inline-block;
        opacity: 0.7;

    }
    .circle {
        position: relative;
        vertical-align: middle;
        display: inline-block;
        width: 230px;
        border-bottom-right-radius: 50px;
        border-top-right-radius: 50px;
        background: #dae8f4;
        height: 40px;
        margin-left: -15px;
        padding: 7px 19px 10px 25px;
    }

    .circle img {
        float: right;
        padding: 1px;
        font-size: 20px;
    }

    .img-container {
        display: inline-block;
        margin-top: -10px;
    }
    .skill-tree .row {
        margin-bottom: 30px;
    }
    .bottomright {
        position: absolute;
        bottom: -20px;
        right: 4px;
        font-size: 15px;
        font-weight: 300;
    }

    .topright {
        position: absolute;
        top: 0px;
        right: 4px;
        font-size: 15px;
        font-weight: 300;
    }


	.notify-badge{
	    position: absolute;
	    right: 0px;
	    top: 20px;
	    background: red;
	    text-align: center;
	    border-radius: 0px 0px 0px 15px;
	    color: white;
	    padding: 0px 4px;
	    font-size: 15px;
	}

	.shape_sin{    
	    border-style: solid;height: 0px; width: 0px;
	    -ms-transform:rotate(360deg); /* IE 9 */
	    -o-transform: rotate(360deg);  /* Opera 10.5 */
	    -webkit-transform:rotate(360deg); /* Safari and Chrome */
	    transform:rotate(360deg);
	    position: absolute;
	    border-width: 0px 25px 20px 0;
	    right: 1px;
	    top: 21px;
	}
	.shape{    
	    border-style: solid;height: 0px; width: 0px;
	    -ms-transform:rotate(360deg); /* IE 9 */
	    -o-transform: rotate(360deg);  /* Opera 10.5 */
	    -webkit-transform:rotate(360deg); /* Safari and Chrome */
	    transform:rotate(360deg);
	    position: absolute;
	    border-width: 0px 25px 20px 0;
	    right: 1px;
	    top: 21px;
	}
	.shape {
	    border-color: rgba(255,255,255,0) #d9534f rgba(255,255,255,0) rgba(255,255,255,0);
	}
	.shape_sin {
	    border-color: rgba(255,255,255,0) #d9534f rgba(255,255,255,0) rgba(255,255,255,0);
	}

	.shape-text{
	color: #fff;
	    color: #fff;
	    font-size: 18px;
	    font-weight: bolder;
	    position: relative;
	    right: -15px;
	    top: -8px;
	    white-space: nowrap;
	    -ms-transform: rotate(30deg);
	    -o-transform: rotate(360deg);
	    /* -webkit-transform: rotate(30deg); */
	    /* transform: rotate(30deg); */
	}

	.this_plus {
	    text-align: center;
	    position: relative;
	    top: 15px;
	}
	.col-xs-5ths,
	.col-sm-5ths,
	.col-md-5ths,
	.col-lg-5ths {
	    position: relative;
	    min-height: 1px;
	    padding-right: 15px;
	    padding-left: 15px;
	}

	.col-xs-5ths {
	    width: 19%;
	    float: left;
	}

	@media (min-width: 768px) {
	    .col-sm-5ths {
	        width: 20%;
	        float: left;
	    }
	}

	@media (min-width: 992px) {
	    .col-md-5ths {
	        width: 7%;
	        float: left;
	    }
	}

	@media (min-width: 1200px) {
	    .col-lg-5ths {
	        width: 20%;
	        float: left;
	    }
	}

	.col-md-5ths {
	    background-color: #eee;
	    margin: 1px;
	    text-align: center;
	    border-radius: 5px;
	    height: 105px;
	}
	.empty {
	    background-color: #fff;
	}
	.class_name {
	    background-color: #2ba5cf;
	    padding: 2px 2px 2px 19px;
	    width: 200px;
	    margin: 10px 10px 10px 0px;
	    border-radius: 78px;
	    color: #fff;
	}

	.remaining_points {

	}

	#total {
	    margin-left: 70px;
	}
</style>
@endsection

@section('content')
<div class="container skill-tree thief">
    <div class="row">
        <div class="class_name"><strong>Thief</strong>
            <span id="total">0</span>/40
        </div>
    </div>
    <!-- 1st row -->
    <div class="row">
        <div class="col-md-5ths col-xs-5ths"> 
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="String Blade Attack<br>Active">
                <div class="img">
                    <div class="shape" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-strong-blade-attack.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Double Attack<br>Passive">
                <div class="img">
                    <div class="shape" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-double-attack.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Improve Flee<br>Passive">
                <div class="img">
                    <div class="shape" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-improve-flee.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Venom Knife<br>Active">
                <div class="img">
                    <div class="shape" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-venom-knife.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Enchant Poison<br>Active">
                <div class="img">
                    <div class="shape" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-enchant-poison.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10" id="enchant-poison">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus">
            </div>
        </div>
    </div>
    <!-- 2nd row -->
    <div class="row">
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Hiding<br>Active">
                <div class="img">
                    <div class="shape" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-hiding.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10" id="hiding">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus hiding">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Detoxify<br>Active">
                <div class="img">
                    <div class="shape" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-detoxify.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="1">0</span>/1</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus">
            </div>
        </div>
    </div>
    <!-- 3rd row -->
    <div class="row">
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Ambush<br>Active<br> <small class='red'>Requires Hiding Lv.5</small>">
                <div class="img">
                    <div class="shape" style="display: none" id="ambushLess">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/thief-ambush.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10" id="ambush">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus ambush" style="opacity: 0.1; pointer-events: none">
            </div>
        </div>  
    </div>
</div>   

<div class="container skill-tree assassin">
    <div class="row">
        <div class="class_name"><strong>Assassin</strong>
            <span id="total_assassin">0</span>/40
        </div>
    </div>
    <!-- 1st row -->
    <div class="row">
        <div class="col-md-5ths col-xs-5ths"> 
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Katar Mastery<br>Passive">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-katar-mastery.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Twin Blade Penetration<br>Passive">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-twin-blade-penetration.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="5">0</span>/5</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Sonic Blow<br>Active">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-sonic-blow.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Grimtooth<br>Active">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-grimtooth.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Enhanced Enchant Poison<br>Passive<br> <small class='red'>Requires Enchant Poison Lv.5</small>">
                <div class="img">
                    <div class="shape_sin" style="display: none" id="EnchantPoisonLess">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-enhanced-enchant-poison.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10" id="enhanced-enchant-poison">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin enhanced-enchant-poison" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
    </div>
    <!-- 2nd row -->
    <div class="row">
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Back Sliding<br>Active">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-back-sliding.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Sonic Acceleration<br>Passive<br><small class='red'>Requires Sonic Blow Lv.5</small> ">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-sonic-acceleration.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="5">0</span>/5</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths empty">
        </div>

    </div>
    <!-- 3rd row -->
    <div class="row">
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths empty">
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Virus Diffusion<br>Active">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-virus-diffusion.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="10">0</span>/10</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
        <div class="col-md-5ths col-xs-5ths">
            <div class="img-container" data-toggle="tooltip" data-placement="bottom" title="Venom Dart<br>Active">
                <div class="img">
                    <div class="shape_sin" style="display: none">
                        <div class="shape-text">
                            -                             
                        </div>
                    </div>
                    <img src="https://ragnarokmobile.net/frontend/img/jobs/skills/assassin-venom-dart.png" alt="" width="60px">
                    <div class="bottomright"><span class="skillpoint" data-max="5">0</span>/5</div>
                </div>
            </div>
            <div class="this_plus">
                <img src="{{ asset('frontend/img/plus.png') }}" alt="" class="plus_sin" style="opacity: 0.1; pointer-events: none;">
            </div>
        </div>
    </div>
</div>   
@endsection

@section('script')
<script src="{{ asset('frontend/js/popper.min.js') }}"></script>
<script>
    
$(document).ready(function(){


    $('.img-container').tooltip({ html: true })

    $('.shape').on('click', function() {

        var span = $(this).siblings().find('span').text();
        var int_span = parseInt(span, 10);

        var max = $(this).siblings().find('.skillpoint').data("max");

        var total = $('#total').text();
        var int_total = parseInt(total);
        $('#total').text(int_total - 1);


        if(int_total < 43) {
        	$('.shape_sin').siblings().find('.skillpoint').text(0);
        	$('.shape_sin').hide();
        	$('.plus_sin').css('opacity', 0.1).css('pointer-events', 'none');
        }

        $(this).siblings().find('span').text(int_span - 1);

        $(this).parent().parent().siblings().find('img').css('opacity', 1).css('pointer-events', 'auto');

        // hide badge and set image opacity to low once skillpoint is 0
        if(int_span == 1) {
            $(this).hide();
            $(this).parent().css('opacity', 0.7);
        }

        var ambush_point = $('#ambush').text();
        var hidingpoint = $('#hiding').text();
        if(hidingpoint < 5){
            $('.ambush').css('opacity', 0.1).css('pointer-events', 'none');
            $('#ambush').text(0);
            $('#ambushLess').hide();
            $('#ambushLess').parent().css('opacity', 0.7);
            $('#total').text(int_total - ambush_point-1);
        }

        var enhanced_enchant_poison = $('#enhanced-enchant-poison').text();
        var enchant_poison = $('#enchant-poison').text();
        // if(enchant_poison < 5){
        //     $('.enhanced-enchant-poison').css('opacity', 0.1).css('pointer-events', 'none');
        //     $('#enhanced-enchant-poison').text(0);
        //     $('#EnchantPoisonLess').hide();
        //     $('#EnchantPoisonLess').parent().css('opacity', 0.7);
        //     // $('#total_assassin').text(int_total - enhanced_enchant_poison-1);
        // }



    });

    $('.plus').on('click', function(){

        var span = $(this).parent().siblings().find('.skillpoint').text();
        var int_span = parseInt(span, 10);

        var max = $(this).parent().siblings().find('.skillpoint').data("max");
        
        var total = $('#total').text();
        var int_total = parseInt(total);
        
        var hidingpoint = $('#hiding').text();
        var int_hidingpoint = parseInt(hidingpoint);
        var enchant_poison = $('#enchant-poison').text();

        if(int_total <= 39) {
            var badge = $(this).parent().siblings().find('.shape').show();
            $('#total').text(int_total + 1);
            if(int_span < max) {
                $(this).parent().siblings().find('.skillpoint').text(int_span + 1);
                $(this).parent().siblings().find('.img').css('opacity', 1);
            } 
            if(int_span >= max-1) {
                $(this).css('opacity', 0.1).css('pointer-events', 'none');
            }
            if(int_hidingpoint >= 4) {
                $('.ambush').css('opacity', 1.0).css('pointer-events', 'auto');
            }
            if(enchant_poison >= 5-1) {
                $('.enhanced-enchant-poison').css('opacity', 1.0).css('pointer-events', 'auto');
            }
            // if($('#ambush').text() == 10) {
            // 	$(this).css('opacity', 0.1).css('pointer-events', 'none');
            // }
        }
        if(int_total == 39) {
        	$('.plus_sin').css('opacity', 1).css('pointer-events', 'auto');
        }

    });

    // assassin cross
    $('.shape_sin').on('click', function() {

        var span = $(this).siblings().find('span').text();
        var int_span = parseInt(span, 10);

        var max = $(this).siblings().find('.skillpoint').data("max");

        var total = $('#total_assassin').text();
        var int_total = parseInt(total);
        $('#total_assassin').text(int_total - 1);


        $(this).siblings().find('span').text(int_span - 1);

        $(this).parent().parent().siblings().find('img').show();

        if(int_span == 1) {
            $(this).hide();
            $(this).parent().css('opacity', 0.7);
        }

        var ambush_point = $('#ambush').text();
        var hidingpoint = $('#hiding').text();
        if(hidingpoint < 5){
            $('.ambush').css('opacity', 0.1).css('pointer-events', 'none');
            $('#ambush').text(0);
            $('#ambushLess').hide();
            $('#ambushLess').parent().css('opacity', 0.7);
            $('#total_assassin').text(int_total - ambush_point-1);
        }

        var enhanced_enchant_poison = $('#enhanced-enchant-poison').text();
        var enchant_poison = $('#enchant-poison').text();
        if(enchant_poison < 5){
            $('.enhanced-enchant-poison').css('opacity', 0.1).css('pointer-events', 'none');
            $('#enhanced-enchant-poison').text(0);
            $('#EnchantPoisonLess').hide();
            $('#EnchantPoisonLess').parent().css('opacity', 0.7);
            $('#total_assassin').text(int_total - enhanced_enchant_poison-1);
        }

    });


    $('.plus_sin').on('click', function(){

        var span = $(this).parent().siblings().find('.skillpoint').text();
        var int_span = parseInt(span, 10);

        var max = $(this).parent().siblings().find('.skillpoint').data("max");
        
        var total = $('#total_assassin').text();
        var int_total = parseInt(total);
        
        var total_1 = $('#total').text();
        var int_total1 = parseInt(total_1);

        // var hidingpoint = $('#hiding').text();
        // var enchant_poison = $('#enchant-poison').text();

        if(int_total <= 39 && int_total1 >= 40) {
            var badge = $(this).parent().siblings().find('.shape_sin').show();
            $('#total_assassin').text(int_total + 1);
            if(int_span < max) {
                $(this).parent().siblings().find('.skillpoint').text(int_span + 1);
                $(this).parent().siblings().find('.img').css('opacity', 1);
            } 
            if(int_span >= max-1) {
                $(this).hide();
            }
            // if(hidingpoint >= 5-1) {
            //     $('.ambush').css('opacity', 1.0).css('pointer-events', 'auto');
            // }
            // if(enchant_poison >= 5-1) {
            //     $('.enhanced-enchant-poison').css('opacity', 1.0).css('pointer-events', 'auto');
            // }
        }

    });

});


</script>
@endsection