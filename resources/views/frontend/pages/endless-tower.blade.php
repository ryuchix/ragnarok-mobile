@extends('layouts.endless-tower')

@section('title', 'Endless Tower in Ragnarok Mobile Eternal Love')
@section('description', str_limit(strip_tags('Endless Tower Floor Monsters List, Endless Tower MVP, Endless Tower Mini. Endless Tower resets every Monday at 6PM GMT+8 on SEA Server. Selected floors consists of MVPS and Mini bosses and they are based on the last digit of the channel you are in. Example, if you are in PH25, all channels that ends in 5 like 5, 15, 25, 35 etc will have the same monsters.'), 160, ''))
@section('canonical', asset('endless-tower/'))
@section('image', asset('frontend/img/endless-tower-min.jpg'))

@section('header')
<style>
    body {
        font-family: 'Lato';
        margin: 0 auto;
        width: 100%;
    }
    table {
        width: 100%;
    }
    table th {
        text-align: left;
        font-size: 18px;
        background: #446CB3;
        color: #fff;
        padding-left: 10px;
    }
    table tr {
        font-size: 16px;
        background: #ECECEC;
    }
    table tr:hover {
        background: #D2D7D3;
    }
    table td {
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 10px;
    }
    caption {
        font-size: 20px;
        padding: 20px;
    }
    #wp-calendar {
        display:none;
    }
    
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) { 
        body {
            width: 100% !important;
        }
        table th {
        font-size: 16px;
    }
        table tr {
        font-size: 14px;
    }
        table td {
        padding-right: 5px;
    }
    }

    .align-middle a > img {
        display: block !important;
        padding-bottom: 3px;
    }

    .align-middle img {
        display: block !important;
        padding-bottom: 3px;
    }

    .table>thead>tr>th {
        padding: 0px !important;
        text-align: center;
    }
    
</style>

@desktop
    <style type="text/css"></style>
@elsedesktop
    <style type="text/css">
    .align-middle a > img {
        display: block !important;
        padding-bottom: 3px;
        min-width: 40px;
    }

    </style>
@enddesktop

@endsection



@section('content')
<h1>Endless Tower</h1>
<p>Endless Tower resets every Monday at 6PM GMT+8 on SEA Server. Selected floors consists of MVPS and Mini bosses and they are based on the last digit of the channel you are in. Example, if you are in PH25, all channels that ends in 5 like 5, 15, 25, 35 etc will have the same monsters.</p>

<div id="put-here"></div>

@include('frontend.pages.simple_dom_php')

<?php

 //base url
$base = 'http://www.roguard.net/game/endless-tower/';

$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_URL, $base);
curl_setopt($curl, CURLOPT_REFERER, $base);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
$str = curl_exec($curl);
curl_close($curl);

// Create a DOM object
$html_base = new simple_html_dom();
// Load HTML from a string
$html_base->load($str);

//get all category links
foreach($html_base->find('.table-responsive') as $element) {
    echo $element;
}

$html_base->clear(); 
unset($html_base);

?>

<em>Thank you roguard.net for the list.</em>
<div class="clear"></div>
<div class="fb-comments" data-href="{{ url('endless-tower') }}" data-numposts="5"  data-width="100%"></div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('frontend/js/site.js') }}"></script>
@endsection