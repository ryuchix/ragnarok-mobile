@extends('layouts.pages')

@section('title', 'Swordsman, Knight, Lord Knight Skill Simulator in Ragnarok Mobile Eternal Love. ROM Skill Calculator')
@section('description', str_limit(strip_tags('Ragnarok Mobile Skill Simulator, Ragnarok Mobile skill planner, ROM skill simulator, ROM skill planner, ROM character skills, skill planner, skill planner, ragnarok mobile, character skill report, rom skill database, rom skill description'), 160, ''))
@section('canonical', asset('knight-skill-simulator'))
@section('image', asset('frontend/img/endless-tower-min.jpg'))

@section('content')

<h1 class="simulator-title">Swordsman/Knight/Lord Knight Skill Simulator</h1>
    <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#myModal" id="open-modal" style="margin-left: 20px;margin-top: -35px;">Summary</button>

<div class="icon" id="smax1">
	<em id="span">Swordsman: <span id="re" class="smax1">0</span>/40 <span style="float: right;">Remaining points: <span id="rejob" class="rejob">0</span></span></em>
</div>

<div class="job" id="job1">
	<!-- 1st row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" aftNum="3" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_15001.png" width="60px" alt="">
			<span>Bash</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" aftNum="7" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_18001.png" width="60px" alt="">
			<span>Sword Mastery</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" aftNum="4" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_17001.png" width="60px" alt="">
			<span>Taunt</span>
		</div>
	</div>
	<!-- 2nd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" preNum="0" preLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_16001.png" width="60px" alt="">
			<span>Magnum Break</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4">
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" preNum="2" preLevel="5" aftNum="8" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_14001.png" width="60px" alt="">
			<span>Endure</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" preNum="3" preLevel="5" id="agility">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_19001.png" width="60px" alt="">
			<span>Increase Recuperative Power</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4">
	</div>
	<div class="col-xs-4 col-sm-4">
	</div>
</div>

<div class="icon" id="smax2">
	<em id="span">Knight: <span id="re" class="smax2">0</span>/40 <span style="float: right;">Remaining points: <span class="rejob">0</span></span></em>
</div>
<div class="job" id="job2">
	<!-- 1st row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_32001.png" width="60px" alt="">
			<span>Counter Attack</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill"  max="10" now="0" job="2" aftNum="17" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_22001.png" width="60px" alt="">
			<span>One-Hand Quicken</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" preNum="4" preLevel="5">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_44001.png" width="60px" alt="">
			<span>Heart of Steel</span>
		</div>
	</div>
	<!-- 2nd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="2" preNum="8" preLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_20001.png" width="60px" alt="">
			<span>Bowling Bash</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" aftNum="12" aftLevel="5">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_24001.png" width="60px" alt="">
			<span>Cavalry Combat</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="2" aftNum="16" aftLevel="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_29001.png" width="60px" alt="">
			<span>Aura Blade</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" preNum="10" preLevel="5" aftNum="13" aftLevel="5">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_24001.png" width="60px" alt="">
			<span>Cavalry Mastery</span>
		</div>
	</div>
	<!-- 4th row -->
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="2" preNum="12" preLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_23001.png" width="60px" alt="">
			<span>Brandish Spear</span>
		</div>
	</div>	
	<!-- 5th row -->
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="2" aftNum="15" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_26001.png" width="60px" alt="">
			<span>Spear Mastery</span>
		</div>
	</div>
	<!-- 5th row -->
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="2" preNum="14" preLevel="5" aftNum="18" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_21001.png" width="60px" alt="">
			<span>Pierce</span>
		</div>
	</div>
</div>
<div class="icon" id="smax3">
	<em id="span">Lord Knight: <span id="re" class="smax3">0</span>/40 <span style="float: right;">Remaining points: <span class="rejob">0</span></span></em>
</div>
<div class="job" id="job3">
	<!-- 1st row -->
	<div class="col-xs-4 col-sm-4 skill-image skill"  max="10" now="0" job="3" preNum="11" preLevel="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_25001.png" width="60px" alt="">
			<span>Head Crush</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3" aftNum="7" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_68001.png" width="60px" alt="">
			<span>Sword Parry</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3" preNum="15" preLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_28001.png" width="60px" alt="">
			<span>Spiral Pierce</span>
		</div>
	</div>
	<!-- 2nd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_27001.png" width="60px" alt="">
			<span>Concentration</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3" aftNum="23" aftLevel="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_69001.png" width="60px" alt="">
			<span>HP Alight</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_34001.png" width="60px" alt="">
			<span>Call of Justice</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_31001.png" width="60px" alt="">
			<span>Lord's Aura</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3" preNum="20" preLevel="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_33001.png" width="60px" alt="">
			<span>Frenzy</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/knight-skills') }}/skill_30001.png" width="60px" alt="">
			<span>Joint Beat</span>
		</div>
	</div>
</div>

 <!-- sample modal content -->
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Skill Simulator Summary</h4>
        </div>
        <div id="screen">
		</div>
		<div style="text-align: center; margin: 20px 20px 0px 20px">
			<button id="screen_this" type="button" class="btn btn-primary btn-lg">Save as Image</button>
			<button id="screen_thiss" type="button" class="btn btn-primary btn-lg" style="display: none"><i class="fa fa-spinner fa-spin"></i> Loading</button>
			<button id="download_image" style="display: none" type="button" class="btn btn-primary btn-lg" >Download</button>
		</div>
			
		<div id="capture" class="modal-body" style="display: block;overflow: auto;">
	        <h3 style="float: right;">Remaining Points: <span id="remaining-points">0</span></h3>
	        <h3>Used Points: <span id="used-points">0</span></h3><small class="copyright_text">This skill simulator is provided by www.ragnarokmobile.net. Create yours now.</small>
			<p class="spans first_job">Swordsman:  <span>0</span>/40</p>
	        <div id="myskill_1"></div>
	        <div class="clear"></div>
	        <p class="spans second_job">Knight:  <span>0</span>/40</p>
	        <div id="myskill_2"></div>
	        <div class="clear"></div>
	        <p class="spans third_job">Lord Knight:  <span>0</span>/40</p>
	        <div id="myskill_3"></div>
	        <div class="clear"></div>
        </div>
        <div class="modal-footer" style="border-top: 0px">
          
        </div>
	 </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

@endsection

@section('script')
<script src="{{ asset('frontend/js/html2canvas.min.js') }}"></script>
<script src="{{ asset('frontend/js/simulator.js') }}"></script>

@endsection