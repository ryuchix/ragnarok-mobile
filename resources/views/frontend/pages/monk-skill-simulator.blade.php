@extends('layouts.pages')

@section('title', 'Acolyte, Monk, Champion Skill Simulator in Ragnarok Mobile Eternal Love. ROM Skill Calculator')
@section('description', str_limit(strip_tags('Ragnarok Mobile Skill Simulator, Ragnarok Mobile skill planner, ROM skill simulator, ROM skill planner, ROM character skills, skill planner, skill planner, ragnarok mobile, character skill report, rom skill database, rom skill description'), 160, ''))
@section('canonical', asset('monk-skill-simulator'))
@section('image', asset('frontend/img/endless-tower-min.jpg'))

@section('content')

<h1 class="simulator-title">Acolyte/Monk/Champion Skill Simulator</h1>
    <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#myModal" id="open-modal" style="margin-left: 20px;margin-top: -35px;">Summary</button>

<div class="icon" id="smax1">
	<em id="span">Acolyte: <span id="re" class="smax1">0</span>/40 <span style="float: right;">Remaining points: <span id="rejob" class="rejob">0</span></span></em>
</div>

<div class="job" id="job1">
	<!-- 1st row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" aftNum="9" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_145001.png" width="60px" alt="">
			<span>Holy Light</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_144001.png" width="60px" alt="">
			<span>Heal</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_150001.png" width="60px" alt="">
			<span>Kyrie Eleison</span>
		</div>
	</div>
	<!-- 2nd row -->
	<div class="col-xs-4 col-sm-4">
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" aftNum="5" aftLevel="5" id="blessing">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_146001.png" width="60px" alt="">
			<span>Blessing</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="1" now="0" job="1">
		<div class="skill-icon">
			<em>0/1</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_147001.png" width="60px" alt="">
			<span>Ruwach</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4">
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1" preNum="3" preLevel="5" id="agility">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_157001.png" width="60px" alt="">
			<span>Increase Agility</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="1">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_148001.png" width="60px" alt="">
			<span>Light Shield</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4">
	</div>
	<div class="col-xs-4 col-sm-4">
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="1">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_149001.png" width="60px" alt="">
			<span>Maces Mastery</span>
		</div>
	</div>
</div>

<div class="icon" id="smax2">
	<em id="span">Monk: <span id="re" class="smax2">0</span>/40 <span style="float: right;">Remaining points: <span class="rejob">0</span></span></em>
</div>
<div class="job" id="job2">
	<!-- 1st row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" aftNum="11" aftLevel="5">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_301001.png" width="60px" alt="">
			<span>Call Spirits</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill"  max="10" now="0" job="2">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_311001.png" width="60px" alt="">
			<span>Iron Fist</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="2" aftNum="13" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_313001.png" width="60px" alt="">
			<span>Triple Attack</span>
		</div>
	</div>
	<!-- 2nd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="1" now="0" job="2" preNum="8" preLevel="5">
		<div class="skill-icon">
			<em>0/1</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_315001.png" width="60px" alt="">
			<span>Zen</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" aftNum="15" aftLevel="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_303001.png" width="60px" alt="">
			<span>Critical Explosion</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" preNum="10" preLevel="5" aftNum="16" aftLevel="3" >
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_304001.png" width="60px" alt="">
			<span>Chain Combo</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="2" aftNum="17" aftLevel="5">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_314001.png" width="60px" alt="">
			<span>Flee</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" preNum="12" preLevel="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_306001.png" width="60px" alt="">
			<span>Asura Strike</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" preNum="13" preLevel="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_305001.png" width="60px" alt="">
			<span>Combo Finish</span>
		</div>
	</div>
	<!-- 4th row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" preNum="14" preLevel="5">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_307001.png" width="60px" alt="">
			<span>Steel Body</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" aftNum="20" aftLevel="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_312001.png" width="60px" alt="">
			<span>Spirits Recovery</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="2" aftNum="23" aftLevel="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_310001.png" width="60px" alt="">
			<span>Investigate</span>
		</div>
	</div>
	<!-- 5th row -->
	<div class="col-xs-4 col-sm-4"></div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="1" now="0" job="2" preNum="18" preLevel="3">
		<div class="skill-icon">
			<em>0/1</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_309001.png" width="60px" alt="">
			<span>Body Relocation</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4"></div>
</div>
<div class="icon" id="smax3">
	<em id="span">Champion: <span id="re" class="smax3">0</span>/40 <span style="float: right;">Remaining points: <span class="rejob">0</span></span></em>
</div>
<div class="job" id="job3">
	<!-- 1st row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3" aftNum="24" aftLevel="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_322001.png" width="60px" alt="">
			<span>Spirit Bomb Refining</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill"  max="10" now="0" job="3" aftNum="25" aftLevel="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_316001.png" width="60px" alt="">
			<span>Tiger Knuckle Fist</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3" aftNum="21" aftLevel="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_321001.png" width="60px" alt="">
			<span>Finger Offensive</span>
		</div>
	</div>
	<!-- 2nd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3" preNum="21" preLevel="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_325001.png" width="60px" alt="">
			<span>Critical Explosion - Break</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3" preNum="22" preLevel="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_317001.png" width="60px" alt="">
			<span>Chain Crush Combo</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_323001.png" width="60px" alt="">
			<span>Fist Refining</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3" preNum="22" preLevel="5">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_320001.png" width="60px" alt="">
			<span>Blade Stop</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_318001.png" width="60px" alt="">
			<span>Ki Explosion</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 skill-image skill" max="10" now="0" job="3">
		<div class="skill-icon">
			<em>0/10</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_324001.png" width="60px" alt="">
			<span>Preemptive Strike</span>
		</div>
	</div>
	<!-- 3rd row -->
	<div class="col-xs-4 col-sm-4 skill-image skill" max="5" now="0" job="3">
		<div class="skill-icon">
			<em>0/5</em>
			<b><img src="{{ asset('frontend/img/minus.png') }}" width="20px"></b>
			<img class="skill-image" src="{{ asset('frontend/img/acolyte-skills') }}/skill_319001.png" width="60px" alt="">
			<span>Palm Push Strike</span>
		</div>
	</div>
	<div class="col-xs-4 col-sm-4">
	</div>
	<div class="col-xs-4 col-sm-4">
	</div>
</div>

 <!-- sample modal content -->
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Skill Simulator Summary</h4>
        </div>
        <div id="screen">
		</div>
		<div style="text-align: center; margin: 20px 20px 0px 20px">
			<button id="screen_this" type="button" class="btn btn-primary btn-lg">Save as Image</button>
			<button id="screen_thiss" type="button" class="btn btn-primary btn-lg" style="display: none"><i class="fa fa-spinner fa-spin"></i> Loading</button>
			<button id="download_image" style="display: none" type="button" class="btn btn-primary btn-lg" >Download</button>
		</div>
			
		<div id="capture" class="modal-body" style="display: block;overflow: auto;">
	        <h3 style="float: right;">Remaining Points: <span id="remaining-points">0</span></h3>
	        <h3>Used Points: <span id="used-points">0</span></h3><small class="copyright_text">This skill simulator is provided by www.ragnarokmobile.net. Create yours now.</small>
			<p class="spans first_job">Acolyte:  <span>0</span>/40</p>
	        <div id="myskill_1"></div>
	        <div class="clear"></div>
	        <p class="spans second_job">Monk:  <span>0</span>/40</p>
	        <div id="myskill_2"></div>
	        <div class="clear"></div>
	        <p class="spans third_job">Champion:  <span>0</span>/40</p>
	        <div id="myskill_3"></div>
	        <div class="clear"></div>
        </div>
        <div class="modal-footer" style="border-top: 0px">
          
        </div>
	 </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

@endsection

@section('script')
<script src="{{ asset('frontend/js/html2canvas.min.js') }}"></script>
<script src="{{ asset('frontend/js/simulator.js') }}"></script>

@endsection