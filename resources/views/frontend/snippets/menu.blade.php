<!--header-->
<div class="header">
<!--menu-->
<nav id="main_menu" class="navbar" role="navigation">
  <div class="container">
        <div class="navbar-header">
    <!--toggle-->
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
			<i class="fa fa-bars"></i>
		</button>
	<!--logo-->
		<div class="logo">
			<a href="{{ config('app.url', 'https://www.ragnarokmobile.net') }}" title="Welcome to Ragnarok Mobile; Your source for Ragnarok M: Eternal Love"><img src="{{ asset('frontend/img/logowhite.png') }}" alt="" class="animated bounceInDown" /></a> 
		</div>
	</div>
       
        <div class="collapse navbar-collapse" id="menu">
            <ul class="nav navbar-nav pull-right">
               			<li class="dropdown {{ active('/') }}"><a href="{{ url('/') }}" title="Home">Home</a></li>
						<li class="dropdown {{ active('guides*') }} {{ active('quests*') }}"><a href="{{ url('/guides') }}" title="Guides">Guides</a></li>
						<li class="dropdown 
						{{ active('monsters') }}
						{{ active('monster*') }}
						{{ active('cards') }}
						{{ active('card*') }}
						{{ active('items') }}
						{{ active('item*') }}"><a href="javascript:{}">Database</a>
							<ul class="dropdown-menu" style="display: none;">
								<li><a href="{{ url('monsters') }}" title="Monsters">Monsters</a></li>
								<li><a href="{{ url('cards') }}" title="Cards">Cards</a></li>
								<li><a href="{{ url('items') }}" title="Items">Items</a></li>
								<li><a href="{{ url('maps') }}" title="Maps">Maps</a></li>
								<li><a href="{{ url('valhalla-ruins') }}" title="Valhalla Ruins">Valhalla Ruins</a></li>
							</ul>
						</li>
						<li class="dropdown {{ active('jobs') }}"><a href="{{ url('jobs') }}" title="Ragnarok Jobs and Skills">Jobs and Skills</a></li>
						<li class="dropdown {{ active('guide/cooking-recipes') }}"><a href="{{ url('guide/cooking-recipes') }}" title="Cooking Recipes">Cooking Recipes</a></li>
						<li class="dropdown {{ active('endless-tower') }}"><a href="{{ url('endless-tower') }}" title="Endless Tower">Endless Tower</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
<!--//header-->



<!-- <li class="dropdown {{ active('monsters*') }}"><a href="{{ url('monsters') }}" title="Monsters">Monsters</a></li>
						<li class="dropdown {{ active('cards') }}"><a href="{{ url('cards') }}" title="Cards">Cards</a></li>
						<li class="dropdown {{ active('items') }}"><a href="{{ url('items') }}" title="Items">Items</a></li>							 -->

