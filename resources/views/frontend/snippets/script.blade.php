<script src="{{ asset('frontend/js/jquery.js') }}"></script>			
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>	
<script src="{{ asset('frontend/js/jquery.nivo.slider.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/scripts.js') }}"></script>
<!-- carousel -->
<script type="text/javascript" src="{{ asset('frontend/js/jquery.carouFredSel-6.2.1-packed.js') }}"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=551581325353902';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127889756-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127889756-1');
</script>
@yield('script')
