<!-- sidebar -->
<div class="sidebar col-md-3">

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-4504257796054092",
          enable_page_level_ads: true
     });
</script>
 <h4><span>Like us on Facebook</span></h4>
	<div class="fb-page" data-href="https://www.facebook.com/Ragnarok-Mobile-Database-2187501354865223" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/readnewsfeedph" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/readnewsfeedph">Read Newsfeed</a></blockquote></div>
<h4><span>Search</span></h4>

<form action="{{ url('search') }}" method="get">        
	<div class="box-body">
		<div class="form-group">
			@desktop
			<input type="text" name="q" class="desktop-search-input" style="font-size: 20px;" placeholder="search..." autocomplete="off">
			<div class="pull-left" style="width: 100%"><button type="submit" class="btn btn-primary btn-custom btn-rounded" style="width: 100%"><strong>Submit</strong></button>&nbsp;</div>
			@elsedesktop
			<input type="text" name="q" class="mobile-search-input" placeholder="search..." autocomplete="off">
			<div class="pull-left" style="width: 100%"><button type="submit" class="btn btn-primary btn-custom btn-rounded mobile-search-submit-btn" style="">Submit</button>&nbsp;</div>
			@enddesktop
			<div class="clear"></div>
		</div>
	</div>
</form>

@include('frontend.events.index')

<h4><span>Latest guides</span></h4>
<ul>
    @foreach ($guides as $guide)
        <li><a href="{{ url('').'/guide/'.$guide->slug }}" title="Read {{ $guide->title }}">{{ $guide->title }}</a></li>
    @endforeach
</ul>
</div>