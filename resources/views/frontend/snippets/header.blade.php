<head>
	<meta charset="utf-8">
	<title>@yield('title', config('app.name', 'Ragnarok Mobile | ROM')) | ROM</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0">
	<meta name="description" content="@yield('description', 'Your ultimate guide for Ragnarok Mobile Eternal Love. Your source for Ragnarok M Monsters, Cards, Quests, Database, Items, Market Prices, Exchange Price List and Stats and Skills calculator. ROM')">
	<meta name="author" content="Ryok">
	<meta name="keywords" content="@yield('keywords', 'ROM, ROM Exchange price, market finance, Ragnarok, online, RO, ragnarok mobile, ragnarok m, ragnarok eternal love, database, guide, job, quest, headgear quest, monster drops, item information, skill description, skill simulator, stat calculator, ragnarok tools, ragnarok mobile english')">

	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="@yield('title', config('app.name', 'Ragnarok Mobile'))" />
	<meta property="og:description" content="@yield('description', 'Your ultimate guide for Ragnarok Mobile Eternal Love. Your source for Ragnarok M Monsters, Cards, Quests, Database, Items and Stats and Skills calculator.')" />
	<link rel="canonical" href="@yield('canonical', config('APP_URL', 'https://www.ragnarokmobile.net'))" /> 
	<meta property="og:url" content="@yield('canonical', config('APP_URL', 'https://www.ragnarokmobile.net'))" />
	<meta property="og:site_name" content="@yield('title', config('app.name', 'Ragnarok Mobile'))" />
	<meta property="fb:app_id" content="551581325353902" />
	<meta property="og:image" content="@yield('image', asset('frontend/img/default-og-image.png'))" />
	<meta property="og:image:secure_url" content="@yield('image', asset('frontend/img/default-og-image.png'))" />
	<meta property="og:image:width" content="640" />
	<meta property="og:image:height" content="380" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:description" content="@yield('description', 'Your ultimate guide for Ragnarok Mobile Eternal Love. Your source for Ragnarok M Monsters, Cards, Quests, Database, Items and Stats and Skills calculator.')" />
	<meta name="twitter:title" content="@yield('title', config('app.name', 'Ragnarok Mobile'))" />
	<meta name="twitter:site" content="@i3ryok" />
	<meta name="twitter:image" content="@yield('image', asset('frontend/img/default-og-image.png'))" />
	<meta name="twitter:creator" content="@i3ryok" />
    <link rel="shortcut icon" href="{{ asset('image/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('image/favicon.ico') }}" type="image/x-icon">
	<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
	<meta name="google-site-verification" content="tXkumO3D7QVjD_ULWknN52UfzrI86JCjDnqSSZK2xis" />
	<!--[if IE]>
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
	<![endif]-->

	<link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/theme.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/settings.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/colour.css') }}" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	@yield('header')
</head>