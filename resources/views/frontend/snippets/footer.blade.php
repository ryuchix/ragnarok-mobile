<div id="footer">
	<h3 class="center follow">
		<div style="text-align: center;">
			<img src="{{ asset('frontend/img/logowhite.png') }}" alt="" class="animated tada" width="200px" />
		</div>
		<p>
			Hey guys! Please like our facebook page to get latest news and updates from the game and from us. Also, we invite everyone to join our growing community. Thank you and keep sharing {{ config('APP_NAME', 'Ragnarok  Mobile - Ultimate Guide for Ragnarok M: Eternal Love') }}
		</p>
	</h3>
	<div class="follow_us">
		<a href="#" class="fa fa-twitter follow_us" title="Follow us on Twitter"></a>
		<a href="https://www.facebook.com/ragnarokmobile.net" class="fa fa-facebook follow_us" title="Like us on Facebook"></a>
		<a href="#" class="fa fa-youtube follow_us" title="Subscribe to our youtube channel"></a>
	</div>
</div>

<!-- footer 2 -->
<div id="footer2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="copyright">
					{{ config('APP_NAME', 'Ragnarok  Mobile - Ultimate Guide for Ragnarok M: Eternal Love') }}
					&copy;
					<script type="text/javascript">
					//<![CDATA[
					var d = new Date()
					document.write(d.getFullYear())
						//]]>
					</script>
					- All Rights Reserved
				</div>
				<div><a href="{{ url('privacy-policy') }}">Privacy Policy</a></div>
			</div>
		</div>
	</div>
</div>
<!-- up to top -->
<a href="#"><i class="go-top fa fa-angle-double-up"></i></a>
<!--//end-->