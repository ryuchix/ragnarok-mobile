<div id="banner">
	<div class="container intro_wrapper">
		<div class="inner_content">

			<!--welcome-->
			<div class="welcome_index">
				Here you will find <span class="hue_block white normal">Guides</span>, <span class="hue_block white normal">Tips</span>, and game database such as <span class="hue">Monsters</span>, <span class="hue">Cards</span> and <span class="hue">Items</span> that will help you in the game.
			</div>
			<!--//welcome-->
		</div>
	</div>
</div>
<!--//banner-->