@extends('layouts.page')

@section('title', $card->name .', ' . $card->name .' effect, ' . $card->name . ' drop rate. Card Market Price, Exchange Price for Ragnarok Mobile')
@section('description', str_limit(strip_tags('Get ' . $card->name . ', '.$card->name . ' effect. Card Market Price, Exchange Price Ragnarok Cards database, Ragnarok card drop, What monsters drops the card, Card effect, card permanent buff effect, cards drop rate. Search Ragnarok cards. Show Ragnarok Cards.'), 160, ''))
@section('canonical', asset('card/'.$card->slug))

@section('banner')
<!-- <div id="banner">
	<div class="container intro_wrapper">
		<div class="inner_content">
			<h1 class="title">{{ $card->name }}</h1>
			<h1 class="intro">
				<h2>Card Effect</h2>
				{!! $card->content !!}
				<h2>Permanent Buff</h2>
				{!! $card->buff !!}
			</h1>
		</div>
	</div>
</div> -->
<!--//banner-->
@endsection

@section('content')
<div class="post">
	<div class="row">
		<div class="col-md-12 ">

			<div class="row whitebg">
				<div class="col-md-4">
					<img src="{{ $card->image == null ? asset('frontend/img/default-no-mons-image.jpg') : asset('uploads/images/cards/small/' . $card->image) }}" alt="">
				</div> 
				<div class="col-md-8 card-name">
					<h1><strong>{{ $card->name }}</strong></h1>
					<table class="table table-bordered table-striped">
						<tr>
							<td style="vertical-align: middle;">Main Effect</td>
							<td>{!! nl2br($card->content) !!}</td>
						</tr>
						<tr>
							<td style="vertical-align: middle;">Draw/Loot/Craft Effect</td>
							<td>{!! $card->buff !!}</td>
						</tr>
						<tr>
							<td style="vertical-align: middle;">Deposit Effect</td>
							<td>{!! $card->deposit !!}</td>
						</tr>
						<tr>
							<td style="vertical-align: middle;">Slot</td>
							<td>{{ $card->equipable }}</td>
						</tr>
						<tr>
							<td style="vertical-align: middle;">Type</td>
							<td>{{ $card->type }}</td>
						</tr>
					</table>
				</div>
				<div class="clear"></div>
		        	<div class="col-md-12">
		            <div class="card">
		                <div class="card-body">

		                <?php if ($prices != null): ?>
					        <h2>Market Exchange Price</h2>
							<?php 	
								$market_dates = array();
								$market_prices = array();
								$market_prices_sea = array();
							 ?>
		                	<?php foreach ($prices as $price): ?>

							<?php foreach ($price['global']['week']['data'] as $key => $value): ?>

								<?php $market_dates[] = Carbon\Carbon::parse($value['time'])->format('F d'); ?>

								<?php $market_prices[] = (strval($value['price'])) ?>

							<?php endforeach ?>

							<?php foreach ($price['sea']['week']['data'] as $key => $value): ?>

								<?php $market_prices_sea[] = strval($value['price']) ?>

							<?php endforeach ?>

		                	<div class="item-chart_server__37cgg">
		                		@desktop
		                		<span class="item-chart_server-name__3bqrM item-chart_global__2ppHw" style="color: #007bff">Global</span>
		                		<span class="item-chart_server-price__1r2rn">{{ number_format($price['global']['latest']) }} Z</span>
			                	<span class="item-chart_server-{{ $price['global']['week']['change'] == 0 ? "normal" : $price['global']['week']['change'] < 0 ? "red" : "success" }}">{{ $price['global']['week']['change'] > 0 ? "+" : "" }}{{ $price['global']['week']['change'] }}</span>
		                	 	@else
								<table>
			                		<tr>
			                			<td><span class="item-chart_server-name__3bqrM item-chart_global__2ppHw" style="color: #007bff;  display: inline-block; text-align: right;">Global</span></td>
			                			<td><span class="item-chart_server-price__1r2rn">{{ number_format($price['global']['latest']) }} Z</span></td>
			                			<td><span class="item-chart_server-{{ $price['global']['week']['change'] == 0 ? "normal" : $price['global']['week']['change'] < 0 ? "red" : "success" }}">{{ $price['global']['week']['change'] > 0 ? "+" : "" }} {{ $price['global']['week']['change'] }}</span></td>
			                		</tr>
			                	</table>
		                	 	@enddesktop
		                	</div>

		                	<div class="item-chart_server__37cgg">
		                		@desktop
								<span class="item-chart_server-name__3bqrM item-chart_global__2ppHw">Sea</span>
								<span class="item-chart_server-price__1r2rn">{{ number_format($price['sea']['latest']) }} Z</span>
									<span class="item-chart_server-{{ $price['sea']['week']['change'] == 0 ? "normal" : $price['sea']['week']['change'] < 0 ? "red" : "success" }}" >{{ $price['sea']['week']['change'] > 0 ? "+" : ""}}{{ $price['sea']['week']['change'] }}</span>
		                	 	@else
								<table>
			                		<tr>
			                			<td><span class="item-chart_server-name__3bqrM item-chart_global__2ppHw" style="color: #dc3545; display: inline-block; text-align: right;">Sea</span></td>
			                			<td><span class="item-chart_server-price__1r2rn">{{ number_format($price['sea']['latest']) }} Z</span></td>
			                			<td><span class="item-chart_server-{{ $price['sea']['week']['change'] == 0 ? "normal" : $price['sea']['week']['change'] < 0 ? "red" : "success" }}">{{ $price['sea']['week']['change'] > 0 ? "+" : ""}} {{ $price['sea']['week']['change'] }}</span></td>
			                		</tr>
			                	</table>
		                	 	@enddesktop
		                		
		                			
		                	</div>
		                <?php endforeach ?>

		                 @desktop
		                    <canvas id="chLine" height="100"></canvas>
		                    <small>	API provided by: romexchange.com</small>
		                @else
		                    <canvas id="chLine" height="300"></canvas>
		                    <small>	API provided by: romexchange.com</small>
		                <?php endif ?>
		                
		               
		                @enddesktop
		                </div>
		            </div>
		        </div>
				<h2 style="padding-left: 15px;">Dropped by</h2>
				<ul class="dropped-ul">
					<li class="dropped-li">
						<div class="is-clearfix">
							@foreach($monsters as $monster)
							@if($monster->slug != "hydra" && $monster->slug != "goblinknife" && $monster->slug != "goblinbuckler" && $monster->slug != "goblinhammer" && $monster->slug != "goblinspear" && $monster->slug != "goblinaxe" && $monster->slug != "marc" && $monster->slug != "deniro" && $monster->slug != "piere" && $monster->slug != "horong" && $monster->slug != "sohee-1" && $monster->slug != "rideword-1" && $monster->slug != "baphomet-jr-1" && $monster->slug != "alarm-1" && $monster->slug != "bathory-1")
							<a class="pull-left" href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">
								<img class="is75px75px" src="{{ $monster->big_image == null ? asset('frontend/img/noimage.png') : url('uploads/images/monsters/large').'/'.$monster->big_image }}">
							</a>
							<div class="is-pulled-left">
								<a href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">{{ $monster == null ? '' : $monster->name }}</a>
								<p><span>{{ $monster == null ? '' : 'Level' }}</span>
									<span>{{ $monster == null ? '' : $monster->level }}</span><br>
									<span>{{ $monster == null ? '' : $monster->type['name'] }}</span>
								</p>
							</div>
							@endif
							@endforeach
						</div>
					</li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

@endsection

<?php if ($prices != null): ?>
@section('script')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>

<script type="text/javascript">
	/* chart.js chart examples */

// chart colors
var colors = ['#007bff','#28a745','#333333','#c3e6cb','#dc3545','#6c757d'];

/* large line chart */
var chLine = document.getElementById("chLine");
var globaldates = <?php echo json_encode($market_dates) ?>;
var globalprices = <?php echo json_encode($market_prices) ?>;
var seaprices = <?php echo json_encode($market_prices_sea) ?>;
var chartData = {

  labels: globaldates,
  datasets: [{
    data: globalprices,
    backgroundColor: 'transparent',
    borderColor: colors[0],
    borderWidth: 4,
    pointBackgroundColor: colors[0],
    label: "Global"
  },
  {
    data: seaprices,
    backgroundColor: colors[3],
    borderColor: colors[1],
    borderWidth: 4,
    pointBackgroundColor: colors[1],
    label: "SEA"
  }]
};

if (chLine) {
  new Chart(chLine, {
  type: 'line',
  data: chartData,
  options: {
			tooltips: {
			  callbacks: {
					title: function (tooltipItem, data) { return data.labels[tooltipItem[0].index]; },
                        label: function (tooltipItem, data) {
                            var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            var total = eval(data.datasets[tooltipItem.datasetIndex].data.join("+"));
                            return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " Z";
                        },
                        //footer: function(tooltipItem, data) { return 'Total: 100 planos.'; }
                    
			  } // end callbacks:
			}, //end tooltips
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: false,
          userCallback: function(value, index, values) {
							// Convert the number to a string and splite the string every 3 charaters from the end
							value = value.toString();
							value = value.split(/(?=(?:...)*$)/);
							value = value.join(',');
							return value + " Z";
						}
        }
      }]
    },
    legend: {
      display: true
    }
  }
  });
}

</script>

@endsection

<?php endif ?>