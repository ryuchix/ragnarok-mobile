@extends('layouts.page')

@section('title', 'Ragnarok Cards, Card Effect, Card Drop Rate for Ragnarok Mobile')
@section('description', str_limit(strip_tags('Ragnarok Cards database, Ragnarok card drop, What monsters drops the card, Card effect, card permanent buff effect, cards drop rate. Search Ragnarok cards. Show Ragnarok Cards.'), 160, ''))
@section('canonical', asset('cards/'))


@section('banner')
<div id="banner">
	<div class="container intro_wrapper">
		<div class="inner_content">
			<h1 class="title">Cards</h1>
			<h1 class="intro">
				Here you can find Ragnarok Card and their effect. You can use the <strong>search</strong> function to find the specific card.
			</h1>
		</div>
	</div>
</div>
<!--//banner-->
@endsection
@section('content')
<form action="{{ url('cards') }}" method="get">        
  <div class="box-body">
    <div class="form-group">
      @desktop
      <input type="text" name="search" style="width: 30%; margin-bottom: 5px" placeholder="Search card" value="{{ isset($_GET['search']) != null ? $_GET['search'] : "" }}">
      <button type="submit" class="btn btn-primary btn-custom btn-rounded">Submit</button>
      <a class="btn btn-info btn-custom btn-rounded advanced">Advanced</a>
    <div id="advanced-d" style="display: none;">      
      <select name="slot" id="filter" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Slots</option>
          <option value="Weapon">Weapon</option>
          <option value="Armor">Armor</option>
          <option value="Garment">Garment</option>
          <option value="Footgear">Footgear</option>
          <option value="Headgear">Headgear</option>
          <option value="Shield">Shield</option>
          <option value="Accessory">Accessory</option>
        </select>
    </div>

    </div>    
    @elsedesktop
    <input type="text" name="search" style="width: 100%; margin-bottom: 5px" placeholder="Search card" value="{{ isset($_GET['search']) != null ? $_GET['search'] : "" }}">
    <div id="advanced-d" style="display: none;">      
      <select name="slot" id="filter" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Slots</option>
          <option value="Weapon">Weapon</option>
          <option value="Armor">Armor</option>
          <option value="Garment">Garment</option>
          <option value="Footgear">Footgear</option>
          <option value="Headgear">Headgear</option>
          <option value="Shield">Shield</option>
          <option value="Accessory">Accessory</option>
        </select>
    </div>
      <div class="form-group" style="margin-top: 5px">
          <button type="submit" class="btn btn-primary btn-custom btn-rounded">Submit</button>
          <a class="btn btn-info btn-custom btn-rounded advanced">Advanced</a>
        </div>
    @enddesktop
  </div>
</form>
@desktop
    <div class="table-responsive">
      <table class="table">
        <tbody>
        @if (count($cards) > 0)
         @php $count = 1 @endphp
         @foreach ($cards as $card)
         <tr>
          <td style="width: 200px"><a href="{{ url('card/' . $card->slug) }}"><img class="card-image" src="{{ ($card->image == null) ? asset('frontend/img/default-no-mons-image.jpg') : asset('uploads/images/cards/small/' . $card->image) }}" width="88px" height="88px"></a></td>
          <td style="vertical-align: middle; width: 200px"><a href="{{ url('card/' . $card->slug) }}">{{ $card->name }}</a> @auth <br><a href="{{ url('admin/cards').'/'.$card->id.'/edit' }}"><i class="fa fa-pencil"></i> Edit</a> @endauth</td>
          <td style="vertical-align: middle;">{!! nl2br($card->content) !!}</td>
          <td style="vertical-align: middle;">{!! $card->equipable !!}</td>
        </tr>
        @php $count ++ @endphp
        @endforeach
        @else
          <p style="padding-top: 20px">No result found</p>
        @endif
      </tbody>
    </table>
  </div>
  @elsedesktop
  @if (count($cards) > 0)  
   @foreach ($cards as $card)
              <div class="whitebg-2" style="height: 115px; overflow:hidden">
              @desktop  
              <a class="pull-left" href="{{ $card == null ? '' : url('card').'/'.$card->slug }}">
                <img class="is75px75px" src="{{ $card->image == null ? asset('frontend/img/default-no-mons-image.jpg') : url('uploads/images/cards/small').'/'.$card->image }}">
              </a>
              @elsedesktop
              <a class="pull-left" href="{{ $card == null ? '' : url('card').'/'.$card->slug }}">
                <img class="is115px90px" src="{{ $card->image == null ? asset('frontend/img/default-no-mons-image.jpg') : url('uploads/images/cards/small').'/'.$card->image }}">
              </a>
              @enddesktop
              <div class="is-pulled-left">
                <a href="{{ $card == null ? '' : url('card').'/'.$card->slug }}">{{ $card == null ? '' : $card->name }}</a>
                <br>
                {!! $card->equipable !!} / {!! $card->type !!}
                <br>
                {!! nl2br($card->content) !!}
                </p>
              </div>
 

</div>
 @endforeach
 @else 
<div class="whitebg-2">
  <p style="padding-top: 10px">No result found</p>
</div>
 @endif

  @enddesktop

  <div style="text-align: center"> {!! $cards->appends(Input::except('page'))->links('vendor.pagination.default') !!}</div>

  @endsection

  @section('script')
<script>
  $('.advanced').on('click', function(){
    $('#advanced-d').toggle();
  });
  
    $(document).ready(function(){
          
        var url_string = window.location.href;
        var url = new URL(url_string);
        var c = url.searchParams.get("slot");
    
        $('#filter option[value="'+c+'"]').prop('selected', true);
    
      });
  
</script>
  @endsection