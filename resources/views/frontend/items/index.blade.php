@extends('layouts.page')

@section('title', 'Ragnarok Items, Ragnarok Equipments, Euipment Drop Rate for Ragnarok Mobile')
@section('description', str_limit(strip_tags('Ragnarok items database, Ragnarok equipment drop, What monsters drops the item, equipment effect, item drop rate. Search Ragnarok equipment, items. Show Ragnarok equipments.'), 160, ''))
@section('canonical', asset('items/'))


@section('banner')
<div id="banner">
	<div class="container intro_wrapper">
		<div class="inner_content">
			<h1 class="title">Items/Equipments</h1>
			<h1 class="intro">
				Here you can find Ragnarok Items, Equipment, Drop location. You can use the <strong>search</strong> function to find the specific item.
			</h1>
		</div>
	</div>
</div>
<!--//banner-->
@endsection
@section('content')
<!-- <form action="{{ url('items') }}" method="get" style="float: right">        
  <div class="box-body">
    <div class="form-group"><a href="{{ url('items') }}">Clear</a> 
      <select name="filter" id="filter" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
        <option value="none">Filter by</option>
        @foreach($item_types as $item_type)
        <option value="{{ $item_type->name }}">{{ $item_type->name }}</option>
        @endforeach
      </select>
    </div>
  </div>
</form> -->
<form action="{{ url('items') }}" method="get">        
  <div class="box-body">
    <div class="form-group">
      @desktop
      <input type="text" name="search" style="width: 30%; margin-bottom: 5px" placeholder="Search item"  value="{{ isset($_GET['search']) != null ? $_GET['search'] : "" }}">
      <button type="submit" class="btn btn-primary btn-custom btn-rounded">Submit</button>
      <a class="btn btn-info btn-custom btn-rounded advanced-d">Advanced</a>
      <div id="advanced-d" style="display: none;">
        <!-- filters -->
        <select name="item_type" id="advanced" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Item Types</option>
          @foreach($item_types as $item_type)
          <option value="{{ $item_type->name }}">{{ $item_type->name }}</option>
          @endforeach
        </select>

      </div>
    </div>
      @elsedesktop
        <input type="text" name="search" style="width: 100%; margin-bottom: 5px" placeholder="Search item"  value="{{ isset($_GET['search']) != null ? $_GET['search'] : "" }}">
      <div id="advanced" style="display: none;">
        <!-- filter -->
      <select name="item_type" id="advanced" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
        <option value="">All Item Types</option>
        @foreach($item_types as $item_type)
        <option value="{{ $item_type->name }}">{{ $item_type->name }}</option>
        @endforeach
      </select>
    </div>
        <div class="form-group" style="margin-top: 5px">
          <button type="submit" class="btn btn-primary btn-custom btn-rounded">Submit</button>
          <a class="btn btn-info btn-custom btn-rounded advanced">Advanced</a>
        </div>
      @enddesktop

      </div>
</form>
<style>
  
.td-image {
  min-width: 100px;
}

</style>
@desktop
<div class="table-responsive">
    <table class="table">
      <tbody>
        @if (count($items) > 0)
      	@php $count = 1 @endphp
      	@foreach ($items as $item)
      	<tr>
      		<td class="td-image" style="width: 150px"><a href="{{ url('item/' . $item->slug) }}"><img src="{{ ($item->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $item->image) }}"></a></td>
      		<td style="vertical-align: middle; width: 200px"><a href="{{ url('item/' . $item->slug) }}">{{ $item->name }}</a></td>
      		<td style="vertical-align: middle;">{!! $item->content == null ? $item->description : nl2br(e($item->content)) !!}</td>
          <td style="vertical-align: middle;">{{ $item->type }} @auth <br><a href="{{ url('admin/items').'/'.$item->id.'/edit' }}"><i class="fa fa-pencil"></i> Edit</a>          {!! Form::model($item ,['route' => ['items.destroy', $item->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
          <button type="submit" class="btn btn-danger btn-xs">Delete</button>
          {!! Form::close() !!} @endauth</td>
      	</tr>
      	@php $count ++ @endphp
      	@endforeach
        @else 
          <p style="padding-top: 20px">No result found</p>
        @enddesktop
      </tbody>
    </table>
  </div>
  @elsedesktop
  @if (count($items) > 0)
   @foreach ($items as $item)
              <div class="whitebg-2">

              <a style="padding-top: 15px" class="pull-left" href="{{ $item->image == null ? '' : url('item').'/'.$item->slug }}">
                <img class="is75px75px" src="{{ ($item->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $item->image) }}">
              </a>
              <div class="is-pulled-left">
                <a href="{{ $item == null ? '' : url('item').'/'.$item->slug }}">{{ $item == null ? '' : $item->name }}</a>
                <p>{{ $item->type }}<br>{!! str_limit($item->description, 50, '...') !!}
                </p>
              </div>
 

</div>
 @endforeach
 @else 
<div class="whitebg-2">
  <p style="padding-top: 10px">No result found</p>
</div>
 @endif
  @enddesktop

  <div style="text-align: center"> {!! $items->appends(Input::except('page'))->links('vendor.pagination.default') !!}</div>

@endsection

@section('script')
<script>
      $('.advanced').on('click', function(){
        $('#advanced').toggle();
      });
      $('.advanced-d').on('click', function(){
        $('#advanced-d').toggle();
      });
      
    $(document).ready(function(){
          
        var url_string = window.location.href;
        var url = new URL(url_string);
        var c = url.searchParams.get("item_type");
    
        $('#advanced option[value="'+c+'"]').prop('selected', true);
    
      });
  
</script>
@endsection