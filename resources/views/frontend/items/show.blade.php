@extends('layouts.page')

@section('title', $item->name .', ' . $item->name .' effect, ' . $item->name . ' drop rate ' . $item->name .' Market Price, Exchange Price for Ragnarok Mobile')
@section('description', str_limit(strip_tags('Get ' . $item->name . ', '.$item->name . ' effect.' . $item->name .' Market Price, Exchange Price Ragnarok items database, Ragnarok item drop, What monsters drops the item, item effect, item permanent buff effect, items drop rate. Search Ragnarok items. Show Ragnarok items.'), 160, ''))
@section('canonical', url('item' .'/'. $item->slug))

@section('banner')
<!-- to do -->
@endsection

@section('content')

<div class="post">
	<div class="row whitebg">
		<div class="col-md-12">
			<img src="{{ asset('uploads/images/items/small/' . $item->image)}}" alt="" class="pull-left">

			<div class="is-pulled-left">
				<h1 style="margin-top: 0px; margin-bottom: -5px">{{ $item->name }}</h1>
				<p><span>{{ $item->type }} {{ ($item->type == "Equipment") ? "- ".$item->equipable : "" }}</span></p>
			</div>
			<br>
			<div class="alert alert-info"><h3>{!! $item->description !!}</h3></div>
			<div class="clear"></div>
			<div class="col-md-12 item-info">
		        <div class="col-md-12">
		            <div class="card">
		                <div class="card-body">

		                <?php if ($prices != null): ?>
					        <h2>Market Exchange Price</h2>
							<?php 	
								$market_dates = array();
								$market_prices = array();
								$market_prices_sea = array();
							 ?>
		                	<?php foreach ($prices as $price): ?>

							<?php foreach ($price['global']['week']['data'] as $key => $value): ?>

								<?php $market_dates[] = Carbon\Carbon::parse($value['time'])->format('F d'); ?>

								<?php $market_prices[] = (strval($value['price'])) ?>

							<?php endforeach ?>

							<?php foreach ($price['sea']['week']['data'] as $key => $value): ?>

								<?php $market_prices_sea[] = strval($value['price']) ?>

							<?php endforeach ?>

		                	<div class="item-chart_server__37cgg">
		                		@desktop
		                		<span class="item-chart_server-name__3bqrM item-chart_global__2ppHw" style="color: #007bff">Global</span>
		                		<span class="item-chart_server-price__1r2rn">{{ number_format($price['global']['latest']) }} Z</span>
			                	<span class="item-chart_server-{{ $price['global']['week']['change'] == 0 ? "normal" : $price['global']['week']['change'] < 0 ? "red" : "success" }}">{{ $price['global']['week']['change'] > 0 ? "+" : "" }}{{ $price['global']['week']['change'] }}</span>
		                	 	@else
								<table>
			                		<tr>
			                			<td><span class="item-chart_server-name__3bqrM item-chart_global__2ppHw" style="color: #007bff;  display: inline-block; text-align: right;">Global</span></td>
			                			<td><span class="item-chart_server-price__1r2rn">{{ number_format($price['global']['latest']) }} Z</span></td>
			                			<td><span class="item-chart_server-{{ $price['global']['week']['change'] == 0 ? "normal" : $price['global']['week']['change'] < 0 ? "red" : "success" }}">{{ $price['global']['week']['change'] > 0 ? "+" : "" }} {{ $price['global']['week']['change'] }}</span></td>
			                		</tr>
			                	</table>
		                	 	@enddesktop
		                	</div>
		                	<div class="item-chart_server__37cgg">
		                		@desktop
								<span class="item-chart_server-name__3bqrM item-chart_global__2ppHw">Sea</span>
								<span class="item-chart_server-price__1r2rn">{{ number_format($price['sea']['latest']) }} Z</span>
									<span class="item-chart_server-{{ $price['sea']['week']['change'] == 0 ? "normal" : $price['sea']['week']['change'] < 0 ? "red" : "success" }}" >{{ $price['sea']['week']['change'] > 0 ? "+" : ""}}{{ $price['sea']['week']['change'] }}</span>
		                	 	@else
								<table>
			                		<tr>
			                			<td><span class="item-chart_server-name__3bqrM item-chart_global__2ppHw" style="color: #dc3545; display: inline-block; text-align: right;">Sea</span></td>
			                			<td><span class="item-chart_server-price__1r2rn">{{ number_format($price['sea']['latest']) }} Z</span></td>
			                			<td><span class="item-chart_server-{{ $price['sea']['week']['change'] == 0 ? "normal" : $price['sea']['week']['change'] < 0 ? "red" : "success" }}">{{ $price['sea']['week']['change'] > 0 ? "+" : ""}} {{ $price['sea']['week']['change'] }}</span></td>
			                		</tr>
			                	</table>
		                	 	@enddesktop
		                		
		                			
		                	</div>

		                <?php endforeach ?>

		                 @desktop
		                    <canvas id="chLine" height="100"></canvas>
		                    <small>	API provided by: romexchange.com</small>
		                @else
		                    <canvas id="chLine" height="300"></canvas>
		                    <small>	API provided by: romexchange.com</small>
		                <?php endif ?>
		                
		               
		                @enddesktop
		                </div>
		            </div>
		        </div>
			<div class="col-md-12 item-info">
				@if($item->type == "Equipment")
				<h2>Basic Information</h2>
				<table class="table table-bordered table-striped">
					<tr>
						<td>Type</td>
						<td>{{ $item->type }}</td>
					</tr>
					<tr>
						<td>Slot</td>
						<td>{{ $item->equipable }}</td>
					</tr>
					<tr>
						<td>Jobs</td>
						<td>
							@foreach($jobs as $job)
								{{ $job->name }}, 
							@endforeach
						</td>
					</tr>
					<tr>
						<td>Effect</td>
						<td>
							{!! nl2br(e($item->content)) !!}
						</td>
					</tr>
					@if ($item->upgrade_of != 0)
					<tr>
						<td>Upgrade of</td>
						<td>
							<a href="{{ url('item').'/'.$item_upgrade_of->slug }}" class="vertical-align"><img src="{{ asset('uploads/images/items/small/' . $item_upgrade_of->image)}}"> {{ $item_upgrade_of->name }}</a>
						</td>
					</tr>
					@endif
					@if ($item->upgrade_to != 0)
					<tr>
						<td>Upgradable to</td>
						<td>
							<a href="{{ url('item').'/'.$item_upgrade_to->slug }}" class="vertical-align"><img src="{{ asset('uploads/images/items/small/' . $item_upgrade_to->image)}}"> {{ $item_upgrade_to->name }}</a>
						</td>
					</tr>
					@endif

				</table>
				@endif
				<div class="clear"></div>
				@if($item->has_craft == 1)
				<div class="col-md-12 item-info">
					<h2>Formula</h2>
					<ul class="fa-ul" style="margin-left: 0px">
						@foreach ($item_crafts as $item_craft)
						@php $citem = \App\Item::find($item_craft->item_id) @endphp
						<li style="padding-bottom: 10px;"> <a href="{{ url('item').'/'.$citem->slug }}"><img src="{{ ($item->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $citem->image) }}" alt="" width="25px" height="25px">  {{ $citem->name }}</a> x {{ $item_craft->quantity }}</li>
						@endforeach
					</ul>
					@if($item->craft_location != null || $item->craft_location != 0)
					<h2>Location</h2>
					<img src="{{ asset('uploads/images/maps/small/').'/'.$map->image }}" class="map-image">
					<div class="clear"></div>
					{{ $map->name }}
					@endif
				</div>
				@endif

			<!-- START item tiers section -->
			@if($itemTiers != null && count($itemTiers) > 0)
			<div class="col-md-12 item-info">
				<h2>Tier Upgrade</h2>

				
				<table class="table table-bordered table-striped">
					<tbody>
						@foreach($itemTiers as $itemTier)
						<tr>
							<td width="100" rowspan="2">{{ $itemTier->tier }}</td>
							<td>{{ $itemTier->effect }}</td>
						</tr>
						<tr>
							<td>
								<ul style="list-style-type: none; margin-left: -40px">
									@php
									$process = \App\ItemTierProcess::where('item_tier_id', $itemTier->id)->with('item:id,name,image,slug')->get();
									@endphp

									@foreach($process as $p_item)
									
										<li style="padding: 5px 5px 5px 0px">
											<a href="{{ url('item').'/'.$p_item['item']['slug'] }}"><img src="{{ ($p_item['item']['image'] == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $p_item['item']['image']) }}" alt="" width="25px" height="25px"> {{ $p_item['item']['name'] }}</a> x {{ $p_item->quantity }}
										</li>

									@endforeach
								</ul>
							</td>
						</tr>
						@endforeach
						@if ($item->upgrade_to != 0)
						<tr>
    						<td width="100" rowspan="2">Final<td>
							<a href="{{ url('item').'/'.$item_upgrade_to->slug }}" class="vertical-align"><img src="{{ asset('uploads/images/items/small/' . $item_upgrade_to->image)}}"> {{ $item_upgrade_to->name }}</a>
						</td></td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
			@endif
			<!-- END item tiers section -->
			</div>

			</div>

			@if(count($monsters) > 0 || count($item_drops) > 0)
			<h2>Dropped by</h2>
			<ul class="dropped-items-ul">
				@foreach ($monsters as $monster)
				<li class="dropped-li">
					<div class="is-clearfix">
						<a class="pull-left" href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">
							<img class="is75px75px" src="{{ $monster->big_image == null ? asset('frontend/img/noimage.png') : url('uploads/images/monsters/large').'/'.$monster->big_image }}">
						</a>
						<div class="is-pulled-left">
							<a href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">{{ $monster == null ? '' : $monster->name }}</a>
							<p><span>{{ $monster == null ? '' : 'Level' }}</span>
								<span>{{ $monster == null ? '' : $monster->level }}</span><br>
								<span>{{ $monster == null ? '' : $monster->type['name'] }}</span>
							</p>
						</div>
					</div>
				</li>
				@endforeach
				@foreach ($item_drops as $item_drop)
				<li class="dropped-li">
					<div class="is-clearfix">
						<a class="pull-left" href="{{ $item_drop == null ? '' : url('access').'/'.$item_drop->slug }}">
							<img class="is75px75px" src="{{ $item_drop->image == null ? asset('frontend/img/noimage.png') : url('uploads/images/drops/small').'/'.$item_drop->image }}">
						</a>
						<div class="is-pulled-left">
							<a href="{{ $item_drop == null ? '' : url('access').'/'.$item_drop->slug }}">{{ $item_drop == null ? '' : $item_drop->name }}</a>
							<p><span>{{ $item_drop->type }}</span>
						</div>
					</div>
				</li>
				<br>
				@endforeach
			</ul>
			@endif

		</div>
   	</div>
</div>

@endsection
<?php if ($prices != null): ?>
@section('script')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>

<script type="text/javascript">
	/* chart.js chart examples */

// chart colors
var colors = ['#007bff','#28a745','#333333','#c3e6cb','#dc3545','#6c757d'];

/* large line chart */
var chLine = document.getElementById("chLine");
var globaldates = <?php echo json_encode($market_dates) ?>;
var globalprices = <?php echo json_encode($market_prices) ?>;
var seaprices = <?php echo json_encode($market_prices_sea) ?>;
var chartData = {

  labels: globaldates,
  datasets: [{
    data: globalprices,
    backgroundColor: 'transparent',
    borderColor: colors[0],
    borderWidth: 4,
    pointBackgroundColor: colors[0],
    label: "Global"
  },
  {
    data: seaprices,
    backgroundColor: colors[3],
    borderColor: colors[1],
    borderWidth: 4,
    pointBackgroundColor: colors[1],
    label: "SEA"
  }]
};

if (chLine) {
  new Chart(chLine, {
  type: 'line',
  data: chartData,
  options: {
			tooltips: {
			  callbacks: {
					title: function (tooltipItem, data) { return data.labels[tooltipItem[0].index]; },
                        label: function (tooltipItem, data) {
                            var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            var total = eval(data.datasets[tooltipItem.datasetIndex].data.join("+"));
                            return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " Z";
                        },
                        //footer: function(tooltipItem, data) { return 'Total: 100 planos.'; }
                    
			  } // end callbacks:
			}, //end tooltips
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: false,
          userCallback: function(value, index, values) {
							// Convert the number to a string and splite the string every 3 charaters from the end
							value = value.toString();
							value = value.split(/(?=(?:...)*$)/);
							value = value.join(',');
							return value + " Z";
						}
        }
      }]
    },
    legend: {
      display: true
    }
  }
  });
}

</script>

@endsection

<?php endif ?>