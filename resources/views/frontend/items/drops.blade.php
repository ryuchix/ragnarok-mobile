@extends('layouts.page')

@section('title', $drop->name .', ' . $drop->name .' effect, ' . $drop->name . ' drop rate ' . 'for Ragnarok Mobile')
@section('description', str_limit(strip_tags('Get ' . $drop->name . ', '.$drop->name . ' effect.' . ' Ragnarok drops database, Ragnarok drop drop, What monsters drops the drop, drop effect, drop permanent buff effect, drops drop rate. Search Ragnarok drops. Show Ragnarok drops.'), 160, ''))
@section('canonical', url($drop->slug))

@section('content')

<div class="post">
	<div class="row whitebg">
		<div class="col-md-12">
			<img src="{{ asset('uploads/images/drops/small/' . $drop->image)}}" alt="" class="pull-left">

			<div class="is-pulled-left">
				<h1 style="margin-top: 0px; margin-bottom: -5px">{{ $drop->name }}</h1>
				<p><span>{{ $drop->type }}</span></p>
			</div>
			<br>
			<div class="alert alert-info"><h3>{!! $drop->description !!}</h3></div>
			<div class="clear"></div>
			@if(count($item_drops) > 0)
			<h2>Contains</h2>
			<ul class="dropped-items-ul">
				@foreach ($item_drops as $item_drop)
				<li class="dropped-li">
					<div class="is-clearfix">
						<a class="pull-left" href="{{ $item_drop == null ? '' : url('item').'/'.$item_drop->slug }}">
							<img class="is75px75px" src="{{ $item_drop->image == null ? asset('frontend/img/noimage.png') : url('uploads/images/items/small').'/'.$item_drop->image }}">
						</a>
						<div class="is-pulled-left">
							<a href="{{ $item_drop == null ? '' : url('item').'/'.$item_drop->slug }}">{{ $item_drop == null ? '' : $item_drop->name }}</a>
							<p><span>{{ $item_drop->type }}</span>
						</div>
					</div>
				</li>
				<br>
				@endforeach
			</ul>
			@endif

		</div>
   	</div>
</div>

@endsection