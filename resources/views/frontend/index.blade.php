@extends('layouts.frontend')

@section('content')	
<form action="{{ url('search') }}" method="get">        
	<div class="box-body">
		<div class="form-group">
			@desktop
			<input type="text" name="q" class="desktop-search-input" placeholder="search..." autocomplete="off" style="padding: 15px">
			<div class="center"><button type="submit" class="btn btn-primary btn-custom btn-rounded" style=" "><strong>Submit</strong></button>&nbsp;</div>
			@elsedesktop
			<input type="text" name="q" class="mobile-search-input" placeholder="search..." autocomplete="off">
			<div class="center"><button type="submit" class="btn btn-primary btn-custom btn-rounded mobile-search-submit-btn" style="
			">Submit</button>&nbsp;</div>
			@enddesktop
			<div id="advanced-d" style="display: none;">

			</div>
		</div>
	</div>
</form>
<div class="col-md-12 pad45">
	<div class="row">
		<div class="row">
			<div class="col-sm-6 col-md-3">
				<div class="tile">
					<img class="tile-image" alt="" src="{{ asset('frontend/img/weapon.png') }}">
					<h3 class="tile-title">Equipments</h3>
					<p>Checkout our complete ragnarok items and equipments database. Filter them to narrow your search.</p>
					<h6><a class="btn btn-primary btn-custom btn-rounded" href="{{ url('items') }}"><b>read more</b></a></h6>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="tile">
					<img class="tile-image" alt="" src="{{ asset('frontend/img/monster.png') }}">
					<h3 class="tile-title">Monsters</h3>
					<p>Go check our monsters database and know their locations, statistics, attributes and drops.</p>
					<h6><a class="btn btn-primary btn-custom btn-rounded" href="{{ url('monsters') }}"><b>read more</b></a></h6>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="tile">
					<img class="tile-image" alt="" src="{{ asset('frontend/img/cards.png') }}">
					<h3 class="tile-title">Cards</h3>
					<p>View our complete collection of Ragnarok Mobile cards with their drop location, drop rate and who drops it.</p>
					<h6><a class="btn btn-primary btn-custom btn-rounded" href="{{ url('cards') }}"><b>read more</b></a></h6>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="tile tile-hot">
					<img class="tile-image" alt="" src="{{ asset('frontend/img/cooking.png') }}">
					<h3 class="tile-title">Cooking Recipes</h3>
					<p>Get the complete list of cooking recipes in game. Start cooking and level up your cooking manual.</p>
					<h6><a class="btn btn-primary btn-custom btn-rounded" href="{{ url('guide/cooking-recipes') }}"><b>read more</b></a></h6>
				</div>
			</div>
		</div><!-- /tiles -->
	</div>
</div>

<div class="row">
	<!--column 2 slider-->
		<!-- <div class="col-xs-12 col-sm-12 col-md-12 pad30">
			<h1 id="grid-options">Latest Updates</h1>
			<div id="slider_home">
				@foreach($blogs as $blog)
				<div class="slider-item" style="width: 253px">	
					<div class="slider-image">
						<div class="hover_colour" style="width: 100%">
							<a href="{{ url(strtolower($blog->category) . '/' . $blog->slug) }}">
								<img src="{{ asset('uploads/images/blogs/'.$blog->image) }}" alt="" height="170px" /></a>
						</div>
					</div>
					<div class="slider-title">
						<h3><a href="{{ url(strtolower($blog->category) . '/' . $blog->slug) }}">{{ $blog->title }}</a></h3>
						<p>{!! str_limit($blog->excerpt, 200, '...') !!}</p>
					</div>
				</div>
				@endforeach
			</div>
			<div id="sl-prev" class="widget-scroll-prev"><i class="fa fa-chevron-left white"></i></div>
			<div id="sl-next" class="widget-scroll-next"><i class="fa fa-chevron-right white but_marg"></i></div>
			<div class="pad25"></div> 
		</div> -->
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function($) {
		 //<![CDATA[
		 $(window).load(function() {
		 	$('#nslider').nivoSlider({
		 		effect: 'fade', directionNav: true, pauseOnHover: true});
		 });

		 $("#slider_home").carouFredSel({ 
		 	width : "100%", 
		 	height : "auto",
		 	responsive : true,
		 	auto : false,
		 	items : { width : 280, visible: { min: 1, max: 4 }
		 },
		 swipe : { onTouch : true, onMouse : true },
		 scroll: { items: 1, },
		 prev : { button : "#sl-prev", key : "left"},
		 next : { button : "#sl-next", key : "right" }
		});
			//]]>
		});
//]]>
</script>
@endsection