@extends('layouts.page')

@section('title', 'Swordsman Class, Swordsman Skill, Swordsman Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Swordsman Class, Swordsman Skill, Swordsman Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('swordsman'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/swordsman-min.png') }}" alt="Swordsman" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Swordsman</h1>
					<span>1st Job</span><br>

					<p>He/She, who continuously moves forward to further improve his/her own strength, brings the skills and physical strength gained through training, along with strong armor and weapons to join the world of adventure.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/swordsman-min.png') }}" alt="Swordsman" width="300px">
				<div class="jobs">
					<h1>Swordsman</h1>
					<span>1st Job</span><br>

					<p>He/She, who continuously moves forward to further improve his/her own strength, brings the skills and physical strength gained through training, along with strong armor and weapons to join the world of adventure.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered" id="bash">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/swordsman-bash.jpg') }}" alt="Bash Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Bash</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 2.0m</span><br>
									<span class="skill-state red">Requires Swordsman Lv.11</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Deals (Atk110%) Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Deals (Atk120%) Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Deals (Atk130%) Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Deals (Atk140%) Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Deals (Atk150%) Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Deals (Atk160%) Dmg to the target. Has a 6% chance to stun the target.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Deals (Atk170%) Dmg to the target. Has a 12% chance to stun the target.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Deals (Atk180%) Dmg to the target. Has a 18% chance to stun the target.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Deals (Atk190%) Dmg to the target. Has a 24% chance to stun the target.	</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Deals (Atk200%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Deals (Atk205%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Deals (Atk210%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Deals (Atk215%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Deals (Atk220%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Deals (Atk225%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deals (Atk230%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deals (Atk235%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deals (Atk240%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deals (Atk245%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deals (Atk250%) Dmg to the target. Has a 30% chance to stun the target.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="magnum-break">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/swordsman-magnum-break.jpg') }}" alt="Magnum Break Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Magnum Break</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.5m</span><br>
									<span class="skill-state red">Requires Swordsman Lv.15</span><br>
									<span class="skill-state red">Requires <a href="#bash">Bash</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk115%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk130%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk145%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk160%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk175%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk190%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk205%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk220%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk235%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back all nearby units and deals (Atk250%) fire Dmg. Applies Auto Attacks with fire attribute and gains 20% Atk damage. Lasts 10 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="taunt">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/swordsman-taunt.jpg') }}" alt="Taunt Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Taunt</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.0m</span><br>
									<span class="skill-state red">Requires Swordsman Lv.15</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>5</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 3 sec and increasing their Atk by 50%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>5</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 3 sec and increasing their Atk by 45%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>5</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 3 sec and increasing their Atk by 40%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>5</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 3 sec and increasing their Atk by 35%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>5</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 3 sec and increasing their Atk by 30%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 4 sec, increasing their Atk by 28%, lowering their Def by 9%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 4.5 sec, increasing their Atk by 26%, lowering their Def by 12%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 5 sec, increasing their Atk by 24%, lowering their Def by 15%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 5.5 sec, increasing their Atk by 22%, lowering their Def by 18%. Does not affect on players.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Taunts enemies in the target area, forcing them to attack you for 6 sec, increasing their Atk by 20%, lowering their Def by 21%. Does not affect on players.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="endure">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/swordsman-endure.jpg') }}" alt="Endure Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Endure</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.0m</span><br>
									<span class="skill-state red">Requires Swordsman Lv.18</span><br>
									<span class="skill-state red">Requires <a href="#taunt">Taunt</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 10 M.Def. Endure lasts 10 sec but ends instantly after taking 7 attacks.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 20 M.Def. Endure lasts 13 sec but ends instantly after taking 8 attacks.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 30 M.Def. Endure lasts 16 sec but ends instantly after taking 9 attacks.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 40 M.Def. Endure lasts 19 sec but ends instantly after taking 10 attacks.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 50 M.Def. Endure lasts 22 sec but ends instantly after taking 11 attacks.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 60 M.Def. Endure lasts 25 sec but ends instantly after taking 12 attacks.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 70 M.Def. Endure lasts 28 sec but ends instantly after taking 13 attacks.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 80 M.Def. Endure lasts 31 sec but ends instantly after taking 14 attacks.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 90 M.Def. Endure lasts 34 sec but ends instantly after taking 15 attacks.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">When attacked, self won't pause or get knocked back but gains 100 M.Def. Endure lasts 37 sec but ends instantly after taking 16 attacks.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="sword-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/swordsman-sword-mastery.jpg') }}" alt="Sword Mastery Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Sword Mastery</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-state red">Requires Swordsman Lv.11</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 4, Auto Attack increases by 20</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 8, Auto Attack increases by 40</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 12, Auto Attack increases by 60</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 16, Auto Attack increases by 80</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 20, Auto Attack increases by 100</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 24, Auto Attack increases by 120</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 28, Auto Attack increases by 140</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 32, Auto Attack increases by 160</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 36, Auto Attack increases by 180</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using swords, Atk increase by 40, Auto Attack increases by 200</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="increase-recuperative-power">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/swordsman-increase-recuperative-power.jpg') }}" alt="Increase Recuperative Power Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Increase Recuperative Power</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-state red">Requires Swordsman Lv.15</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*0.2% +5) HP every 10 sec. Increases effects of HP Regen items by 5%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*0.4% +10) HP every 10 sec. Increases effects of HP Regen items by 10%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*0.6% +15) HP every 10 sec. Increases effects of HP Regen items by 15%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*0.8% +20) HP every 10 sec. Increases effects of HP Regen items by 20%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*1.0% +25) HP every 10 sec. Increases effects of HP Regen items by 25%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*1.2% +30) HP every 10 sec. Increases effects of HP Regen items by 30%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*1.4% +35) HP every 10 sec. Increases effects of HP Regen items by 35%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*1.6% +40) HP every 10 sec. Increases effects of HP Regen items by 40%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*1.8% +45) HP every 10 sec. Increases effects of HP Regen items by 45%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*2.0% +50) HP every 10 sec. Increases effects of HP Regen items by 50%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*2.2% +60) HP every 10 sec. Increases effects of HP Regen items by 55%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*2.4% +70) HP every 10 sec. Increases effects of HP Regen items by 60%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*2.6% +80) HP every 10 sec. Increases effects of HP Regen items by 65%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*2.8% +90) HP every 10 sec. Increases effects of HP Regen items by 70%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Permanently increases HP Regen Spd. Restore (Max HP*3.0% +100) HP every 10 sec. Increases effects of HP Regen items by 75%
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection