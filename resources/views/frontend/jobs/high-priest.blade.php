@extends('layouts.page')

@section('title', 'High Priest Class, High Priest Skill, High Priest Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('High Priest Class, High Priest Skill, High Priest Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('high-priest'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/high_priest-min.png') }}" alt="High Priest" width="300px" class="pull-left">
				<div class="jobs">
					<h1>High Priest</h1>
					<span>Acolyte 2nd Transcendent Job</span><br>

					<p>The High Priest is the epitome of spiritual calm. A bulwark in the storm of chaos, he is more than capable of protecting his comrades when faced with even the most ferocious enemy.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/high_priest-min.png') }}" alt="High Priest" width="300px"></div>
				<div class="jobs">
					<h1>High Priest</h1>
					<span>Acolyte 2nd Transcendent Job</span><br>

					<p>The High Priest is the epitome of spiritual calm. A bulwark in the storm of chaos, he is more than capable of protecting his comrades when faced with even the most ferocious enemy.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="meditatio">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-meditatio.png') }}" alt="Meditatio Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Meditatio</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases Max SP by 1%, MP restore Spd by 3% and healing effects by 2%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases Max SP by 2%, MP restore Spd by 6% and healing effects by 4%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases Max SP by 3%, MP restore Spd by 9% and healing effects by 6%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases Max SP by 4%, MP restore Spd by 12% and healing effects by 8%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases Max SP by 5%, MP restore Spd by 15% and healing effects by 10%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increases Max SP by 6%, MP restore Spd by 18% and healing effects by 12%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increases Max SP by 7%, MP restore Spd by 21% and healing effects by 14%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increases Max SP by 8%, MP restore Spd by 24% and healing effects by 16%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increases Max SP by 9%, MP restore Spd by 27% and healing effects by 18%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increases Max SP by 10%, MP restore Spd by 30% and healing effects by 20%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="lex-divina">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-lex-divina.png') }}" alt="Lex Divina Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Lex Divina</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Silences an enemy for 10 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Silences an enemy for 20 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Silences an enemy for 30 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Silences an enemy for 40 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Silences an enemy for 50 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="lex-aeterna">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-lex-aeterna.png') }}" alt="Lex Aeterna Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Lex Aeterna</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range red">Requires <a href="#lex-divina">Lex Divina</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Cause an enemy to take 100% more damage from the next attack</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="decrease-agility">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-decrease-agility.png') }}" alt="Decrease Agility Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Decrease Agility</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 3 Agi for 10 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 6 Agi for 20 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 9 Agi for 30 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 12 Agi for 40 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 15 Agi for 50 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 18 Agi for 60 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 21 Agi for 70 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 24 Agi for 80 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>31</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 27 Agi for 90 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>33</td>
								<td class="skill-description" colspan="3">Reduces the target's 30% Move Spd and 30 Agi for 100 sec. Also, Removes effects provided by <a href="{{ url('acolyte#increase-agility') }}">Increase Agility</a>, <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> and <a href="{{ url('sniper#wind-walk') }}">Wind Walk</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="angelus">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-angelus.png') }}" alt="Angelus Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Angelus</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 9.0m</span><br>
								<span class="skill-range">Cast Time: 0.0s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.5s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Provides 15% quality Def for the whole Party for 30 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.5s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Provides 30% quality Def for the whole Party for 60 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.5s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Provides 45% quality Def for the whole Party for 90 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.5s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Provides 60% quality Def for the whole Party for 120 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.5s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Provides 75% quality Def for the whole Party for 150 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>3.5s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Provides 90% quality Def for the whole Party for 180 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>3.5s</td>
								<td>41</td>
								<td class="skill-description" colspan="3">Provides 105% quality Def for the whole Party for 210 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>3.5s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Provides 120% quality Def for the whole Party for 240 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>3.5s</td>
								<td>47</td>
								<td class="skill-description" colspan="3">Provides 135% quality Def for the whole Party for 270 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Provides 150% quality Def for the whole Party for 300 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="assumptio">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-assumptio.png') }}" alt="Assumptio Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Assumptio</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 9.0m</span><br>
								<span class="skill-range">Cast Time: 1.0s</span><br>
								<span class="skill-range red">Requires <a href="#angelus">Angelus</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.1s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Priest prays for allies, Granting the target and all equipped allies 10% Def and 10% MDef for 20 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.1s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Priest prays for allies, Granting the target and all equipped allies 20% Def and 20% MDef for 40 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.1s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Priest prays for allies, Granting the target and all equipped allies 30% Def and 30% MDef for 60 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.1s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Priest prays for allies, Granting the target and all equipped allies 40% Def and 40% MDef for 80 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.1s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Priest prays for allies, Granting the target and all equipped allies 50% Def and 50% MDef for 100 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="suffragium">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-suffragium.png') }}" alt="Suffragium Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Suffragium</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 9.0m</span><br>
								<span class="skill-range">Cast Time: 5.0s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Priest prays for the whole Party, reducing their chant time by 10% for 50 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Priest prays for the whole Party, reducing their chant time by 20% for 40 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Priest prays for the whole Party, reducing their chant time by 30% for 30 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Priest prays for the whole Party, reducing their chant time by 40% for 20 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Priest prays for the whole Party, reducing their chant time by 50% for 10 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="jud">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-jud.png') }}" alt="Jud Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Jud</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>1.5s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*320%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>1.8s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*340%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>2.1s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*360%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>2.4s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*380%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>2.6s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*400%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>0.5s</td>
								<td>2.9s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*420%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>0.5s</td>
								<td>3.2s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*440%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>0.5s</td>
								<td>3.5s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*460%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>0.5s</td>
								<td>3.8s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*480%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*500%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*520%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*540%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*560%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*580%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*600%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>62</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*620%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*640%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>66</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*660%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>68</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*680%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*700%*BaseLv/50) holy M.Dmg to an enemy, dealing half damage to enemies near the target.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="magic-prayer">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-magic-prayer.png') }}" alt="Magic Prayer Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Magic Prayer</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Priest spends 3% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Priest spends 6% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Priest spends 9% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Priest spends 12% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Priest spends 15% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Priest spends 18% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Priest spends 21% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Priest spends 24% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Priest spends 27% less SP when using skills.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Priest spends 30% less SP when using skills.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="demon-bane">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-demon-bane.png') }}" alt="Demon Bane Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Demon Bane</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 6 points, the Dmg dealt to devil and undead monsters increases by 5%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 12 points, the Dmg dealt to devil and undead monsters increases by 10%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 18 points, the Dmg dealt to devil and undead monsters increases by 15%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 24 points, the Dmg dealt to devil and undead monsters increases by 20%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 30 points, the Dmg dealt to devil and undead monsters increases by 25%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 36 points, the Dmg dealt to devil and undead monsters increases by 30%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 42 points, the Dmg dealt to devil and undead monsters increases by 35%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 48 points, the Dmg dealt to devil and undead monsters increases by 40%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 54 points, the Dmg dealt to devil and undead monsters increases by 45%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 12 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 11 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 10 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 9 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 8 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 7 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 6 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 5 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 4 LUK increases Critical Damage by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Increases self quality Atk increases by 60 points, the Dmg dealt to devil and undead monsters increases by 50%. Each 3 LUK increases Critical Damage by 1%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="safety-wall">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-priest-safety-wall.png') }}" alt="Safety Wall Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Safety Wall</span><br>
									<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range red">Special Cost: <a href="{{ url('item/blue-gemstone') }}">Blue Gemstone</a> x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>4.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 5 sec and can take up to 3 attacks. Up to 1 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.6s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 10 sec and can take up to 4 attacks. Up to 1 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.2s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 15 sec and can take up to 5 attacks. Up to 1 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.8s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 20 sec and can take up to 6 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.4s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 25 sec and can take up to 7 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 30 sec and can take up to 8 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.6s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 35 sec and can take up to 9 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.2s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 40 sec and can take up to 10 attacks. Up to 3 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>0.8s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 45 sec and can take up to 11 attacks. Up to 3 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>0.4s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 50 sec and can take up to 12 attacks. Up to 3 walls can exist at a time</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection