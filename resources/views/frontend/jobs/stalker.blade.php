@extends('layouts.page')

@section('title', 'Stalker Class, Stalker Skill, Stalker Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Stalker Class, Stalker Skill, Stalker Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('stalker'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/stalker-min.png') }}" alt="Stalker" width="240px" class="pull-left">
				<div class="jobs">
					<h1>Stalker</h1>
					<span>Thief's 2nd Transcendent Job</span><br>

					<p>The love of pursuit and the thrill of the chase is personified in the elusive Stalker... Those targeted by a Stalker had best give up all hope of escape. Those that wish to capture a Stalker should simply accept defeat. Undetectable and uncapturable, Stalkers specialize in spying, harassing, chasing and shaking off enemies.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
					<img src="{{ url('frontend/img/jobs/class/stalker-min.png') }}" alt="Stalker" width="300px">
				</div>
				<div class="jobs">
					<h1>Stalker</h1>
					<span>Thief's 2nd Transcendent Job</span><br>

					<p>The love of pursuit and the thrill of the chase is personified in the elusive Stalker... Those targeted by a Stalker had best give up all hope of escape. Those that wish to capture a Stalker should simply accept defeat. Undetectable and uncapturable, Stalkers specialize in spying, harassing, chasing and shaking off enemies.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered" id="triangle-shot">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-triangle-shop.png') }}" alt="Triangle Shot Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Triangle Shot</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m</span><br>
									<span class="skill-range red">Requires <a href="{{ url('rogue#repeated-firing') }}">Repeated Firing</a> Lv.1*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (100% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (110% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (120% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (130% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (140% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (150% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (160% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (170% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (180% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (190% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.5s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (195% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.5s</td>
								<td>31</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (200% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.5s</td>
								<td>33</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (205% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.5s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (210% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.5s</td>
								<td>37</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (215% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.5s</td>
								<td>39</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (220% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.5s</td>
								<td>41</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (225% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.5s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (230% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.5s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (235% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.5s</td>
								<td>47</td>
								<td class="skill-description" colspan="3">Shoot 3 arrows at the same time, deal triple Dmg (240% Atk) to a single enemy; bows must be equipped to release this skill</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="snatcher">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-snatcher.png') }}" alt="Snatcher Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Snatcher</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('rogue#rob') }}">Rob</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Caster has a 6% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 2% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Caster has a 7% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 4% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Caster has a 8% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 6% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Caster has a 9% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 8% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Caster has a 10% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 10% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Caster has a 11% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 12% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Caster has a 12% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 14% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Caster has a 13% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 16% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Caster has a 14% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 18% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Caster has a 15% chance to trigger <a href="{{ url('rogue#snatch') }}">Snatch</a> when launching auto attacks. Permanently increase 20% Zeny when releasing <a href="{{ url('rogue#snatch') }}">Snatch</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="rogue-nature">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-rogue-nature.png') }}" alt="Rogue's Nature Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Rogue's Nature</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently increase 1 points of Dexterity, 1% ASPD</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently increase 2 points of Dexterity, 2% ASPD</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently increase 3 points of Dexterity, 3% ASPD</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently increase 4 points of Dexterity, 4% ASPD</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently increase 5 points of Dexterity, 5% ASPD</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Permanently increase 6 points of Dexterity, 6% ASPD</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Permanently increase 7 points of Dexterity, 7% ASPD</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Permanently increase 8 points of Dexterity, 8% ASPD</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Permanently increase 9 points of Dexterity, 9% ASPD</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Permanently increase 10 points of Dexterity, 10% ASPD</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="intimidate">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-indimidate.png') }}" alt="Intimidate Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Intimidate</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>12</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (200% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>14</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (240% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>16</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (280% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>18</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (320% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>20</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (360% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>22</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (400% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>24</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (440% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>26</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (480% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>28</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (520% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>30</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deal Dmg (560% Atk) to an enemy, and teleports the target to anywhere on the map. The target cannot move for 2 seconds after being teleported.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="close-confine">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-close-confine.png') }}" alt="Close Confine Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Close Confine</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range red">Requires <a href="#intimidate">Intimidate</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>20</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Catches one enemy within 3 seconds, setting target into a captive state, but the caster is unable to move and his Flee increases by 4. Following skills can remove the captive state: <a href="{{ url('thief#hiding') }}">Hiding</a>, <a href="#vanished">Vanished</a>, <a href="{{ url('monk#body-relocation') }}">Body Relocation</a>, Ice Tomb, Furious Rage</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>22</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Catches one enemy within 6 seconds, setting target into a captive state, but the caster is unable to move and his Flee increases by 8. Following skills can remove the captive state: <a href="{{ url('thief#hiding') }}">Hiding</a>, <a href="#vanished">Vanished</a>, <a href="{{ url('monk#body-relocation') }}">Body Relocation</a>, Ice Tomb, Furious Rage</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>24</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Catches one enemy within 9 seconds, setting target into a captive state, but the caster is unable to move and his Flee increases by 12. Following skills can remove the captive state: <a href="{{ url('thief#hiding') }}">Hiding</a>, <a href="#vanished">Vanished</a>, <a href="{{ url('monk#body-relocation') }}">Body Relocation</a>, Ice Tomb, Furious Rage</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>26</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Catches one enemy within 12 seconds, setting target into a captive state, but the caster is unable to move and his Flee increases by 16. Following skills can remove the captive state: <a href="{{ url('thief#hiding') }}">Hiding</a>, <a href="#vanished">Vanished</a>, <a href="{{ url('monk#body-relocation') }}">Body Relocation</a>, Ice Tomb, Furious Rage</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>28</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Catches one enemy within 15 seconds, setting target into a captive state, but the caster is unable to move and his Flee increases by 20. Following skills can remove the captive state: <a href="{{ url('thief#hiding') }}">Hiding</a>, <a href="#vanished">Vanished</a>, <a href="{{ url('monk#body-relocation') }}">Body Relocation</a>, Ice Tomb, Furious Rage</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="state-pursuit">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-state-pursuit.png') }}" alt="State Pursuit Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">State Pursuit</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 1.5% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 3.0% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 4.5% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 6.0% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 7.5% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 8.0% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 8.5% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 9.0% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 9.5% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increase damage dealt by 10.0% to targets under Slowing, Stunnin, Darkness effect, and hit by <a href="#close-confine">Close Confine</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="disabling-strike">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-disabling-strike.png') }}" alt="Disabling Strike Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Disabling Strike</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m (+3.5m with bow)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>24</td>
								<td>4s</td>
								<td class="skill-description" colspan="3">Deals Dmg (150% Atk) to an enemy, reducing the target's healing effect by 15% for 5 seconds</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>26</td>
								<td>4s</td>
								<td class="skill-description" colspan="3">Deals Dmg (200% Atk) to an enemy, reducing the target's healing effect by 30% for 5 seconds</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>28</td>
								<td>4s</td>
								<td class="skill-description" colspan="3">Deals Dmg (250% Atk) to an enemy, reducing the target's healing effect by 45% for 5 seconds</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>30</td>
								<td>4s</td>
								<td class="skill-description" colspan="3">Deals Dmg (300% Atk) to an enemy, reducing the target's healing effect by 60% for 5 seconds</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>32</td>
								<td>4s</td>
								<td class="skill-description" colspan="3">Deals Dmg (350% Atk) to an enemy, reducing the target's healing effect by 75% for 5 seconds</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="vanished">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-vanished.png') }}" alt="Vanished Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Vanished</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('rogue#enhanced-hiding') }}">Enhanced Hiding</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>18</td>
								<td>50s</td>
								<td class="skill-description" colspan="3">Immediately enter hiding state, This state will lasts 5 seconds no matter what damage and anti-hiding skill dealt to the caster, and the damage will decrease by 60% in the duration</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>21</td>
								<td>45s</td>
								<td class="skill-description" colspan="3">Immediately enter hiding state, This state will lasts 5 seconds no matter what damage and anti-hiding skill dealt to the caster, and the damage will decrease by 65% in the duration</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>24</td>
								<td>40s</td>
								<td class="skill-description" colspan="3">Immediately enter hiding state, This state will lasts 5 seconds no matter what damage and anti-hiding skill dealt to the caster, and the damage will decrease by 70% in the duration</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>27</td>
								<td>35s</td>
								<td class="skill-description" colspan="3">Immediately enter hiding state, This state will lasts 5 seconds no matter what damage and anti-hiding skill dealt to the caster, and the damage will decrease by 75% in the duration</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>30</td>
								<td>30s</td>
								<td class="skill-description" colspan="3">Immediately enter hiding state, This state will lasts 5 seconds no matter what damage and anti-hiding skill dealt to the caster, and the damage will decrease by 80% in the duration</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="strip-accessory">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-strip-accessory.png') }}" alt="Strip Accessory Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Strip Accessory</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range">Cast Time: 1.5s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>20</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 3% chance to remove 1 accessory from an enemy, causing the target loses all accessory's attributes for 4 seconds. Increasing DEX and LUK will boost success rate. Target's Atk will be decrease by 30% when the skill is used on non-Boss monsters</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 6% chance to remove 1 accessory from an enemy, causing the target loses all accessory's attributes for 6 seconds. Increasing DEX and LUK will boost success rate. Target's Atk will be decrease by 30% when the skill is used on non-Boss monsters</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>30</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 9% chance to remove 1 accessory from an enemy, causing the target loses all accessory's attributes for 8 seconds. Increasing DEX and LUK will boost success rate. Target's Atk will be decrease by 30% when the skill is used on non-Boss monsters</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 4</td>
								<td>1.0s</td>
								<td>30</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 12% chance to remove 1 accessory from an enemy, causing the target loses all accessory's attributes for 10 seconds. Increasing DEX and LUK will boost success rate. Target's Atk will be decrease by 30% when the skill is used on non-Boss monsters</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 5</td>
								<td>1.0s</td>
								<td>30</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 15% chance to remove 1 accessory from an enemy, causing the target loses all accessory's attributes for 12 seconds. Increasing DEX and LUK will boost success rate. Target's Atk will be decrease by 30% when the skill is used on non-Boss monsters</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>1.0s</td>
								<td>30</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 18% chance to remove 1 accessory from an enemy, causing the target loses all accessory's attributes for 14 seconds. Increasing DEX and LUK will boost success rate. Target's Atk will be decrease by 30% when the skill is used on non-Boss monsters</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="double-accessory-removal">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-double-accessory-removal.png') }}" alt="Double Accessory Removal Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Double Accessory Removal</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#strip-accessory">Strip Accessory</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When Strip Accessory is successfully launched, it has a 30% chance to remove accessories on 2 positions, and the basic success rate of Strip Accessory will permanently increases by 3%. Each release will trigger only one state among Strip Weapon, Strip Accessory, Double Accessory Removal</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When Strip Accessory is successfully launched, it has a 30% chance to remove accessories on 2 positions, and the basic success rate of Strip Accessory will permanently increases by 6%. Each release will trigger only one state among Strip Weapon, Strip Accessory, Double Accessory Removal</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When Strip Accessory is successfully launched, it has a 30% chance to remove accessories on 2 positions, and the basic success rate of Strip Accessory will permanently increases by 9%. Each release will trigger only one state among Strip Weapon, Strip Accessory, Double Accessory Removal</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="strip-weapon">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-strip-weapon.png') }}" alt="Strip Weapon Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Strip Weapon</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#double-accessory-removal">Double Accessory Removal</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When <a href="#strip-accessory">Strip Accessory</a> is successfully launched, it has a 30% chance to unload the target's weapons so the target loses all attributes on the weapon's position. The basic success rate of <a href="#strip-accessory">Strip Accessory</a> will permanently increases by 3%. Each release will trigger only one state among <a href="#strip-weapon">Strip Weapon</a>, <a href="#strip-accessory">Strip Accessory</a>, <a href="#double-accessory-removal">Double Accessory Removal</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When <a href="#strip-accessory">Strip Accessory</a> is successfully launched, it has a 30% chance to unload the target's weapons so the target loses all attributes on the weapon's position. The basic success rate of <a href="#strip-accessory">Strip Accessory</a> will permanently increases by 6%. Each release will trigger only one state among <a href="#strip-weapon">Strip Weapon</a>, <a href="#strip-accessory">Strip Accessory</a>, <a href="#double-accessory-removal">Double Accessory Removal</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When <a href="#strip-accessory">Strip Accessory</a> is successfully launched, it has a 30% chance to unload the target's weapons so the target loses all attributes on the weapon's position. The basic success rate of <a href="#strip-accessory">Strip Accessory</a> will permanently increases by 9%. Each release will trigger only one state among <a href="#strip-weapon">Strip Weapon</a>, <a href="#strip-accessory">Strip Accessory</a>, <a href="#double-accessory-removal">Double Accessory Removal</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="strip-shield">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-strip-shield.png') }}" alt="Strip Shield Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Strip Shield</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range">Cast Time: 1.5s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 3% chance to remove an enemy's off-hand weapon, cause the target loses all the deputy's attributes for 4 seconds. Increasing DEX and LUK will boost success rate. Target's Def will decrease by 50% when the skill is used on non-BOSS monsters</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 6% chance to remove an enemy's off-hand weapon, cause the target loses all the deputy's attributes for 6 seconds. Increasing DEX and LUK will boost success rate. Target's Def will decrease by 50% when the skill is used on non-BOSS monsters</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 9% chance to remove an enemy's off-hand weapon, cause the target loses all the deputy's attributes for 8 seconds. Increasing DEX and LUK will boost success rate. Target's Def will decrease by 50% when the skill is used on non-BOSS monsters</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 4</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 12% chance to remove an enemy's off-hand weapon, cause the target loses all the deputy's attributes for 10 seconds. Increasing DEX and LUK will boost success rate. Target's Def will decrease by 50% when the skill is used on non-BOSS monsters</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 5</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 15% chance to remove an enemy's off-hand weapon, cause the target loses all the deputy's attributes for 12 seconds. Increasing DEX and LUK will boost success rate. Target's Def will decrease by 50% when the skill is used on non-BOSS monsters</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Has a 18% chance to remove an enemy's off-hand weapon, cause the target loses all the deputy's attributes for 14 seconds. Increasing DEX and LUK will boost success rate. Target's Def will decrease by 50% when the skill is used on non-BOSS monsters</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="strip-boots">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-strip-boots.png') }}" alt="Strip Boots Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Strip Boots</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#strip-shield">Strip Shield</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 3%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 6%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 9%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="strip-helm">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-strip-helm.png') }}" alt="Strip Helm Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Strip Helm</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#strip-boots">Strip Boots</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 3%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 6%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 9%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="strip-helm">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/stalker-strip-armor.png') }}" alt="Strip Armor Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Strip Armor</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#strip-helm">Strip Helm</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 3%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 6%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When <a href="#strip-shield">Strip Shield</a> is successfully launched, it has a equal chance to unload the target's shoes so the target loses all attributes on feet. The basic success rate of <a href="#strip-shield">Strip Shield</a> will permanently increases by 9%. Each release will trigger only one state among <a href="#strip-armor">Strip Armor</a>, <a href="#strip-shield">Strip Shield</a>, <a href="#strip-boots">Strip Boots</a>, <a href="#strip-helm">Strip Helm</a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection