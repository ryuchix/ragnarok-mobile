@extends('layouts.page')

@section('title', 'Crusader Class, Crusader Skill, Crusader Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Crusader Class, Crusader Skill, Crusader Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('crusader'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/crusaders-min.png') }}" alt="Crusader" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Crusader</h1>
					<span>Swordsman's 2nd Job</span><br>

					<p>Trained to prepare for the holy war since childhood, he/she begins with great powers.</p>

					<p>As opposed to other Knights, he/she has pledged to serve God. Any hardship or pain is considered the way to God and he/she fights with all strength even overcoming death.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/crusaders-min.png') }}" alt="Crusader" width="300px">
				<div class="jobs">
					<h1>Crusader</h1>
					<span>Swordsman's 2nd Job</span><br>

					<p>Trained to prepare for the holy war since childhood, he/she begins with great powers.</p>

					<p>As opposed to other Knights, he/she has pledged to serve God. Any hardship or pain is considered the way to God and he/she fights with all strength even overcoming death.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered" id="heal">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-heal.jpg') }}" alt="Heal Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Heal</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Restore (12* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Restore (20* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Restore (28* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Restore (36* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Restore (44* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Restore (52* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>31</td>
								<td class="skill-description" colspan="3">Restore (60* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Restore (68* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>37</td>
								<td class="skill-description" colspan="3">Restore (76* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Restore (84* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>41</td>
								<td class="skill-description" colspan="3">Restore (86* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Restore (88* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Restore (90* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Restore (92* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Restore (94* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Restore (96* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>47</td>
								<td class="skill-description" colspan="3">Restore (98* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Restore (100* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>49</td>
								<td class="skill-description" colspan="3">Restore (102* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Restore (104* Rounded ( (BaseLv+INT) /10) ) HP to self and allies. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="holy-cross">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-holy-cross.jpg') }}" alt="Holy Cross Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Holy Cross</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>9</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx330% holy Dmg + M.Atkx330% holy M.Dmg to an enemy. Has a 3% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>11</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx360% holy Dmg + M.Atkx360% holy M.Dmg to an enemy. Has a 6% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx390% holy Dmg + M.Atkx390% holy M.Dmg to an enemy. Has a 9% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx420% holy Dmg + M.Atkx420% holy M.Dmg to an enemy. Has a 12% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx450% holy Dmg + M.Atkx450% holy M.Dmg to an enemy. Has a 15% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx480% holy Dmg + M.Atkx480% holy M.Dmg to an enemy. Has a 18% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx510% holy Dmg + M.Atkx510% holy M.Dmg to an enemy. Has a 21% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx540% holy Dmg + M.Atkx540% holy M.Dmg to an enemy. Has a 24% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx570% holy Dmg + M.Atkx570% holy M.Dmg to an enemy. Has a 27% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx600% holy Dmg + M.Atkx600% holy M.Dmg to an enemy. Has a 30% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.5s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx630% holy Dmg + M.Atkx630% holy M.Dmg to an enemy. Has a 33% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.5s</td>
								<td>31</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx660% holy Dmg + M.Atkx660% holy M.Dmg to an enemy. Has a 36% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.5s</td>
								<td>33</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx690% holy Dmg + M.Atkx690% holy M.Dmg to an enemy. Has a 39% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.5s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx720% holy Dmg + M.Atkx720% holy M.Dmg to an enemy. Has a 42% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.5s</td>
								<td>37</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx750% holy Dmg + M.Atkx750% holy M.Dmg to an enemy. Has a 45% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.5s</td>
								<td>39</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx780% holy Dmg + M.Atkx780% holy M.Dmg to an enemy. Has a 48% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.5s</td>
								<td>41</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx810% holy Dmg + M.Atkx810% holy M.Dmg to an enemy. Has a 51% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.5s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx840% holy Dmg + M.Atkx840% holy M.Dmg to an enemy. Has a 54% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.5s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx870% holy Dmg + M.Atkx870% holy M.Dmg to an enemy. Has a 57% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.5s</td>
								<td>47</td>
								<td class="skill-description" colspan="3">Attacks the target with the power of the holy cross, dealing Atkx900% holy Dmg + M.Atkx900% holy M.Dmg to an enemy. Has a 60% chance to blind the target for 10 sec. Deals half damage when equipped with non-spear weapon.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="shie">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-shie.jpg') }}" alt="Shie. Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Shie.</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 3.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Smashes the target with the shield, Deals 140%*Dmg and knocking it back for 3m. Has a 8% chance to stun the target for 3 sec. Shield type weapons are required. Damage dealt is affected by Vit, Dex and Shield Refine Level.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Smashes the target with the shield, Deals 180%*Dmg and knocking it back for 3m. Has a 16% chance to stun the target for 3 sec. Shield type weapons are required. Damage dealt is affected by Vit, Dex and Shield Refine Level.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Smashes the target with the shield, Deals 220%*Dmg and knocking it back for 3m. Has a 24% chance to stun the target for 3 sec. Shield type weapons are required. Damage dealt is affected by Vit, Dex and Shield Refine Level.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Smashes the target with the shield, Deals 260%*Dmg and knocking it back for 3m. Has a 32% chance to stun the target for 3 sec. Shield type weapons are required. Damage dealt is affected by Vit, Dex and Shield Refine Level.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Smashes the target with the shield, Deals 300%*Dmg and knocking it back for 3m. Has a 40% chance to stun the target for 3 sec. Shield type weapons are required. Damage dealt is affected by Vit, Dex and Shield Refine Level.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="shield-chain">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-shield-chain.png') }}" alt="Shield Chain Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Shield Chain</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 4.0m</span><br>
									<span class="skill-range">Cast Time: 1.5 seconds</span><br>
									<span class="skill-range red">Requires <a href="#shie">Shie.</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (100%xCombos 1) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (110%xCombos 1) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (120%xCombos 2) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (130%xCombos 2) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (140%xCombos 3) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (150%xCombos 3) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (160%xCombos 4) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (170%xCombos 4) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (180%xCombos 5) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (200%xCombos 5) to a single enemy. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (205%xCombos 5) to a single enemy. M.Def increased by 20 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (210%xCombos 5) to a single enemy. M.Def increased by 40 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (215%xCombos 5) to a single enemy. M.Def increased by 60 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (220%xCombos 5) to a single enemy. M.Def increased by 80 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (225%xCombos 5) to a single enemy. M.Def increased by 100 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (230%xCombos 5) to a single enemy. M.Def increased by 120 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (235%xCombos 5) to a single enemy. M.Def increased by 140 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (240%xCombos 5) to a single enemy. M.Def increased by 160 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (245%xCombos 5) to a single enemy. M.Def increased by 180 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Use shield to deal Dmg: (250%xCombos 5) to a single enemy. M.Def increased by 200 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr> 
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="auto-guard">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-auto-guard.jpg') }}" alt="Auto Guard Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Auto Guard</span><br>
									<span class="skill-state">Active Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Crusader has a 14% chance to parry when attacked. Lasts 30 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Crusader has a 18% chance to parry when attacked. Lasts 40 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Crusader has a 22% chance to parry when attacked. Lasts 50 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Crusader has a 26% chance to parry when attacked. Lasts 60 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Crusader has a 30% chance to parry when attacked. Lasts 70 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Crusader has a 34% chance to parry when attacked. Lasts 80 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Crusader has a 38% chance to parry when attacked. Lasts 90 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Crusader has a 42% chance to parry when attacked. Lasts 100 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Crusader has a 46% chance to parry when attacked. Lasts 110 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Crusader has a 50% chance to parry when attacked. Lasts 120 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="devotion">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-devotion.jpg') }}" alt="Devotion Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Devotion</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 7.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>Time</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>2.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Crusader protects 1 allies by transferring all their damage taken to self. Lasts 30 sec. The skill is invalid to allies 7m away from the caster. Cannot target the crusader class.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Crusader protects 1 allies by transferring all their damage taken to self. Lasts 45 sec. The skill is invalid to allies 7m away from the caster. Cannot target the crusader class.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>1.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Crusader protects 1 allies by transferring all their damage taken to self. Lasts 60 sec. The skill is invalid to allies 7m away from the caster. Cannot target the crusader class.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>0.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Crusader protects 1 allies by transferring all their damage taken to self. Lasts 75 sec. The skill is invalid to allies 7m away from the caster. Cannot target the crusader class.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>0.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Crusader protects 1 allies by transferring all their damage taken to self. Lasts 90 sec. The skill is invalid to allies 7m away from the caster. Cannot target the crusader class.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="spear-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-spear-mastery.png') }}" alt="Spear Mastery Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Spear Mastery</span><br>
									<span class="skill-state">Passive Skill</span
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 8, Auto Attack increases by 20</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 16, Auto Attack increases by 40</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 24, Auto Attack increases by 60</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 32, Auto Attack increases by 80</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 40, Auto Attack increases by 100</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 48, Auto Attack increases by 120</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 56, Auto Attack increases by 140</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 64, Auto Attack increases by 160</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 72, Auto Attack increases by 180</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 80, Auto Attack increases by 200</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 100, Auto Attack increases by 220</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 120, Auto Attack increases by 240</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 140, Auto Attack increases by 260</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 160, Auto Attack increases by 280</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 180, Auto Attack increases by 300</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 200, Auto Attack increases by 320</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 220, Auto Attack increases by 340</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 240, Auto Attack increases by 360</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 260, Auto Attack increases by 380</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 280, Auto Attack increases by 400</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="spear-quicken">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-spear-quicken.png') }}" alt="Spear Quicken Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Spear Quicken</span><br>
									<span class="skill-state">Active Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 12%, Crit increases by 7 points, Flee increases by 2 points, lasts 30 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 14%, Crit increases by 9 points, Flee increases by 4 points, lasts 60 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 16%, Crit increases by 11 points, Flee increases by 6 points, lasts 90 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 18%, Crit increases by 13 points, Flee increases by 8 points, lasts 120 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 20%, Crit increases by 15 points, Flee increases by 10 points, lasts 150 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 22%, Crit increases by 17 points, Flee increases by 12 points, lasts 180 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 24%, Crit increases by 19 points, Flee increases by 14 points, lasts 210 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 26%, Crit increases by 21 points, Flee increases by 16 points, lasts 240 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>65</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 28%, Crit increases by 23 points, Flee increases by 18 points, lasts 270 sec. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 100, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 200, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 300, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 400, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 500, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 600, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 700, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 800, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 900, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When using spears, ASPD increases by 30%, Crit increases by 25 points, Flee increases by 20 points, Auto Attack increases by 1000, lasts 300 sec. Spear type weapons are required.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="cavalry-combat">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-cavalry-combat.jpg') }}" alt="Cavalry Combat Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Cavalry Combat</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 12% ASPD</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 9% ASPD</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 6% ASPD</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 3% ASPD</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 0% ASPD</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="cavalry-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-cavalry-mastery.jpg') }}" alt="Cavalry Mastery Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Cavalry Mastery</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#cavalry-combat">Cavalry Combat</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Deals 4% more damage to large size monsters while in <a href="#cavalry-combat">Cavalry Combat</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Deals 8% more damage to large size monsters while in <a href="#cavalry-combat">Cavalry Combat</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Deals 12% more damage to large size monsters while in <a href="#cavalry-combat">Cavalry Combat</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Deals 16% more damage to large size monsters while in <a href="#cavalry-combat">Cavalry Combat</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Deals 20% more damage to large size monsters while in <a href="#cavalry-combat">Cavalry Combat</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="divine-protection">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/crusader-divine-protection.jpg') }}" alt="Divine Protection Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Divine Protection</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases 2 Def. Reduces damage taken from undead or devil monsters by 1%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases 4 Def. Reduces damage taken from undead or devil monsters by 2%</td>
							</tr> 
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases 6 Def. Reduces damage taken from undead or devil monsters by 3%</td>
							</tr> 
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases 8 Def. Reduces damage taken from undead or devil monsters by 4%</td>
							</tr> 
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases 10 Def. Reduces damage taken from undead or devil monsters by 5%</td>
							</tr> 
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increases 12 Def. Reduces damage taken from undead or devil monsters by 6%</td>
							</tr> 
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increases 14 Def. Reduces damage taken from undead or devil monsters by 7%</td>
							</tr> 
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increases 16 Def. Reduces damage taken from undead or devil monsters by 8%</td>
							</tr> 
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increases 18 Def. Reduces damage taken from undead or devil monsters by 9%</td>
							</tr> 
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 1%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 2%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 3%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 4%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 5%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 6%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 7%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 8%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 9%.</td>
							</tr> 
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Increases 20 Def. Reduces damage taken from undead or devil monsters by 10%. Reduces damage taken from Poison, Shadow, Undead elements by 10%.</td>
							</tr> 
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection