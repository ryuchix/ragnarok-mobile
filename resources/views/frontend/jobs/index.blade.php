@extends('layouts.page')

@section('title', 'Ragnarok Mobile Job Class and Skills Database | Ragnarok Mobile Skills Database')
@section('description', str_limit(strip_tags('Here are the list of Ragnarok Job Class and its skill tree in Ragnarok Mobile Eternal Love. First Job, Second Job, Trancendent Job, Archer Tree, Merchant Tree, Thief, Acolyte Tree, Magician Tree, Swordsman Tree'), 160, ''))
@section('canonical', asset('jobs'))

@section('header')
<style type="text/css">
	.jobs {
		text-align: center; 
		width: 100%;
	}
	.jobs td{
		min-width: 100px;
	}
	.jobs   .class-name {
		background-color: #2ba6cb;
	    color: #fff;
	    width: 100px;
	}

</style>
@endsection


@section('content')
<div class="post">
	<div class="row">
		<div class="col-md-12 ">
			<div class="row whitebg">
				<center><h1 style="margin-bottom: -10px;">Ragnarok Mobile Job Class and Skills</h1>
				<h4>Here are the most accurate Job Class and Skills Database. Select the Job to view their respective skills.</h4></em><br><br>
				@include('frontend.jobs.class.thief')
				<br><br>
				@include('frontend.jobs.class.merchant')
				<br><br>
				@include('frontend.jobs.class.acolyte')
				<br><br>
				@include('frontend.jobs.class.archer')
				<br><br>
				@include('frontend.jobs.class.magician')
				<br><br>
				@include('frontend.jobs.class.swordsman')
				<br><br>
				<div class="clear"></div>
<div class="fb-comments" data-href="{{ url('endless-tower') }}" data-numposts="5" data-width="100%"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
@endsection