@extends('layouts.page')

@section('title', 'Creator Class, Creator Skill, Creator Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Creator Class, Creator Skill, Creator Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('creator'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/creator-min.png') }}" alt="Creator" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Creator</h1>
					<span>Merchant's 2nd Transcendent Job</span><br>

					<p>With knowledge comes understanding, and with understanding comes mastery. The Creator's knowledge of science has reached the saturation point, leading to a mastery over the laws of nature... </p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/creator-min.png') }}" alt="Creator" width="300px"></div>
				<div class="jobs">
					<h1>Creator</h1>
					<span>Merchant's 2nd Transcendent Job</span><br>

					<p>With knowledge comes understanding, and with understanding comes mastery. The Creator's knowledge of science has reached the saturation point, leading to a mastery over the laws of nature...</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="powerful-acid-demonstration">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-powerful-acid-demonstration.png') }}" alt="Powerful Acid Demonstration Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Powerful Acid Demonstration</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> Lv.3*</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Make <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> bring Fire M.Dmg equal to 80% M.Atk, and SP cost of <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> increases by 2 points</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Make <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> bring extra Fire M.Dmg equal to 110% M.Atk, and SP cost of <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> increases by 4 points</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Make <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> bring extra Fire M.Dmg equal to 140% M.Atk, and SP cost of <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> increases by 6 points</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Make <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> bring extra Fire M.Dmg equal to 170% M.Atk, and SP cost of <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> increases by 8 points</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Make <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> bring extra Fire M.Dmg equal to 200% M.Atk, and SP cost of <a href="{{ url('alchemist#acid-demonstration') }}">Acid Demonstration</a> increases by 10 points</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="improved-acid-demonstration">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-improved-acid-demonstration.png') }}" alt="Improved Acid Demonstration Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Improved Acid Demonstration</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 2.5s</span><br>
									<span class="skill-range red">Requires <a href="#powerful-acid-demonstration">Powerful Acid Demonstration</a> Lv.3*</span><br>
									<span class="skill-range red">Special Cost: Bottle Grenade x1 and Acid Bottle x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x360% + target's VIT) Dmg to an enemy. Has a 1% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x440% + target's VIT) Dmg to an enemy. Has a 2% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x520% + target's VIT) Dmg to an enemy. Has a 3% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x600% + target's VIT) Dmg to an enemy. Has a 4% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x680% + target's VIT) Dmg to an enemy. Has a 5% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x760% + target's VIT) Dmg to an enemy. Has a 6% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x840% + target's VIT) Dmg to an enemy. Has a 7% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x920% + target's VIT) Dmg to an enemy. Has a 8% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1000% + target's VIT) Dmg to an enemy. Has a 9% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1080% + target's VIT) Dmg to an enemy. Has a 10% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1110% + target's VIT) Dmg to an enemy. Has a 11% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1140% + target's VIT) Dmg to an enemy. Has a 12% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1080% + target's VIT) Dmg to an enemy. Has a 10% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1200% + target's VIT) Dmg to an enemy. Has a 14% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1230% + target's VIT) Dmg to an enemy. Has a 15% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>2.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1260% + target's VIT) Dmg to an enemy. Has a 16% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>2.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1290% + target's VIT) Dmg to an enemy. Has a 17% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>2.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1320% + target's VIT) Dmg to an enemy. Has a 18% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>2.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1350% + target's VIT) Dmg to an enemy. Has a 19% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>2.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Deals ((Atk + M.Atk) x1380% + target's VIT) Dmg to an enemy. Has a 20% chance to break enemy's weapon and armor. This skill ignore Flee, Auto Guard and Sword Parry. Acid Terror skill level affects VIT factor</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="gene-research">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-gene-research.png') }}" alt="Gene Research Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Gene Research</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 1 point, SP Regen increases by 1 point</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 2 point, SP Regen increases by 2 point</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 3 point, SP Regen increases by 3 point</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 4 point, SP Regen increases by 4 point</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 5 point, SP Regen increases by 5 point</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 6 point, SP Regen increases by 6 point</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 7 point, SP Regen increases by 7 point</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 8 point, SP Regen increases by 8 point</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 9 point, SP Regen increases by 9 point</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Alchemist's Intelligence permanently increases by 10 point, SP Regen increases by 10 point</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="chemical-protection-weapon">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-chemical-protection-weapon.png') }}" alt="Chemical Protection Weapon Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Chemical Protection Weapon</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Special Cost: Glistening Coat x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td class="skill-description" colspan="3">Release alchemy - <a href="#chemical-protection-weapon">Chemical Protection Weapon</a> to an ally, protecting their weapon from damage within 100 seconds</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td class="skill-description" colspan="3">Release alchemy - <a href="#chemical-protection-weapon">Chemical Protection Weapon</a> to an ally, protecting their weapon from damage within 200  seconds</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td class="skill-description" colspan="3">Release alchemy - <a href="#chemical-protection-weapon">Chemical Protection Weapon</a> to an ally, protecting their weapon from damage within 300 seconds</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="chemical-protection-armor">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-chemical-protection-armor.png') }}" alt="Chemical Protection Armor Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Chemical Protection Armor</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Requires <a href="#chemical-protection-weapon">Chemical Protection Weapon</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">While Alchemy - <a href="#chemical-protection-weapon">Chemical Protection Weapon</a> is active, the ally's armor is protected from damage for 100 seconds</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">While Alchemy - <a href="#chemical-protection-weapon">Chemical Protection Weapon</a> is active, the ally's armor is protected from damage for 200  seconds</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">While Alchemy - <a href="#chemical-protection-weapon">Chemical Protection Weapon</a> is active, the ally's armor is protected from damage for 300 seconds</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="chemical-weapon-destruction">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-chemical-weapon-destruction.png') }}" alt="Chemical Weapon Destruction Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Chemical Weapon Destruction</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#chemical-protection-armor">Chemical Protection Armor</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 15%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 20%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 25%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 30%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 35%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 40%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 45%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 50%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 55%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 60%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 62%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 64%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 66%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 68%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">The caster's trigger probability of Weapon Break, Armor Break will be affected by Dexterity and Luck, the maximum value is 70%. Acid Demonstration has only 1/4 of the original value affected by Dexterity and luck.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="chemical-helm-destruction">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-chemical-helm-destruction.png') }}" alt="Chemical Helm Destruction Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Chemical Helm Destruction</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#chemical-weapon-destruction">Chemical Weapon Destruction</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When the enemy's armor is broken by alchemist's skills, there is a 10% chance to break target's helmet</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When the enemy's armor is broken by alchemist's skills, there is a 20% chance to break target's helmet</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When the enemy's armor is broken by alchemist's skills, there is a 30% chance to break target's helmet</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="chemical-shield-destruction">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-chemical-shield-destruction.png') }}" alt="Chemical shield Destruction Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Chemical Shield Destruction</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#chemical-helm-destruction">Chemical Helm Destruction</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When the enemy's armor is broken by alchemist's skills, there is a 10% chance to break target's off-hand weapon</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When the enemy's armor is broken by alchemist's skills, there is a 20% chance to break target's off-hand weapon</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When the enemy's armor is broken by alchemist's skills, there is a 30% chance to break target's off-hand weapon</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="hp-potion-cast">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-hp-potion-cast.png') }}" alt="HP Potion Cast Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">HP Potion Cast</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 8.0m</span><br>
									<span class="skill-range red">Special Cost: Condensed White Potion x1</span><br>
									<span class="skill-range red">Requires <a href="{{ url('alchemist#pharmacy') }}">Pharmacy</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>3</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 10% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 14%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>4</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 20% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 18%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>5</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 30% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 22%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>6</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 40% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 26%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>7</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 50% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 30%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 60% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 34%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>9</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 70% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 38%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 80% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 42%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>11</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 90% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 46%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Throw Condensed White Potion to an ally, and restores target's 100% potion effects; This skill's effect is influenced by target's VIT and caster's INT. Effect of restorative potions is improved by 50%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="mp-potion-cast">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-mp-potion-cast.png') }}" alt="MP Potion Cast Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">MP Potion Cast</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 8.0m</span><br>
									<span class="skill-range red">Special Cost: Condensed Blue Potion x1</span><br>
									<span class="skill-range red">Requires <a href="#hp-potion-cast">HP Potion Cast</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>3</td>
								<td class="skill-description" colspan="3">Throw condensed blue potion to an ally, and restores target's 100% potion effects; the skill's effect is influenced by target's VIT and caster's INT</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="homunculus-strength-ii">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-homunculus-strength-ii.png') }}" alt="Homunculus Strength II Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Homunculus Strength II</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('alchemist#homunculus-strength-i') }}">Homunculus Strength I</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 10%. Permanently increase Homunculus' attributes, Lif: M.Atk +1%, Dmg Reduc. +2%; Amistr: Max HP +1%, ASPD +5%; Vanilmirth: M.Atk +1%, Dmg Reduc. + 2%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 20%. Permanently increase Homunculus' attributes, Lif: M.Atk +2%, Dmg Reduc. +4%; Amistr: Max HP +2%, ASPD +10%; Vanilmirth: M.Atk +2%, Dmg Reduc. + 4%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 30%. Permanently increase Homunculus' attributes, Lif: M.Atk +3%, Dmg Reduc. +6%; Amistr: Max HP +3%, ASPD +15%; Vanilmirth: M.Atk +3%, Dmg Reduc. + 6%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 40%. Permanently increase Homunculus' attributes, Lif: M.Atk +4%, Dmg Reduc. +8%; Amistr: Max HP +4%, ASPD +20%; Vanilmirth: M.Atk +4%, Dmg Reduc. + 8%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 50%. Permanently increase Homunculus' attributes, Lif: M.Atk +5%, Dmg Reduc. +10%; Amistr: Max HP +5%, ASPD +25%; Vanilmirth: M.Atk +5%, Dmg Reduc. + 10%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 60%. Permanently increase Homunculus' attributes, Lif: M.Atk +6%, Dmg Reduc. +12%; Amistr: Max HP +6%, ASPD +30%; Vanilmirth: M.Atk +6%, Dmg Reduc. + 12%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 70%. Permanently increase Homunculus' attributes, Lif: M.Atk +7%, Dmg Reduc. +14%; Amistr: Max HP +7%, ASPD +35%; Vanilmirth: M.Atk +7%, Dmg Reduc. + 14%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 80%. Permanently increase Homunculus' attributes, Lif: M.Atk +8%, Dmg Reduc. +16%; Amistr: Max HP +8%, ASPD +40%; Vanilmirth: M.Atk +8%, Dmg Reduc. + 16%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 90%. Permanently increase Homunculus' attributes, Lif: M.Atk +9%, Dmg Reduc. +18%; Amistr: Max HP +9%, ASPD +45%; Vanilmirth: M.Atk +9%, Dmg Reduc. + 18%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increase the skill multiplier of Hell Plant by 100%. Permanently increase Homunculus' attributes, Lif: M.Atk +10%, Dmg Reduc. +20%; Amistr: Max HP +10%, ASPD +50%; Vanilmirth: M.Atk +10%, Dmg Reduc. + 20%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="hell-plant">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-hell-plant.png') }}" alt="Hell Plant Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Hell Plant</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m</span><br>
									<span class="skill-range red">Special Cost: Plant Bottle x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk210% Neutral Dmg with a 12% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk230% Neutral Dmg with a 14% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk250% Neutral Dmg with a 16% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk270% Neutral Dmg with a 18% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk290% Neutral Dmg with a 20% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk310% Neutral Dmg with a 22% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk330% Neutral Dmg with a 24% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk350% Neutral Dmg with a 26% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk370% Neutral Dmg with a 28% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Summons a Hell Plant to attack a single enemy target, dealing Atk390% Neutral Dmg with a 30% chance to inflict Bleed and Curse on the target. Hell Plant can attack hidden units and reveal them when its attack connects. Plant exists for 30s</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="super-life-psychic">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/creator-super-life-psychic.png') }}" alt="Super Life Psychic Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Super Life Psychic</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('alchemist#life-psychic') }}">Life Psychic</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">In Life Psychic state, each point of master's vitality can increase Amistr's Atk by 3, increase Vanilmirth's M.Atk by 1</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">In Life Psychic state, each point of master's vitality can increase Amistr's Atk by 4, increase Vanilmirth's M.Atk by 2</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">In Life Psychic state, each point of master's vitality can increase Amistr's Atk by 5, increase Vanilmirth's M.Atk by 3</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">In Life Psychic state, each point of master's vitality can increase Amistr's Atk by 6, increase Vanilmirth's M.Atk by 4</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">In Life Psychic state, each point of master's vitality can increase Amistr's Atk by 7, increase Vanilmirth's M.Atk by 5</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td class="skill-description" colspan="3">In Life Psychic state, increase Lif's M.Atk by 100, Amistr's Max HP by 2%, Vanilmirth's M.Atk by 2%, each point of master's vitality can increase Amistr's Atk by 7, increase Vanilmirth's M.Atk by 5</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td class="skill-description" colspan="3">In Life Psychic state, increase Lif's M.Atk by 200, Amistr's Max HP by 4%, Vanilmirth's M.Atk by 4%, each point of master's vitality can increase Amistr's Atk by 7, increase Vanilmirth's M.Atk by 5</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td class="skill-description" colspan="3">In Life Psychic state, increase Lif's M.Atk by 300, Amistr's Max HP by 6%, Vanilmirth's M.Atk by 6%, each point of master's vitality can increase Amistr's Atk by 7, increase Vanilmirth's M.Atk by 5</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td class="skill-description" colspan="3">In Life Psychic state, increase Lif's M.Atk by 400, Amistr's Max HP by 8%, Vanilmirth's M.Atk by 8%, each point of master's vitality can increase Amistr's Atk by 7, increase Vanilmirth's M.Atk by 5</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td class="skill-description" colspan="3">In Life Psychic state, increase Lif's M.Atk by 500, Amistr's Max HP by 10%, Vanilmirth's M.Atk by 10%, each point of master's vitality can increase Amistr's Atk by 7, increase Vanilmirth's M.Atk by 5</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection