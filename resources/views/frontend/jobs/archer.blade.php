@extends('layouts.page')

@section('title', 'Archer Class, Archer Skill, Archer Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Archer Class, Archer Skill, Archer Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('archer'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/archer-min.png') }}" alt="Archer" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Archer</h1>
					<span>1st Job</span><br>

					<p>With the special ability to handle bow and arrows, he/she became an Archer with an almost perfect accuracy in hitting targets. His/Her arrows hit the aimed target and those that hit vital spots are dangerous and powerful enough to take the target's life. But because of a kind heart, he/she loves and adores the Mountain Village Payon in which he/she grew up in.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/archer-min.png') }}" alt="Archer" width="300px"></div>
				<div class="jobs">
					<h1>Archer</h1>
					<span>1st Job</span><br>

					<p>With the special ability to handle bow and arrows, he/she became an Archer with an almost perfect accuracy in hitting targets. His/Her arrows hit the aimed target and those that hit vital spots are dangerous and powerful enough to take the target's life. But because of a kind heart, he/she loves and adores the Mountain Village Payon in which he/she grew up in.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="double-strafe">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/archer-double-strafe.jpg') }}" alt="Double Strafe Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Double Strafe</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="#vultures-eye">Vulture's Eye</a> Lv.10)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk100%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk110%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk120%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk130%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk140%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk150%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk160%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk170%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk180%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Fires 2 arrows that deal double (Atk190%) Dmg to an enemy. Bow type weapons are required</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="vultures-eye">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/archer-vultures-eye.jpg') }}" alt="Vulture's Eye Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Vulture's Eye</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Archer Lv.15</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently increases range 5% and hit 2</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently increases range 10% and hit 4</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently increases range 15% and hit 6</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently increases range 20% and hit 8</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently increases range 25% and hit 10</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Permanently increases range 30% and hit 12</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Permanently increases range 35% and hit 14</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Permanently increases range 40% and hit 16</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Permanently increases range 45% and hit 18</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 6%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 12%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 18%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 24%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 30%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 36%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 42%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 48%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 54%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Permanently increases range 50% and hit 20. When using Bow type weapon, Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 60%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="arrow-shower">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/archer-arrow-shower.jpg') }}" alt="Arrow Shower Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Arrow Shower</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="#vultures-eye">Vulture's Eye</a> Lv.10)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk75%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk90%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk105%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk120%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk135%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk150%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.5s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk165%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.5s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk180%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.5s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk195%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk210%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>3.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk240%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>3.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk270%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>3.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk300%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>3.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk330%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk360%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>3.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk390%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>3.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk420%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>3.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk450%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>3.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk480%) Dmg to up to 8 enemy units.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Shoots 4 waves of arrows to the target area, each wave dealing (Atk510%) Dmg to up to 8 enemy units.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="arrow-repel">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/archer-arrow-repel.jpg') }}" alt="Arrow Repel Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Arrow Repel</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="#vultures-eye">Vulture's Eye</a> Lv.10)</span><br>
									<span class="skill-range red">Requires Archer Lv.15</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>15</td>
								<td>3.5s</td>
								<td class="skill-description" colspan="3">Shoots a powerful arrow, dealing (Atk200%) Dmg to the target and knocking it back	</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>15</td>
								<td>3.0s</td>
								<td class="skill-description" colspan="3">Shoots a powerful arrow, dealing (Atk250%) Dmg to the target and knocking it back	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>15</td>
								<td>2.5s</td>
								<td class="skill-description" colspan="3">Shoots a powerful arrow, dealing (Atk300%) Dmg to the target and knocking it back	</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>15</td>
								<td>2.0s</td>
								<td class="skill-description" colspan="3">Shoots a powerful arrow, dealing (Atk350%) Dmg to the target and knocking it back	</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>15</td>
								<td>1.5s</td>
								<td class="skill-description" colspan="3">Shoots a powerful arrow, dealing (Atk400%) Dmg to the target and knocking it back	</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="owls-eye">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/archer-owls-eye.jpg') }}" alt="Owl's Eye Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Owl's Eye</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 1</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 2</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 3</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 4</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 5</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 6</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 7</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 8</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 9</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Permanently increases Dex 10</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="elemental-arrow">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/archer-elemental-arrow.jpg') }}" alt="Elemental Arrow Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Elemental Arrow</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 3, Auto Attack increases by 10</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 6, Auto Attack increases by 20</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 9, Auto Attack increases by 30</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 12, Auto Attack increases by 40</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 15, Auto Attack increases by 50</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 18, Auto Attack increases by 60</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 21, Auto Attack increases by 70</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 24, Auto Attack increases by 80</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 27, Auto Attack increases by 90</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 100</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 110. Auto Attack extra increases (DEX*0.5) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 120. Auto Attack extra increases (DEX*1.0) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 130. Auto Attack extra increases (DEX*1.5) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 140. Auto Attack extra increases (DEX*2.0) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 150. Auto Attack extra increases (DEX*2.5) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 160. Auto Attack extra increases (DEX*3.0) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 170. Auto Attack extra increases (DEX*3.5) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 180. Auto Attack extra increases (DEX*4.0) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 190. Auto Attack extra increases (DEX*4.5) Atk.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">When using bows, Atk increases by 30, Auto Attack increases by 200. Auto Attack extra increases (DEX*5.0) Atk.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="ankle-snare">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/archer-ankle-snare.jpg') }}" alt="Ankle Snare Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Ankle Snare</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="#vultures-eye">Vulture's Eye</a> Lv.10)</span><br>
									<span class="skill-range red">Requires Archer Lv.15</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 2 seconds (this effect cannot be removed by Panacea). At most 2 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 4 seconds (this effect cannot be removed by Panacea). At most 2 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 6 seconds (this effect cannot be removed by Panacea). At most 2 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 8 seconds (this effect cannot be removed by Panacea). At most 2 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 10 seconds (this effect cannot be removed by Panacea). At most 2 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 10 seconds (this effect cannot be removed by Panacea). At most 3 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 10 seconds (this effect cannot be removed by Panacea). At most 4 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 10 seconds (this effect cannot be removed by Panacea). At most 5 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 10 seconds (this effect cannot be removed by Panacea). At most 6 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>0.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Lay traps in specified areas, the enemy caught in the trap will be snared for 10 seconds (this effect cannot be removed by Panacea). At most 7 traps can be laid at the same time. The same target would not be snared within 1.5 seconds after the snare effect.	</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection