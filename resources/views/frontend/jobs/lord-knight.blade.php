@extends('layouts.page')

@section('title', 'Lord Knight Class, Lord Knight Skill, Lord Knight Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Lord Knight Class, Lord Knight Skill, Lord Knight Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('lord-knight'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/lord_knight-min.png') }}" alt="Lord Knight" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Lord Knight</h1>
					<span>Swordsman's 2nd Transcendent Job</span><br>

					<p>Under the instructions of the King, he/she has devoted to live as a Knight serving the King. He/She has the ability to move very quickly riding a PecoPeco. Having greater strength and the ability to attack than a Swordsman, he/she points his/her weapon at those who endanger the weak and the King.</p>

					<p>The ability to move quickly, combined with a variety of different physical attacks puts fear into all enemies</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/lord_knight-min.png') }}" alt="Lord Knight" width="300px">
				<div class="jobs">
					<h1>Lord Knight</h1>
					<span>Swordsman's 2nd Transcendent Job</span><br>

					<p>Under the instructions of the King, he/she has devoted to live as a Knight serving the King. He/She has the ability to move very quickly riding a PecoPeco. Having greater strength and the ability to attack than a Swordsman, he/she points his/her weapon at those who endanger the weak and the King.</p>

					<p>The ability to move quickly, combined with a variety of different physical attacks puts fear into all enemies</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="concentration">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-concentration-min.png') }}" alt="Counter Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Counter Attack</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>30.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Grants whole Party with Endure effect. Knight gains 4% Atk, 10 HIT. Reduces Def provided by equipments by 8% for 25 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>30.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Grants whole Party with Endure effect. Knight gains 8% Atk, 20 HIT. Reduces Def provided by equipments by 16% for 30 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>30.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Grants whole Party with Endure effect. Knight gains 12% Atk, 30 HIT. Reduces Def provided by equipments by 24% for 35 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>30.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Grants whole Party with Endure effect. Knight gains 16% Atk, 40 HIT. Reduces Def provided by equipments by 32% for 40 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>30.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Grants whole Party with Endure effect. Knight gains 20% Atk, 50 HIT. Reduces Def provided by equipments by 40% for 45 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="lords-aura">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-lords-aura-min.png') }}" alt="Lord's Aura Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Lord's Aura</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Lord Knight provides 12% DMG for the whole Party for 30 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Lord Knight provides 14% DMG for the whole Party for 60 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Lord Knight provides 16% DMG for the whole Party for 90 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Lord Knight provides 18% DMG for the whole Party for 120 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Lord Knight provides 20% DMG for the whole Party for 150 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Lord Knight provides 22% DMG for the whole Party for 180 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>68</td>
								<td class="skill-description" colspan="3">Lord Knight provides 24% DMG for the whole Party for 210 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>72</td>
								<td class="skill-description" colspan="3">Lord Knight provides 26% DMG for the whole Party for 240 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>76</td>
								<td class="skill-description" colspan="3">Lord Knight provides 28% DMG for the whole Party for 270 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>10.0s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Lord Knight provides 30% DMG for the whole Party for 300 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="head-crush">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-head-crush-min.png') }}" alt="Head Crush Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Head Crush</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span><br>
									<span class="skill-range red">Requires <a href="{{ url('knight#aura-blade') }}">Aura Blade</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 10 Atk, Has a 4% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 20 Atk, Has a 6% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 30 Atk, Has a 8% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 40 Atk, Has a 10% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 50 Atk, Has a 12% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 60 Atk, Has a 14% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 70 Atk, Has a 16% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 80 Atk, Has a 18% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 90 Atk, Has a 20% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 60 sec to improve 100 Atk, Has a 22% chance to apply Bleed status with Auto Attacks. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 80 sec to improve 110 Atk, Has a 22% chance to apply Bleed status with Auto Attacks. Deals additional 5% Dmg to Bleeding target. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 100 sec to improve 120 Atk, Has a 22% chance to apply Bleed status with Auto Attacks. Deals additional 10% Dmg to Bleeding target. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 120 sec to improve 130 Atk, Has a 22% chance to apply Bleed status with Auto Attacks. Deals additional 15% Dmg to Bleeding target. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 140 sec to improve 140 Atk, Has a 22% chance to apply Bleed status with Auto Attacks. Deals additional 20% Dmg to Bleeding target. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>0.5s</td>
								<td>75.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Knight enters Head Crush status in 160 sec to improve 150 Atk, Has a 22% chance to apply Bleed status with Auto Attacks. Deals additional 25% Dmg to Bleeding target. If the attack is critical, Applies Bleed for sure. Stacks up to 3 layers. Sword type weapons are required.	</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="joint-beat">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-joint-beat-min.png') }}" alt="Joint Beat Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Joint Beat</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk60%) Dmg. Has a 10% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk70%) Dmg. Has a 15% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk80%) Dmg. Has a 20% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk90%) Dmg. Has a 25% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk100%) Dmg. Has a 30% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk110%) Dmg. Has a 35% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk120%) Dmg. Has a 40% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk130%) Dmg. Has a 45% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk140%) Dmg. Has a 50% chance to apply 1 negative status for 10 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Strikes at the joints of the target, dealing (Atk150%) Dmg. Has a 55% chance to apply 1 negative status for 10 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="sword-parry">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-sword-parry-min.png') }}" alt="Sword Parry Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Sword Parry</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span><br>
									<span class="skill-range red">Requires <a href="{{ url('knight#one-hand-quicken') }}">One-Hand Quicken</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 23% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 26% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 29% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 32% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 35% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 38% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 41% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 44% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 47% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 50% to trigger if the caster still takes damage when using Sword Parry to block attacks. Reduces 100% damage taken for 30 sec. Its trigger rate in the Arena is influenced by self and the rival's AGI.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="spiral-pierce">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-spiral-pierce-min.png') }}" alt="Spiral Pierce Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Spiral Pierce</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>11</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*40% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*80% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*120% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*160% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*200% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*240% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*280% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*320% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*360% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*400% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*440% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*480% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*520% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*560% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*600% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*640% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*680% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*720% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*760% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>0.5s</td>
								<td>4.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Attacks an enemy with the spinning spear, dealing (Str Factor + Atk*Weapon ClassFactor + Refine Atk Factor + Base Lv Factor)*800% Dmg. Stuns the target for 2 sec, deals more damage if the target is a small size monster. Spear type weapons are required.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="call-of-justice">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-call-of-justice-min.png') }}" alt="Call of Justice Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Call of Justice</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Each 5 VIT provides Lord Knight with 1 Atk</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Each 5 VIT provides Lord Knight with 2 Atk</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Each 5 VIT provides Lord Knight with 3 Atk</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Each 5 VIT provides Lord Knight with 4 Atk</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Each 5 VIT provides Lord Knight with 5 Atk</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="hp-alight">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-hp-alight-min.png') }}" alt="HP Alight Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">HP Alight</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +50) real damage to nearby enemies. Lasts 12 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +100) real damage to nearby enemies. Lasts 14 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +150) real damage to nearby enemies. Lasts 16 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +200) real damage to nearby enemies. Lasts 18 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +250) real damage to nearby enemies. Lasts 20 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +300) real damage to nearby enemies. Lasts 22 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +350) real damage to nearby enemies. Lasts 24 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +400) real damage to nearby enemies. Lasts 26 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +450) real damage to nearby enemies. Lasts 28 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 1% self Max HP to deal (2%Max HP + 10%Max SP +500) real damage to nearby enemies. Lasts 30 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 2% self Max HP to deal (4%Max HP + 20%Max SP +600) real damage to nearby enemies. Lasts 30 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 2% self Max HP to deal (4%Max HP + 20%Max SP +700) real damage to nearby enemies. Lasts 30 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 2% self Max HP to deal (4%Max HP + 20%Max SP +800) real damage to nearby enemies. Lasts 30 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 2% self Max HP to deal (4%Max HP + 20%Max SP +900) real damage to nearby enemies. Lasts 30 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.5s</td>
								<td>30.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Strikes enemies heavily at the cost of knight's HP. Each 1 sec, reduces 2% self Max HP to deal (4%Max HP + 20%Max SP +1000) real damage to nearby enemies. Lasts 30 sec. Becomes invalid when HP is < 10% and won't restore HP and SP naturally if used</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="frenzy">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/lord-knight-frenzy-min.png') }}" alt="Frenzy Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Frenzy</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m</span><br>
									<span class="skill-range red">Requires Lord Knight Lv.90</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 90%, Healing received decreases by 100%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 80%, Healing received decreases by 90%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 70%, Healing received decreases by 80%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 60%, Healing received decreases by 70%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 50%, Healing received decreases by 60%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 40%, Healing received decreases by 50%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 30%, Healing received decreases by 40%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 20%, Healing received decreases by 30%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 10%, Healing received decreases by 20%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.5s</td>
								<td>90.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Caster's own HP recovers to 100%, Mana value is cleared out; Caster will enter a frenzy state for the next 20 seconds, Max HP increases by 100%, Moving speed increases by 30%, Flee increases by 50%, Def and M.Def decreases by 0%, Healing received decreases by 10%</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection