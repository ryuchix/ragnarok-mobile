@extends('layouts.page')

@section('title', 'Hunter Class, Hunter Skill, Hunter Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Hunter Class, Hunter Skill, Hunter Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('hunter'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/hunter-min.png') }}" alt="Hunter" width="250px" class="pull-left">
				<div class="jobs">
					<h1>Hunter</h1>
					<span>Archer 2nd Job</span><br>

					<p>His/Her goal is to attack from a distance, but using the Falcon he/she became friends with while staying in the forest, he/she is able to rush an enemy as well. The Falcon also has the keenness to keep an eye out for an opportunity to attack.</p>
					<p>The Hunter is an outstanding job that has the ability to counter and attack in any situation.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/hunter-min.png') }}" alt="Hunter" width="300px"></div>
				<div class="jobs">
					<h1>Hunter</h1>
					<span>Archer 2nd Job</span><br>

					<p>His/Her goal is to attack from a distance, but using the Falcon he/she became friends with while staying in the forest, he/she is able to rush an enemy as well. The Falcon also has the keenness to keep an eye out for an opportunity to attack.</p>
					<p>The Hunter is an outstanding job that has the ability to counter and attack in any situation.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered" id="improve-concentration">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-improve-concentration.png') }}" alt="Improve Concentration Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Improve Concentration</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 1 and DEX 1</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 2 and DEX 2</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 3 and DEX 3</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 4 and DEX 4</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 5 and DEX 5</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 6 and DEX 6</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 7 and DEX 7</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 8 and DEX 8</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 9 and DEX 9</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10 and DEX 10</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 1, Critical Hit 1, Critical Damage 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 2, Critical Hit 2, Critical Damage 2%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 3, Critical Hit 3, Critical Damage 3%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 4, Critical Hit 4, Critical Damage 4%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 5, Critical Hit 5, Critical Damage 5%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 6, Critical Hit 6, Critical Damage 6%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 7, Critical Hit 7, Critical Damage 7%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 8, Critical Hit 8, Critical Damage 8%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 9, Critical Hit 9, Critical Damage 9%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Permanently increase Agi 10, DEX 10, LUK 10, Critical Hit 10, Critical Damage 10%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="splitting-arrows">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-splitting-arrows.png') }}" alt="Splitting Arrows Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Splitting Arrows</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a> Lv.10)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 180% Atk to an enemy, applying 75% complete splash damage to up to 3 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 230% Atk to an enemy, applying 75% complete splash damage to up to 3 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 280% Atk to an enemy, applying 75% complete splash damage to up to 3 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 330% Atk to an enemy, applying 75% complete splash damage to up to 4 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 380% Atk to an enemy, applying 75% complete splash damage to up to 4 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 430% Atk to an enemy, applying 75% complete splash damage to up to 4 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 480% Atk to an enemy, applying 75% complete splash damage to up to 5 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 530% Atk to an enemy, applying 75% complete splash damage to up to 5 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>31</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 580% Atk to an enemy, applying 75% complete splash damage to up to 5 nearby enemy targets.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>33</td>
								<td class="skill-description" colspan="3">Shoots split arrows to deal arrow Dmg equal to 630% Atk to an enemy, applying 75% complete splash damage to up to 6 nearby enemy targets.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="heavy-arrow">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-heavy-arrow.png') }}" alt="Heavy Arrow Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Heavy Arrow</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a> Lv.10)</span><br>
									<span class="skill-range red">Requires <a href="#splitting-arrows">Splitting Arrows</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>32</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk275%) arrow Dmg to the target. In the next 2.8 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>34</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk300%) arrow Dmg to the target. In the next 3.6 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>36</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk325%) arrow Dmg to the target. In the next 4.4 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>38</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk350%) arrow Dmg to the target. In the next 5.2 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>40</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk375%) arrow Dmg to the target. In the next 6.0 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>42</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk400%) arrow Dmg to the target. In the next 6.8 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>44</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk425%) arrow Dmg to the target. In the next 7.6 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>46</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk450%) arrow Dmg to the target. In the next 8.4 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>48</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk475%) arrow Dmg to the target. In the next 9.2 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>50</td>
								<td>5.5</td>
								<td class="skill-description" colspan="3">Shoots with all strength, Dealing (Atk500%) arrow Dmg to the target. In the next 10.0 sec, the target suffers 30% more damage from auto attacks dealt by archers</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="blitz-beat">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-blitz-beat.png') }}" alt="Blitz Beat Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Blitz Beat</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (35 + <a href="#beastmastery">Beastmastery</a> damage + INT(Intelligence/2)*2 + INT(Dexterity/10)*2) *1.0, to a single enemy; and the beast deals damage: (Atk + Refine Atk)*5%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (40 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *1.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*10%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (45 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *2.0, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*15%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (50 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *2.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*20%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (55 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *3.0, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*25%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (60 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *3.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*30%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (65 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *4.0, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*35%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (70 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *4.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*40%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (75 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.0, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*45%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (80 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*50%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (85 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*55%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (90 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*60%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (95 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*65%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (100 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*70%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (105 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*75%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (110 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*80%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (115 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*85%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (120 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*90%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (125 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*95%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Wild Beast has a 5% chance to deal Neutral Dmg (ignore Def): (130 + <a href="#beastmastery">Beastmastery</a> damage + Rounded(INT/2) *2 + Rounded(DEX/10) *2) *5.5, to a single enemy; and the beast deals additional damage: (Atk + Refine Atk)*100%, Such damage is not affected by Astrolabe or active attack multiplier. Higher LUK increases the chance of bast strike. Beast damage is influenced by INT and DEX, and INT has a greater influence</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="falcon-assault">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-falcon-assault.png') }}" alt="Falcon Assault Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Falcon Assault</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a> Lv.10)</span><br>
									<span class="skill-range red">Requires <a href="#blitz-beat">Blitz Beat</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Sends the falcon to attack an enemy, dealing damage equal to 220% of <a href="#blitz-beat">Blitz Beat</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Sends the falcon to attack an enemy, dealing damage equal to 290% of <a href="#blitz-beat">Blitz Beat</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Sends the falcon to attack an enemy, dealing damage equal to 360% of <a href="#blitz-beat">Blitz Beat</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Sends the falcon to attack an enemy, dealing damage equal to 430% of <a href="#blitz-beat">Blitz Beat</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Sends the falcon to attack an enemy, dealing damage equal to 500% of <a href="#blitz-beat">Blitz Beat</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="beastmastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-beastmastery.jpg') }}" alt="Beastmastery Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Beastmastery</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increase beast damage by 6</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increase beast damage by 12</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increase beast damage by 18</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increase beast damage by 24</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increase beast damage by 30</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increase beast damage by 36</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increase beast damage by 42</td>
							</tr>
							<tr>
								<td>Level 81</td>
								<td class="skill-description" colspan="3">Increase beast damage by 48</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increase beast damage by 54</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increase beast damage by 60</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Increase beast damage by 66</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Increase beast damage by 72</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Increase beast damage by 78</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Increase beast damage by 84</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Increase beast damage by 90</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Increase beast damage by 96</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Increase beast damage by 102</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Increase beast damage by 108</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Increase beast damage by 114</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Increase beast damage by 120</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="lighting-arrow">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-lighting-arrow.png') }}" alt="Lightning Arrow Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Lightning Arrow</span><br>
									<span class="skill-state">Active Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>CD</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>1.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Shoots Ligtning Arrows to the sky to reveal all hidden enemy units within 3m. Lasts 10 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>1.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Shoots Ligtning Arrows to the sky to reveal all hidden enemy units and traps within 4m. Lasts 15 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>1.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Shoots Ligtning Arrows to the sky to reveal all hidden enemy units and traps within 5m. Lasts 20 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="frost-trap">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-frost-trap.png') }}" alt="Frost Trap Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Frost Trap</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a> Lv.10)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Places a trap in the target area, dealing (Dex * (3+BaseLv/100) * (1+Int/35) * 20%) water M.Dmg based on Def to all enemies in range when triggered. Has a 12% chance to freeze targets. If the targets are not frozen, slows them by 30% for 4 sec. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Places a trap in the target area, dealing (Dex * (3+BaseLv/100) * (1+Int/35) * 40%) water M.Dmg based on Def to all enemies in range when triggered. Has a 14% chance to freeze targets. If the targets are not frozen, slows them by 35% for 5 sec. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Places a trap in the target area, dealing (Dex * (3+BaseLv/100) * (1+Int/35) * 60%) water M.Dmg based on Def to all enemies in range when triggered. Has a 16% chance to freeze targets. If the targets are not frozen, slows them by 40% for 6 sec. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Places a trap in the target area, dealing (Dex * (3+BaseLv/100) * (1+Int/35) * 80%) water M.Dmg based on Def to all enemies in range when triggered. Has a 18% chance to freeze targets. If the targets are not frozen, slows them by 45% for 7 sec. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Places a trap in the target area, dealing (Dex * (3+BaseLv/100) * (1+Int/35) * 100%) water M.Dmg based on Def to all enemies in range when triggered. Has a 20% chance to freeze targets. If the targets are not frozen, slows them by 50% for 8 sec. Can have up to 2 traps of this kind.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="claymore-trap">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-claymore-trap.png') }}" alt="Claymore Trap Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Claymore Trap</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a> Lv.10)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it deals (Dex* (3+BaseLv/100)*(1+Int/35)*90%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*140%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*190%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*240%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*290%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*340%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*390%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*440%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*490%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10x</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap in the specified area. When triggered, it Deals (Dex* (3+BaseLv/100)*(1+Int/35)*540%) earth M.Dmg based on Def to all enemies in range. Can have up to 4 traps of this kind at a time</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="detonator">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/hunter-detonator.png') }}" alt="Detonator Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Detonator</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a> Lv.10)</span><br>
									<span class="skill-range red">Requires <a href="#claymore-trap">Claymore Trap</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 10%. Could also tap to detonate traps.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 20%. Could also tap to detonate traps.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 30%. Could also tap to detonate traps.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 4</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 40%. <a href="#frost-trap">Frost Trap</a>, <a href="#claymore-trap">Claymore Trap</a> and <a href="{{ url('sniper#land-mine') }}">Land Mine</a> explode area increased by 0.2m</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 5</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 50%. <a href="#frost-trap">Frost Trap</a>, <a href="#claymore-trap">Claymore Trap</a> and <a href="{{ url('sniper#land-mine') }}">Land Mine</a> explode area increased by 0.4m</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 60%. <a href="#frost-trap">Frost Trap</a>, <a href="#claymore-trap">Claymore Trap</a> and <a href="{{ url('sniper#land-mine') }}">Land Mine</a> explode area increased by 0.6m</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 70%. <a href="#frost-trap">Frost Trap</a>, <a href="#claymore-trap">Claymore Trap</a> and <a href="{{ url('sniper#land-mine') }}">Land Mine</a> explode area increased by 0.8m</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Detonates all placed traps, increasing their damage dealt by 80%. <a href="#frost-trap">Frost Trap</a>, <a href="#claymore-trap">Claymore Trap</a> and <a href="{{ url('sniper#land-mine') }}">Land Mine</a> explode area increased by 1.0m</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection