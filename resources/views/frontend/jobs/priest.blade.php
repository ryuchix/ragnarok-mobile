@extends('layouts.page')

@section('title', 'Priest Class, Priest Skill, Priest Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Priest Class, Priest Skill, Priest Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('priest'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/priest-min.png') }}" alt="Priest" width="250px" class="pull-left">
				<div class="jobs">
					<h1>Priest</h1>
					<span>Acolyte 2nd Job</span><br>

					<p>While serving God and witnessing the miracles, the Priest is the only one that decides to join the world of adventure following the voice of God. He/She helps the group by casting supportive spells, and with a well-trained spirit helps members of the group Heal. Also, he/she has exceptional fighting skills against Undead monsters and by purifying their souls, returns them to the ground.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/priest-min.png') }}" alt="Priest" width="300px"></div>
				<div class="jobs">
					<h1>Priest</h1>
					<span>Acolyte 2nd Job</span><br>

					<p>While serving God and witnessing the miracles, the Priest is the only one that decides to join the world of adventure following the voice of God. He/She helps the group by casting supportive spells, and with a well-trained spirit helps members of the group Heal. Also, he/she has exceptional fighting skills against Undead monsters and by purifying their souls, returns them to the ground.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="holy-booster">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-holy-booster.jpg') }}" alt="Holy Booster Strike Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Holy Booster</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('acolyte#holy-light') }}">Holy Light Strike</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases damage dealt by <a href="{{ url('acolyte#holy-light') }}">Holy Light Strike</a> by 10%. Has a 12% chance to stun the enemy unit for 2 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases damage dealt by <a href="{{ url('acolyte#holy-light') }}">Holy Light Strike</a> by 20%. Has a 14% chance to stun the enemy unit for 2 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases damage dealt by <a href="{{ url('acolyte#holy-light') }}">Holy Light Strike</a> by 30%. Has a 16% chance to stun the enemy unit for 2 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases damage dealt by <a href="{{ url('acolyte#holy-light') }}">Holy Light Strike</a> by 40%. Has a 18% chance to stun the enemy unit for 2 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases damage dealt by <a href="{{ url('acolyte#holy-light') }}">Holy Light Strike</a> by 50%. Has a 20% chance to stun the enemy unit for 2 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="resurrection">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-resurrection.jpg') }}" alt="Resurrection Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Resurrection</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('item/blue-gemstone') }}">Blue Gemstone</a> x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0s</td>
								<td>6.0s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Revives an ally in the select area, restoring their HP by 30%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>4.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Revives an ally in the select area, restoring their HP by 50%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>2.0s</td>
								<td>120</td>
								<td class="skill-description" colspan="3">Revives an ally in the select area, restoring their HP by 70%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>0s</td>
								<td>140</td>
								<td class="skill-description" colspan="3">Revives an ally in the select area, restoring their HP by 90%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="aspersio">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-aspersio.jpg') }}" alt="Aspersio Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Aspersio</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Grants an Ally's Auto Attacks with holy attr effect for 40 sec</td>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Grants an Ally's Auto Attacks with holy attr effect for 100 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="increase-spiritual-recovery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-increase-spiritual-recovery.jpg') }}" alt="Increase Spiritual Recover Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Increase Spiritual Recovery</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (3+0.2% Max SP) SP and increases SP granted by items by 5%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (6+0.4% Max SP) SP and increases SP granted by items by 10%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (9+0.6% Max SP) SP and increases SP granted by items by 15%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (12+0.8% Max SP) SP and increases SP granted by items by 20%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (15+1.0% Max SP) SP and increases SP granted by items by 25%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (18+1.2% Max SP) SP and increases SP granted by items by 30%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (21+1.4% Max SP) SP and increases SP granted by items by 35%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (24+1.6% Max SP) SP and increases SP granted by items by 40%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (27+1.8% Max SP) SP and increases SP granted by items by 45%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (30+2.0% Max SP) SP and increases SP granted by items by 50%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="sanctuary">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-sanctuary.jpg') }}" alt="Sanctuary Attack Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Sanctuary</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>2.58s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 100/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 100 HP every sec. Holy strength works on up to 6 targets</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>2.72s</td>
								<td>39</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 180/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 180 HP every sec. Holy strength works on up to 7 targets</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>2.86s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 260/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 260 HP every sec. Holy strength works on up to 8 targets</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>3.00s</td>
								<td>57</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 340/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 340 HP every sec. Holy strength works on up to 9 targets</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>3.3s</td>
								<td>66</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 420/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 420 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>3.44s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 500/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 500 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>3.58s</td>
								<td>84</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 580/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 580 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>3.72s</td>
								<td>93</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 660/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 660 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>3.86s</td>
								<td>102</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 740/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 740 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>107</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 820/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 820 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>112</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 840/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 840 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>117</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 860/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 860 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>122</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 880/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 880 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>127</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 900/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 900 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>132</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 920/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 920 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>137</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 940/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 940 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>142</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 960/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 960 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>147</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 980/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 980 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>152</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 1000/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 1000 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>2.0s</td>
								<td>4.0s</td>
								<td>157</td>
								<td class="skill-description" colspan="3">Grants the target area with holy strength, dealing 1020/2 holy damage to undead and devil units in it. Lasts 11 sec. Grants friendly units in it with 1020 HP every sec. Holy strength works on up to 10 targets</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="clearance">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-clearance.jpg') }}" alt="Clearance Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Clearance</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Has a 20% chance to expel random 2 debuffs for ally units in the selected area. As the skill levels up, the expelled effects are: Blind, Sleep	</td>
							</tr>
							<tr>	
								<td>Level 2</td>
								<td>1.5s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Has a 40% chance to expel random 2 debuffs for ally units in the selected area. As the skill levels up, the expelled effects are: Blind, Sleep, Curse, Freeze</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>66</td>
								<td class="skill-description" colspan="3">Has a 60% chance to expel random 2 debuffs for ally units in the selected area. As the skill levels up, the expelled effects are: Blind, Sleep, Curse, Freeze, Petrify, Stun</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>72</td>
								<td class="skill-description" colspan="3">Has a 80% chance to expel random 2 debuffs for ally units in the selected area. As the skill levels up, the expelled effects are: Blind, Sleep, Curse, Freeze, Petrify, Stun, Silence, Bleed</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>78</td>
								<td class="skill-description" colspan="3">Has a 100% chance to expel random 2 debuffs for ally units in the selected area. As the skill levels up, the expelled effects are: Blind, Sleep, Curse, Freeze, Petrify, Stun, Silence, Bleed, Burning</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="magnificat">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-magnificat.jpg') }}" alt="Magnificat Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Magnificat</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span><br>
									<span class="skill-range">Cast Time: 4.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>4.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Provides 40% SP restore spd for the whole Party for 40 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>4.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Provides 80% SP restore spd for the whole Party for 60 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>4.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Provides 120% SP restore spd for the whole Party for 80 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>4.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Provides 160% SP restore spd for the whole Party for 100 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>4.0s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Provides 200% SP restore spd for the whole Party for 120 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="gloria">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-gloria.jpg') }}" alt="Gloria Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Gloria</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Priest chants the holy song, Grants the whole Party 6 LUK for 30 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Priest chants the holy song, Grants the whole Party 12 LUK for 30 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Priest chants the holy song, Grants the whole Party 18 LUK for 30 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Priest chants the holy song, Grants the whole Party 24 LUK for 30 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Priest chants the holy song, Grants the whole Party 30 LUK for 30 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="turn-undead">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-turn-undead.jpg') }}" alt="Turn Undead Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Turn Undead</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 1.2s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 10) points of holy damage to target, and deals holy damage equal to M.Atk x60% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 20) points of holy damage to target, and deals holy damage equal to M.Atk x120% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 30) points of holy damage to target, and deals holy damage equal to M.Atk x180% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 40) points of holy damage to target, and deals holy damage equal to M.Atk x240% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 50) points of holy damage to target, and deals holy damage equal to M.Atk x300% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 60) points of holy damage to target, and deals holy damage equal to M.Atk x360% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 70) points of holy damage to target, and deals holy damage equal to M.Atk x420% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 80) points of holy damage to target, and deals holy damage equal to M.Atk x480% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 90) points of holy damage to target, and deals holy damage equal to M.Atk x540% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 100) points of holy damage to target, and deals holy damage equal to M.Atk x600% to the devil monsters. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 110) points of holy damage to target, and deals holy damage equal to M.Atk x660% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 10%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 120) points of holy damage to target, and deals holy damage equal to M.Atk x720% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 20%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 130) points of holy damage to target, and deals holy damage equal to M.Atk x780% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 30%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 140) points of holy damage to target, and deals holy damage equal to M.Atk x840% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 40%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 150) points of holy damage to target, and deals holy damage equal to M.Atk x900% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 50%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 160) points of holy damage to target, and deals holy damage equal to M.Atk x960% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 60%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 170) points of holy damage to target, and deals holy damage equal to M.Atk x1020% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 70%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 180) points of holy damage to target, and deals holy damage equal to M.Atk x1080% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 80%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 190) points of holy damage to target, and deals holy damage equal to M.Atk x1140% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 90%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a chance to kill an undead target instantly (doesn't work on BOSS). If failed, deals (Role level + Intelligence + 200) points of holy damage to target, and deals holy damage equal to M.Atk x1200% to the devil monsters. Increases <a href="#magnus-exorcismus">Magnus Exorcismus</a> Dmg by 100%. Chance of instant kill is affected by INT, LUK, Skill level and target's current HP.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="magnus-exorcismus">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-magnus-exorcismus.jpg') }}" alt="Magnus Exorcismus Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Magnus Exorcismus</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 9.0s</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('item/blue-gemstone') }}">Blue Gemstone</a> x1</span><br>
									<span class="skill-range red">Requires <a href="#turn-undead">Turn Undead</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>4.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 100% Holy M.Dmg; the skill takes effect 3 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>4.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 200% Holy M.Dmg; the skill takes effect 3 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>4.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 300% Holy M.Dmg; the skill takes effect 4 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>4.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 400% Holy M.Dmg; the skill takes effect 4 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>4.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 500% Holy M.Dmg; the skill takes effect 5 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>4.0s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 600% Holy M.Dmg; the skill takes effect 5 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>4.0s</td>
								<td>68</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 700% Holy M.Dmg; the skill takes effect 6 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>4.0s</td>
								<td>72</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 800% Holy M.Dmg; the skill takes effect 6 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>4.0s</td>
								<td>76</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 900% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>4.0s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1000% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>4.0s</td>
								<td>84</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1025% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>4.0s</td>
								<td>88</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1050% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>4.0s</td>
								<td>92</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1075% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>4.0s</td>
								<td>96</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1100% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>4.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1125% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>4.0s</td>
								<td>104</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1150% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>4.0s</td>
								<td>108</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1175% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>4.0s</td>
								<td>112</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1200% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>4.0s</td>
								<td>116</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1225% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>4.0s</td>
								<td>120</td>
								<td class="skill-description" colspan="3">Form a cross-shaped zone in specified areas, Undead and devil monsters in this zone will suffer 1250% Holy M.Dmg; the skill takes effect 7 times, the damage takes effect every 1.5 seconds</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="impositio-manus">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/priest-impositio-manus.jpg') }}" alt="Impositio Manus Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Impositio Manus</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 8 Atk for all allies. Self Auto Attacks deal 10 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 16 Atk for all allies. Self Auto Attacks deal 20 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 24 Atk for all allies. Self Auto Attacks deal 30 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 32 Atk for all allies. Self Auto Attacks deal 40 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 40 Atk for all allies. Self Auto Attacks deal 50 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>3.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 48 Atk for all allies. Self Auto Attacks deal 60 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>3.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 56 Atk for all allies. Self Auto Attacks deal 70 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>3.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 64 Atk for all allies. Self Auto Attacks deal 80 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>3.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 72 Atk for all allies. Self Auto Attacks deal 90 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Priest prays for friendly units, increasing 80 Atk for all allies. Self Auto Attacks deal 100 size correction damage that ignores Def. The effect lasts 120 sec.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection