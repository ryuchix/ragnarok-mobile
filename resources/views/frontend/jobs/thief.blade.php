@extends('layouts.page')

@section('title', 'Thief Class, Thief Skill, Thief Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Thief Class, Thief Skill, Thief Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('thief'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/thief-min.png') }}" alt="Thief" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Thief</h1>
					<span>1st Job</span><br>

					<p>Born and raised as an orphan without even knowing his/her parents' faces, wandering in the back alleys of cities, King Tristan 3rd's recruitment for adventurers was a great opportunity.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/thief-min.png') }}" alt="Thief" width="300px"></div>
				<div class="jobs">
					<h1>Thief</h1>
					<span>1st Job</span><br>

					<p>Born and raised as an orphan without even knowing his/her parents' faces, wandering in the back alleys of cities, King Tristan 3rd's recruitment for adventurers was a great opportunity.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="strong-blade-attack">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-strong-blade-attack.png') }}" alt="Strong Blade Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Strong Blade Attack</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>5</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 110%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>6</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 120%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>7</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 130%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>8</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 140%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>9</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 150%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>10</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 160%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>11</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 170%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>12</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 180%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>13</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 190%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>14</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Deals (Atk 200%) Dmg to an enemy and reduces its Move Spd by 30%. last 5 sec.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="double-attack">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-double-attack.png') }}" alt="Double Attack Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Double Attack</span><br>
									<span class="skill-state">Passive</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 5% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 10% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 15% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 20% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 25% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 30% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 35% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 40% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 45% chance to deal double damage</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal double damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 205% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 210% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 215% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 220% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 225% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 230% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 235% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 240% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 245% damage</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">When launching auto attacks with daggers, there is a 50% chance to deal 250% damage</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="improve-flee">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-improve-flee.png') }}" alt="Improve flee Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Improve Flee</span><br>
									<span class="skill-state">Passive</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 3 and Move Sped 1%.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 6 and Move Sped 2%.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 9 and Move Sped 3%.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 12 and Move Sped 4%.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 15 and Move Sped 5%.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 18 and Move Sped 6%.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 21 and Move Sped 7%.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 24 and Move Sped 8%.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 27 and Move Sped 9%.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Permanently increases Flee 30 and Move Sped 10%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Permanently increases Flee by 30, Flee rate by 1% and Move Spd by 11%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Permanently increases Flee by 30, Flee rate by 2% and Move Spd by 12%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Permanently increases Flee by 30, Flee rate by 3% and Move Spd by 13%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Permanently increases Flee by 30, Flee rate by 4% and Move Spd by 14%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Permanently increases Flee by 30, Flee rate by 5% and Move Spd by 15%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="hiding">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-hiding.png') }}" alt="Hiding Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Hiding</span><br>
									<span class="skill-state">Active Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.3s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 80%.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.3s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 75%.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.3s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 70%.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.3s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 65%.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.3s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 60%.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>0.3s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 55%.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>0.3s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 50%.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>0.3s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 45%.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>0.3s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 40%.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>0.3s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Becomes hiding for 30 sec, and will not be spotted by enemies. Meanwhile Move Spd will be reduced by 35%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="ambush">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-ambush.png') }}" alt="Ambush Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Ambush</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1s</td>
								<td>11</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 150% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 180% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 210% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 240% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 270% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 300% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 330% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 360% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 390% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Deal Dmg equal to 420% Atk to a single enemy; If the skill is released in hiding states, it deals more 100% Dmg and stuns the target for 3 seconds; Katar or daggers must be equipped to release this skill.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="enchant-poison">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-enchant-poison.png') }}" alt="Enchant Poison Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Enchant Poison</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('item/poison') }}">Poison</a> x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 30 sec. Auto attacks has 3% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 60 sec. Auto attacks has 4% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 90 sec. Auto attacks has 5% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 120 sec. Auto attacks has 6% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 150 sec. Auto attacks has 7% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 180 sec. Auto attacks has 8% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 210 sec. Auto attacks has 9% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 240 sec. Auto attacks has 10% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 270 sec. Auto attacks has 11% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Applies poison to the weapon that lasts 300 sec. Auto attacks has 12% chance to poison the enemy unit for 15 sec. Poisoned units lose Def, HPR and SPR as well as (2% Max HP +3) HP every sec. HP reduction effects stack up to 3 layers. When HP gets too low, HP losing effects won't work however the defaults brought by poison still exist.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="detoxify">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-detoxify.png') }}" alt="Detoxify Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Detoxify</span><br>
									<span class="skill-state">Active Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1s</td>
								<td>10</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Remove your and the while Party's poison status and [Poison Cast] effect</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="venom-knife">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/thief-venom-knife.png') }}" alt="Venom Knife Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Venom Knife</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 115%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 130%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 145%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 160%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 175%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 190%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2s</td>
								<td>31</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 205%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 220%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2s</td>
								<td>37</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 235%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Attacks the target with Venom Knife, dealing (Atk 250%) poison Dmg. If the target is poisoned, Deals 100% more damage.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection