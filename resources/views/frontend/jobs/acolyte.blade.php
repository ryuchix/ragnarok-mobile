@extends('layouts.page')

@section('title', 'Acolyte Class, Acolyte Skill, Acolyte Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Acolyte Class, Acolyte Skill, Acolyte Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('acolyte'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/acolyte-min.png') }}" alt="Acolyte" width="250px" class="pull-left">
				<div class="jobs">
					<h1>Acolyte</h1>
					<span>1st Job</span><br>

					<p>Living a life serving the Church, he/she sets out to join the adventurers recruited by King Tristan 3rd, in hope of finding new powers from within him/herself.</p>
					<p>He/she sets out on the journey after getting permission from the head of the Church. Following teachings, he/she does not use weapons that can hurt others. Instead,</p>
					<p>He/she sets out on the journey after getting permission from the head of the Church. Following teachings, he/she does not use weapons that can hurt others. Instead,</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/acolyte-min.png') }}" alt="Acolyte" width="300px"></div>
				<div class="jobs">
					<h1>Acolyte</h1>
					<span>1st Job</span><br>

					<p>Living a life serving the Church, he/she sets out to join the adventurers recruited by King Tristan 3rd, in hope of finding new powers from within him/herself.</p>
					<p>He/she sets out on the journey after getting permission from the head of the Church. Following teachings, he/she does not use weapons that can hurt others. Instead,</p>
					<p>He/she sets out on the journey after getting permission from the head of the Church. Following teachings, he/she does not use weapons that can hurt others. Instead,</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="holy-light">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-holy-light.jpg') }}" alt="Holy Light Strike Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Holy Light Strike</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 1.0s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk120%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk140%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk160%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk180%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk200%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk220%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk240%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk260%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk280%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with holy light, dealing (M.Atk300%) holy M.Dmg and some real M.Dmg. It also expels Kyrie Eleison</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="heal">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-heal.jpg') }}" alt="Heal Attack Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Heal</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Restore (12* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Restore (20* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Restore (28* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Restore (36* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Restore (44* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Restore (52* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>31</td>
								<td class="skill-description" colspan="3">Restore (60* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Restore (68* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>37</td>
								<td class="skill-description" colspan="3">Restore (76* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Restore (84* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Restore (86* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Restore (88* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>49</td>
								<td class="skill-description" colspan="3">Restore (90* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Restore (92* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">Restore (94* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Restore (96* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>61</td>
								<td class="skill-description" colspan="3">Restore (98* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Restore (100* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>67</td>
								<td class="skill-description" colspan="3">Restore (102* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Restore (104* Rounded ( (BaseLv + INT) /10) ) HP to self and targeted ally. When the targets are undead monsters, the skill deals Holy Damage: 100% Healing Value/2</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="kyrie-eleison">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-kyrie-eleison.jpg') }}" alt="Kyrie Eleison Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Kyrie Eleison</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span><br>
									<span class="skill-range">Cast Time: 2.0s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 5 times. Damage Cap = Benefier 12% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 6 times. Damage Cap = Benefier 14% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 6 times. Damage Cap = Benefier 16% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 7 times. Damage Cap = Benefier 18% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 7 times. Damage Cap = Benefier 20% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 8 times. Damage Cap = Benefier 22% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 8 times. Damage Cap = Benefier 24% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 9 times. Damage Cap = Benefier 26% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 9 times. Damage Cap = Benefier 28% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 1500 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 3000 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 4500 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 6000 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 7500 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>2.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 9000 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>2.0s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 10500 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>2.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 12000 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>2.0s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 13500 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>2.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Give a shield that blocks Dmg to an ally. The shield protects for limit times to take limited damage. The shield becomes invalid when either limit is met. Can block up to 10 times. Damage Cap = Benefier 30% Max HP + 15000 HP. The shield lasts 120 sec but becomes invalid when hit by <a href="#holy-light">Holy Light Strike</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="blessing">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-blessing.png') }}" alt="Blessing Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Blessing</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span><br>
									<span class="skill-range red">Requires Acolyte Lv.13*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 2 Str, Dex and Int for 120 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 4 Str, Dex and Int for 140 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 6 Str, Dex and Int for 160 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 8 Str, Dex and Int for 180 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 10 Str, Dex and Int for 200 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 12 Str, Dex and Int for 220 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 14 Str, Dex and Int for 240 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 16 Str, Dex and Int for 260 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 18 Str, Dex and Int for 280 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 20 Str, Dex and Int for 300 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>68</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 21 Str, Dex and Int for 300 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>72</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 22 Str, Dex and Int for 300 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>76</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 23 Str, Dex and Int for 300 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 24 Str, Dex and Int for 300 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>84</td>
								<td class="skill-description" colspan="3">Provides the target and all allies with 25 Str, Dex and Int for 300 sec. Removes Curse and Petrify from the target ally.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="increase-agility">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-increase-agility.jpg') }}" alt="Increase Agility Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Increase Agility</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#blessing">Blessing</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 2 AGI points and 30% Move Spd for 120 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 4 AGI points and 30% Move Spd for 140 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 6 AGI points and 30% Move Spd for 160 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 8 AGI points and 30% Move Spd for 180 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 10 AGI points and 30% Move Spd for 200 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 12 AGI points and 30% Move Spd for 220 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 14 AGI points and 30% Move Spd for 240 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 16 AGI points and 30% Move Spd for 260 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 18 AGI points and 30% Move Spd for 280 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3"><a href="#blessing">Blessing</a> gets the effect of <a href="#increase-agility">Increase Agility</a>, increase 20 AGI points and 30% Move Spd for 300 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="ruwach">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-blessing.png') }}" alt="Ruwach Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Ruwach</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span><br>
									<span class="skill-range red">Requires Acolyte Lv.15*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>10</td>
								<td>0.5s</td>
								<td class="skill-description" colspan="3">Searches hidden enemy units with the holy spirit in 10 sec, dealing (M.Atk150%) holy M.Dmg to any enemy units found</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="light-shield">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-light-shield.jpg') }}" alt="Light Shield Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Light Shield</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires <a href="#ruwach">Ruwach</a> Lv.1*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Becomes immune to ranged Atk. The barrier could take up to 10 times of attack for 8 sec. Could have 1 barriers at the same time	</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Becomes immune to ranged Atk. The barrier could take up to 10 times of attack for 9 sec. Could have 1 barriers at the same time	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Becomes immune to ranged Atk. The barrier could take up to 10 times of attack for 10 sec. Could have 2 barriers at the same time	</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Becomes immune to ranged Atk. The barrier could take up to 10 times of attack for 11 sec. Could have 2 barriers at the same time	</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Becomes immune to ranged Atk. The barrier could take up to 10 times of attack for 12 sec. Could have 3 barriers at the same time	</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="maces-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/acolyte-mace-mastery.jpg') }}" alt="Maces Mastery Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Maces Mastery</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 6 points, Auto Attack increases by 20 points</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 12 points, Auto Attack increases by 40 points</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 18 points, Auto Attack increases by 60 points</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 24 points, Auto Attack increases by 80 points</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 30 points, Auto Attack increases by 100 points</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 36 points, Auto Attack increases by 120 points</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 42 points, Auto Attack increases by 140 points</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 48 points, Auto Attack increases by 160 points</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 54 points, Auto Attack increases by 180 points</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 200 points</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 220 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 240 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 2%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 260 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 3%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 280 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 4%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 300 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 5%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 320 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 6%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 340 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 7%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 360 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 8%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 380 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 9%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">When using Maces and Knuckles, Atk increases by 60 points, Auto Attack increases by 400 points. When using Maces, AGI, LUK and Crit increases by 1. When using Knuckles, Atk increases by 10%.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection