@extends('layouts.page')

@section('title', 'Rogue Class, Rogue Skill, Rogue Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Rogue Class, Rogue Skill, Rogue Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('rogue'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/rougue-min.png') }}" alt="Rogue" width="240px" class="pull-left">
				<div class="jobs">
					<h1>Rogue</h1>
					<span>2nd Job</span><br>

					<p>Enhancing his/her skills learned from the days as a neighborhood ruffian, everybody avoids his/her eyes. Living in dullness, he/she decides to join the adventurers gathered by King Tristan III, thinking maybe the skills would work on monsters as well.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
					<img src="{{ url('frontend/img/jobs/class/rougue-min.png') }}" alt="Rogue" width="300px">
				</div>
				<div class="jobs">
					<h1>Rogue</h1>
					<span>2nd Job</span><br>

					<p>Enhancing his/her skills learned from the days as a neighborhood ruffian, everybody avoids his/her eyes. Living in dullness, he/she decides to join the adventurers gathered by King Tristan III, thinking maybe the skills would work on monsters as well.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="double-strafe">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-Double-Strafe.jpg') }}" alt="Double-Strafe Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Double Strafe</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>11</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (100% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 10%.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (110% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 20%.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (120% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 130%.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (130% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 40%.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (140% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 50%.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (150% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 60%.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (160% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 70%.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (170% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 80%.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (180% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 90%.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Shoot 2 arrows at the same time, deal double Dmg (190% Atk) to a single enemy, increase the skill multiplier of <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> by 100%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="repeated-firing">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-repeated-firing.png') }}" alt="Repeated Firing Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Repeated Firing</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#double-strafe">Double Strafe</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Auto attack has a 3% chance to automatically launch <a href="#double-strafe">Double Strafe</a> of the same level.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Auto attack has a 6% chance to automatically launch <a href="#double-strafe">Double Strafe</a> of the same level.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Auto attack has a 9% chance to automatically launch <a href="#double-strafe">Double Strafe</a> of the same level.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-complusion.png') }}" alt="Compulsion Discount Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Compulsion Discount</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Receive 3% discount when buying items from NPC</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Receive 4% discount when buying items from NPC</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Receive 5% discount when buying items from NPC</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Receive 6% discount when buying items from NPC</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Receive 7% discount when buying items from NPC</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="snatch">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-snatch.png') }}" alt="Snatch Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Snatch</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>6</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 5%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>7</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 10%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>8</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 15%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>9</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 20%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>10</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 25%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>11</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 30%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>12</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 35%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>13</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 40%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>14</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 45%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>15</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Rogue has a 5% chance to steal Zeny from monsters, the ratio is 50%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="rob">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-rob.png') }}" alt="Compulsion Rob Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Rob</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Snatch Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 2% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 4% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 6% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 8% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 10% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 12% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 14% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 16% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 18% chance to receive another copy.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When attaining raw materials from wild monsters, player has a 20% chance to receive another copy.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="backstab">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-backstab.png') }}" alt="Back Stab Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Back Stab</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range red">Requires <a href="{{ url('thief#ambush') }}">Ambush</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>18</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 360% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>20</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 420% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>22</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 480% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>24</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 540% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>26</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 600% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>28</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 660% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>30</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 720% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>32</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 780% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>34</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 840% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 900% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 920% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 940% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 960% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 980% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 1000% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 1020% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 1040% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 1060% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 1080% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>2.0s</td>
								<td>36</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Deals Atk 1100% Damage to a single target. Increase 100% damage after the using <a href="{{ url('thief#ambush') }}">Ambush</a>.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-raid.png') }}" alt="Raid Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Raid</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m</span><br>
									<span class="skill-range red">Requires <a href="#backstab">Back Stab</a> Lv.1*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>20</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Caster deals 180% Dmg to all enemies in range, with a 7% chance to inflict snared, Stunned and Darkness on enemy units. Enemy targets hit by Raid will suffer 11% more damage for the next 5 seconds.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>20</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Caster deals 210% Dmg to all enemies in range, with a 9% chance to inflict snared, Stunned and Darkness on enemy units. Enemy targets hit by Raid will suffer 12% more damage for the next 5 seconds.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>20</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Caster deals 240% Dmg to all enemies in range, with a 11% chance to inflict snared, Stunned and Darkness on enemy units. Enemy targets hit by Raid will suffer 13% more damage for the next 5 seconds.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>20</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Caster deals 270% Dmg to all enemies in range, with a 13% chance to inflict snared, Stunned and Darkness on enemy units. Enemy targets hit by Raid will suffer 14% more damage for the next 5 seconds.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>20</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Caster deals 300% Dmg to all enemies in range, with a 15% chance to inflict snared, Stunned and Darkness on enemy units. Enemy targets hit by Raid will suffer 15% more damage for the next 5 seconds.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-tunnel-drives.png') }}" alt="Tunnel Drive Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Tunnel Drive</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Further decrease the penalty of moving speed by 5% in hiding states, permanently increases the Flee value by 2%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Further decrease the penalty of moving speed by 10% in hiding states, permanently increases the Flee value by 4%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Further decrease the penalty of moving speed by 15% in hiding states, permanently increases the Flee value by 6%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Further decrease the penalty of moving speed by 20% in hiding states, permanently increases the Flee value by 8%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Further decrease the penalty of moving speed by 25% in hiding states, permanently increases the Flee value by 10%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="enhanced-hiding">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-enhanced-hiding.png') }}" alt="Enhanced Hiding Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Enhanced Hiding</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Tunnel Drive Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Within 3 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Within 5 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Within 7 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Within 9 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Within 11 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Within 13 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid. Increase Movement Speed in hiding state by 4%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Within 15 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid Increase Movement Speed in hiding state by 8%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Within 17 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid Increase Movement Speed in hiding state by 12%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Within 19 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid Increase Movement Speed in hiding state by 16%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Within 21 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid Increase Movement Speed in hiding state by 20%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="dagger-proficiency">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-dagger-prof.png') }}" alt="Dagger Proficiency Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Dagger Proficiency</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 6 points, Auto Attack increases by 20 points</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 12 points, Auto Attack increases by 40 points</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 18 points, Auto Attack increases by 60 points</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 24 points, Auto Attack increases by 80 points</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 30 points, Auto Attack increases by 100 points</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 36 points, Auto Attack increases by 120 points</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 42 points, Auto Attack increases by 140 points</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 48 points, Auto Attack increases by 160 points</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 54 points, Auto Attack increases by 180 points</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 200 points</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 220 points, Def penetration increases by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 240 points, Def penetration increases by 2%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 260 points, Def penetration increases by 3%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 280 points, Def penetration increases by 4%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 300 points, Def penetration increases by 5%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 320 points, Def penetration increases by 6%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 340 points, Def penetration increases by 7%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 360 points, Def penetration increases by 8%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 380 points, Def penetration increases by 9%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">When using daggers, Atk increases by 60 points, Auto Attack increases by 400 points, Def penetration increases by 10%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="vultures-eye">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-vultures-eye.png') }}" alt="Vulture's Eye Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Vulture's Eye</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 2, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 5%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 4, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 10%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 6, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 15%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 8, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 20%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 10, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 25%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 12, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 30%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 14, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 35%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 16, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 40%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 18, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 45%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 6%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 12%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 18%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 24%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 30%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 36%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 42%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 48%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 54%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">When equiping Bow: Hit increases by 20, the Distance of Auto Attack, <a href="#double-strafe">Double Strafe</a>, <a href="{{ url('stalker#triangle-shot') }}">Triangle Shot</a> and Disabling Strike will increase by 50%. Auto Attack and <a href="#double-strafe">Double Strafe</a> Damage will be increased with distance, up to a maximum of 60%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="plagiarism">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/rogue-plagirsm.png') }}" alt="Plagiarism Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Plagiarism</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 1 skill from monster.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 2 skill from monster.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 3 skill from monster.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 4 skill from monster.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 5 skill from monster.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 6 skill from monster.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 7 skill from monster.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 8 skill from monster.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 9 skill from monster.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>50</td>
								<td>3s</td>
								<td class="skill-description" colspan="3">Can learn only level 10 skill from monster.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection