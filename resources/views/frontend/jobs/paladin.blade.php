@extends('layouts.page')

@section('title', 'Paladin Class, Paladin Skill, Paladin Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Paladin Class, Paladin Skill, Paladin Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('paladin'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/paladin-min.png') }}" alt="Paladin" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Paladin</h1>
					<span>Swordsman's 2nd Transcendent Job</span><br>

					<p>Do you believe in God? Witness the Paladin's might, and see the power of the Lord for yourself. In devoutly following the path of righteousness, Crusaders can surpass their limits, becoming holy Paladins that specialize in providing support to their comrades.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/paladin-min.png') }}" alt="Paladin" width="300px">
				<div class="jobs">
					<h1>Paladin</h1>
					<span>Swordsman's 2nd Transcendent Job</span><br>

					<p>Do you believe in God? Witness the Paladin's might, and see the power of the Lord for yourself. In devoutly following the path of righteousness, Crusaders can surpass their limits, becoming holy Paladins that specialize in providing support to their comrades.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="grand-cross">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-grand-cross-min.png') }}" alt="Grand Cross Skill Image">
								</td>
								<td colspan="6">
									<span class="skill-name">Grand Cross</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 3.0m</span><br>
									<span class="skill-range">Cast Time: 3.0s</span><br>
									<span class="skill-range red">Requires <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, Deals Holy damage: Atk x210% and Holy M.Dmg: M.Atk x210%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x270% and Holy M.Dmg: M.Atk x270%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x330% and Holy M.Dmg: M.Atk x330%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>62</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x390% and Holy M.Dmg: M.Atk x390%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x450% and Holy M.Dmg: M.Atk x450%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>78</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x510% and Holy M.Dmg: M.Atk x510%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>86</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x570% and Holy M.Dmg: M.Atk x570%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>94</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x630% and Holy M.Dmg: M.Atk x630%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>102</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x690% and Holy M.Dmg: M.Atk x690%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>110</td>
								<td class="skill-description" colspan="3">Summons a cross zone centered on yourself, judging at most 6 enemy targets in the rage, dealing Holy damage: Atk x750% and Holy M.Dmg: M.Atk x750%, but yourself will be injured. This skill ignores the target's Flee and is not influenced by damage reflection shield</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="providence">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-providence-min.png') }}" alt="Providence Skill Image">
								</td>
								<td colspan="6">
									<span class="skill-name">Providence</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 9.0m</span><br>
									<span class="skill-range">Cast Time: 1.0s</span><br>
									<span class="skill-range red">Requires <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 2% and Holy damage by 2% done to the whole Party for 120 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 30% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 20% for the caster</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 4% and Holy damage by 4% done to the whole Party for 140 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 60% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 40% for the caster</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 6% and Holy damage by 6% done to the whole Party for 160 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 90% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 60% for the caster</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 8% and Holy damage by 8% done to the whole Party for 180 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 120% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 80% for the caster</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 10% and Holy damage by 10% done to the whole Party for 200 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 150% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 100% for the caster</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 12% and Holy damage by 12% done to the whole Party for 220 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 180% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 120% for the caster</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 14% and Holy damage by 14% done to the whole Party for 240 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 210% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 140% for the caster</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 16% and Holy damage by 16% done to the whole Party for 260 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 240% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 160% for the caster</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 18% and Holy damage by 18% done to the whole Party for 280 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 270% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 180% for the caster</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Invokes the protection from gods which Decreases Demon damage by 20% and Holy damage by 20% done to the whole Party for 300 sec. Increases the skill multiplier of <a href="#grand-cross">Grand Cross</a> by 300% and <a href="{{ url('crusader#holy-cross') }}">Holy Cross</a> by 200% for the caster</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="shield-boomerang">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-shield-boomerang-min.png') }}" alt="Shield Boomerang Skill Image">
								</td>
								<td colspan="6">
									{!! config('app.tip') !!}
									<span class="skill-name">Shield Boomerang</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Cast Time: 1.0s</span><br>
									<span class="skill-range">SP: 12</span><br>
									<span class="skill-range">Delay: 1.0s</span><br>
									<span class="skill-range red">Requires <a href="{{ url('crusader#shield-chain') }}">Shield Chain</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Range</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (360%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2)). Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (420%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2)). Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>4.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (480%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2)). Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>5.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (540%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2)). Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2)). Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*10%)). Increases Def by 30 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*20%)). Increases Def by 60 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*30%)). Increases Def by 90 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*40%)). Increases Def by 120 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*50%)). Increases Def by 150 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*60%)). Increases Def by 180 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*70%)). Increases Def by 210 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*80%)). Increases Def by 240 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*90%)). Increases Def by 270 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>6.0m</td>
								<td class="skill-description" colspan="3">Throw shields to enemy in distance, dealing damage (600%*(Vitality*8 + Rounded(VIT*VIT/100)*4 + Rounded (DEX/5) + Rounded(LUK/5) + Shield Refining Level*Shield Refining Level*2 + (Atk+Refine Atk)*100%)). Increases Def by 300 for 3 seconds. Shield as off-hand weapon must be equipped.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="retribution">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-retribution-min.png') }}" alt="Retribution Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Retribution</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases self Atk by 5. Deals 1% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases self Atk by 10. Deals 2% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases self Atk by 15. Deals 3% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases self Atk by 20. Deals 4% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases self Atk by 25. Deals 5% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increases self Atk by 30. Deals 6% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increases self Atk by 35. Deals 7% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increases self Atk by 40. Deals 8% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increases self Atk by 45. Deals 9% more damage to undead and devil monsters</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 1%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 2%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 3%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 4%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 5%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 6%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 7%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 8%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 9%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Increases self Atk by 50. Deals 10% more damage to undead and devil monsters. Neutral and Holy attributes damage increases by 10%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="selfless-shield">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-selfless-shield-min.png') }}" alt="Selfless Shield Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Selfless Shield</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 4.0m</span><br>
									<span class="skill-range red">Requires <a href="{{ url('crusader#devotion') }}">Devotion</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 33%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 36%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 39%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 42%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 45%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 48%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 51%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 54%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>65</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 57%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>20.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Continuously casts a spell to protect all Party members within 4 meters for 10 seconds, reducing their damage received (excluding himself) by 60%. Cannot coexist with Devotion status. Requires a shield</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="defending-aura">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-defending-aura-min.png') }}" alt="Defending Aura Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Defending Aura</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 4.0m</span><br>
									<span class="skill-range red">Requires <a href="{{ url('crusader#devotion') }}">Devotion</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">The caster takes 10% less damage from ranged physical auto attacks but loses 65% Move Spd and 20% ASPD. The status could be passed to allies via <a href="{{ url('crusader#devotion') }}">Devotion</a>. Lasts 120 sec. Shield type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">The caster takes 25% less damage from ranged physical auto attacks but loses 65% Move Spd and 15% ASPD. The status could be passed to allies via <a href="{{ url('crusader#devotion') }}">Devotion</a>. Lasts 120 sec. Shield type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">The caster takes 40% less damage from ranged physical auto attacks but loses 65% Move Spd and 10% ASPD. The status could be passed to allies via <a href="{{ url('crusader#devotion') }}">Devotion</a>. Lasts 120 sec. Shield type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">The caster takes 55% less damage from ranged physical auto attacks but loses 65% Move Spd and 5% ASPD. The status could be passed to allies via <a href="{{ url('crusader#devotion') }}">Devotion</a>. Lasts 120 sec. Shield type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">The caster takes 70% less damage from ranged physical auto attacks but loses 65% Move Spd and 0% ASPD. The status could be passed to allies via <a href="{{ url('crusader#devotion') }}">Devotion</a>. Lasts 120 sec. Shield type weapons are required.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="reflect-shield">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-reflect-shield-min.png') }}" alt="Reflect Shield Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Reflect Shield</span><br>
									<span class="skill-state">Active Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 8% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 11% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 14% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 17% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 20% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 23% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>65</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 26% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 29% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 32% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">When the caster takes melee Atk, deals 35% reflecting damage to the damage dealing. Lasts 300 sec. Shield as off-hand weapon must be equipped.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="sacrifice">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-sacrifice-min.png') }}" alt="Sacrifice Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Sacrifice</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Requires <a href="#reflect-shield">Reflect Shield</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.1) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.2) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.3) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*30%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*60%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*90%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*120%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*150%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*180%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*210%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*240%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*270%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Caster enters Sacrifice status to deal (9% Max HP x1.4 + (Atk+Refine Atk)*300%) forced lane damage at the cost of 9% max HP with the next 5 auto attacks. Lasts 90 sec. The attack ignores Flee and defense</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="bliss">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-bliss-min.png') }}" alt="Bliss Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Bliss</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently increases Healing received by 1% and VIT by 1</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently increases Healing received by 2% and VIT by 2</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently increases Healing received by 3% and VIT by 3</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently increases Healing received by 4% and VIT by 4</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently increases Healing received by 5% and VIT by 5</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="faith">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/pala-faith-min.png') }}" alt="Faith Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Faith</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#bliss">Bliss</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Permanently gains 200 max HP. Takes 5% less holy damage</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Permanently gains 400 max HP. Takes 10% less holy damage</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Permanently gains 600 max HP. Takes 15% less holy damage</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Permanently gains 800 max HP. Takes 20% less holy damage</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Permanently gains 1000 max HP. Takes 25% less holy damage</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Permanently gains 1200 max HP. Takes 30% less holy damage</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Permanently gains 1400 max HP. Takes 35% less holy damage</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Permanently gains 1600 max HP. Takes 40% less holy damage</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Permanently gains 1800 max HP. Takes 45% less holy damage</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Permanently gains 2000 max HP. Takes 50% less holy damage</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection