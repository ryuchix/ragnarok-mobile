@extends('layouts.page')

@section('title', 'Assassin Class, Assassin Skill, Assassin Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Assassin Class, Assassin Skill, Assassin Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('assassin'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/assassin-min.png') }}" alt="Assassin" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Assassin</h1>
					<span>Thief's 2nd Job</span><br>

					<p>Existing in darkness, never entering the world of light, they were always feared by others. With the ability to secretly enter places and poison an enemy to get rid of them without a trace, they were worthy of being feared.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/assassin-min.png') }}" alt="Assassin" width="300px"></div>
				<div class="jobs">
					<h1>Assassin</h1>
					<span>Thief's 2nd Job</span><br>

					<p>Existing in darkness, never entering the world of light, they were always feared by others. With the ability to secretly enter places and poison an enemy to get rid of them without a trace, they were worthy of being feared.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="katar-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-katar-mastery.png') }}" alt="Katar Mastery Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Katar Mastery</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">+3 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">+6 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">+9 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">+12 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">+15 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">+18 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">+21 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">+24 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">+27 when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">+30 when using Katars type weapons.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="grimtooth">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-grimtooth.png') }}" alt="Grimtooth Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Grimtooth</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>5</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 150%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 30% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>6</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 160%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 35% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>7</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 170%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 40% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 180%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 45% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>9</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 190%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 50% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 200%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 55% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>11</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 210%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 60% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 220%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 65% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 230%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 70% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Attacks an enemy with spikes, dealing (Atk 240%) Dmg to akk enemies in the target area. This can only be used when the caster is hiding. Has a 75% chance to remain hiding while casting. Katar type weapons is required.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="twin-blade-penetration">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-twin-blade-penetration.png') }}" alt="Twin Blade Penetration Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Twin Blade Penetration</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('thief#double-attack') }}">Double Attack</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 5% chance to reduce the enemy unit's equipment Def by 50% for 4 sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 10% chance to reduce the enemy unit's equipment Def by 50% for 4 sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 15% chance to reduce the enemy unit's equipment Def by 50% for 4 sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 20% chance to reduce the enemy unit's equipment Def by 50% for 4 sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 1.5% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 3.0% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 4.5% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 6.0% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 7.5% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 9.0% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 10.5% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 12.0% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 13.5% Def penetration for 4 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Auto Attacks dealt with Dagger type weapons has a 25% chance to reduce the enemy unit's equipment Def by 50% for 4 sec, and has a 10% chance to increase 15.0% Def penetration for 4 seconds.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="sonic-blow">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-sonic-blow.png') }}" alt="Sonic Blow Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Sonic Blow</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 240%) Dmg to an enemy. Has a 12% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 280%) Dmg to an enemy. Has a 14% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 320%) Dmg to an enemy. Has a 16% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 360%) Dmg to an enemy. Has a 18% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 400%) Dmg to an enemy. Has a 20% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 440%) Dmg to an enemy. Has a 22% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 480%) Dmg to an enemy. Has a 24% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 520%) Dmg to an enemy. Has a 26% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 560%) Dmg to an enemy. Has a 28% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 600%) Dmg to an enemy. Has a 30% chance to stun it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 620%) Dmg to an enemy. Has a 32% chance to stun it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 640%) Dmg to an enemy. Has a 34% chance to stun it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 660%) Dmg to an enemy. Has a 36% chance to stun it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 680%) Dmg to an enemy. Has a 38% chance to stun it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Cuts the target quickly, dealing (Atk 700%) Dmg to an enemy. Has a 40% chance to stun it.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="sonic-acceleration">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-sonic-acceleration.png') }}" alt="Sonic Acceleration Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Sonic Acceleration</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Sonic Blow deals 20% more damage and permanently increases self Hit by 6.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Sonic Blow deals 40% more damage and permanently increases self Hit by 12.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Sonic Blow deals 60% more damage and permanently increases self Hit by 18.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Sonic Blow deals 80% more damage and permanently increases self Hit by 24.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Sonic Blow deals 100% more damage and permanently increases self Hit by 30.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="back-sliding">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-back-sliding.png') }}" alt="Back Sliding Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Back Sliding</span><br>
									<span class="skill-state">Active Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>6</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 3 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>7</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 6 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>8</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 9 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>9</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 12 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>10</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 15 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>11</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 18 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>12</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 21 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>13</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 24 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>14</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 27 flee and becomes hiding for 5 sec.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>15</td>
								<td>5s</td>
								<td class="skill-description" colspan="3">Rolls back to evade enemy attack. Gains 30 flee and becomes hiding for 5 sec.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="enhanced-enchant-poison">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-enhanced-enchant-poison.png') }}" alt="Enhanced Enchant Poison Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Enhanced Enchant Poison</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 4 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 1 more sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 8 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 2 more sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 12 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 3 more sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 16 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 4 more sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 20 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 5 more sec.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 24 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 6 more sec.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 28 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 7 more sec.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 32 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 8 more sec.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 36 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 9 more sec.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">The poisoned weapon gains 40 Atk. Poison caused by [Enchant Poison], [Venom Dart] and [Virus Diffusion] lasts 10 more sec.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="virus-diffusion">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-virus-diffusion.png') }}" alt="Virus Diffusion Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Virus Diffusion</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.5s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 60%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.5s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 70%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.5s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 80%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.5s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 90%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 100%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.5s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 110%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.5s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 120%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.5s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 130%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 140%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.5s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Spreads plague to an enemy, dealing (Atk 150%) Dmg to the target and all nearby enemies. If the target is poisoned, the poison spreads to all enemies in the area. The poison effects stack up to 3 layers.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="venom-dart">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-venom-dart.png') }}" alt="Venom Dart Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Venom Dart</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('item/poison') }}">Poison</a> x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>10</td>
								<td>2.5s</td>
								<td class="skill-description" colspan="3">Deals (Atk 130%) Dmg to an enemy and poison it. The poison status cannot stack.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>15</td>
								<td>2.5s</td>
								<td class="skill-description" colspan="3">Deals (Atk 160%) Dmg to an enemy and poison it. The poison status cannot stack.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>20</td>
								<td>2.5s</td>
								<td class="skill-description" colspan="3">Deals (Atk 190%) Dmg to an enemy and poison it. The poison status cannot stack.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>25</td>
								<td>2.5s</td>
								<td class="skill-description" colspan="3">Deals (Atk 220%) Dmg to an enemy and poison it. The poison status cannot stack.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>30</td>
								<td>2.5s</td>
								<td class="skill-description" colspan="3">Deals (Atk 250%) Dmg to an enemy and poison it. The poison status cannot stack.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection