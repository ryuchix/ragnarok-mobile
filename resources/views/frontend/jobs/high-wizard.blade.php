@extends('layouts.page')

@section('title', 'High Wizard Class, High Wizard Skill, High Wizard Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('High Wizard Class, High Wizard Skill, High Wizard Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('high-wizard'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/high_wizard-min.png') }}" alt="High Wizard" width="280px" class="pull-left">
				<div class="jobs">
					<h1>High Wizard</h1>
					<span>Mage's 2nd Transcendent Job</span><br>

					<p>The Mage and Wizard may be able to tap into mystical energies, but the immeasurable power of the elements is only fully realized in the spells cast by the High Wizard. Only a fool would dare challenge the awesome might of a High Wizard's advanced elemental skills head on...</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/high_wizard-min.png') }}" alt="High Wizard" width="300px"></div>
				<div class="jobs">
					<h1>High Wizard</h1>
					<span>Mage's 2nd Transcendent Job</span><br>

					<p>The Mage and Wizard may be able to tap into mystical energies, but the immeasurable power of the elements is only fully realized in the spells cast by the High Wizard. Only a fool would dare challenge the awesome might of a High Wizard's advanced elemental skills head on...</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="napalm-vulcan">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-napalm-vulcan.png') }}" alt="Napalm Vulcan Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Napalm Vulcan</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 1.2s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>7</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*150%) ghost M.Dmg. Has a 3% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*200%) ghost M.Dmg. Has a 6% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*250%) ghost M.Dmg. Has a 9% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*300%) ghost M.Dmg. Has a 12% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*350%) ghost M.Dmg. Has a 15% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*400%) ghost M.Dmg. Has a 18% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>49</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*450%) ghost M.Dmg. Has a 21% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*500%) ghost M.Dmg. Has a 24% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>63</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*550%) ghost M.Dmg. Has a 27% chance to curse it</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Attacks an enemy continuously with will power, dealing (M.Atk*600%) ghost M.Dmg. Has a 30% chance to curse it</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="magic-power">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-magic-power.png') }}" alt="Magic Power Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Magic Power</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Cast Time: 1.0s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>20</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 5%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>25</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 10%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>30</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 15%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>35</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 20%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>40</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 25%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>45</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 30%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>50</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 35%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>55</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 40%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>60</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 45%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>65</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>75</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 2%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>85</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 4%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>95</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 6%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>105</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 8%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>115</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 10%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>125</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 12%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>135</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 14%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>145</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 16%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>155</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 18%) buff which lasts for 30 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>165</td>
								<td class="skill-description" colspan="3">In the next 30 seconds, magic skill damage will increases by 50%. When using this skill, randomly acquire a (wind/earth/water/fire attribute damage increase by 20%) buff which lasts for 30 seconds.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="alight">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-alight.png') }}" alt="Alight Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Alight</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">All fire skills have a 5% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk7%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">All fire skills have a 10% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk9%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">All fire skills have a 15% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk11%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">All fire skills have a 20% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk13%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">All fire skills have a 25% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk15%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">All fire skills have a 30% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk17%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">All fire skills have a 35% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk19%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">All fire skills have a 40% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk21%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">All fire skills have a 45% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk23%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">All fire skills have a 50% chance to inflict Alight status on enemy units hit, every 1 sec dealing (M.Atk25%) fire M.Dmg. Lasts 8 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="flame-dash">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-flame-dash.png') }}" alt="Flame Dash Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Flame Dash</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires <a href="#alight">Alight</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>0.8s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 60% +150) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>1.1s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 80% +200) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>1.4s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 100% +250) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>1.7s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 120% +300) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>2.0s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 140% +350) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>2.3s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 160% +400) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>2.6s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 180% +450) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>2.9s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 200% +500) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>3.2s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 220% +550) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>3.5s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Attacks the target with flame charge, dealing (M.Atk 240% +600) fire M.Dmg that ignores Mdef. If the target is in Alight status, explodes to deal massive fire M.Dmg to nearby enemies. The skill cannot apply Alight status	</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="safety-wall">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-safety-wall.png') }}" alt="Safety Wall Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Safety Wall</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('item/blue-gemstone') }}">Blue Gemstone</a> x1</span><br>
									<span class="skill-range red">Requires <a href="{{ url('wizard#energy-coat') }}">Energy Coat</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>4.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 5 sec and can take up to 3 attacks. Up to 1 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.6s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 10 sec and can take up to 4 attacks. Up to 1 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.2s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 15 sec and can take up to 5 attacks. Up to 1 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.8s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 20 sec and can take up to 6 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.4s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 25 sec and can take up to 7 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 30 sec and can take up to 8 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.6s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 35 sec and can take up to 9 attacks. Up to 2 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.2s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 40 sec and can take up to 10 attacks. Up to 3 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>0.8s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 45 sec and can take up to 11 attacks. Up to 3 walls can exist at a time</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>0.4s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Builds a defensive wall in the target area, reducing melee DMG taken by 100% for the whole Party in it (self included). The wall lasts 50 sec and can take up to 12 attacks. Up to 3 walls can exist at a time</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="frost-nova">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-frost-nova.png') }}" alt="Frost Nova Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Frost Nova</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires <a href="{{ url('wizard#storm-gust') }}">Storm Gust</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>6.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 38% chance to freeze all nearby enemies for 1.5 sec. Targets not frozen will suffer (M.Atk*110%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>6.0s</td>
								<td>86</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 43% chance to freeze all nearby enemies for 3.0 sec. Targets not frozen will suffer (M.Atk*120%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>5.5s</td>
								<td>82</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 48% chance to freeze all nearby enemies for 4.5 sec. Targets not frozen will suffer (M.Atk*130%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>5.5s</td>
								<td>78</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 53% chance to freeze all nearby enemies for 6.0 sec. Targets not frozen will suffer (M.Atk*140%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>74</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 58% chance to freeze all nearby enemies for 7.5 sec. Targets not frozen will suffer (M.Atk*150%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>5.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 63% chance to freeze all nearby enemies for 9.0 sec. Targets not frozen will suffer (M.Atk*160%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>4.5s</td>
								<td>66</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 68% chance to freeze all nearby enemies for 10.5 sec. Targets not frozen will suffer (M.Atk*170%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>4.5s</td>
								<td>62</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 73% chance to freeze all nearby enemies for 12.0 sec. Targets not frozen will suffer (M.Atk*180%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>4.0s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 78% chance to freeze all nearby enemies for 13.5 sec. Targets not frozen will suffer (M.Atk*190%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>4.0s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 83% chance to freeze all nearby enemies for 15.0 sec. Targets not frozen will suffer (M.Atk*200%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>3.2s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 83% chance to freeze all nearby enemies for 15.0 sec. Targets not frozen will suffer (M.Atk*210%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>2.4s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 83% chance to freeze all nearby enemies for 15.0 sec. Targets not frozen will suffer (M.Atk*220%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>1.6s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 83% chance to freeze all nearby enemies for 15.0 sec. Targets not frozen will suffer (M.Atk*230%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>0.8s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 83% chance to freeze all nearby enemies for 15.0 sec. Targets not frozen will suffer (M.Atk*240%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>0.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Freezes air around, Has a 83% chance to freeze all nearby enemies for 15.0 sec. Targets not frozen will suffer (M.Atk*250%) water M.Dmg</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="stone-curse">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-stone-curse.png') }}" alt="Stone Curse Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Stone Curse</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 1.0s</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('item/blue-gemstone') }}">Blue Gemstone</a> x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Has a 24% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Has a 28% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Has a 32% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Has a 36% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Has a 40% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Has a 44% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">Has a 48% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Has a 52% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>65</td>
								<td class="skill-description" colspan="3">Has a 56% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Has a 60% chance to petrify all enemies in the target area (Suffers less earth Dmg/more fire Dmg). Lasts 60 sec. Every 5 sec, petrified units lose 1% HP still they have ~25% HP remaining.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="marsh-pond">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/high-wizard-marsh-pond.png') }}" alt="Marsh Pond Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Marsh Pond</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 3.0s</span><br>
									<span class="skill-range red">Requires <a href="#stone-curse">Stone Curse</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 25% Move Spd and 3 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 30% Move Spd and 6 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 35% Move Spd and 9 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 40% Move Spd and 12 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 45% Move Spd and 15 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 50% Move Spd and 18 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 55% Move Spd and 21 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 60% Move Spd and 24 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 65% Move Spd and 27 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd and 30 Agi and Dex for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>62</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 10% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 20% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>66</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 30% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>68</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 40% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 50% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>72</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 60% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>74</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 70% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>76</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 80% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>78</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 90% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Creates a pool in the target area. Any enemy units entering the area lose 70% Move Spd, 30 Agi and Dex, equipment ASPD 90% for 30 sec. Expels all acceleration buffs.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection