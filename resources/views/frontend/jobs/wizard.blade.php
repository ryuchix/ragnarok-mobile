@extends('layouts.page')

@section('title', 'Wizard Class, Wizard Skill, Wizard Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Wizard Class, Wizard Skill, Wizard Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('wizard'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/wizard-min.png') }}" alt="Wizard" width="280px" class="pull-left">
				<div class="jobs">
					<h1>Wizard</h1>
					<span>Mage's 2nd Job</span><br>

					<p>Devoting his/her life to Magic even more than a Mage, you can find a big aurora of magic surrounding his/her body seen on nobody else. One day, after reading the recruiting notice posted by King Tristan III, he/she sets out to join the group of adventurers to test out the magic he/she has been researching for so long. </p>

					<p>With increased intelligence and magic, Wizards have the ability to wipe out an enemy in battle by casting a spell with the power to cause an enormous amount of damage at once. But because so much of his/her life was devoted to learning and researching magic, he/she is not as physically fit as some other job classes. Therefore, long marches and battles may be quite overwhelming.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/wizard-min.png') }}" alt="Wizard" width="300px"></div>
				<div class="jobs">
					<h1>Wizard</h1>
					<span>Mage's 2nd Job</span><br>

					<p>Devoting his/her life to Magic even more than a Mage, you can find a big aurora of magic surrounding his/her body seen on nobody else. One day, after reading the recruiting notice posted by King Tristan III, he/she sets out to join the group of adventurers to test out the magic he/she has been researching for so long. </p>

					<p>With increased intelligence and magic, Wizards have the ability to wipe out an enemy in battle by casting a spell with the power to cause an enormous amount of damage at once. But because so much of his/her life was devoted to learning and researching magic, he/she is not as physically fit as some other job classes. Therefore, long marches and battles may be quite overwhelming.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="magic-crasher">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-magic-crasher.png') }}" alt="Magic Crasher Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Magic Crasher</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 0.3s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.3s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deals (M.Atk180%) lane M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.3s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deals (M.Atk260%) lane M.Dmg to the target	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.3s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deals (M.Atk340%) lane M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.3s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deals (M.Atk420%) lane M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.3s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deals (M.Atk500%) lane M.Dmg to the target</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="fire-ball">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-fire-ball.png') }}" alt="Fire Ball Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Fire Ball</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 1.9s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*130%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*160%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*190%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*220%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*250%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*280%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*310%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*340%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>65</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*370%) fire M.Dmg</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Attacks an enemy and all enemies around it with fire ball, dealing (M.Atk*400%) fire M.Dmg</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="sight">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-sight.png') }}" alt="Sight Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Sight</span><br>
									<span class="skill-state">Active Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.3s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Finds hiding enemy units with fire balls. Lasts 20 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="sightrasher">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-sightrasher.png') }}" alt="Sightrasher Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Sightrasher</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Cast Time: 0.6s</span><br>
									<span class="skill-range red">Special Cost: <a href="#sight">Sight</a> x1</span><br>
									<span class="skill-range red">Requires <a href="#sight">Sight</a> Lv.1*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk120%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>37</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk140%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>39</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk160%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>41</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk180%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk200%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk220%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>47</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk240%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>49</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk260%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>51</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk280%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>53</td>
								<td class="skill-description" colspan="3">Knocks back the enemy unit with Sight, dealing (M.Atk300%) fire M.Dmg to nearby enemies (Knock back is invalid in PVP)</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="soul-drain">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-soul-drain.png') }}" alt="Soul Drain Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Soul Drain</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="{{ url('mage#increase-spiritual-recovery') }}">Increase Spiritual Recovery</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases 2% Max SP. For each enemy unit killed, recovers self (Player Level*3%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases 4% Max SP. For each enemy unit killed, recovers self (Player Level*6%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases 6% Max SP. For each enemy unit killed, recovers self (Player Level*9%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases 8% Max SP. For each enemy unit killed, recovers self (Player Level*12%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases 10% Max SP. For each enemy unit killed, recovers self (Player Level*15%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increases 12% Max SP. For each enemy unit killed, recovers self (Player Level*18%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increases 14% Max SP. For each enemy unit killed, recovers self (Player Level*21%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increases 16% Max SP. For each enemy unit killed, recovers self (Player Level*24%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increases 18% Max SP. For each enemy unit killed, recovers self (Player Level*27%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increases 20% Max SP. For each enemy unit killed, recovers self (Player Level*30%) SP. The effect only works on targets locked by the required attribute.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="energy-coat">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-energy-coat.png') }}" alt="Energy Coat Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Energy Coat</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Cast Time: 0.6s</span><br>
									<span class="skill-range red">Special Cost: <a href="#sight">Sight</a> x1</span><br>
									<span class="skill-range red">Requires <a href="#sight">Sight</a> Lv.1*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10%; the effect lasts 5 seconds.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10%; the effect lasts 10 seconds.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10%; the effect lasts 15 seconds.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10%; the effect lasts 20 seconds.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>110</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10%; the effect lasts 25 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>0.5s</td>
								<td>120</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10%, ignore 1% M.Def; the effect lasts 5 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>0.5s</td>
								<td>130</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10% ignore 2% M.Def; the effect lasts 10 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>0.5s</td>
								<td>140</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10% ignore 3% M.Def; the effect lasts 15 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>0.5s</td>
								<td>150</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10% ignore 4% M.Def; the effect lasts 20 seconds.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>0.5s</td>
								<td>160</td>
								<td class="skill-description" colspan="3">Cover your whole body with spiritual energy. Receive 70% Dmg Reduction, raise M.Dmg by 10% ignore 5% M.Def; the effect lasts 25 seconds.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="storm-gust">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-storm-gust.png') }}" alt="Storm Gust Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Storm Gust</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>5.0s</td>
								<td>3.3s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*140%) water M.Dmg to all enemies in the target area and knocking them back. Has a 60% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>5.0s</td>
								<td>3.9s</td>
								<td>74</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*180%) water M.Dmg to all enemies in the target area and knocking them back. Has a 55% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>5.0s</td>
								<td>4.4s</td>
								<td>78</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*220%) water M.Dmg to all enemies in the target area and knocking them back. Has a 50% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>5.0s</td>
								<td>5.0s</td>
								<td>82</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*260%) water M.Dmg to all enemies in the target area and knocking them back. Has a 45% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>5.0s</td>
								<td>5.6s</td>
								<td>86</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*300%) water M.Dmg to all enemies in the target area and knocking them back. Has a 40% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>5.0s</td>
								<td>6.1s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*340%) water M.Dmg to all enemies in the target area and knocking them back. Has a 35% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>5.0s</td>
								<td>6.7s</td>
								<td>94</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*380%) water M.Dmg to all enemies in the target area and knocking them back. Has a 30% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>5.0s</td>
								<td>7.2s</td>
								<td>98</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*420%) water M.Dmg to all enemies in the target area and knocking them back. Has a 25% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>5.0s</td>
								<td>7.8s</td>
								<td>102</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*460%) water M.Dmg to all enemies in the target area and knocking them back. Has a 20% chance to freeze them for 7 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>5.0s</td>
								<td>8.4s</td>
								<td>106</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*500%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>4.9s</td>
								<td>8.4s</td>
								<td>110</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*525%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>4.8s</td>
								<td>8.4s</td>
								<td>114</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*550%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>4.7s</td>
								<td>8.4s</td>
								<td>118</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*575%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>4.6s</td>
								<td>8.4s</td>
								<td>122</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*600%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>4.5s</td>
								<td>8.4s</td>
								<td>126</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*625%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>4.4s</td>
								<td>8.4s</td>
								<td>130</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*650%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>4.3s</td>
								<td>8.4s</td>
								<td>134</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*675%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>4.2s</td>
								<td>8.4s</td>
								<td>138</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*700%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>4.1s</td>
								<td>8.4s</td>
								<td>142</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*725%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>4.0s</td>
								<td>8.4s</td>
								<td>146</td>
								<td class="skill-description" colspan="3">Creates blizzard, dealing (M.Atk*750%) water M.Dmg to all enemies in the target area and knocking them back. Has a 15% chance to freeze them for 7 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="jupitel-thunder">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-jupitel-thunder.png') }}" alt="Jupitel Thunder Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Jupitel Thunder</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.1s</td>
								<td>2.4s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk480%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.1s</td>
								<td>2.8s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk560%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.1s</td>
								<td>3.3s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk640%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.1s</td>
								<td>3.8s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk720%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.1s</td>
								<td>4.3s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk800%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>0.1s</td>
								<td>4.8s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk880%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>0.1s</td>
								<td>5.2s</td>
								<td>72</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk960%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>0.1s</td>
								<td>5.7s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk1040%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>0.1s</td>
								<td>6.2s</td>
								<td>88</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk1120%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>0.1s</td>
								<td>6.7s</td>
								<td>96</td>
								<td class="skill-description" colspan="3">Attacks the target with ligtning balls, delaing (M.Atk1200%) wind M.Dmg and knocking it back for 2m (Knock back is invalid in PVP)</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="lord-of-vermilion">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-lord-of-vermilion.png') }}" alt="Lord of Vermilion Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Lord of Vermilion</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires <a href="#jupitel-thunder">Jupitel Thunder</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>5.0s</td>
								<td>7.2s</td>
								<td>65</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*120%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 4% chance to blind them. The skill takes effects for up to 1 times</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>5.0s</td>
								<td>6.9s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*140%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 8% chance to blind them. The skill takes effects for up to 1 times</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>5.0s</td>
								<td>6.7s</td>
								<td>75</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*160%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 12% chance to blind them. The skill takes effects for up to 2 times</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>5.0s</td>
								<td>6.4s</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*180%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 16% chance to blind them. The skill takes effects for up to 2 times</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>5.0s</td>
								<td>6.2s</td>
								<td>85</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*200%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 20% chance to blind them. The skill takes effects for up to 2 times</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>5.0s</td>
								<td>6.0s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*220%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 24% chance to blind them. The skill takes effects for up to 3 times</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>5.0s</td>
								<td>5.7s</td>
								<td>95</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*240%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 28% chance to blind them. The skill takes effects for up to 3 times</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>5.0s</td>
								<td>5.5s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*260%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 32% chance to blind them. The skill takes effects for up to 3 times</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>5.0s</td>
								<td>5.2s</td>
								<td>105</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*280%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 36% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>5.0s</td>
								<td>5.0s</td>
								<td>110</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*300%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 40% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>4.9s</td>
								<td>5.0s</td>
								<td>115</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*350%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 44% chance to blind them. The skill takes effects for up to 4 timess</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>4.8s</td>
								<td>5.0s</td>
								<td>120</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*400%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 48% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>4.7s</td>
								<td>5.0s</td>
								<td>125</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*450%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 52% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>4.6s</td>
								<td>5.0s</td>
								<td>130</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*500%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 56% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>4.5s</td>
								<td>5.0s</td>
								<td>135</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*550%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 60% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>4.4s</td>
								<td>5.0s</td>
								<td>140</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*600%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 64% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>4.3s</td>
								<td>5.0s</td>
								<td>145</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*650%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 68% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>4.2s</td>
								<td>5.0s</td>
								<td>150</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*700%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 72% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>4.1s</td>
								<td>5.0s</td>
								<td>155</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*750%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 76% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>4.0s</td>
								<td>5.0s</td>
								<td>160</td>
								<td class="skill-description" colspan="3">Channels lightning strikes, dealing (M.Atk*800%) wind M.Dmg to all enemies in the target area and knocking them back. Has a 80% chance to blind them. The skill takes effects for up to 4 times</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="heavens-drive">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/wizard-heavens-drive.png') }}" alt="Heaven's Drive Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Heaven's Drive</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires <a href="#jupitel-thunder">Jupitel Thunder</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>0.9s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk350%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.2s</td>
								<td>1.9s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk400%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.4s</td>
								<td>2.8s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk450%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.6s</td>
								<td>3.8s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk500%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.8s</td>
								<td>4.8s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk550%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>5.7s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk600%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.2s</td>
								<td>6.7s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk650%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.4s</td>
								<td>7.6s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk700%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.6s</td>
								<td>8.6s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk750%) earth M.Dmg to units in it.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.8s</td>
								<td>9.6s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk800%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.7s</td>
								<td>9.5s</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk870%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.6s</td>
								<td>9.5s</td>
								<td>94</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk940%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.5s</td>
								<td>9.4s</td>
								<td>98</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1010%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.4s</td>
								<td>9.4s</td>
								<td>102</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1080%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.3s</td>
								<td>9.3s</td>
								<td>106</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1150%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>2.2s</td>
								<td>9.3s</td>
								<td>110</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1220%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>2.1s</td>
								<td>9.2s</td>
								<td>114</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1290%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>2.0s</td>
								<td>9.2s</td>
								<td>118</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1360%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.9s</td>
								<td>9.1s</td>
								<td>122</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1430%) earth M.Dmg to units in it.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.8s</td>
								<td>9.1s</td>
								<td>126</td>
								<td class="skill-description" colspan="3">Raises the ground of the target area, dealing (M.Atk1500%) earth M.Dmg to units in it.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection