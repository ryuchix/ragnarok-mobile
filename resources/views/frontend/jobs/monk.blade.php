@extends('layouts.page')

@section('title', 'Monk Class, Monk Skill, Monk Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Monk Class, Monk Skill, Monk Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('monk'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/monk-min.png') }}" alt="Monk" width="250px" class="pull-left">
				<div class="jobs">
					<h1>Monk</h1>
					<span>Acolyte 2nd Job</span><br>

					<p>It is said that the faith of the acolyte is not only for the mind, but the path one walks must also be for the body.</p>
					<p>With combo and separate attacks, the mysterious power of one must not only be physically strong, but also have great strength and tough blow/hits.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/monk-min.png') }}" alt="Monk" width="300px"></div>
				<div class="jobs">
					<h1>Monk</h1>
					<span>Acolyte 2nd Job</span><br>

					<p>It is said that the faith of the acolyte is not only for the mind, but the path one walks must also be for the body.</p>
					<p>With combo and separate attacks, the mysterious power of one must not only be physically strong, but also have great strength and tough blow/hits.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="call-spirits">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-call-spirits.jpg') }}" alt="Call Spirits Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Call Spirits</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Time: 1.0s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Surrounds the caster with spirit bombs, each increasing Atk by 10 and ignoring 2% Def of enemy unit. Could have up to 1 bombs. Lasts 300 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Surrounds the caster with spirit bombs, each increasing Atk by 10 and ignoring 2% Def of enemy unit. Could have up to 2 bombs. Lasts 300 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Surrounds the caster with spirit bombs, each increasing Atk by 10 and ignoring 2% Def of enemy unit. Could have up to 3 bombs. Lasts 300 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Surrounds the caster with spirit bombs, each increasing Atk by 10 and ignoring 2% Def of enemy unit. Could have up to 4 bombs. Lasts 300 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Surrounds the caster with spirit bombs, each increasing Atk by 10 and ignoring 2% Def of enemy unit. Could have up to 5 bombs. Lasts 300 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="zen">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-zen.jpg') }}" alt="Zen Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Zen</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Time: 2.0s</span><br>
									<span class="skill-range red">Requires <a href="#call-spirits">Call Spirits</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Gathers spirit bombs to the max (up to 5 at a time)</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="iron-fist">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-iron-fist.jpg') }}" alt="Iron Fist Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Iron Fist</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 4, Auto Attack increases by 20</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 8, Auto Attack increases by 40</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 12, Auto Attack increases by 60</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 16, Auto Attack increases by 80</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 20, Auto Attack increases by 100</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 24, Auto Attack increases by 120</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 28, Auto Attack increases by 140</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 32, Auto Attack increases by 160</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 36, Auto Attack increases by 180</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using Kunckles or fighting without weapon, Atk increases by 40, Auto Attack increases by 200</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="critical-explosion">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-critical-explosion.jpg') }}" alt="Critical Explosion Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Critical Explosion</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Special Cost: <a href="#call-spirits">Spirit Bomb</a> x5</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Ignite spirit bombs around, caster enters 180s <a href="#critical-explosion">Critical Explosion</a>; Crit increases by 10, every spirit bomb of <a href="#call-spirits">Call Spirits</a> will extra increase its Atk by 20%, but HP and SP will not be naturally recovered. During <a href="#critical-explosion">Critical Explosion</a>, Monk's Auto Attack will increase by (Intelligence x5)</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Ignite spirit bombs around, caster enters 180s <a href="#critical-explosion">Critical Explosion</a>; Crit increases by 12.5, every spirit bomb of <a href="#call-spirits">Call Spirits</a> will extra increase its Atk by 40%, but HP and SP will not be naturally recovered. During <a href="#critical-explosion">Critical Explosion</a>, Monk's Auto Attack will increase by (Intelligence x5)</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Ignite spirit bombs around, caster enters 180s <a href="#critical-explosion">Critical Explosion</a>; Crit increases by 15, every spirit bomb of <a href="#call-spirits">Call Spirits</a> will extra increase its Atk by 60%, but HP and SP will not be naturally recovered. During <a href="#critical-explosion">Critical Explosion</a>, Monk's Auto Attack will increase by (Intelligence x5)</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Ignite spirit bombs around, caster enters 180s <a href="#critical-explosion">Critical Explosion</a>; Crit increases by 17.5, every spirit bomb of <a href="#call-spirits">Call Spirits</a> will extra increase its Atk by 80%, but HP and SP will not be naturally recovered. During <a href="#critical-explosion">Critical Explosion</a>, Monk's Auto Attack will increase by (Intelligence x5)</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Ignite spirit bombs around, caster enters 180s <a href="#critical-explosion">Critical Explosion</a>; Crit increases by 20, every spirit bomb of <a href="#call-spirits">Call Spirits</a> will extra increase its Atk by 100%, but HP and SP will not be naturally recovered. During <a href="#critical-explosion">Critical Explosion</a>, Monk's Auto Attack will increase by (Intelligence x5)</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="asura-strike">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-asura-strike.jpg') }}" alt="Asura Strike Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Asura Strike</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 3.0m</span><br>
									<span class="skill-range red">Requires <a href="#critical-explosion">Critical Explosion</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>4.0s</td>
								<td>4.0s</td>
								<td>ALL</td>
								<td>10.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(4+Remaining SP/100) + 2500 + 500x5) lane Dmg to the target. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.5s</td>
								<td>3.5s</td>
								<td>ALL</td>
								<td>10.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(8+Remaining SP/100) + 2500 + 500x10) lane Dmg to the target. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>ALL</td>
								<td>10.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(12+Remaining SP/100) + 2500 + 500x15) lane Dmg to the target. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.5s</td>
								<td>2.5s</td>
								<td>ALL</td>
								<td>10.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(16+Remaining SP/100) + 2500 + 500x20) lane Dmg to the target. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>2.0s</td>
								<td>ALL</td>
								<td>10.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(20+Remaining SP/100) + 2500 + 500x25) lane Dmg to the target. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>1.5s</td>
								<td>2.0s</td>
								<td>ALL</td>
								<td>9.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(24+Remaining SP/100) + 2500 + 500x30) lane Dmg to the target. Ignores 6% of target's race reduction, attribute reduction, Dmg reduction and Dmg reduction skill. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>1.5s</td>
								<td>2.0s</td>
								<td>ALL</td>
								<td>8.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(28+Remaining SP/100) + 2500 + 500x35) lane Dmg to the target. Ignores 12% of target's race reduction, attribute reduction, Dmg reduction and Dmg reduction skill. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>1.5s</td>
								<td>2.0s</td>
								<td>ALL</td>
								<td>7.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(32+Remaining SP/100) + 2500 + 500x40) lane Dmg to the target. Ignores 18% of target's race reduction, attribute reduction, Dmg reduction and Dmg reduction skill. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>1.5s</td>
								<td>2.0s</td>
								<td>ALL</td>
								<td>6.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(36+Remaining SP/100) + 2500 + 500x45) lane Dmg to the target. Ignores 24% of target's race reduction, attribute reduction, Dmg reduction and Dmg reduction skill. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>1.5s</td>
								<td>2.0s</td>
								<td>ALL</td>
								<td>5.0s</td>
								<td class="skill-description" colspan="3">Spends all SP to deal (Atk*(40+Remaining SP/100) + 2500 + 500x50) lane Dmg to the target. Ignores 30% of target's race reduction, attribute reduction, Dmg reduction and Dmg reduction skill. It can only be used when the user is in <a href="#critical-explosion">Critical Explosion</a> status. When it is used, the caster cannot restore SP for 10 sec and gain no naturally restored SP for 300 sec. Bomb cost is decided by the previous skill casted: Direct Release: Bomb Cost x5, Combo Finish/Blade Stop: Bomb Cost x3, Tiger Knuckle Fist: Bomb Cost x3, Chain Crush Combo: Bomb Cost x1</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="triple-attack">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-triple-attack.jpg') }}" alt="Triple Attack Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Triple Attack</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 120% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 140% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 160% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 180% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 200% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 220% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 240% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 260% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 280% Dmg when launching auto attacks</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 300% Dmg when launching auto attacks</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 310% Dmg and silence enemy for 0.5 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 320% Dmg and silence enemy for 1.0 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 330% Dmg and silence enemy for 1.5 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 340% Dmg and silence enemy for 2.0 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 350% Dmg and silence enemy for 2.5 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 360% Dmg and silence enemy for 3.0 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 370% Dmg and silence enemy for 3.5 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 380% Dmg and silence enemy for 4.0 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 390% Dmg and silence enemy for 4.5 seconds when launching auto attacks.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Allows Monk to have a 20% chance to deal 400% Dmg and silence enemy for 5.0 seconds when launching auto attacks.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="chain-combo">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-chain-combo.jpg') }}" alt="Chain Combo Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Chain Combo</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span><br>
									<span class="skill-range red">Requires <a href="#triple-attack">Triple Attack</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">The skill must be casted after <a href="#triple-attack">Triple Attack</a>. It deals 400% Dmg to the target. Damage dealt by the skill is slightly bossted by VIT</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.4s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">The skill must be casted after <a href="#triple-attack">Triple Attack</a>. It deals 480% Dmg to the target. Damage dealt by the skill is slightly bossted by VIT</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.3s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">The skill must be casted after <a href="#triple-attack">Triple Attack</a>. It deals 560% Dmg to the target. Damage dealt by the skill is slightly bossted by VIT</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.2s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">The skill must be casted after <a href="#triple-attack">Triple Attack</a>. It deals 640% Dmg to the target. Damage dealt by the skill is slightly bossted by VIT</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.1s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">The skill must be casted after <a href="#triple-attack">Triple Attack</a>. It deals 720% Dmg to the target. Damage dealt by the skill is slightly bossted by VIT</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="combo-finish">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-combo-finish.jpg') }}" alt="Combo Finish Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Combo Finish</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span><br>
									<span class="skill-range red">Special Cost: <a href="#call-spirits">Spirit Bomb</a> x1 And <a href="#chain-combo">Chain Combo</a> x1</span><br>
									<span class="skill-range red">Requires <a href="#chain-combo">Chain Combo</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 560% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.4s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 640% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.3s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 720% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.2s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 800% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.1s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 880% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>1.0s</td>
								<td>37</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 1000	% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>1.0s</td>
								<td>39</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 1120% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>1.0s</td>
								<td>41</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 1240% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 1360% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>1.0s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="#chain-combo">Chain Combo</a>, this skill deals 1480% Dmg to a single enemy. When <a href="#combo-finish">Combo Finish</a> is put in Auto-skill Bar, <a href="#chain-combo">Chain Combo</a> and <a href="#combo-finish">Combo Finish</a> will automatically be released after triggering <a href="#triple-attack">Triple Attack</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="flee">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-flee.jpg') }}" alt="Flee Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Flee</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 1%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 3%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 5%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 7%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 9%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 11%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 13%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 15%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 17%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 2% chance to recover 5% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 4% chance to recover 10% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 6% chance to recover 15% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 8% chance to recover 20% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 10% chance to recover 25% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 12% chance to recover 30% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 14% chance to recover 35% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 16% chance to recover 40% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 18% chance to recover 45% HP from the magical damage received.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Increases monk Flee by 19%. There is a 20% chance to recover 50% HP from the magical damage received.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="steel-body">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-steel-body.jpg') }}" alt="Steel Body Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Steel Body</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Cast Time: 5.0s</span><br>
									<span class="skill-range red">Requires <a href="#flee">Flee</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>200</td>
								<td class="skill-description" colspan="3">Enters Steel Body status for 30 sec, taking 85% less damage but losing 45% ASPD. Move Spd drops to 75% of the original value and cannot get any speed boost. Cannot use skills proactively in the status. Skills triggered by auto attacks still work</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>200</td>
								<td class="skill-description" colspan="3">Enters Steel Body status for 60 sec, taking 85% less damage but losing 40% ASPD. Move Spd drops to 75% of the original value and cannot get any speed boost. Cannot use skills proactively in the status. Skills triggered by auto attacks still work</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>200</td>
								<td class="skill-description" colspan="3">Enters Steel Body status for 90 sec, taking 85% less damage but losing 35% ASPD. Move Spd drops to 75% of the original value and cannot get any speed boost. Cannot use skills proactively in the status. Skills triggered by auto attacks still work</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>200</td>
								<td class="skill-description" colspan="3">Enters Steel Body status for 120 sec, taking 85% less damage but losing 30% ASPD. Move Spd drops to 75% of the original value and cannot get any speed boost. Cannot use skills proactively in the status. Skills triggered by auto attacks still work</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>200</td>
								<td class="skill-description" colspan="3">Enters Steel Body status for 150 sec, taking 85% less damage but losing 25% ASPD. Move Spd drops to 75% of the original value and cannot get any speed boost. Cannot use skills proactively in the status. Skills triggered by auto attacks still work</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="investigate">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-investigate.jpg') }}" alt="Investigate Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Investigate</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span><br>
									<span class="skill-range">Cast Time: 1.0s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>CD</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*1 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*2 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*3 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*4 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*5 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>3.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*5.4 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>3.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*5.8 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>3.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*6.2 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>3.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*6.6 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>3.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Injects spirit bombs into an enemy, dealing Atk*(2 + 1.5*7.0 + (Def/120)) lane Dmg. Ignores Flee and defense. Enemies with high Def suffers high Dmg</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="spirits-recovery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-spirits-recovery.jpg') }}" alt="Spirits Recovery Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Spirits Recovery</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Monk restore HP: (Max HP/500 +4)*1 and SP: (Max SP/500 +4)*1 every 10 sec. When caster is in <a href="#critical-explosion">Critical Explosion</a> or the 300s penalty time of <a href="#asura-strike">Asura Strike</a>, the restoration is still effective.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Monk restore HP: (Max HP/500 +4)*2 and SP: (Max SP/500 +4)*2 every 10 sec. When caster is in <a href="#critical-explosion">Critical Explosion</a> or the 300s penalty time of <a href="#asura-strike">Asura Strike</a>, the restoration is still effective.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Monk restore HP: (Max HP/500 +4)*3 and SP: (Max SP/500 +4)*3 every 10 sec. When caster is in <a href="#critical-explosion">Critical Explosion</a> or the 300s penalty time of <a href="#asura-strike">Asura Strike</a>, the restoration is still effective.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Monk restore HP: (Max HP/500 +4)*4 and SP: (Max SP/500 +4)*4 every 10 sec. When caster is in <a href="#critical-explosion">Critical Explosion</a> or the 300s penalty time of <a href="#asura-strike">Asura Strike</a>, the restoration is still effective.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Monk restore HP: (Max HP/500 +4)*5 and SP: (Max SP/500 +4)*5 every 10 sec. When caster is in <a href="#critical-explosion">Critical Explosion</a> or the 300s penalty time of <a href="#asura-strike">Asura Strike</a>, the restoration is still effective.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="body-relocation">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/monk-body-relocation.jpg') }}" alt="Body Relocation Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Body Relocation</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Requires <a href="#spirits-recovery">Spirits Recovery</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>CD</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Monk teleports forward. Body Relocation doesn't consume <a href="#call-spirits">Spirit Bomb</a> while <a href="#critical-explosion">Critical Explosion</a> is active. Consumes 1 <a href="#call-spirits">Spirit Bomb</a> normally. This skill will put <a href="#asura-strike">Asura Strike</a> into a 2-second Cast Delay</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection