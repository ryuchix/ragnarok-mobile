@extends('layouts.page')

@section('title', 'Champion Class, Champion Skill, Champion Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Champion Class, Champion Skill, Champion Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('champion'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/champion-min.png') }}" alt="Champion" width="250px" class="pull-left">
				<div class="jobs">
					<h1>Champion</h1>
					<span>Acolyte 2nd Transcendent Job</span><br>

					<p>Eventually, the Monk realizes the limits of the human mind and body... The force in his punches is feared, but his spirit is unsatisfied: His mind, body and spirit are not yet in perfect harmony. Through intense dedication and rigorous training, the Monk can finally tap into the total strength of his entire being as a Champion of faith.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/champion-min.png') }}" alt="Champion" width="300px"></div>
				<div class="jobs">
					<h1>Champion</h1>
					<span>Acolyte 2nd Transcendent Job</span><br>

					<p>Eventually, the Monk realizes the limits of the human mind and body... The force in his punches is feared, but his spirit is unsatisfied: His mind, body and spirit are not yet in perfect harmony. Through intense dedication and rigorous training, the Monk can finally tap into the total strength of his entire being as a Champion of faith.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="spirit-bomb-refining">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-spirit-bomb-refining.png') }}" alt="Spirit Bomb Refining Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Spirit Bomb Refining</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 1 Atk, 0.5% Def Penetration and 1% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 1 Atk, 0.5% Def Penetration and 2% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 1 Atk, 0.5% Def Penetration and 3% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 1 Atk, 0.5% Def Penetration and 4% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 1 Atk, 0.5% Def Penetration and 5% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 2 Atk, 1% Def Penetration and 6% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 2 Atk, 1% Def Penetration and 7% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 2 Atk, 1% Def Penetration and 8% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 2 Atk, 1% Def Penetration and 9% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Each spirit bomb grants the carrier with 2 Atk, 1% Def Penetration and 10% Atk boost for each spirit bomb of <a href="#finger-offensive">Finger Offensive</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="critical-explosion-break">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-critical-explosion-break.png') }}" alt="Critical Explosion -Break Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Critical Explosion -Break</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires <a href="#spirit-bomb-refining">Spirit Bomb Refining</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 1 STR</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 2 STR</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 3 STR</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 4 STR</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 5 STR</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 6 STR</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 7 STR</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 8 STR</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 9 STR</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">In <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status, Monk gains 10 STR</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="fist-refining">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-fist-refining.png') }}" alt="Fist Refining Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Fist Refining</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Atk +6 more when using Knuckles class weapon or bare-handed</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Atk +12 more when using Knuckles class weapon or bare-handed</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Atk +18 more when using Knuckles class weapon or bare-handed</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Atk +24 more when using Knuckles class weapon or bare-handed</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Atk +30 more when using Knuckles class weapon or bare-handed</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="tiger-knuckle-fist">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-tiger-knuckle-fist.png') }}" alt="Tiger Knuckle Fist Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Tiger Knuckle Fist</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span><br>
									<span class="skill-range red">Requires At least level 1 <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>7</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>, This skill deals 140% Dmg to a single enemy, with a 20% to fix the target for 2 seconds. When <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> is put in Auto-skill Bar, <a href="{{ url('monk#chain-combo') }}">Chain Combo</a>, <a href="{{ url('monk#combo-finish') }}">Combo Finish</a> and <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a>	</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.4s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>, This skill deals 240% Dmg to a single enemy, with a 30% to fix the target for 4 seconds. When <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> is put in Auto-skill Bar, <a href="{{ url('monk#chain-combo') }}">Chain Combo</a>, <a href="{{ url('monk#combo-finish') }}">Combo Finish</a> and <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a>	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.3s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>, This skill deals 340% Dmg to a single enemy, with a 40% to fix the target for 6 seconds. When <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> is put in Auto-skill Bar, <a href="{{ url('monk#chain-combo') }}">Chain Combo</a>, <a href="{{ url('monk#combo-finish') }}">Combo Finish</a> and <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a>	</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.2s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>, This skill deals 440% Dmg to a single enemy, with a 50% to fix the target for 8 seconds. When <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> is put in Auto-skill Bar, <a href="{{ url('monk#chain-combo') }}">Chain Combo</a>, <a href="{{ url('monk#combo-finish') }}">Combo Finish</a> and <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a>	</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.1s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>, This skill deals 540% Dmg to a single enemy, with a 60% to fix the target for 10 seconds. When <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> is put in Auto-skill Bar, <a href="{{ url('monk#chain-combo') }}">Chain Combo</a>, <a href="{{ url('monk#combo-finish') }}">Combo Finish</a> and <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a>	</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="chain-crush-combo">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-chain-crush-combo.png') }}" alt="Chain Crush Combo Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Chain Crush Combo</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('monk#call-spirits') }}">Spirit Bomb</a> x2 And <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> x1</span><br>
									<span class="skill-range red">Requires <a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 500% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 600% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 700% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 800% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>45</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 900% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 1000% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 1100% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 1200% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>65</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 1300% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 1400% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 1600% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 1800% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 2000% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 2200% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.5s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">The target must endure the combo skills after <a href="{{ url('monk#combo-finish') }}">Combo Finish</a>/<a href="#tiger-knuckle-fist">Tiger Knuckle Fist</a>, this skill deals 2400% Dmg to a single enemy. When <a href="#chain-crush-combo">Chain Crush Combo</a> is put in Auto-skill Bar, Chain Combo, Combo Finish, Tiger Knuckle Fist and <a href="#chain-crush-combo">Chain Crush Combo</a> will automatically be released after triggering <a href="{{ url('monk#triple-attack') }}">Triple Attack</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="preemptive-strike">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-preemtive-strike.png') }}" alt="Preemptive Strike Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Preemptive Strike</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 4%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 6%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 8%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 10%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 12%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 14%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 16%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 18%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 20%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Increases ASPD of monk by 22%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="finger-offensive">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-finger-offensive.png') }}" alt="Finger Offensive Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Finger Offensive</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 8.0m</span><br>
									<span class="skill-range">Cast Time: 2.0s</span><br>
									<span class="skill-range red">Requires <a href="{{ url('monk#investigate') }}">Investigate</a> Lv.3*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>CD</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>5.0s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 100% Dmg	</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>5.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 150% Dmg	</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>5.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 200% Dmg	</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>5.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 250% Dmg	</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>5.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 300% Dmg	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>4.6s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 320% Dmg	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>4.2s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 340% Dmg	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>3.8s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 360% Dmg	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>3.4s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 380% Dmg	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>3.0s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Attacks an enemy with all spirit bombs (up to 5) from afar, each dealing 400% Dmg	</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="blade-stop">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-blade-stop.png') }}" alt="Blade Stop Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Blade Stop</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range red">Special Cost: <a href="{{ url('monk#call-spirits') }}">Spirit Bomb</a> x1</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Enters 1.1 sec guidance, During which if the caster takes auto attack, the attacker will enter a 10 sec no-move status with the monk if it is 2m near.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Enters 1.2 sec guidance, During which if the caster takes auto attack, the attacker will enter a 20 sec no-move status with the monk if it is 2m near. As the skill levels up, Monk could use some skills in no-move status: Lv.2 <a href="#finger-offensive">Finger Offensive</a>.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Enters 1.3 sec guidance, During which if the caster takes auto attack, the attacker will enter a 30 sec no-move status with the monk if it is 2m near. As the skill levels up, Monk could use some skills in no-move status: Lv.2 <a href="#finger-offensive">Finger Offensive</a>; Lv.3 <a href="{{ url('monk#investigate') }}">Investigate</a></td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Enters 1.4 sec guidance, During which if the caster takes auto attack, the attacker will enter a 40 sec no-move status with the monk if it is 2m near. As the skill levels up, Monk could use some skills in no-move status: Lv.2 <a href="#finger-offensive">Finger Offensive</a>; Lv.3 <a href="{{ url('monk#investigate') }}">Investigate</a>; Lv.4 <a href="{{ url('monk#chain-combo') }}">Chain Combo</a></td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Enters 1.5 sec guidance, During which if the caster takes auto attack, the attacker will enter a 50 sec no-move status with the monk if it is 2m near. As the skill levels up, Monk could use some skills in no-move status: Lv.2 <a href="#finger-offensive">Finger Offensive</a>; Lv.3 <a href="{{ url('monk#investigate') }}">Investigate</a>; Lv.4 <a href="{{ url('monk#chain-combo') }}">Chain Combo</a> and Lv.5 <a href="{{ url('monk#asura-strike') }}">Asura Strike</a></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="ki-explosion">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-ki-explosion.png') }}" alt="Ki Explosion Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Ki Explosion</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>HP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>11</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Isolates the target, dealing 220% Dmg to an enemy and knocking all targets in the area by 2m. Has a 50% chance to stun the target for 2 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>12</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Isolates the target, dealing 240% Dmg to an enemy and knocking all targets in the area by 2.5m. Has a 55% chance to stun the target for 2 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>13</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Isolates the target, dealing 260% Dmg to an enemy and knocking all targets in the area by 3m. Has a 60% chance to stun the target for 2 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>14</td>
								<td>80</td>
								<td class="skill-description" colspan="3">Isolates the target, dealing 280% Dmg to an enemy and knocking all targets in the area by 3.5m. Has a 65% chance to stun the target for 2 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>15</td>
								<td>90</td>
								<td class="skill-description" colspan="3">Isolates the target, dealing 300% Dmg to an enemy and knocking all targets in the area by 4m. Has a 70% chance to stun the target for 2 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="palm-push-strike">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/champion-palm-push-strike.png') }}" alt="Palm Push Strike Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Palm Push Strike</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Knocks back the target for 3m, dealing 300% Dmg. Can only be casted in the <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status. Could knock back units in the Dark Barrier</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Knocks back the target for 3m, dealing 400% Dmg. Can only be casted in the <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status. Could knock back units in the Dark Barrier</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Knocks back the target for 3m, dealing 500% Dmg. Can only be casted in the <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status. Could knock back units in the Dark Barrier</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back the target for 3m, dealing 600% Dmg. Can only be casted in the <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status. Could knock back units in the Dark Barrier</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Knocks back the target for 3m, dealing 700% Dmg. Can only be casted in the <a href="{{ url('monk#critical-explosion') }}">Critical Explosion</a> status. Could knock back units in the Dark Barrier</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection