@extends('layouts.page')

@section('title', 'Whitesmith Class, Whitesmith Skill, Whitesmith Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Whitesmith Class, Whitesmith Skill, Whitesmith Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('whitesmith'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/whitesmith-min.png') }}" alt="Whitesmith" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Whitesmith</h1>
					<span>Merchant's 2nd Transcendent Job</span><br>

					<p>When the Blacksmith transcends the limitations of working with Steel, the realization that any material existing in the world can be used to create valuable items naturally follows. It is this level of artistry in creating weapons that defines the Whitesmith. The Whitesmith also specializes in manipulating fire and using Mace and Ax weapons in battle.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/whitesmith-min.png') }}" alt="Whitesmith" width="300px">
				<div class="jobs">
					<h1>Whitesmith</h1>
					<span>Merchant's 2nd Transcendent Job</span><br>

					<p>When the Blacksmith transcends the limitations of working with Steel, the realization that any material existing in the world can be used to create valuable items naturally follows. It is this level of artistry in creating weapons that defines the Whitesmith. The Whitesmith also specializes in manipulating fire and using Mace and Ax weapons in battle.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="cart-boost">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-cart-boost.png') }}" alt="Cart Boost Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Cart Boost</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Increases Move Spd by 30% for 90 sec. Removes Decrease Agility status and cannot be expelled or removed by Marsh Pond. Cannot stack with other speed up status. Carts are required.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="cart-termination">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-cart-termination.png') }}" alt="Cart Termination Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Cart Termination</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 3.0m</span><br>
									<span class="skill-state red">Requires Cart Boost Lvl.1 *</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 600 Zeny to deal 450% lane Dmg to the target. Has a 5% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 700 Zeny to deal 450% lane Dmg to the target. Has a 10% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 800 Zeny to deal 450% lane Dmg to the target. Has a 15% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 900 Zeny to deal 450% lane Dmg to the target. Has a 20% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 1000 Zeny to deal 450% lane Dmg to the target. Has a 25% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 1100 Zeny to deal 450% lane Dmg to the target. Has a 30% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 1200 Zeny to deal 450% lane Dmg to the target. Has a 35% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 1300 Zeny to deal 450% lane Dmg to the target. Has a 40% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 1400 Zeny to deal 450% lane Dmg to the target. Has a 45% chance to stun it.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">When Cart Boost is on, Spends 1500 Zeny to deal 450% lane Dmg to the target. Has a 50% chance to stun it.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="overthrust-max">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-overthrust-max.png') }}" alt="Overthrust Max Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Overthrust Max</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Cost 3000 Zeny to increase your own Dmg by 10%; the effect lasts 180 seconds.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Cost 3500 Zeny to increase your own Dmg by 20%; the effect lasts 180 seconds.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Cost 4000 Zeny to increase your own Dmg by 30%; the effect lasts 180 seconds.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Cost 4500 Zeny to increase your own Dmg by 40%; the effect lasts 180 seconds.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Cost 5000 Zeny to increase your own Dmg by 50%; the effect lasts 180 seconds.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="repair-weapon">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-repair-weapon.png') }}" alt="Repair Weapon Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Repair Weapon</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 5.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>1.5s</td>
								<td class="skill-description" colspan="3">Use it to repair all of an ally's equipment affected by [Weapon Break], [Armor Break], [Shield Break], [Helmet Break].</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="meltdown">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-meltdown.png') }}" alt="Meltdown Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Meltdown</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 15 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 15 and 15 respectively. Player gas a 1% chance to destroy target's weapon, a 0.7% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 20 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 30 and 30 respectively. Player gas a 2% chance to destroy target's weapon, a 1.4% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 25 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 45 and 45 respectively. Player gas a 3% chance to destroy target's weapon, a 2.1% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 30 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 60 and 60 respectively. Player gas a 4% chance to destroy target's weapon, a 2.8% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 35 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 75 and 75 respectively. Player gas a 5% chance to destroy target's weapon, a 3.5% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 40 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 90 and 90 respectively. Player gas a 6% chance to destroy target's weapon, a 4.2% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 45 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 105 and 105 respectively. Player gas a 7% chance to destroy target's weapon, a 4.9% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 50 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 120 and 120 respectively. Player gas a 8% chance to destroy target's weapon, a 5.6% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 55 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 135 and 135 respectively. Player gas a 9% chance to destroy target's weapon, a 6.3% chance to destory their armor.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 60 seconds of Meltdown, when attacking monsters. Enemy unit's Def and Atk will reduce by 150 and 150 respectively. Player gas a 10% chance to destroy target's weapon, a 7.0% chance to destory their armor.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 65 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 165 and 165 respectively. Player has a 11% chance to destroy target's weapon, a 7.7% chance to destroy their armor. Self Auto Attack damage increases by 3%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 70 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 180 and 180 respectively. Player has a 12% chance to destroy target's weapon, a 8.4% chance to destroy their armor. Self Auto Attack damage increases by 6%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 75 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 195 and 195 respectively. Player has a 13% chance to destroy target's weapon, a 9.1% chance to destroy their armor. Self Auto Attack damage increases by 9%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 80 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 210 and 210 respectively. Player has a 14% chance to destroy target's weapon, a 9.8% chance to destroy their armor. Self Auto Attack damage increases by 12%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 85 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 225 and 225 respectively. Player has a 15% chance to destroy target's weapon, a 10.5% chance to destroy their armor. Self Auto Attack damage increases by 15%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 90 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 240 and 240 respectively. Player has a 16% chance to destroy target's weapon, a 11.2% chance to destroy their armor. Self Auto Attack damage increases by 18%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 95 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 255 and 255 respectively. Player has a 17% chance to destroy target's weapon, a 11.9% chance to destroy their armor. Self Auto Attack damage increases by 21%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 100 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 270 and 270 respectively. Player has a 18% chance to destroy target's weapon, a 12.6% chance to destroy their armor. Self Auto Attack damage increases by 24%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 105 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 285 and 285 respectively. Player has a 19% chance to destroy target's weapon, a 13.3% chance to destroy their armor. Self Auto Attack damage increases by 27%</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Enter 110 seconds of Meltdown, when attacking monsters, Enemy unit's Def and Atk will be reduced by 300 and 300 respectively. Player has a 20% chance to destroy target's weapon, a 14.0% chance to destroy their armor. Self Auto Attack damage increases by 30%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="side-split">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-side-split.png') }}" alt="Side Split Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Side Split</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 230% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 260% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 290% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 320% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 350% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 380% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>33</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 410% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 440% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>39</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 470% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Knocks back nearby enemies while swinging the ace, dealing 500% Dmg to nearby enemies Axe type weapons must be equipped.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="humans-heart-light">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-human-heart.png') }}" alt="Human's Heart Light Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Human's Heart Light</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 3%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 6%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 9%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 12%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 15%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 16%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 17%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 18%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 19%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 20%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 21%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 22%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 23%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 24%. The effect is reduced by 2/3 in PVP</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Reduces damage taken from Demi-Human monster by 25%. The effect is reduced by 2/3 in PVP</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="elite-alloy-craft">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-elite-alloy-craft.png') }}" alt="Elite Alloy Craft Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Elite Alloy Craft</span><br>
									<span class="skill-state">Active Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 10.0% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 11.3% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 12.5% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 13.8% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 15.0% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 16.3% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 17.5% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 18.8% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 20.0% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Whitesmith learns to make alloys that could assist yoou in battle. Skill level affects 21.3% success rate, increasing the chance to make advanced alloy by 15%. The final success rate is also affected by Dex and Luk. After learning it, has a chance to get Fuel x1 when picking up items.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="manufacturing-master">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-manufacturing-master.png') }}" alt="Manufacturing Master Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Manufacturing Master</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 5%.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 10%.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 15%.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 20%.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 25%.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 30%.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 35%.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 40%.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 45%.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Whitesmith Dex and Luk's effect to refine success rate increases by 50%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="greed">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-greed.png') }}" alt="Greed Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Greed</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 2% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 4% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 6% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 7% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 10% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 12% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 14% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 16% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 18% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When merchants get materials from wild monsters, has a 20% chance to get 1 more share (Invalid to drops and MVP and mini).</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="trading-master">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/whitesmith-trading-master.png') }}" alt="Trading Master Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Trading Master</span><br>
									<span class="skill-state">Active Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Adds item class cap for the exchange by 1. Use the skill to open the Exchange</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Adds item class cap for the exchange by 2. Use the skill to open the Exchange</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Adds item class cap for the exchange by 3. Use the skill to open the Exchange</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Adds item class cap for the exchange by 4. Use the skill to open the Exchange</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Adds item class cap for the exchange by 5. Use the skill to open the Exchange</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection