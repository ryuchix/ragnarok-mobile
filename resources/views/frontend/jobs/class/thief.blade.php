				<table class="jobs">
					<tr style="">
						<td>
							<a href="{{ url('thief') }}" title="thief">
								<img src="{{ url('frontend/img/jobs/class/thief-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<tr class="class-name">
							<td>
								<span>Thief</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('assassin') }}" title="assassin">
								<img src="{{ url('frontend/img/jobs/class/assassin-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('assassin-cross') }}" title="assassin cross">
								<img src="{{ url('frontend/img/jobs/class/assassin_crosss-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/guillotine_cross-min.png') }}" alt="" width="100px">
							
						</td>
						<tr class="class-name">
							<td>
								<span>Assassin</span>
							</td>
							<td>
								<span>Assassin Cross</span>
							</td>
							<td style="display: none">
								<span>Guillotine Cross</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('rogue') }}" title="rogue">
								<img src="{{ url('frontend/img/jobs/class/rougue-min.png') }}" alt="" width="85px">
							</a>
						</td>
						<td>
							<a href="{{ url('stalker') }}" title="stalker">
								<img src="{{ url('frontend/img/jobs/class/stalker-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/shadow_chaser-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Rouge</span>
							</td>
							<td>
								<span>Stalker</span>
							</td>
							<td style="display: none">
								<span>Shadow Chaser</span>
							</td>
						</tr>
					</tr>
				</table>