				<table class="jobs">
					<tr style="">
						<td>
							<a href="{{ url('swordsman') }}" title="swordsman">
								<img src="{{ url('frontend/img/jobs/class/swordsman-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<tr class="class-name">
							<td>
								<span>Swordsman</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('knight') }}" title="knight">
								<img src="{{ url('frontend/img/jobs/class/knight-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('lord-knight') }}" title="Lord knight">
								<img src="{{ url('frontend/img/jobs/class/lord_knight-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/runeknight-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Knight</span>
							</td>
							<td>
								<span>Lord Knight</span>
							</td>
							<td style="display: none">
								<span>Rune Knight</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('crusader') }}" title="crusader">
								<img src="{{ url('frontend/img/jobs/class/crusaders-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('paladin') }}" title="paladin">
								<img src="{{ url('frontend/img/jobs/class/paladin-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/royal guard-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Crusader</span>
							</td>
							<td>
								<span>Paladin</span>
							</td>
							<td style="display: none">
								<span>Royal Guard</span>
							</td>
						</tr>
					</tr>
				</table>