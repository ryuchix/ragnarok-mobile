				<table class="jobs">
					<tr style="">
						<td>
							<a href="{{ url('archer') }}" title="archer">
								<img src="{{ url('frontend/img/jobs/class/archer-min.png') }}" alt="" width="90px">
							</a>
						</td>
						<tr class="class-name">
							<td>
								<span>Archer</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('hunter') }}" title="hunter">
								<img src="{{ url('frontend/img/jobs/class/hunter-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('sniper') }}" title="sniper">
								<img src="{{ url('frontend/img/jobs/class/sniper-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/ranger-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Hunter</span>
							</td>
							<td>
								<span>Sniper</span>
							</td>
							<td style="display: none">
								<span>Ranger</span>
							</td>
						</tr>
					</tr>
				</table>