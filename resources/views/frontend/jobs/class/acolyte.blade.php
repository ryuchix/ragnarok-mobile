				<table class="jobs">
					<tr style="">
						<td>
							<a href="{{ url('acolyte') }}" title="acolyte">
								<img src="{{ url('frontend/img/jobs/class/acolyte-min.png') }}" alt="" width="80px">
							</a>
						</td>
						<tr class="class-name">
							<td>
								<span>Acolyte</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('priest') }}" title="priest">
								<img src="{{ url('frontend/img/jobs/class/priest-min.png') }}" alt="" width="80px">
							</a>
						</td>
						<td>
							<a href="{{ url('high-priest') }}" title="High Priest">
								<img src="{{ url('frontend/img/jobs/class/high_priest-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/arcbishop-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Priest</span>
							</td>
							<td>
								<span>High Priest</span>
							</td>
							<td style="display: none">
								<span>Arc Bishop</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('monk') }}" title="monk">
								<img src="{{ url('frontend/img/jobs/class/monk-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('champion') }}" title="champion">
								<img src="{{ url('frontend/img/jobs/class/champion-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/sura-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Monk</span>
							</td>
							<td>
								<span>Champion</span>
							</td>
							<td style="display: none">
								<span>Sura</span>
							</td>
						</tr>
					</tr>
				</table>