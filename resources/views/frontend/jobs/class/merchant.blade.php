				<table class="jobs">
					<tr style="">
						<td>
							<a href="{{ url('merchant') }}" title="merchant">
								<img src="{{ url('frontend/img/jobs/class/merchant-min.png') }}" alt="" width="90px">
							</a>
						</td>
						<tr class="class-name">
							<td>
								<span>Merchant</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('blacksmith') }}" title="blacksmith">
								<img src="{{ url('frontend/img/jobs/class/blacksmith-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('whitesmith') }}" title="whitesmith">
								<img src="{{ url('frontend/img/jobs/class/whitesmith-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/mechanic-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Blacksmith</span>
							</td>
							<td>
								<span>Whitesmith</span>
							</td>
							<td style="display: none">
								<span>Mechanic</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('alchemist') }}" title="alchemist">
								<img src="{{ url('frontend/img/jobs/class/alchemist-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('creator') }}" title="creator">
							<img src="{{ url('frontend/img/jobs/class/creator-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/genetics2-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Alchemist</span>
							</td>
							<td>
								<span>Creator</span>
							</td>
							<td style="display: none">
								<span>Genetic</span>
							</td>
						</tr>
					</tr>
				</table>