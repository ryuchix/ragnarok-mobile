				<table class="jobs">
					<tr style="">
						<td>
							<a href="{{ url('mage') }}" title="mage">
								<img src="{{ url('frontend/img/jobs/class/magician-min.png') }}" alt="" width="90px">
							</a>
						</td>
						<tr class="class-name">
							<td>
								<span>Mage</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td>
							<a href="{{ url('wizard') }}" title="wizard">
								<img src="{{ url('frontend/img/jobs/class/wizard-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td>
							<a href="{{ url('high-wizard') }}" title="high wizard">
								<img src="{{ url('frontend/img/jobs/class/high_wizard-min.png') }}" alt="" width="100px">
							</a>
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/warlock-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td>
								<span>Wizard</span>
							</td>
							<td>
								<span>High Wizard</span>
							</td>
							<td style="display: none">
								<span>Warlock</span>
							</td>
						</tr>
					</tr>
				</table>
				<table class="jobs">
					<tr>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/sage-min.png') }}" alt="" width="100px">
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/professor-min.png') }}" alt="" width="100px">
						</td>
						<td style="display: none">
							<img src="{{ url('frontend/img/jobs/class/sorcerer-min.png') }}" alt="" width="100px">
						</td>
						<tr class="class-name">
							<td style="display: none">
								<span>Sage</span>
							</td>
							<td style="display: none">
								<span>Professor</span>
							</td>
							<td style="display: none">
								<span>Sorcerer</span>
							</td>
						</tr>
					</tr>
				</table>