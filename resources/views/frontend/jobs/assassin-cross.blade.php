@extends('layouts.page')

@section('title', 'Assassin Cross Class, Assassin Cross Skill, Assassin Cross Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Assassin Cross Class, Assassin Cross Skill, Assassin Cross Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('assassin-cross'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/assassin_crosss-min.png') }}" alt="Assassin Cross" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Assassin Cross</h1>
					<span>Thief's 2nd Transcendent Job</span><br>

					<p>The Assassin Cross utilizes stealth and confusion in order to land the perfect killing blow. With greater lethal arts and mastery of poison than the original Assassin, the Assassin Cross has a wider variety of brutal methods to overcome enemies. All without leaving the shadows...</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/assassin_crosss-min.png') }}" alt="Assassin Cross" width="300px"></div>
				<div class="jobs">
					<h1>Assassin Cross</h1>
					<span>Thief's 2nd Transcendent Job</span><br>

					<p>The Assassin Cross utilizes stealth and confusion in order to land the perfect killing blow. With greater lethal arts and mastery of poison than the original Assassin, the Assassin Cross has a wider variety of brutal methods to overcome enemies. All without leaving the shadows...</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered" id="advanced-katar-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-advance-katar-research.png') }}" alt="Advanced Katar Research Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Advanced Katar Research</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">+2% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">+4% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">+5% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">+8% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">+10% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">+12% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">+14% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">+16% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">+18% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">+20% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +1% when using Katars type weapons</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +2% when using Katars type weapons</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +3% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +4% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +5% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +6% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +7% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +8% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +9% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">+20%, physical Dmg +10% when using Katars type weapons.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="assassination-heart">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-assasination-heart.png') }}" alt="Assassination Heart Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Assassination Heart</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">+2% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">+4% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">+5% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">+8% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">+10% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">+12% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">+14% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">+16% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">+18% when using Katars type weapons.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">+20% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">+33% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">+36% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">+39% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">+42% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">+45% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">+48% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">+51% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">+54% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">+57% when using Katars type weapons.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">+60% when using Katars type weapons.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="assassination-halo">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-assassination-halo.png') }}" alt="Assassination Halo Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Assassination Halo</span><br>
									<span class="skill-state">Active Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Increases Crit by 2 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Increases Crit by 4 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Increases Crit by 6 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Increases Crit by 8 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Increases Crit by 10 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Increases Crit by 12 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>48.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Increases Crit by 14 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Increases Crit by 16 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Increases Crit by 18 for the whole Party for 30 sec.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Increases Crit by 20 for the whole Party for 30 sec.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="slash">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-slash.png') }}" alt="Slash Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Slash</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Deals (Atk 220%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Deals (Atk 280%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Deals (Atk 340%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Deals (Atk 400%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Deals (Atk 460%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Deals (Atk 520%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Deals (Atk 580%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Deals (Atk 640%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Deals (Atk 700%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Deals (Atk 760%) Dmg to the target. In 10 sec, the target suffers 100% more damage from the next auto attack Triggers slaying effects if the target has < 30% HP, dealing 100% more damage.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="enhanced-hiding">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-enhanced-hiding.png') }}" alt="Enhanced Hiding Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Enhanced Hiding</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Within 3 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Within 5 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Within 7 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Within 9 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Within 11 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Within 13 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid. Increase Movement Speed in hiding state by 4%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Within 15 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid. Increase Movement Speed in hiding state by 8%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Within 17 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid. Increase Movement Speed in hiding state by 12%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Within 19 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid. Increase Movement Speed in hiding state by 16%.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Within 21 seconds after hiding state is removed, Enhanced or available skills in previous state are still valid. Increase Movement Speed in hiding state by 20%.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="enchant-deadly-poison">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-enchant-deadly-poison.png') }}" alt="Enchant Deadly Poison Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Enchant Deadly Poison</span><br>
									<span class="skill-state">Active Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
								<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>25</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 3% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>30</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 6% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>35</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 9% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>40</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 12% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>45</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 15% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>50</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 18% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>55</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 21% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>60</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 24% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>65</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 27% for 40 sec.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>70</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>75</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 1% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>80</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 2% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>85</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 3% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>90</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 4% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>95</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 5% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>100</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 6% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>105</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 7% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>110</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 8% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>115</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 9% for 40 sec</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>120</td>
								<td>40.0s</td>
								<td class="skill-description" colspan="3">Instantly increases Atk of Assassin Cross by 30% for 40 sec, Def penetration by 10% for 40 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="soul-breaker">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-soul-breaker.png') }}" alt="Soul Breaker Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Soul Breaker</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>Casting Time</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.2s</td>
								<td>1.2s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x180% + Intellegence x25 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.4s</td>
								<td>1.4s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 240% + Intellegence x30 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.6s</td>
								<td>1.6s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 300% + Intellegence x35 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.8s</td>
								<td>1.8s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 360% + Intellegence x40 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>2.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 420% + Intellegence x45 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.2s</td>
								<td>2.2s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x480% + Intellegence x50 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.4s</td>
								<td>2.4s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 540% + Intellegence x55 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.6s</td>
								<td>2.6s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 600% + Intellegence x60 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.8s</td>
								<td>2.8s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 660% + Intellegence x65 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 720% + Intellegence x70 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x780% + Intelligence x70 + Random (Intelligence x5, Intelligence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x840% + Intelligence x70 + Random (Intelligence x5, Intelligence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x900% + Intelligence x70 + Random (Intelligence x5, Intelligence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x960% + Intelligence x70 + Random (Intelligence x5, Intelligence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x1020% + Intelligence x70 + Random (Intelligence x5, Intelligence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 1080% + Intelligence x70 + Random (Intelligence x5, Intelligence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 1140% + Intellegence x70 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 1200% + Intellegence x70 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk 1260% + Intellegence x70 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Fire waves from afar, deal Dmg (Atk x1320% + Intellegence x70 + Random (Intellgence x50)) to a single enemy. Attribute bonus only affects the Dmg caused by Atk, intellegence damage bonus and random damage ignore Def.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="meteor-assault">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/assassin-cross-meteor-assault.png') }}" alt="Meteor Assault Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Meteor Assault</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 134%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 168%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 202%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 236%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 270%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>3.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 304%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>3.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 338%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>3.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 372%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>3.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 406%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Attacks all nearby enemies, dealing (Atk 440%) Dmg. Enemies has a 25% chance to gain 1 of the status: stun, blind and bleed. When in hiding status, has a double chance to apply debuff to enemies.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection