@extends('layouts.page')

@section('title', 'Alchemist Class, Alchemist Skills, Alchemist Skill Description Alchemist Skill Effect in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Alchemist Class, Alchemist Skills, Alchemist Skill Description Alchemist Skill Effect in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('alchemist'))

@section('content')
<div class="post">
	<div class="row">
		<div class="col-md-12 ">
			<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/alchemist-min.png') }}" alt="Alchemist" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Alchemist</h1>
					<span>Merchant's 2nd Job</span><br>

					<p>Active in fields from ancient biotechnology to combining and creating mixtures, he/she joins the adventurers recruited by King Tristan III to perfect his/her biotechnology, and in the end, to successfully summon a Homunculus. </p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/alchemist-min.png') }}" alt="Alchemist" width="300px"></div>
				<div class="jobs">
					<h1>Alchemist</h1>
					<span>Merchant's 2nd Job</span><br>

					<p>Active in fields from ancient biotechnology to combining and creating mixtures, he/she joins the adventurers recruited by King Tristan III to perfect his/her biotechnology, and in the end, to successfully summon a Homunculus. </p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
				<table class="table table-bordered" id="acid-terror">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-acid-terror.png') }}" alt="Acid Terror Skill Image">
							</td>
							<td colspan="5">
								{!! config('app.tip') !!}
								<span class="skill-name">Acid Terror</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range">Cast Time: 1.5s</span><br>
								<span class="skill-range red">Special Cost: Acid Bottle x1</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>2.0s</td>
							<td>12</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x300% + M.Atk x300%) to a single enemy, with a 6% chance to cause HP loss and 1% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>2.0s</td>
							<td>14</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x370% + M.Atk x350%) to a single enemy, with a 7% chance to cause HP loss and 2% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>2.0s</td>
							<td>16</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x440% + M.Atk x400%) to a single enemy, with a 8% chance to cause HP loss and 3% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>2.0s</td>
							<td>18</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x510% + M.Atk x450%) to a single enemy, with a 9% chance to cause HP loss and 4% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>2.0s</td>
							<td>20</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x580% + M.Atk x500%) to a single enemy, with a 10% chance to cause HP loss and 5% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td>2.0s</td>
							<td>22</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x650% + M.Atk x550%) to a single enemy, with a 11% chance to cause HP loss and 6% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td>2.0s</td>
							<td>24</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x720% + M.Atk x600%) to a single enemy, with a 12% chance to cause HP loss and 7% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td>2.0s</td>
							<td>26</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x790% + M.Atk x650%) to a single enemy, with a 13% chance to cause HP loss and 8% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td>2.0s</td>
							<td>28</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x860% + M.Atk x700%) to a single enemy, with a 14% chance to cause HP loss and 9% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td>2.0s</td>
							<td>30</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x930% + M.Atk x750%) to a single enemy, with a 15% chance to cause HP loss and 10% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 11</td>
							<td>2.0s</td>
							<td>31</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x950% + M.Atk x760%) to a single enemy, with a 16% chance to cause HP loss and 11% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 12</td>
							<td>2.0s</td>
							<td>32</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x970% + M.Atk x770%) to a single enemy, with a 17% chance to cause HP loss and 12% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 13</td>
							<td>2.0s</td>
							<td>33</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x990% + M.Atk x780%) to a single enemy, with a 18% chance to cause HP loss and 13% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 14</td>
							<td>2.0s</td>
							<td>34</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x1010% + M.Atk x790%) to a single enemy, with a 19% chance to cause HP loss and 14% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 15</td>
							<td>2.0s</td>
							<td>35</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x1030% + M.Atk x800%) to a single enemy, with a 20% chance to cause HP loss and 15% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 16</td>
							<td>2.0s</td>
							<td>36</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x1050% + M.Atk x810%) to a single enemy, with a 21% chance to cause HP loss and 16% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 17</td>
							<td>2.0s</td>
							<td>37</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x1070% + M.Atk x820%) to a single enemy, with a 22% chance to cause HP loss and 17% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 18</td>
							<td>2.0s</td>
							<td>38</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x1090% + M.Atk x830%) to a single enemy, with a 23% chance to cause HP loss and 18% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 19</td>
							<td>2.0s</td>
							<td>39</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x1110% + M.Atk x840%) to a single enemy, with a 24% chance to cause HP loss and 19% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 20</td>
							<td>2.0s</td>
							<td>40</td>
							<td class="skill-description" colspan="3">Use acid to Deals Dmg (Atk x1130% + M.Atk x850%) to a single enemy, with a 25% chance to cause HP loss and 20% chance to break enemy's armor. The skill's damage ignores enemy's Flee, Kyrie Eleison and Auto Guard</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="acid-demonstration">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-acid-demonstration.png') }}" alt="Acid Demonstration Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Acid Demonstration</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range">Cast Time: 2.0s</span><br>
								<span class="skill-range red">Special Cost: Bottle Grenade x1</span><br>
								<span class="skill-range red">Requires <a href="#acid-terror">Acid Terror</a> Lv.5*</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.0s</td>
							<td>6</td>
							<td class="skill-description" colspan="3">Throw flaming bottle to a specified area, causing 180% Fire Dmg per second to all enemies in a 2-meter radius; This effect lasts 30 seconds, with a 5% chance to break enemy's weapons.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.0s</td>
							<td>9</td>
							<td class="skill-description" colspan="3">Throw flaming bottle to a specified area, causing 210% Fire Dmg per second to all enemies in a 2-meter radius; This effect lasts 30 seconds, with a 5% chance to break enemy's weapons.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.0s</td>
							<td>12</td>
							<td class="skill-description" colspan="3">Throw flaming bottle to a specified area, causing 240% Fire Dmg per second to all enemies in a 2-meter radius; This effect lasts 30 seconds, with a 5% chance to break enemy's weapons.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1.0s</td>
							<td>15</td>
							<td class="skill-description" colspan="3">Throw flaming bottle to a specified area, causing 270% Fire Dmg per second to all enemies in a 2-meter radius; This effect lasts 30 seconds, with a 5% chance to break enemy's weapons.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1.0s</td>
							<td>18</td>
							<td class="skill-description" colspan="3">Throw flaming bottle to a specified area, causing 300% Fire Dmg per second to all enemies in a 2-meter radius; This effect lasts 30 seconds, with a 5% chance to break enemy's weapons.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered" id="axe-hammer-mastery">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-axe-hammer-mastery.png') }}" alt="Axe Hammer Mastery Skill Image">
							</td>
							<td colspan="5">
								{!! config('app.tip') !!}
								<span class="skill-name">Axe Hammer Mastery</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 6 points</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 12 points</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 18 points</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 24 points</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 30 points</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 36 points</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 42 points</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 48 points</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 54 points</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 11</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 1%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 12</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 2%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 13</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 3%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 14</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 4%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 15</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 5%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 16</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 6%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 17</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 7%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 18</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 8%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 19</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 9%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 20</td>
							<td class="skill-description" colspan="3">Permanently increase the Atk of Axes and Maces by 60 points, physical Atk 10%.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="gene-modification">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-gene-modification.png') }}" alt="Gene Modification Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Gene Modification</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Alchemist's Luck permanently increases 1 points, Dexterity increases by 1 points</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Alchemist's Luck permanently increases 1 points, Dexterity increases by 2 points</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Alchemist's Luck permanently increases 1 points, Dexterity increases by 3 points</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Alchemist's Luck permanently increases 1 points, Dexterity increases by 4 points</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Alchemist's Luck permanently increases 1 points, Dexterity increases by 5 points</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="learning-potion">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-learning-potion.png') }}" alt="Learning Potion Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Learning Potion</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 12%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 2%.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 14%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 4%.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 16%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 6%.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 18%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 8%.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 20%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 10%.</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 22%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 12%.</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 24%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 14%.</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 26%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 16%.</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 28%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 18%.</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">When using restoring potion or casting <a href="{{ url('creator#hp-potion-cast') }}">HP Potion Cast</a>, <a href="{{ url('creator#mp-potion-cast') }}">MP Potion Cast</a>, the effect increases by 30%, Success rate of <a href="#pharmacy">Pharmacy</a> increases by 20%.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="pharmacy">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-pharmacy.png') }}" alt="Pharmacy Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Pharmacy</span><br>
								<span class="skill-state">Active Skill</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 2%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 4%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 6%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 8%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 10%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 12%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 14%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 16%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 18%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Alchemist learn how to produce potions and alchemy materials, the success rate increases by 20%. Luck and Dexterity increase the success rate of Pharmacy (30% as the upper limit)</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="sphere-mine">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-sphere-mine.png') }}" alt="Sphere Mine Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Sphere Mine</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.5s</td>
							<td>8</td>
							<td class="skill-description" colspan="3">Summons Sphere Mine to launch Suicidal Destruction attack against an enemy, dealing 160% Atk +200 damage</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.5s</td>
							<td>9</td>
							<td class="skill-description" colspan="3">Summons Sphere Mine to launch Suicidal Destruction attack against an enemy, dealing 220% Atk +400 damage</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.5s</td>
							<td>10</td>
							<td class="skill-description" colspan="3">Summons Sphere Mine to launch Suicidal Destruction attack against an enemy, dealing 280% Atk +600 damage</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1.5s</td>
							<td>11</td>
							<td class="skill-description" colspan="3">Summons Sphere Mine to launch Suicidal Destruction attack against an enemy, dealing 340% Atk +800 damage</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1.5s</td>
							<td>12</td>
							<td class="skill-description" colspan="3">Summons Sphere Mine to launch Suicidal Destruction attack against an enemy, dealing 400% Atk +1000 damage</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="sphere-parasitism">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-sphere-parasitism.png') }}" alt="Sphere Parasitism Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Sphere Parasitism</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range red">Requires <a href="#sphere-mine">Sphere Mine</a> Lv.5*</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.5s</td>
							<td>10</td>
							<td class="skill-description" colspan="3">Applies Sphere on the target and causes it to self destruct, dealing <a href="#sphere-mine">Sphere Mine</a> x1.4 damage</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.5s</td>
							<td>12</td>
							<td class="skill-description" colspan="3">Applies Sphere on the target and causes it to self destruct, dealing <a href="#sphere-mine">Sphere Mine</a> x1.6 damage</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.5s</td>
							<td>14</td>
							<td class="skill-description" colspan="3">Applies Sphere on the target and causes it to self destruct, dealing <a href="#sphere-mine">Sphere Mine</a> x1.8 damage</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1.5s</td>
							<td>16</td>
							<td class="skill-description" colspan="3">Applies Sphere on the target and causes it to self destruct, dealing <a href="#sphere-mine">Sphere Mine</a> x2.0 damage</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1.5s</td>
							<td>18</td>
							<td class="skill-description" colspan="3">Applies Sphere on the target and causes it to self destruct, dealing <a href="#sphere-mine">Sphere Mine</a> x2.2 damage</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="homunculus-strength-i">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-homunculus-strength-i.png') }}" alt="Homunculus Strength I Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Homunculus Strength I</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Permanently increases Homunculus' attributes, Lif: Max HP +2000, M.Atk +100; Amistr: Atk +100, ASPD +5%; Vanilmirth: M.Atk +100, Def +40</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Permanently increases Homunculus' attributes, Lif: Max HP +4000, M.Atk +200; Amistr: Atk +200, ASPD +10%; Vanilmirth: M.Atk +200, Def +80</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Permanently increases Homunculus' attributes, Lif: Max HP +6000, M.Atk +300; Amistr: Atk +300, ASPD +15%; Vanilmirth: M.Atk +300, Def +120</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Permanently increases Homunculus' attributes, Lif: Max HP +8000, M.Atk +400; Amistr: Atk +400, ASPD +20%; Vanilmirth: M.Atk +400, Def +160</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Permanently increases Homunculus' attributes, Lif: Max HP +10000, M.Atk +500; Amistr: Atk +500, ASPD +25%; Vanilmirth: M.Atk +500, Def +200</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="call-homunculus">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-call-homunculus.png') }}" alt="Call Homunculus Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Call Homunculus</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range">Cast Time: 1.0s</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.0s</td>
							<td>80</td>
							<td class="skill-description" colspan="3">Caster learns to summon Homunculus. To summon Lif	</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.0s</td>
							<td>80</td>
							<td class="skill-description" colspan="3">Caster learns to summon Homunculus. To summon Lif / Amistr</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.0s</td>
							<td>80</td>
							<td class="skill-description" colspan="3">Caster learns to summon Homunculus. To summon Lif / Amistr / Vanilmirth</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="resurrect-homunculus">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-resurrect-homunculus.png') }}" alt="Resurrect Homunculus Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Resurrect Homunculus</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range red">Special Cost: <a href="{{ url('item/blue-gemstone') }}">Blue Gemstone</a> x1</span><br>
								<span class="skill-range red">Requires <a href="#call-homunculus">Call Homunculus</a> Lv.1*</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>Time</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.0s</td>
							<td>6.0s</td>
							<td>80</td>
							<td class="skill-description" colspan="3">Revive Homunculus and restore 25% HP</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.0s</td>
							<td>4.0s</td>
							<td>100</td>
							<td class="skill-description" colspan="3">Revive Homunculus and restore 50% HP</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.0s</td>
							<td>2.0s</td>
							<td>120</td>
							<td class="skill-description" colspan="3">Revive Homunculus and restore 75% HP</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1.0s</td>
							<td>0.0s</td>
							<td>140</td>
							<td class="skill-description" colspan="3">Revive Homunculus and restore 100% HP</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered" id="life-psychic">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/alchemist-life-psychic.png') }}" alt="Life Psychic Skill Image">
							</td>
							<td colspan="5">
								{!! config('app.tip') !!}
								<span class="skill-name">Life Psychic</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 6.0m</span><br>
								<span class="skill-range red">Requires <a href="#resurrect-homunculus">Resurrect Homunculus</a> Lv.1*</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>CD</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>20</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 120 seconds; master spend 11% Atk, M.Atk to increase Amistr's Atk: Master's Atk x12% + Master's M.Atk x1%, increase Vanilmirth's M.Atk: Master's Atk x1% + Master's M.Atk x12%, and to increase Hell Plant's damage by 2%</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>24</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 140 seconds; Master spend 12% Atk, M.Atk to increase Amistr's Atk: Master's Atk x14% + Master's M.Atk x2%, increase Vanilmirth's M.Atk: Master's Atk x2% + Master's M.Atk x14%, and to increase Hell Plant's damage by 4%</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>28</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 160 seconds; Master spend 13% Atk, M.Atk to increase Amistr's Atk: Master's Atk x16% + Master's M.Atk x3%, increase Vanilmirth's M.Atk: Master's Atk x3% + Master's M.Atk x16%, and to increase Hell Plant's damage by 6%</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>32</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 180 seconds; Master spend 14% Atk, M.Atk to increase Amistr's Atk: Master's Atk x18% + Master's M.Atk x4%, increase Vanilmirth's M.Atk: Master's Atk x4% + Master's M.Atk x18%, and to increase Hell Plant's damage by 8%</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>36</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 200 seconds; Master spend 15% Atk, M.Atk to increase Amistr's Atk: Master's Atk x20% + Master's M.Atk x5%, increase Vanilmirth's M.Atk: Master's Atk x5% + Master's M.Atk x20%, and to increase Hell Plant's damage by 10%</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>40</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 220 seconds; Master spend 16% Atk, M.Atk to increase Amistr's Atk: Master's Atk x22% + Master's M.Atk x6%, increase Vanilmirth's M.Atk: Master's Atk x6% + Master's M.Atk x22%, and to increase Hell Plant's damage by 12%</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>44</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 240 seconds; Master spend 17% Atk, M.Atk to increase Amistr's Atk: Master's Atk x24% + Master's M.Atk x7%, increase Vanilmirth's M.Atk: Master's Atk x7% + Master's M.Atk x24%, and to increase Hell Plant's damage by 14%</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>48</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 260 seconds; Master spend 18% Atk, M.Atk to increase Amistr's Atk: Master's Atk x26% + Master's M.Atk x8%, increase Vanilmirth's M.Atk: Master's Atk x8% + Master's M.Atk x26%, and to increase Hell Plant's damage by 16%</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>52</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 280 seconds; Master spend 19% Atk, M.Atk to increase Amistr's Atk: Master's Atk x28% + Master's M.Atk x9%, increase Vanilmirth's M.Atk: Master's Atk x9% + Master's M.Atk x28%, and to increase Hell Plant's damage by 18%</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>56</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 300 seconds; Master spend 20% Atk, M.Atk to increase Amistr's Atk: Master's Atk x30% + Master's M.Atk x10%, increase Vanilmirth's M.Atk: Master's Atk x10% + Master's M.Atk x30%, and to increase Hell Plant's damage by 20%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 11</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>60</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 320 seconds; Master spend 21% Atk, M.Atk to increase Amistr's Atk: Master's Atk x32% + Master's M.Atk x11%, increase Vanilmirth's M.Atk: Master's Atk x11% + Master's M.Atk x32%, and to increase Hell Plant's damage by 22%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 12</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>64</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 340 seconds; Master spend 22% Atk, M.Atk to increase Amistr's Atk: Master's Atk x34% + Master's M.Atk x12%, increase Vanilmirth's M.Atk: Master's Atk x12% + Master's M.Atk x34%, and to increase Hell Plant's damage by 24%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 13</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>68</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 360 seconds; Master spend 23% Atk, M.Atk to increase Amistr's Atk: Master's Atk x36% + Master's M.Atk x13%, increase Vanilmirth's M.Atk: Master's Atk x13% + Master's M.Atk x36%, and to increase Hell Plant's damage by 26%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 14</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>72</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 380 seconds; Master spend 24% Atk, M.Atk to increase Amistr's Atk: Master's Atk x38% + Master's M.Atk x14%, increase Vanilmirth's M.Atk: Master's Atk x14% + Master's M.Atk x38%, and to increase Hell Plant's damage by 28%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 15</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>76</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 400 seconds; Master spend 25% Atk, M.Atk to increase Amistr's Atk: Master's Atk x40% + Master's M.Atk x15%, increase Vanilmirth's M.Atk: Master's Atk x15% + Master's M.Atk x40%, and to increase Hell Plant's damage by 30%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 16</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>80</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 420 seconds; Master spend 26% Atk, M.Atk to increase Amistr's Atk: Master's Atk x42% + Master's M.Atk x16%, increase Vanilmirth's M.Atk: Master's Atk x16% + Master's M.Atk x42%, and to increase Hell Plant's damage by 32%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 17</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>84</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 440 seconds; Master spend 27% Atk, M.Atk to increase Amistr's Atk: Master's Atk x44% + Master's M.Atk x17%, increase Vanilmirth's M.Atk: Master's Atk x17% + Master's M.Atk x44%, and to increase Hell Plant's damage by 34%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 18</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>88</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 460 seconds; Master spend 28% Atk, M.Atk to increase Amistr's Atk: Master's Atk x46% + Master's M.Atk x18%, increase Vanilmirth's M.Atk: Master's Atk x18% + Master's M.Atk x46%, and to increase Hell Plant's damage by 36%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 19</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>92</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 480 seconds; Master spend 29% Atk, M.Atk to increase Amistr's Atk: Master's Atk x48% + Master's M.Atk x19%, increase Vanilmirth's M.Atk: Master's Atk x19% + Master's M.Atk x48%, and to increase Hell Plant's damage by 38%</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 20</td>
							<td>1.0s</td>
							<td>3.0s</td>
							<td>96</td>
							<td class="skill-description" colspan="3">Alchemist uses Forbidden Skills to transfer power to Homunculus, the effect Lasts 500 seconds; Master spend 30% Atk, M.Atk to increase Amistr's Atk: Master's Atk x50% + Master's M.Atk x20%, increase Vanilmirth's M.Atk: Master's Atk x20% + Master's M.Atk x50%, and to increase Hell Plant's damage by 40%</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection