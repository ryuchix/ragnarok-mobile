@extends('layouts.page')

@section('title', 'Merchant Class, Merchant Skill, Merchant Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Merchant Class, Merchant Skill, Merchant Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('merchant'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/merchant-min.png') }}" alt="Merchant" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Merchant</h1>
					<span>1st Job</span><br>

					<p>Born in a wealthy merchant family, he/she was taught that everything in life is about money. Thinking that an easy fortune is awaiting, he/she decides to join the other adventurers gathered by King Tristan 3rd.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/merchant-min.png') }}" alt="Merchant" width="300px">
				<div class="jobs">
					<h1>Merchant</h1>
					<span>1st Job</span><br>

					<p>Born in a wealthy merchant family, he/she was taught that everything in life is about money. Thinking that an easy fortune is awaiting, he/she decides to join the other adventurers gathered by King Tristan 3rd.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="cart-attack">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-cart-attack.png') }}" alt="Cart Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Cart Attack</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 3.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 300% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 350% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 400% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 450% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 500% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 550% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 600% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 650% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 700% lane damage. Carts are required.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.5s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with Cart Attack, dealing 750% lane damage. Carts are required.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="enhanced-cart">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-enhanced-cart.png') }}" alt="Enhanced Cart Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Enhanced Cart</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +15</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +30</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +45</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +60</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +75</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +90</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +105</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +120</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +135</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using cart related skills, Atk +150</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="change-cart">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-change-cart.png') }}" alt="Change Cart Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Change Cart</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Increases cart storage cap by 10 slots. When skill level reaches 5, Player can use Light Cart and Cat Cart.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Increases cart storage cap by 15 slots. When skill level reaches 5, Player can use Light Cart and Cat Cart.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Increases cart storage cap by 20 slots. When skill level reaches 5, Player can use Light Cart and Cat Cart.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Increases cart storage cap by 25 slots. When skill level reaches 5, Player can use Light Cart and Cat Cart.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Increases cart storage cap by 30 slots. When skill level reaches 5, Player can use Light Cart and Cat Cart.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="loud-exclamation">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-loud-exclamation.png') }}" alt="Loud Exclamation Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Loud Exclamation</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state">Casting Range: 1.5m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 120 seconds raise your strength by 1 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 140 seconds raise your strength by 2 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 160 seconds raise your strength by 3 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 180 seconds raise your strength by 4 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 200 seconds raise your strength by 5 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 220 seconds raise your strength by 6 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 240 seconds raise your strength by 7 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 260 seconds raise your strength by 8 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 280 seconds raise your strength by 9 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 10 points. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 11 points, all party members strength by 1 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 12 points, all party members strength by 2 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 13 points, all party members strength by 3 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 14 points, all party members strength by 4 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 15 points, all party members strength by 5 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 16 points, all party members strength by 6 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 17 points, all party members strength by 7 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 18 points, all party members strength by 8 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 19 points, all party members strength by 9 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Merchant raises strength with Loud Exclamation, Within 300 seconds raise your strength by 20 points, all party members strength by 10 point. This effect can be removed by <a href="{{ url('high-priest/#clearance') }}">Clearance</a> or Marsh Pond</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="mammonite">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-mammonite.png') }}" alt="Mammonite Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Mammonite</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span><br>
									<span class="skill-range red">Special Cost: Zeny * Multiplier</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
								<th>Multiplier</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>5</td>
								<td>100</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 150% Dmg.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>5</td>
								<td>200</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 200% Dmg.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>5</td>
								<td>300</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 250% Dmg.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>5</td>
								<td>400</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 300% Dmg.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>5</td>
								<td>500</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 350% Dmg.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>5</td>
								<td>600</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 400% Dmg.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>5</td>
								<td>700</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 450% Dmg.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>5</td>
								<td>800</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 500% Dmg.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>5</td>
								<td>900</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 550% Dmg.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1000</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 600% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1050</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 620% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1100</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 640% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1150</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 660% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1200</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 680% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1250</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 700% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1300</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 720% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1350</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 740% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1400</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 760% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1450</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 780% Dmg.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>5</td>
								<td>1500</td>
								<td class="skill-description" colspan="3">Attacks an enemy with gold coins dealing 800% Dmg.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="buying-low">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-buying-low.png') }}" alt="Buying Low Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Buying Low</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Could buy items at a 1% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Could buy items at a 2% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Could buy items at a 3% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Could buy items at a 4% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Could buy items at a 5% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Could buy items at a 6% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Could buy items at a 7% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Could buy items at a 8% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Could buy items at a 9% discount from some NPC merchants.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Could buy items at a 10% discount from some NPC merchants.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="overprice">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-overprice.png') }}" alt="OverPrice Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">OverPrice</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Could rise the price by 6% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Could rise the price by 8% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Could rise the price by 10% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Could rise the price by 12% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Could rise the price by 14% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Could rise the price by 16% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Could rise the price by 18% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Could rise the price by 20% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Could rise the price by 22% to sell items to NPC shops.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Could rise the price by 24% to sell items to NPC shops.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="fund-raising">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/merchant-fund-raising.png') }}" alt="Fund Raising Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Fund Raising</span><br>
									<span class="skill-state">Passive Skill</span><br>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 2% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 4% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 6% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 8% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 10% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 12% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 14% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 16% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 18% more Zeny.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 20% more Zeny.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 22% more Zeny.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 24% more Zeny.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 26% more Zeny.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 28% more Zeny.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Zeny makes the most beautiful music. When merchants pick up Zeny, gets 30% more Zeny.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection