@extends('layouts.page')

@section('title', 'Sniper Class, Sniper Skill, Sniper Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Sniper Class, Sniper Skill, Sniper Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('hunter'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/sniper-min.png') }}" alt="Sniper" width="250px" class="pull-left">
				<div class="jobs">
					<h1>Sniper</h1>
					<span>Archer 2nd Transcendent Job</span><br>

					<p>One shot, one kill! There is no better explanation than this to describe the Sniper class! The Sniper specializes in assassinating enemies from a distance.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/sniper-min.png') }}" alt="Sniper" width="300px"></div>
				<div class="jobs">
					<h1>Sniper</h1>
					<span>Archer 2nd Transcendent Job</span><br>

					<p>One shot, one kill! There is no better explanation than this to describe the Sniper class! The Sniper specializes in assassinating enemies from a distance.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="sharp-shooting">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-sharp-shooting.jpg') }}" alt="Improve Concentration Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Improve Concentration</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with level 10 <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a>)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>CD</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk225%) arrow DMG to the target and granting self (15%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk250%) arrow DMG to the target and granting self (15%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk275%) arrow DMG to the target and granting self (15%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk300%) arrow DMG to the target and granting self (15%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk325%) arrow DMG to the target and granting self (15%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk350%) arrow DMG to the target and granting self (30%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk375%) arrow DMG to the target and granting self (30%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk400%) arrow DMG to the target and granting self (30%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk425%) arrow DMG to the target and granting self (30%*Damage Done) HP</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>6.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Concentrates to shoot, dealing (Atk450%) arrow DMG to the target and granting self (30%*Damage Done) HP</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="true-sight">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-true-sight.jpg') }}" alt="True Sight Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">True Sight</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m (+0.5m with level 10 <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a>)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>43</td>
								<td class="skill-description" colspan="3">Gains high sniper capability in a short time, Gaining all 6 base attributes +5, Atk +1% and Critical +1 for 33 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="ferity-awakening">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-ferity-awakening.jpg') }}" alt="Ferity Awakening Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Ferity Awakening</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When the target has < 30% HP, self has a 6% higher chance to deal critical attack and 6% higher to trigger beast</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When the target has < 30% HP, self has a 12% higher chance to deal critical attack and 12% higher to trigger beast</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When the target has < 30% HP, self has a 18% higher chance to deal critical attack and 18% higher to trigger beast</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When the target has < 30% HP, self has a 24% higher chance to deal critical attack and 24% higher to trigger beast</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When the target has < 30% HP, self has a 30% higher chance to deal critical attack and 30% higher to trigger beast</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When the target has < 34% HP, self has a 30% higher chance to deal critical attack and 30% higher to trigger beast</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When the target has < 38% HP, self has a 30% higher chance to deal critical attack and 30% higher to trigger beast	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When the target has < 42% HP, self has a 30% higher chance to deal critical attack and 30% higher to trigger beast</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When the target has < 46% HP, self has a 30% higher chance to deal critical attack and 30% higher to trigger beast</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When the target has < 50% HP, self has a 30% higher chance to deal critical attack and 30% higher to trigger beast</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="multi-firing">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-multi-firing.jpg') }}" alt="Multi Firing Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Multi Firing</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with level 10 <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a>)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>CD</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk240%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk300%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk360%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk420%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk480%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk540%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk600%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>48</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk660%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk720%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk780%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>54</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk860%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>56</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk940%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1020%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>60</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1100%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>62</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1180%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1260%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>66</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1340%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>68</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1420%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1500%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>3.0s</td>
								<td>3.0s</td>
								<td>72</td>
								<td class="skill-description" colspan="3">Strafes the frontal area, dealing (Atk1580%) lane Dmg to all enemies in range and knocking them back shortly</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="stun">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-stun.jpg') }}" alt="Stun Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Stun</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Using the skill <a href="{{ url('hunter#falcon-assault') }}">Falcon Assault</a> / <a href="#hunting-assault">Hunting Assault</a>, has a 10% chance to stun the enemy unit for 2.6 sec. Stunned targets will take extra 3% damage to stunned targets</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Using the skill <a href="{{ url('hunter#falcon-assault') }}">Falcon Assault</a> / <a href="#hunting-assault">Hunting Assault</a>, has a 20% chance to stun the enemy unit for 3.2 sec. Stunned targets will take extra 6% damage to stunned targets</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Using the skill <a href="{{ url('hunter#falcon-assault') }}">Falcon Assault</a> / <a href="#hunting-assault">Hunting Assault</a>, has a 30% chance to stun the enemy unit for 3.8 sec. Stunned targets will take extra 9% damage to stunned targets</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Using the skill <a href="{{ url('hunter#falcon-assault') }}">Falcon Assault</a> / <a href="#hunting-assault">Hunting Assault</a>, has a 40% chance to stun the enemy unit for 4.4 sec. Stunned targets will take extra 12% damage to stunned targets</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Using the skill <a href="{{ url('hunter#falcon-assault') }}">Falcon Assault</a> / <a href="#hunting-assault">Hunting Assault</a>, has a 50% chance to stun the enemy unit for 5.0 sec. Stunned targets will take extra 15% damage to stunned targets</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="hunting-assault">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-hunting-assault.jpg') }}" alt="Hunting Assault Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Hunting Assault</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with level 10 <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a>)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (120% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (140% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (160% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (180% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (200% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (220% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>33</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (240% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>36</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (260% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>39</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (280% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Falcon deals natural Dmg: (300% [<a href="{{ url('hunter#blitz-beat') }}">Blitz Beat</a>]'s skill damage) to up to 6 enemies in the front row.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="trump-tamer">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-trump-tamer.jpg') }}" alt="Trump Tamer Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Trump Tamer</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Beast gains 6% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Beast gains 7% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Beast gains 8% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Beast gains 9% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Beast gains 10% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Beast gains 11% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Beast gains 12% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Beast gains 13% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Beast gains 14% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Beast gains 15% Atk every time it attacks. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Every time Beast attacks will increase own Atk by 15% and Critical Damage 1%. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Every time Beast attacks will increase own Atk by 15% and Critical Damage 2%. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Every time Beast attacks will increase own Atk by 15% and Critical Damage 3%. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Every time Beast attacks will increase own Atk by 15% and Critical Damage 4%. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Every time Beast attacks will increase own Atk by 15% and Critical Damage 5%. Stacks up to 3 times. Lasts 3 sec.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="wind-walk">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-wind-walk.jpg') }}" alt="Wind Walk Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Wind Walk</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m (+0.5m with level 10 <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a>)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 2% and Flee by 2% for 30 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 4% and Flee by 4% for 60 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>29</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 6% and Flee by 6% for 90 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>32</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 8% and Flee by 8% for 120 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>35</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 10% and Flee by 10% for 150 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 12% and Flee by 12% for 180 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>41</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 14% and Flee by 14% for 210 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>44</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 16% and Flee by 16% for 240 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>47</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 18% and Flee by 18% for 270 sec (Cannot work with Increase Agility)</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Walks like wind, improving self Move Spd by 20% and Flee by 20% for 300 sec (Cannot work with Increase Agility)</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="land-mine">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-land-mine.jpg') }}" alt="Land Mine Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Land Mine</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with level 10  <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a>)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals (Dex* (3 +BaseLv/100) * (1 +Int/35) * 200%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals (Dex* (3 +BaseLv/100) * (1 +Int/35) * 340%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals (Dex* (3 +BaseLv/100) * (1 +Int/35) * 480%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals (Dex* (3 +BaseLv/100) * (1 +Int/35) * 620%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals (Dex* (3 +BaseLv/100) * (1 +Int/35) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 6</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*5%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.	</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 7</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*10%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 8</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*15%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 9</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*20%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 10</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*25%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*30%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*35%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*40%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*45%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.5s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Places a trap which Deals ( (Dex*(3 +BaseLv/100)*(1 +Int/35) + (Atk +Refine Atk)*50%) * 760%) earth M.Dmg based on Def to enemy that triggers it. Can have up to 2 traps of this kind.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="trap-research">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-trap-research.jpg') }}" alt="Trap Research Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Trap Research</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 1, the caster's intelligence permanently increases by 1. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 1, the caster's intelligence permanently increases by 2. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 2, the caster's intelligence permanently increases by 3. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 2, the caster's intelligence permanently increases by 4. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 3, the caster's intelligence permanently increases by 5. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 3, the caster's intelligence permanently increases by 6. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 4, the caster's intelligence permanently increases by 7. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 4, the caster's intelligence permanently increases by 8. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 5, the caster's intelligence permanently increases by 9. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">The upper limit of coexisting traps increases to 5, the caster's intelligence permanently increases by 10. When trap upgrades once, the trap's damage increases by 20 points. The default limit of coexisting traps is 8.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="sandman-trap">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/sniper-sandman-trap.jpg') }}" alt="Sandman Trap Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Sandman Trap</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 5.0m (+2.5m with level 10  <a href="{{ url('archer#vultures-eye') }}">Vulture's Eye</a>)</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>0.5s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Places a trap to the target area. Has a 60% chance to cause enemies in a small area to sleep for 4 sec. Up to 1 traps can be placed at a time. Targets won't fall asleep again in 1.5 sec after removing the sleep status.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>0.5s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Places a trap to the target area. Has a 70% chance to cause enemies in a small area to sleep for 8 sec. Up to 1 traps can be placed at a time. Targets won't fall asleep again in 1.5 sec after removing the sleep status.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>0.5s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Places a trap to the target area. Has a 80% chance to cause enemies in a small area to sleep for 12 sec. Up to 1 traps can be placed at a time. Targets won't fall asleep again in 1.5 sec after removing the sleep status.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>0.5s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Places a trap to the target area. Has a 90% chance to cause enemies in a small area to sleep for 16 sec. Up to 1 traps can be placed at a time. Targets won't fall asleep again in 1.5 sec after removing the sleep status.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>0.5s</td>
								<td>27</td>
								<td class="skill-description" colspan="3">Places a trap to the target area. Has a 100% chance to cause enemies in a small area to sleep for 20 sec. Up to 1 traps can be placed at a time. Targets won't fall asleep again in 1.5 sec after removing the sleep status.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection