@extends('layouts.page')

@section('title', 'Knight Class, Knight Skill, Knight Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Knight Class, Knight Skill, Knight Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('knight'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/knight-min.png') }}" alt="Knight" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Knight</h1>
					<span>Swordsman's 2nd Job</span><br>

					<p>Under the instructions of the King, he/she has devoted to live as a Knight serving the King. He/She has the ability to move very quickly riding a PecoPeco. Having greater strength and the ability to attack than a Swordsman, he/she points his/her weapon at those who endanger the weak and the King.</p>

					<p>The ability to move quickly, combined with a variety of different physical attacks puts fear into all enemies</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/knight-min.png') }}" alt="Knight" width="300px">
				<div class="jobs">
					<h1>Knight</h1>
					<span>Swordsman's 2nd Job</span><br>

					<p>Under the instructions of the King, he/she has devoted to live as a Knight serving the King. He/She has the ability to move very quickly riding a PecoPeco. Having greater strength and the ability to attack than a Swordsman, he/she points his/her weapon at those who endanger the weak and the King.</p>

					<p>The ability to move quickly, combined with a variety of different physical attacks puts fear into all enemies</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="counter-attack">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-counter-attack.jpg') }}" alt="Counter Attack Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Counter Attack</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-state red">Requires Knight Lv.51</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>3</td>
								<td class="skill-description" colspan="3">Enters Counter Attack status. The caster counters enemy unit when hit by melee Auto Attacks. The counter attack cannot miss and deals guaranteed Critical Damage. The status lasts 0.8 sec. Critical Damage increases as the skill levels up</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>3</td>
								<td class="skill-description" colspan="3">Enters Counter Attack status. The caster counters enemy unit when hit by melee Auto Attacks. The counter attack cannot miss and deals guaranteed Critical Damage. The status lasts 1.2 sec. Critical Damage increases as the skill levels up</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>3</td>
								<td class="skill-description" colspan="3">Enters Counter Attack status. The caster counters enemy unit when hit by melee Auto Attacks. The counter attack cannot miss and deals guaranteed Critical Damage. The status lasts 1.6 sec. Critical Damage increases as the skill levels up</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>3</td>
								<td class="skill-description" colspan="3">Enters Counter Attack status. The caster counters enemy unit when hit by melee Auto Attacks. The counter attack cannot miss and deals guaranteed Critical Damage. The status lasts 2.0 sec. Critical Damage increases as the skill levels up</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>3</td>
								<td class="skill-description" colspan="3">Enters Counter Attack status. The caster counters enemy unit when hit by melee Auto Attacks. The counter attack cannot miss and deals guaranteed Critical Damage. The status lasts 2.4 sec. Critical Damage increases as the skill levels up</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="bowling-bash">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-bowling-bash.jpg') }}" alt="Bowling Bash Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Bowling Bash</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.5m</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (50% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (90% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (130% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (170% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (210% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (250% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (290% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (330% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (370% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (410% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 10% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (450% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (490% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (530% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (570% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (610% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (650% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (690% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (730% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (770% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.5s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Cause enemies within range to collide into one another, Deals (810% of Atk) Dmg. The more enemy units in the range, the higher the damage. Each enemy unit increases 30% Dmg</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="heart-of-steel">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-heart-of-steel.jpg') }}" alt="Heart of Steel Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Heart of Steel</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 1.0m</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span><br>
									<span class="skill-range red">Requires <a href="{{ url('swordsman#endure') }}">Endure</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>25.0s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">In 5 sec, takes 50% less Dmg and M.Dmg and gains no-stiff effects provided by Endure for up to 12 times</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>25.0s</td>
								<td>55</td>
								<td class="skill-description" colspan="3">In 5 sec, takes 60% less Dmg and M.Dmg and gains no-stiff effects provided by Endure for up to 13 times</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>25.0s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">In 5 sec, takes 70% less Dmg and M.Dmg and gains no-stiff effects provided by Endure for up to 14 times</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>25.0s</td>
								<td>85</td>
								<td class="skill-description" colspan="3">In 5 sec, takes 80% less Dmg and M.Dmg and gains no-stiff effects provided by Endure for up to 15 times</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>25.0s</td>
								<td>100</td>
								<td class="skill-description" colspan="3">In 5 sec, takes 90% less Dmg and M.Dmg and gains no-stiff effects provided by Endure for up to 16 times</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="aura-blade">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-aura-blade.jpg') }}" alt="Aura Blade Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Aura Blade</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Knight Lv.60</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Knight deals 20 more True Dmg with auto attacks</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Knight deals 40 more True Dmg with auto attacks</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Knight deals 60 more True Dmg with auto attacks</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Knight deals 80 more True Dmg with auto attacks</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Knight deals 100 more True Dmg with auto attacks</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Knight deals 100 more True Dmg with auto attacks, Each 5 AGI brings 1 True Dmg</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Knight deals 100 more True Dmg with auto attacks, Each 5 AGI brings 1.5 True Dmg</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Knight deals 100 more True Dmg with auto attacks, Each 5 AGI brings 2 True Dmg</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Knight deals 100 more True Dmg with auto attacks, Each 5 AGI brings 2.5 True Dmg</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Knight deals 100 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">Knight deals 100 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">Knight deals 140 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">Knight deals 160 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">Knight deals 180 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">Knight deals 200 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">Knight deals 220 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">Knight deals 240 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">Knight deals 260 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">Knight deals 280 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">Knight deals 300 more True Dmg with auto attacks, Each 5 AGI brings 3 True Dmg, Each 1 LUK brings 2 True Dmg</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="one-hand-quicken">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-one-hand-quicken.jpg') }}" alt="One-Hand Quicken Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">One-Hand Quicken</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span><br>
									<span class="skill-range red">Requires <a href="{{ url('swordsman#sword-mastery') }}">Sword Mastery</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>CD</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 30 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 60 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 90 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 120 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 150 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 180 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>38</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 210 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>42</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 240 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 270 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 120. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 240. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 360. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 480. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 600. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 720. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 840. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 960. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 1080. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>30.0s</td>
								<td>50</td>
								<td class="skill-description" colspan="3">Skill ASPD +30% for 300 sec when using sword type weapons. Auto Attack increases by 1200. Sword type weapons are required. (Exclude Daggers type weapons)</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="spear-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-spear-mastery.jpg') }}" alt="Spear Mastery Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Spear Mastery</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 8, Auto Attack increases by 20</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 16, Auto Attack increases by 40</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 24, Auto Attack increases by 60</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 32, Auto Attack increases by 80</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 40, Auto Attack increases by 100</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 48, Auto Attack increases by 120</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 56, Auto Attack increases by 140</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 64, Auto Attack increases by 160</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 72, Auto Attack increases by 180</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 80, Auto Attack increases by 200</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 100, Auto Attack increases by 220</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 120, Auto Attack increases by 240</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 140, Auto Attack increases by 260</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 160, Auto Attack increases by 280</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 180, Auto Attack increases by 300</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 200, Auto Attack increases by 320</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 220, Auto Attack increases by 340</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 240, Auto Attack increases by 360</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 260, Auto Attack increases by 380</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td class="skill-description" colspan="3">When using spears, Atk increases by 280, Auto Attack increases by 400</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="pierce">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-pierce.jpg') }}" alt="Pierce Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Pierce</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.0m</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span><br>
									<span class="skill-range red">Requires <a href="#spear-mastery">Spear Mastery</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>7</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk120%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>8</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk140%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>9</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk160%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk180%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>11</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk200%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk220%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk240%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk260%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>15</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk280%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk300%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk320%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk340%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk360%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk380%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>1.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk400%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 16</td>
								<td>1.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk420%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 17</td>
								<td>1.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk440%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 18</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk460%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 19</td>
								<td>1.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk480%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 20</td>
								<td>1.0s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Attacks the target repeatedly with spears, dealing (Atk500%) Dmg. Applies combo attacks based on enemy size. Small/Medium/Large: 1/2/3 times. Spear type weapons are required.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="cavalry-combat">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-cavalry-combat.jpg') }}" alt="Cavalry Combat Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Cavalry Combat</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Deals 4% more damage to large size monsters while in Cavalry Combat</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Deals 8% more damage to large size monsters while in Cavalry Combat</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Deals 12% more damage to large size monsters while in Cavalry Combat</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Deals 16% more damage to large size monsters while in Cavalry Combat</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Deals 20% more damage to large size monsters while in Cavalry Combat</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="cavalry-mastery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-cavalry-mastery.jpg') }}" alt="Cavalry Combat Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Cavalry Combat</span><br>
									<span class="skill-state">Passive Skill</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span><br>
									<span class="skill-range red">Requires <a href="#cavalry-combat">Cavalry Combat</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 12% ASPD</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 9% ASPD</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 6% ASPD</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 3% ASPD</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Could fight while mounted but loses 0% ASPD</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="brandish-spear">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/knight-brandish-spear.jpg') }}" alt="Brandish Spear Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Brandish Spear</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 2.5m</span><br>
									<span class="skill-range red">Requires Knight Lv.51</span><br>
									<span class="skill-range red">Requires <a href="#cavalry-mastery">Cavalry Mastery</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
								<th>Delay</th>
								<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk130%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk160%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk190%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk220%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk250%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk280%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk310%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk340%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk370%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Deals (Atk400%) Dmg to the target and knock them back 1.5 meters. Can only be used while mounted</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection