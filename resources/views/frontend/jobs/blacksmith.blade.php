@extends('layouts.page')

@section('title', 'Blacksmith Class, Blacksmith Skills, Blacksmith Skill Description Blacksmith Skill Effect in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Blacksmith Class, Blacksmith Skills, Blacksmith Skill Description Blacksmith Skill Effect in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('blacksmith'))

@section('content')
<div class="post">
	<div class="row">
		<div class="col-md-12 ">
			<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/whitesmith-min.png') }}" alt="Blacksmith" width="300px" class="pull-left">
				<div class="jobs">
					<h1>Blacksmith</h1>
					<span>Merchant's 2nd Job</span><br>

					<p>The Blacksmith, searching for ores and minerals and carefully refining them to make weapons, has taken completeness as a principle in making these weapons and built a reputation of trust upon it. After reading the notice recruiting adventurers by King Tristan III, believing his/her weapons will help and to heighten his/her reputation as a Blacksmith, he/she decides to join the world of adventure and while engaged in battles, gathers materials to make more weapons.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/whitesmith-min.png') }}" alt="Blacksmith" width="300px"></div>
				<div class="jobs">
					<h1>Blacksmith</h1>
					<span>Merchant's 2nd Job</span><br>

					<p>The Blacksmith, searching for ores and minerals and carefully refining them to make weapons, has taken completeness as a principle in making these weapons and built a reputation of trust upon it. After reading the notice recruiting adventurers by King Tristan III, believing his/her weapons will help and to heighten his/her reputation as a Blacksmith, he/she decides to join the world of adventure and while engaged in battles, gathers materials to make more weapons.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop

				<table class="table table-bordered table-striped" id="cart-revolution">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-cart-revolution.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Cart Revolution</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 3.0m</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>CD</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.5s</td>
							<td>15</td>
							<td>3s</td>
							<td class="skill-description" colspan="3">Knocks back nearby enemies with the cart, dealing 350% Dmg (Up to 4 targets)</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.5s</td>
							<td>20</td>
							<td>2.5s</td>
							<td class="skill-description" colspan="3">Knocks back nearby enemies with the cart, dealing 400% Dmg (Up to 4 targets)</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.5s</td>
							<td>25</td>
							<td>2s</td>
							<td class="skill-description" colspan="3">Knocks back nearby enemies with the cart, dealing 450% Dmg (Up to 4 targets)</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="weapon-perfection">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-weapon-perfection.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Weapon Perfection</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 1.5m</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1</td>
							<td>15</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 10% more size correction to monsters of any size. Size correction cap: 100%. Last 15 sec.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1s</td>
							<td>20</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 20% more size correction to monsters of any size. Size correction cap: 100%. Last 20 sec.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1s</td>
							<td>25</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 30% more size correction to monsters of any size. Size correction cap: 100%. Last 25 sec.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1s</td>
							<td>30</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 40% more size correction to monsters of any size. Size correction cap: 100%. Last 30 sec.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1s</td>
							<td>35</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 50% more size correction to monsters of any size. Size correction cap: 100%. Last 35 sec.</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td>1s</td>
							<td>40</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 60% more size correction to monsters of any size. Size correction cap: 100%. Last 40 sec.</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td>1s</td>
							<td>45</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 70% more size correction to monsters of any size. Size correction cap: 100%. Last 45 sec.</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td>1s</td>
							<td>50</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 80% more size correction to monsters of any size. Size correction cap: 100%. Last 50 sec.</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td>1s</td>
							<td>55</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 90% more size correction to monsters of any size. Size correction cap: 100%. Last 55 sec.</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td>1s</td>
							<td>60</td>
							<td class="skill-description" colspan="3">Any weapon of any Party member gains 100% more size correction to monsters of any size. Size correction cap: 100%. Last 60 sec.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="over-thrust">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-overthrust.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Over Thrust</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 1.5m</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.5s</td>
							<td>12</td>
							<td class="skill-description" colspan="3">Increases self Atk by 50% for 110 sec. Increases the whole Party's Atk by 5%.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.5s</td>
							<td>16</td>
							<td class="skill-description" colspan="3">Increases self Atk by 75% for 120 sec. Increases the whole Party's Atk by 5%.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.5s</td>
							<td>20</td>
							<td class="skill-description" colspan="3">Increases self Atk by 100% for 130 sec. Increases the whole Party's Atk by 5%.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1.5s</td>
							<td>24</td>
							<td class="skill-description" colspan="3">Increases self Atk by 125% for 140 sec. Increases the whole Party's Atk by 5%.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1.5s</td>
							<td>28</td>
							<td class="skill-description" colspan="3">Increases self Atk by 150% for 150 sec. Increases the whole Party's Atk by 5%.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered" id="adrenaline-rush">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-adrenaline-rush.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								{!! config('app.tip') !!}
								<span class="skill-name">Adrenaline Rush</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 9.0m</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1s</td>
							<td>11</td>
							<td class="skill-description" colspan="3">In 30 sec, Grants all Party members equipped with Axe and Maces type weapins 3% attack speed. Grants 1% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1s</td>
							<td>12</td>
							<td class="skill-description" colspan="3">In 50 sec, Grants all Party members equipped with Axe and Maces type weapins 6% attack speed. Grants 2% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1s</td>
							<td>13</td>
							<td class="skill-description" colspan="3">In 70 sec, Grants all Party members equipped with Axe and Maces type weapins 9% attack speed. Grants 3% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1s</td>
							<td>14</td>
							<td class="skill-description" colspan="3">In 90 sec, Grants all Party members equipped with Axe and Maces type weapins 12% attack speed. Grants 4% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1s</td>
							<td>15</td>
							<td class="skill-description" colspan="3">In 110 sec, Grants all Party members equipped with Axe and Maces type weapins 15% attack speed. Grants 5% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td>1s</td>
							<td>16</td>
							<td class="skill-description" colspan="3">In 130 sec, Grants all Party members equipped with Axe and Maces type weapins 18% attack speed. Grants 6% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td>1s</td>
							<td>17</td>
							<td class="skill-description" colspan="3">In 150 sec, Grants all Party members equipped with Axe and Maces type weapins 21% attack speed. Grants 7% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td>1s</td>
							<td>18</td>
							<td class="skill-description" colspan="3">In 170 sec, Grants all Party members equipped with Axe and Maces type weapins 24% attack speed. Grants 8% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td>1s</td>
							<td>19</td>
							<td class="skill-description" colspan="3">In 190 sec, Grants all Party members equipped with Axe and Maces type weapins 27% attack speed. Grants 9% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td>1s</td>
							<td>20</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 11</td>
							<td>1s</td>
							<td>21</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 100.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 12</td>
							<td>1s</td>
							<td>22</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 200. </td>
						</tr>
						<tr class="breakthrough">
							<td>Level 13</td>
							<td>1s</td>
							<td>23</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 300.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 14</td>
							<td>1s</td>
							<td>24</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 400.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 15</td>
							<td>1s</td>
							<td>25</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 500.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 16</td>
							<td>1s</td>
							<td>26</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 600.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 17</td>
							<td>1s</td>
							<td>27</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 700.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 18</td>
							<td>1s</td>
							<td>28</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 800.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 19</td>
							<td>1s</td>
							<td>29</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 900.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 20</td>
							<td>1s</td>
							<td>30</td>
							<td class="skill-description" colspan="3">In 210 sec, Grants all Party members equipped with Axe and Maces type weapins 30% attack speed. Grants 10% ASPD for those equipped with other weapons.  Under Adrenaline Rush buff, self Auto Attack increaeses by 1000.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="hammerfall">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-hammer-fall.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Hammerfall</span><br>
								<span class="skill-state">Active Skill</span><br>
								<span class="skill-range">Casting Range: 1.5m</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Delay</th>
							<th>SP</th>
							<th>CD</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td>1.5s</td>
							<td>15</td>
							<td>2.5s</td>
							<td class="skill-description" colspan="3">Thumps the ground, dealing 200% Physical Dmg to nearby enemies. Has a 30% chance to stun them (up to 6 targets) for 2 sec.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td>1.5s</td>
							<td>18</td>
							<td>2.5s</td>
							<td class="skill-description" colspan="3">Thumps the ground, dealing 300% Physical Dmg to nearby enemies. Has a 40% chance to stun them (up to 6 targets) for 2 sec.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td>1.5s</td>
							<td>21</td>
							<td>2.5s</td>
							<td class="skill-description" colspan="3">Thumps the ground, dealing 400% Physical Dmg to nearby enemies. Has a 50% chance to stun them (up to 6 targets) for 2 sec.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td>1.5s</td>
							<td>24</td>
							<td>2.5s</td>
							<td class="skill-description" colspan="3">Thumps the ground, dealing 500% Physical Dmg to nearby enemies. Has a 60% chance to stun them (up to 6 targets) for 2 sec.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td>1.5s</td>
							<td>27</td>
							<td>2.5s</td>
							<td class="skill-description" colspan="3">Thumps the ground, dealing 600% Physical Dmg to nearby enemies. Has a 70% chance to stun them (up to 6 targets) for 2 sec.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered" id="improper-means">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-improper-means.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								{!! config('app.tip') !!}
								<span class="skill-name">Improper Means</span><br>
								<span class="skill-state">Passive Skill</span><br>
								<span class="skill-range red">Requires Mammonite Lvl.5*</span>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Increase 5% Zeny to the costs of money attacks, skill multiplier of money attack increases by 30%.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Increase 10% Zeny to the costs of money attacks, skill multiplier of money attack increases by 60%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Increase 15% Zeny to the costs of money attacks, skill multiplier of money attack increases by 90%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Increase 20% Zeny to the costs of money attacks, skill multiplier of money attack increases by 120%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Increase 25% Zeny to the costs of money attacks, skill multiplier of money attack increases by 150%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Increase 30% Zeny to the costs of money attacks, skill multiplier of money attack increases by 180%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Increase 35% Zeny to the costs of money attacks, skill multiplier of money attack increases by 210%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Increase 40% Zeny to the costs of money attacks, skill multiplier of money attack increases by 240%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Increase 45% Zeny to the costs of money attacks, skill multiplier of money attack increases by 270%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Increase 50% Zeny to the costs of money attacks, skill multiplier of money attack increases by 300%.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered" id="skin-tempering">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-skin-tempering.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								{!! config('app.tip') !!}
								<span class="skill-name">Skin Tempering</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 5% and neutral damage taken 1%.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 10% and neutral damage taken 2%.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 15% and neutral damage taken 3%.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 20% and neutral damage taken 4%.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 25% and neutral damage taken 5%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 26% and neutral damage taken 5.6%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 27% and neutral damage taken 6.0%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 28% and neutral damage taken 6.5%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 29% and neutral damage taken 7.0%.</td>
						</tr>
						<tr class="breakthrough">
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Reduces fire damage taken by 30% and neutral damage taken 7.5%.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="weaponry-research">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-weaponry-research.png') }}" alt="Cart Revolution Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Weaponry Reseach</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Increase Atk +4, Hit +3 for the weapon in use. Weapon Production Success Rate +0.5%</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Increase Atk +8, Hit +6 for the weapon in use. Weapon Production Success Rate +1.0%</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Increase Atk +12, Hit +9 for the weapon in use. Weapon Production Success Rate +1.5%</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Increase Atk +16, Hit +12 for the weapon in use. Weapon Production Success Rate +2.0%</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Increase Atk +20, Hit +15 for the weapon in use. Weapon Production Success Rate +2.5%</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Increase Atk +24, Hit +18 for the weapon in use. Weapon Production Success Rate +3.0%</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Increase Atk +28, Hit +21 for the weapon in use. Weapon Production Success Rate +3.5%</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Increase Atk +32, Hit +24 for the weapon in use. Weapon Production Success Rate +4.0%</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Increase Atk +36, Hit +27 for the weapon in use. Weapon Production Success Rate +4.5%</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Increase Atk +40, Hit +30 for the weapon in use. Weapon Production Success Rate +5.0%</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="weapon-making">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-weapon-making.png') }}" alt="Weapon Making Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Weapon Making</span><br>
								<span class="skill-state">Active Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 5.25% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 6.0% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 6.75% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 7.5% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 8.25% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 9.0% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 9.75% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 10.5% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 11.25% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Blacksmith learns to make attribute weapons. Skill level affects 12% success rate. The final success rate is also affected by Def, Luk and  [Weaponry Research]. After learning it, has a change to get Fuel x1 when picking up items.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="orideocon-research">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-orideocon-research.png') }}" alt="Orideocon Research Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Orideocon Research</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Blacksmith could get 0.5% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Blacksmith could get 1.0% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Blacksmith could get 1.5% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Blacksmith could get 2.0% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Blacksmith could get 2.5% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Blacksmith could get 3.0% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Blacksmith could get 3.5% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Blacksmith could get 4.0% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Blacksmith could get 4.5% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Blacksmith could get 5.0% more valuable metals by decomposing equipment. The ratio is also affected by Dex and Luk.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="skin-a-flint">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-skin-a-flint.png') }}" alt="Skin a Flint Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Skin a Flint</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">When items are not sold in Exchange, returns 10% cost for auction.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">When items are not sold in Exchange, returns 30% cost for auction.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">When items are not sold in Exchange, returns 50% cost for auction.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">When items are not sold in Exchange, returns 70% cost for auction.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">When items are not sold in Exchange, returns 90% cost for auction.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="enchanted-stone-craft">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-enchanted-stone-craft.png') }}" alt="Enchanted Stone Craft Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Enchanted Stone Craft</span><br>
								<span class="skill-state">Active Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 20.3% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 22.5% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 24.7% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 26.9% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 29.1% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 31.3% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 33.5% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 35.7% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 37.9% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Blacksmith could make magic stones with special effects. Skill level affects 40.1% success rate. The final success rate is also affected by Dex and Luk. The higher the skill level, the more the advanced formula. After learning it, has a chance to get Fuel x1 when picking up items.</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped" id="refining-expert">
					<tbody>
						<tr>
							<td width="80">
								<img src="{{ asset('frontend/img/jobs/skills/blacksmith-refining-expert.png') }}" alt="Refining Expert Skill Image">
							</td>
							<td colspan="5">
								<span class="skill-name">Refining Expert</span><br>
								<span class="skill-state">Passive Skill</span><br>
							</td>
						</tr>
						<tr>
							<th>Levels</th>
							<th>Description</th>
						</tr>
						<tr>
							<td>Level 1</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 0.5</td>
						</tr>
						<tr>
							<td>Level 2</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 1.0</td>
						</tr>
						<tr>
							<td>Level 3</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 1.5</td>
						</tr>
						<tr>
							<td>Level 4</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 2.0</td>
						</tr>
						<tr>
							<td>Level 5</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 2.5</td>
						</tr>
						<tr>
							<td>Level 6</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 3.0</td>
						</tr>
						<tr>
							<td>Level 7</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 3.5</td>
						</tr>
						<tr>
							<td>Level 8</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 4.0</td>
						</tr>
						<tr>
							<td>Level 9</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 4.5</td>
						</tr>
						<tr>
							<td>Level 10</td>
							<td class="skill-description" colspan="3">Blacksmith Dex and Luk's effect to refine success rate increases by 5.0</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection