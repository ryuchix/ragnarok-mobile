@extends('layouts.page')

@section('title', 'Mage Class, Mage Skill, Mage Skill Description in Ragnarok Mobile')
@section('description', str_limit(strip_tags('Mage Class, Mage Skill, Mage Skill Description in Ragnarok Mobile'), 160, ''))
@section('canonical', asset('mage'))

@section('content')
	<div class="post">
		<div class="row">
			<div class="col-md-12 ">
				<div class="row whitebg">
				@desktop
				<img src="{{ url('frontend/img/jobs/class/magician-min.png') }}" alt="Mage" width="280px" class="pull-left">
				<div class="jobs">
					<h1>Mage</h1>
					<span>1st Job</span><br>

					<p>With much knowledge and intelligence, he/she was able to figure out ways to overpower an enemy by learning about the enemy ahead of time and planing appropriately - and with this ability has overcome many different situations and obstacles. One day, after reading the notice recruiting adventurers by the king, feeling the necessity of a Mage in any adventure, he/she decides to join the of adventure. Others always see the studying and researching image of him/her, and because of the ability to think straightforwardly and make cold, but necessary judgements, colleagues tend to think that he/she has 'no emotions'. However, in real life he/she is a sincere person who likes flowers.</p>
				</div>
				{!! config('app.tips') !!}
				@elsedesktop
				<div style="text-align: left;">
				<img src="{{ url('frontend/img/jobs/class/magician-min.png') }}" alt="Mage" width="300px"></div>
				<div class="jobs">
					<h1>Mage</h1>
					<span>1st Job</span><br>

					<p>With much knowledge and intelligence, he/she was able to figure out ways to overpower an enemy by learning about the enemy ahead of time and planing appropriately - and with this ability has overcome many different situations and obstacles. One day, after reading the notice recruiting adventurers by the king, feeling the necessity of a Mage in any adventure, he/she decides to join the of adventure. Others always see the studying and researching image of him/her, and because of the ability to think straightforwardly and make cold, but necessary judgements, colleagues tend to think that he/she has 'no emotions'. However, in real life he/she is a sincere person who likes flowers.</p>
				</div>
				{!! config('app.tips') !!}
				@enddesktop
					<table class="table table-bordered table-striped" id="soul-strike">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/mage-soul-strike.png') }}" alt="Soul Strike Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Soul Strike</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 0.5s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>CD</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.2s</td>
								<td>9</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*160%) natural M.Dmg to the target. If the target is undead, Deals 5% more damage</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.4s</td>
								<td>7</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*160%) natural M.Dmg to the target. If the target is undead, Deals 10% more damage</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.6s</td>
								<td>12</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*220%) natural M.Dmg to the target. If the target is undead, Deals 15% more damage</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.8s</td>
								<td>10</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*220%) natural M.Dmg to the target. If the target is undead, Deals 20% more damage</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>2.0s</td>
								<td>15</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*280%) natural M.Dmg to the target. If the target is undead, Deals 25% more damage</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.2s</td>
								<td>13</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*280%) natural M.Dmg to the target. If the target is undead, Deals 30% more damage</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.4s</td>
								<td>18</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*340%) natural M.Dmg to the target. If the target is undead, Deals 35% more damage</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.6s</td>
								<td>16</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*340%) natural M.Dmg to the target. If the target is undead, Deals 40% more damage</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.8s</td>
								<td>21</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*400%) natural M.Dmg to the target. If the target is undead, Deals 45% more damage</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>3.0s</td>
								<td>19</td>
								<td>2s</td>
								<td class="skill-description" colspan="3">Summons holy spirits to attack, dealing (M.Atk*400%) natural M.Dmg to the target. If the target is undead, Deals 50% more damage</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="fire-bolt">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/mage-fire-bolt.png') }}" alt="Fire Bolt Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Fire Bolt</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>0.8s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk100%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.2s</td>
								<td>1.2s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk200%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.4s</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk300%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.6s</td>
								<td>2.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk400%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.8s</td>
								<td>2.4s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk500%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>2.8s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk600%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.2s</td>
								<td>3.2s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk700%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.4s</td>
								<td>3.6s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk800%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.6s</td>
								<td>4.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk900%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk1000%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk1100%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk1200%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk1300%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>76</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk1400%) fire M.Dmg and low real M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>82</td>
								<td class="skill-description" colspan="3">Attacks an enemy with fire arrows from the sky, dealing (M.Atk1500%) fire M.Dmg and low real M.Dmg</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="ring-of-fire">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/mage-ring-of-fire.png') }}" alt="Ring of Fire Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Ring of Fire</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires Mage Lv.15</span><br>
									<span class="skill-range red">Requires <a href="#fire-bolt">Fire Bolt</a> Lv.5*</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.5s</td>
								<td>2.0s</td>
								<td>12</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 5 times.</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.5s</td>
								<td>1.8s</td>
								<td>14</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 6 times.</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.5s</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 7 times.</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.5s</td>
								<td>1.4s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 8 times.</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.5s</td>
								<td>1.2s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 9 times.</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.5s</td>
								<td>1.1s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 10 times.</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.5s</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 11 times.</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.5s</td>
								<td>0.9s</td>
								<td>26</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 12 times.</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.5s</td>
								<td>0.8s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 13 times.</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.5s</td>
								<td>0.7s</td>
								<td>30</td>
								<td class="skill-description" colspan="3">Blocks enemies with a fire circle wall, dealing (M.Atk50%) fire M.Dmg to enemy units who try to get through it. Lasts 15 sec. The wall could block 14 times.</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="increase-spiritual-recovery">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/mage-increase-spiritual-recovery.png') }}" alt="Increase Spiritual Recovery Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Increase Spiritual Recovery</span><br>
									<span class="skill-state">Passive Skill</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (3+0.2% Max SP) SP and increases SP granted by items by 5%</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (6+0.4% Max SP) SP and increases SP granted by items by 10%</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (9+0.6% Max SP) SP and increases SP granted by items by 15%</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (12+0.8% Max SP) SP and increases SP granted by items by 20%</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (15+1.0% Max SP) SP and increases SP granted by items by 25%</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (18+1.2% Max SP) SP and increases SP granted by items by 30%</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (21+1.4% Max SP) SP and increases SP granted by items by 35%</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (24+1.6% Max SP) SP and increases SP granted by items by 40%</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (27+1.8% Max SP) SP and increases SP granted by items by 45%</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td class="skill-description" colspan="3">Every 10 sec recovers (30+2.0% Max SP) SP and increases SP granted by items by 50%</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="cold-bolt">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/mage-cold-bolt.png') }}" alt="Cold Bolt Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Cold Bolt</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires Mage Lv.13</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>0.8s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk100%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.2s</td>
								<td>1.2s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk200%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.4s</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk300%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.6s</td>
								<td>2.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk400%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.8s</td>
								<td>2.4s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk500%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>2.8s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk600%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.2s</td>
								<td>3.2s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk700%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.4s</td>
								<td>3.6s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk800%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.6s</td>
								<td>4.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk900%) water M.Dmg</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk1000%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk1100%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk1200%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk1300%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>76</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk1400%) water M.Dmg</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>82</td>
								<td class="skill-description" colspan="3">Attacks an enemy with ice arrows from the sky, dealing (M.Atk1500%) water M.Dmg</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered table-striped" id="frost-diver">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/mage-frost-diver.png') }}" alt="Frost Diver Skill Image">
								</td>
								<td colspan="5">
									<span class="skill-name">Frost Diver</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range">Cast Time: 0.8s</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>25</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*110%) water M.Dmg to the target. Has a 38% chance to freeze it for 3 sec</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.0s</td>
								<td>24</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*120%) water M.Dmg to the target. Has a 41% chance to freeze it for 6 sec</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.0s</td>
								<td>23</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*130%) water M.Dmg to the target. Has a 44% chance to freeze it for 9 sec</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.0s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*140%) water M.Dmg to the target. Has a 47% chance to freeze it for 12 sec</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.0s</td>
								<td>21</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*150%) water M.Dmg to the target. Has a 50% chance to freeze it for 15 sec</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>1.0s</td>
								<td>20</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*160%) water M.Dmg to the target. Has a 53% chance to freeze it for 18 sec</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>1.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*170%) water M.Dmg to the target. Has a 56% chance to freeze it for 21 sec</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>1.0s</td>
								<td>18</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*180%) water M.Dmg to the target. Has a 59% chance to freeze it for 24 sec</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>1.0s</td>
								<td>17</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*190%) water M.Dmg to the target. Has a 62% chance to freeze it for 27 sec</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>1.0s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deals (M.Atk*200%) water M.Dmg to the target. Has a 65% chance to freeze it for 30 sec</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered" id="lightning-bolt">
						<tbody>
							<tr>
								<td width="80">
									<img src="{{ asset('frontend/img/jobs/skills/mage-lighting-bolt.png') }}" alt="Lightning Bolt Skill Image">
								</td>
								<td colspan="5">
									{!! config('app.tip') !!}
									<span class="skill-name">Lightning Bolt</span><br>
									<span class="skill-state">Active Skill</span><br>
									<span class="skill-range">Casting Range: 6.0m</span><br>
									<span class="skill-range red">Requires Mage Lv.18</span>
								</td>
							</tr>
							<tr>
								<th>Levels</th>
							   	<th>Delay</th>
							   	<th>Time</th>
							   	<th>SP</th>
							   	<th>Description</th>
							</tr>
							<tr>
								<td>Level 1</td>
								<td>1.0s</td>
								<td>0.8s</td>
								<td>10</td>
								<td class="skill-description" colspan="3">Deals (M.Atk100%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 2</td>
								<td>1.2s</td>
								<td>1.2s</td>
								<td>13</td>
								<td class="skill-description" colspan="3">Deals (M.Atk200%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 3</td>
								<td>1.4s</td>
								<td>1.5s</td>
								<td>16</td>
								<td class="skill-description" colspan="3">Deals (M.Atk300%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 4</td>
								<td>1.6s</td>
								<td>2.0s</td>
								<td>19</td>
								<td class="skill-description" colspan="3">Deals (M.Atk400%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 5</td>
								<td>1.8s</td>
								<td>2.4s</td>
								<td>22</td>
								<td class="skill-description" colspan="3">Deals (M.Atk500%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 6</td>
								<td>2.0s</td>
								<td>2.8s</td>
								<td>28</td>
								<td class="skill-description" colspan="3">Deals (M.Atk600%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 7</td>
								<td>2.2s</td>
								<td>3.2s</td>
								<td>34</td>
								<td class="skill-description" colspan="3">Deals (M.Atk700%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 8</td>
								<td>2.4s</td>
								<td>3.6s</td>
								<td>40</td>
								<td class="skill-description" colspan="3">Deals (M.Atk800%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 9</td>
								<td>2.6s</td>
								<td>4.0s</td>
								<td>46</td>
								<td class="skill-description" colspan="3">Deals (M.Atk900%) wind M.Dmg to the target</td>
							</tr>
							<tr>
								<td>Level 10</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>52</td>
								<td class="skill-description" colspan="3">Deals (M.Atk1000%) wind M.Dmg to the target</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 11</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>58</td>
								<td class="skill-description" colspan="3">Deals (M.Atk1100%) wind M.Dmg to the target</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 12</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>64</td>
								<td class="skill-description" colspan="3">Deals (M.Atk1200%) wind M.Dmg to the target</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 13</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>70</td>
								<td class="skill-description" colspan="3">Deals (M.Atk1300%) wind M.Dmg to the target</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 14</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>76</td>
								<td class="skill-description" colspan="3">Deals (M.Atk1400%) wind M.Dmg to the target</td>
							</tr>
							<tr class="breakthrough">
								<td>Level 15</td>
								<td>2.8s</td>
								<td>4.4s</td>
								<td>82</td>
								<td class="skill-description" colspan="3">Deals (M.Atk1500%) wind M.Dmg to the target</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	@endsection

	@section('script')
	<script type="text/javascript">
	$("[data-rel=popover]").hover(function(){
		$(this).popover('toggle');
	});
	</script>
	@endsection