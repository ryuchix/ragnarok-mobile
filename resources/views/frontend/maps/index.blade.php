@extends('layouts.page')

@section('title', 'Lists of Maps in Ragnarok Mobile Eternal Love')
@section('description', str_limit(strip_tags('See All Maps in Ragnarok Mobile Eternal Love and Monster locations and drops.'), 160, ''))
@section('canonical', url('maps'))

@section('banner')
<div id="banner">
	<div class="container intro_wrapper">
		<div class="inner_content">
			<h1 class="title">Maps</h1>
			<h1 class="intro">
				Here are the list of Maps in Ragnarok Mobile. Find the location of your target monsters and loots.
			</h1>
		</div>
	</div>
</div>
<!--//banner-->
@endsection

@section('content')
	<ul class="dropped-items-ul" style="padding: 0px 10px 0px 10px;">
		@foreach($maps as $map)
		<li class="dropped-li">
			<div class="is-clearfix">
				<a class="pull-left" href="{{ url('map'.'/'.$map->slug) }}">
					<img class="is75px75px" src="{{ url('uploads/images/maps/small'.'/'.$map->image) }}">
				</a>
				<div class="is-pulled-left">
					<a href="{{ url('map'.'/'.$map->slug) }}">{{ $map->name }}</a>
					<p>{{ $map->type }}<br>
						{{ $map->content }}<br>
						<span></span><br>
					</p>
				</div>
			</div>
		</li>
		@endforeach
	</ul>
@endsection