@extends('layouts.page')

@section('title', $map->name .', ' . $map->name .' NPC, ' . $map->name . ' monsters ' . 'in Ragnarok Mobile')
@section('description', str_limit(strip_tags('See all ' . $map->name . 'monsters, '.$map->name . ' npcs.' . ' Ragnarok maps database, Ragnarok map map, What monsters in the map, Search Ragnarok maps. Show Ragnarok maps.'), 160, ''))
@section('canonical', url($map->slug))

@section('content')

<div class="post">
	<div class="row whitebg">
		<div class="col-md-12">
			<img src="{{ asset('uploads/images/maps/small/' . $map->image)}}" alt="" class="pull-left" width="100px">
			<div class="is-pulled-left">
				<h1 style="margin-top: 0px; margin-bottom: -5px">{{ $map->name }}</h1>
				<p><span>{{ $map->type }}</span></p>
			</div>
			<br>
			<div class="clear"></div>
			<div class="alert alert-info map-alert"><h3>{!! $map->content !!}</h3></div>
			<div class="clear"></div>
			<img src="{{ asset('uploads/images/maps/small/' . $map->image)}}" alt="" class="pull-right" width="400px">
			@desktop

			@elsedesktop
			<div class="clear"></div>
			@enddesktop
			@if(count($map_monsters) > 0)
			<h2>Monsters</h2>
			<ul class="dropped-items-ul">
				@foreach ($map_monsters as $map_monster)
				<li class="dropped-li">
					<div class="is-clearfix">
						<a class="pull-left" href="{{ $map_monster == null ? '' : url('monster').'/'.$map_monster->slug }}">
							<img class="is75px75px" src="{{ $map_monster->big_image == null ? asset('frontend/img/noimage.png') : url('uploads/images/monsters/large').'/'.$map_monster->big_image }}">
						</a>
						<div class="is-pulled-left">
							<a href="{{ $map_monster == null ? '' : url('monster').'/'.$map_monster->slug }}">{{ $map_monster == null ? '' : $map_monster->name }}</a>
							<p><span>{{ $map_monster == null ? '' : 'Level' }}</span>
								<span>{{ $map_monster == null ? '' : $map_monster->level }}</span><br>
								<span>{{ $map_monster == null ? '' : $map_monster->type['name'] }}</span>
							</p>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
			@endif

		</div>
   	</div>
</div>

@endsection