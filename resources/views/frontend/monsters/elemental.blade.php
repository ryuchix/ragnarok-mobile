<table class="table table-bordered">
<!-- neutral -->
@if($monster->element_id == 1)
	@include('frontend.monsters.neutral')
@endif
<!-- fire -->
@if($monster->element_id == 4)
	@include('frontend.monsters.fire')
@endif
<!-- water -->
@if($monster->element_id == 2)
	@include('frontend.monsters.water')
@endif
<!-- wind -->
@if($monster->element_id == 6)
	@include('frontend.monsters.wind')
@endif
<!-- earth -->
@if($monster->element_id == 3)
	@include('frontend.monsters.earth')
@endif
<!-- holy -->
@if($monster->element_id == 8)
	@include('frontend.monsters.holy')
@endif
<!-- shadow -->
@if($monster->element_id == 9)
	@include('frontend.monsters.shadow')
@endif
<!-- ghost -->
@if($monster->element_id == 10)
	@include('frontend.monsters.ghost')
@endif
<!-- undead -->
@if($monster->element_id == 11)
	@include('frontend.monsters.undead')
@endif
<!-- poison -->
@if($monster->element_id == 7)
	@include('frontend.monsters.poison')
@endif
</table>