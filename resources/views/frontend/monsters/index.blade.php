@extends('layouts.page')

@section('title', 'Ragnarok Monsters, Monsters Drop, Monsters Location, Monster Element for Ragnarok Mobile')
@section('description', str_limit(strip_tags('Search Ragnarok monster by name element, race, size, job experience and base experience. Show Ragnarok monster and their location, spawn time, 100% hit, 95% flee, attack speed, walk speed, range, item drop, stats, properties, size, race, mode and other information'), 160, ''))
@section('canonical', asset('monsters/'))

@section('banner')
<div id="banner">
	<div class="container intro_wrapper">
		<div class="inner_content">
			<h1 class="title">Monsters</h1>
			<h1 class="intro">
				Here you can find Ragnarok Monsters and their information. You can use the <strong>search</strong> function to find the specific monster.
			</h1>
		</div>
	</div>
</div>
<!--//banner-->
@endsection
@section('content')

<form action="{{ url('monsters') }}" method="get">        
  <div class="box-body" >
    <div class="form-group">
      @desktop
      <input type="text" name="search" style="width: 30%; margin-bottom: 5px" placeholder="Search monster" value="{{ isset($_GET['search']) != null ? $_GET['search'] : "" }}">
      <button type="submit" class="btn btn-primary btn-custom btn-rounded">Submit</button>
      <a class="btn btn-info btn-custom btn-rounded advanced-d">Advanced</a>
      <div id="advanced-d" style="display: {{ isset($_GET['search']) ? "" : "none" }}">
        <!-- races -->
        <select name="race" id="filter-race" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Races</option>
          @foreach ($races as $race)
          <option value="{{ $race->id }}">{{ $race->name }}</option>
          @endforeach
        </select>
        <!-- elements -->
        <select name="element" id="filter-element" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Elements</option>
          @foreach ($elements as $element)
          <option value="{{ $element->id }}">{{ $element->name }}</option>
          @endforeach
        </select>        
        <!-- sizes -->
          <select name="size" id="filter-size" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Sizes</option>
          @foreach ($sizes as $size)
          <option value="{{ $size->id }}">{{ $size->name }}</option>
          @endforeach
        </select>
        <!-- sort by -->
          <select name="sort" id="sortby" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;display:none">
          <option value="">Sort by</option>
            <option value="name">Name</option>
            <option value="level" >Level</option>
            <option value="hp">HP</option>
            <option value="base_exp">Base exp</option>
            <option value="job_exp">Job exp</option>
        </select>
        <!-- sort by -->
          <select name="order" id="orderby" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;display:none">
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
        </select>
      </div>
    </div>
      @elsedesktop
        <input type="text" name="search" style="width: 100%; margin-bottom: 5px" placeholder="Search monster"  value="{{ isset($_GET['search']) != null ? $_GET['search'] : "" }}">
      <div id="advanced" style="display: none;">
        <!-- races -->
        <select name="race" id="filter-race" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Races</option>
          @foreach ($races as $race)
          <option value="{{ $race->id }}">{{ $race->name }}</option>
          @endforeach
        </select>
        <!-- elements -->
        <select name="element" id="filter-element" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Elements</option>
          @foreach ($elements as $element)
          <option value="{{ $element->id }}">{{ $element->name }}</option>
          @endforeach
        </select>        
        <!-- sizes -->
          <select name="size" id="filter-size" style="-webkit-appearance: menulist-button;height: 43px;border: 2px solid #d8d3cb;color: #888;">
          <option value="">All Sizes</option>
          @foreach ($sizes as $size)
          <option value="{{ $size->id }}">{{ $size->name }}</option>
          @endforeach
        </select>
      </div>
    </div>
        <div class="form-group" style="margin-top: -10px">
          <button type="submit" class="btn btn-primary btn-custom btn-rounded">Submit</button>
          <a class="btn btn-info btn-custom btn-rounded advanced">Advanced</a>
        </div>
      @enddesktop
  </div>
</form>

@desktop
<div class="table-responsive">
    <table class="table">
      <tbody>
        @if (count($monsters) > 0)
      	@php $count = 1 @endphp
      	@foreach ($monsters as $monster)
      	<tr>
      		<td><a href="{{ url('monster/' . $monster->slug) }}"><img src="{{ ($monster->big_image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/monsters/large/' . $monster->big_image) }}"></a></td>
      		<td style="vertical-align: middle;"><a href="{{ url('monster/' . $monster->slug) }}">{{ $monster->name }}</a> @auth <br><a href="{{ url('admin/monsters').'/'.$monster->id.'/edit' }}"><i class="fa fa-pencil"></i> Edit</a> @endauth</td>
      		<td style="vertical-align: middle;">{{ $monster->element['name'] }}</td>
      		<td style="vertical-align: middle;">{{ $monster->race['name'] }}</td>
      		<td style="vertical-align: middle;">{{ $monster->size['name'] }}</td>
      	</tr>
      	@php $count ++ @endphp
      	@endforeach
        @else
        <p style="padding-top: 20px">No result found</p>
        @endif
      </tbody>
    </table>
  </div>
  @elsedesktop
  @if (count($monsters) > 0)
   @foreach ($monsters as $monster)
              <div class="whitebg-2">

              <a class="pull-left" href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">
                <img class="is75px75px" src="{{ $monster->big_image == null ? asset('frontend/img/noimage.png') : url('uploads/images/monsters/large').'/'.$monster->big_image }}">
              </a>
              <div class="is-pulled-left">
                <a href="{{ $monster == null ? '' : url('monster').'/'.$monster->slug }}">{{ $monster == null ? '' : $monster->name }}</a>
                <p>{{ $monster == null ? '' : 'Level' }}
                  {{ $monster == null ? '' : $monster->level }} / Base: {{ $monster->base_exp }} / Job: {{ $monster->job_exp }}<br>
                  {{ $monster->element['name'] }} / {{ $monster->race['name'] }} / {{ $monster->size['name'] }}
                </p>
              </div>
 

</div>
 @endforeach
 @else
<div class="whitebg-2">
  <p style="padding-top: 10px">No result found</p>
</div>
 @endif
 @enddesktop

  <div style="text-align: center"> {!! $monsters->appends(Input::except('page'))->links('vendor.pagination.default') !!}</div>

  @endsection

  @section('script')
<script>

      $('.advanced').on('click', function(){
        $('#advanced').toggle();
      });
      $('.advanced-d').on('click', function(){
        $('#advanced-d').toggle();
      });
      
    //   $('#sortby').change(function() {
    //     $('#orderby').show();
    //   });
      
      $(document).ready(function(){
          
        var url_string = window.location.href;
        var url = new URL(url_string);
        var c = url.searchParams.get("sort");
        var d = url.searchParams.get("order");
        var e = url.searchParams.get("size");
        var f = url.searchParams.get("race");
        var g = url.searchParams.get("element");
    
        $('#sortby option[value="'+c+'"]').prop('selected', true);
        $('#orderby option[value="'+d+'"]').prop('selected', true);
        $('#filter-size option[value="'+e+'"]').prop('selected', true);
        $('#filter-race option[value="'+f+'"]').prop('selected', true);
        $('#filter-element option[value="'+g+'"]').prop('selected', true);
    
        
          
          
          
          
          
          
          
      });
</script>
  @endsection