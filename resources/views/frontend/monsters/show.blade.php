@extends('layouts.page')

@section('title', $monster->name .', ' . $monster->name .' location, ' . $monster->name . ' drops '. $monster->name . ' elements ' .  'for Ragnarok Mobile')
@section('description', str_limit(strip_tags($monster->name .', ' . 'Show ' .$monster->name .' location, ' . 'get ' .$monster->name . ' drops '. 'What is ' .$monster->name . ' elements. '.$monster->name . ' card effect'), 160, ''))
@section('canonical', asset('monster/'.$monster->slug))

@section('banner')
<!-- <div id="banner">
	<div class="container intro_wrapper">
		<div class="inner_content">
			<h1 class="title">{{ $monster->name }}</h1>
			<h1 class="intro">
				{!! $monster->content !!}
			</h1>
		</div>
	</div>
</div> -->
<!--//banner-->
@endsection

@section('content')
<div class="post">
	<div class="row whitebg">
		<div class="col-md-12">
			<img src="{{ asset('uploads/images/monsters/large/' . $monster->big_image)}}" alt="" class="pull-left">
			<h1>{{ $monster->name }}</h1>
			<div class="clear"></div>
			<br>
			<div class="alert alert-info"><h3>{!! $monster->content !!}</h3></div>
			<div class="col-md-6">
			<h3><strong>Basic Information</strong></h3>
				<table class="table table-bordered table-striped">
					<tr>
						<td>Level</td>
						<td>{{ $monster->level }}</td>
					</tr>
					<tr>
						<td>Type</td>
						<td><a href="{{ url('monsters'.'/'.$monster->type['name']) }}">{{ $monster->type['name'] }}</a></td>
					</tr>
					<tr>
						<td>Zone</td>
						<td>{{ $monster->zone['name'] }}</td>
					</tr>
					<tr>
						<td>Race</td>
						<td>{{ $monster->race['name'] }}</td>
					</tr>
					<tr>
						<td>Element</td>
						<td>{{ $monster->element['name'] }}</td>
					</tr>
					<tr>
						<td>Size</td>
						<td>{{ $monster->size['name'] }}</td>
					</tr>
				</table>
				<div class="clear"></div>
				<h3><strong>Attributes</strong></h3>
				<table class="table table-bordered table-striped">
					<tr>
						<td>Atk</td>
						<td>{{ $monster->atk }}</td>
					</tr>
					<tr>
						<td>MAtk</td>
						<td>{{ $monster->matk }}</td>
					</tr>
					<tr>
						<td>Def</td>
						<td>{{ $monster->def }}</td>
					</tr>
					<tr>
						<td>MDef</td>
						<td>{{ $monster->mdef }}</td>
					</tr>
					<tr>
						<td>Hp</td>
						<td>{{ number_format($monster->hp) }}</td>
					</tr>
					<tr>
						<td>Hit</td>
						<td>{{ $monster->hit }}</td>
					</tr>
					<tr>
						<td>Flee</td>
						<td>{{ $monster->flee }}</td>
					</tr>
					<tr>
						<td>Move Speed</td>
						<td>{{ $monster->movement_speed }}</td>
					</tr>
					<tr>
						<td>Atk Speed</td>
						<td>{{ $monster->atk_speed }}</td>
					</tr>
					<tr>
						<td>Base Exp</td>
						<td>{{ number_format($monster->base_exp) }}</td>
					</tr>
					<tr>
						<td>Job Exp</td>
						<td>{{ number_format($monster->job_exp) }}</td>
					</tr>
				</table>
				<div class="clear"></div>
				@if(count($items) > 0)
				<h3><strong>Drops</strong></h3>
				<ul class="fa-ul" style="margin-left: 0px">
					@if($monster->slug != "hydra" && $monster->slug != "goblinknife" && $monster->slug != "goblinbuckler" && $monster->slug != "goblinhammer" && $monster->slug != "goblinspear" && $monster->slug != "goblinaxe" && $monster->slug != "marc" && $monster->slug != "deniro" && $monster->slug != "piere" && $monster->slug != "horong" && $monster->slug != "sohee-1" && $monster->slug != "rideword-1" && $monster->slug != "baphomet-jr-1" && $monster->slug != "alarm-1" && $monster->slug != "bathory-1")
					<li style="padding-bottom: 10px;"> <a href="{{ url('card/'.$monster->card['slug']) }}"><img src="{{ asset('frontend/img/default-card.png') }}" alt="" width="25px" height="25px"> {{ $monster->card['name'] }}</a></li>
					@endif
					@foreach ($items as $item)
					<li style="padding-bottom: 10px;"> <a href="{{ url('item').'/'.$item->slug }}"><img src="{{ ($item->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $item->image) }}" alt="" width="25px" height="25px"> {{ $item->name }}</a></li>
					@endforeach
				</ul>
				@endif
			</div>
			<div class="col-md-6">
				<h3><strong>Elemental Converter</strong></h3>

					@include('frontend.monsters.elemental')

				@if($monster->card_id != null)
				<h3><strong>Card Effect</strong></h3>

				<!-- <img src="{{ asset('uploads/images/cards/small/' . $monster->card['image']) }}"> -->
				<img src="{{ $monster->card['image'] == null ? asset('frontend/img/default-no-mons-image.jpg') : asset('uploads/images/cards/small/' . $monster->card['image']) }}">
				<br>
				@endif
				<h4><strong><a href="{{ url('card/'.$monster->card['slug']) }}">{{ $monster->card['name'] }}</a></strong></h4>
				<table class="table table-bordered table-striped">
					<tr>
						<td style="vertical-align: middle;">Main Effect</td>
						<td>{!! nl2br($monster->card['content']) !!}</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;">Draw/Loot/Craft Effect</td>
						<td>{!! $monster->card['buff'] !!}</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;">Deposit Effect</td>
						<td>{!! $monster->card['deposit'] !!}</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;">Slot</td>
						<td>{{ $monster->card['equipable'] }}</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;">Type</td>
						<td>{{ $monster->card['type'] }}</td>
					</tr>
				</table>
				<div class="clear"></div>
				<h3><strong>On Maps</strong></h3>
				
				@foreach ($maps as $map)
				<a href="{{ url('map').'/'.$map->slug }}"><img src="{{ url('uploads/images/maps/small').'/'.$map->image }}"> <br><strong>{{ $map->name }}</strong></a>
				@endforeach
				
				<div class="clear"></div>
			</div>
		</div>

	</div>
</div>

@endsection