
@section('header')
<style type="text/css">
	.group {
		position: relative;
	}
	.group h3 {
		padding: 5px 10px;
	    background-color: #f3f3f3;
	    color: #8cc152;
	    font-weight: normal;
	    line-height: 1.5;
	}
	.dl.has_pic {
		    min-height: 40px;
    padding-left: 60px;
	}
	.dl.has_pic dt{
		    margin-bottom: 2px;
    color: #e9573f;
	}














</style>
@endsection

<h4><span>Events</span></h4>


<ul class="dropped-items-ul">
    @foreach ($events as $event)
    <div class="event-container">
		<img style="float: left; margin: 0px 15px 15px 0px; width: 20px; height: 20px" src="{{ $event->image == null ? asset('frontend/img/noimage.png') : url('uploads/images/events/small').'/'.$event->image }}" />

		<span style="font-weight: 300; font-size: 14px; position: relative; top: -5px">{{ str_limit($event->name, 35) }}</span><br>
		@php
			$endDate = \Carbon\Carbon::parse($event->end_date);
			$startDate = \Carbon\Carbon::parse($event->start_date)
		@endphp
		<span class="checkDate" style="float: right; font-weight: 200; font-size: 12px; position: relative; top: -12px; color: #888;" data-countdown="{{ $endDate->format('M d Y H:i:s') }}" data-start="{{ $startDate->format('M d Y H:i:s') }}"></span>
		<span style="font-weight: 200; font-size: 12px; position: relative; top: -12px; color: #888;">{{ $event->location }}</span>
		<br style="clear: both;" />
	</div>
    @endforeach
</ul>
