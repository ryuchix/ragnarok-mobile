@extends('layouts.master')
@section('content')
	<!-- Content Header (Page header) -->
	<section class="content-header">              
		<div class="pull-right">
			<a href="{{ url('admin/elements/create') }}" type="button" class="btn bg-purple"><i class="fa fa-plus"></i> Add Element</a>
		</div>
		<h1>Element<small>List of elements</small></h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header "></div>
					<div class="box-body">
						@include('admin.elements.table')
					</div>
					@include('admin.errors.error')
					@include('admin.messages.message')
				</div>
			</div>
		</div>
	</section>
	@include('admin.modals.modal')
@endsection