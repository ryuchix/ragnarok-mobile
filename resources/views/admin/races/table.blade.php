<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($races as $race)
		<tr>
			<td>{{ $count }}</td>
			<td>{{ $race->name }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ route('races.show', $race->id) }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('races.edit', $race->id) }}">Edit</a>
			    {!! Form::model($race ,['route' => ['races.destroy', $race->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>