<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($cards as $card)
		<tr>
			<td>{{ $count }}</td>
			<td><img src="{{ ($card->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/cards/small/' . $card->image) }}" width="60px" height="60px"></td>
			<td>{{ $card->name }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ url('card').'/'.$card->slug }}" target="_blank">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('cards.edit', $card->id) }}">Edit</a>
			    {!! Form::model($card ,['route' => ['cards.destroy', $card->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>
{{ $cards->links() }}