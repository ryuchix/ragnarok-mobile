@extends('layouts.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Update Card<small></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <!-- left column -->
          <div class="col-md-6">
                <div class="box box-primary">
                  <!-- form start -->
                  {!! Form::model($card,['route' => ['cards.update', $card->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}
                      <div class="box-body">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ $card->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Main Effect</label>
                            <textarea name="content" class="textarea" placeholder="Place some text here"
                                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $card->content }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="email">Loot Draw Effect</label>
                            <textarea name="buff" class="textarea" placeholder="Place some text here"
                                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $card->buff }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="email">Deposit Effect</label>
                            <textarea name="deposit" class="textarea" placeholder="Place some text here"
                                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $card->deposit }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="exampleInputFile" name="image">
                            <p class="help-block">Thumbnail</p>
                        </div>
                      </div>
                </div>
        </div>
        <div class="col-md-6">
                <div class="box box-primary">
                  <!-- form start -->
                      <div class="box-body">
                        <div class="form-group">
                          <label for="name">Type</label>
                          <input type="text" class="form-control" name="type" placeholder="Enter type" value="{{ $card->type }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Equipable</label>
                          <input type="text" class="form-control" name="equipable" placeholder="Enter equipable" value="{{ $card->equipable }}">
                        </div><!-- 
                        <div class="form-group">
                          <label>Monster</label>
                          <select name="monster_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select Monster</option>
                            @foreach ($monsters as $monster)
                            <option value="{{ $monster->id }}" {{ ($monster->id == $card->monster_id) ? 'selected' : '' }}>{{ $monster->name }}</option>
                            @endforeach
                          </select>
                        </div> -->
                      </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  {!! Form::close() !!}
                </div>
              @include('admin.errors.error')
              @include('admin.messages.message')
        </div>
        </div>
    </section>
@endsection

@section('footer')
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    $('.select2').select2()
  })
</script>
@endsection