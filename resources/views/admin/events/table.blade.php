<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Desription</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($events as $event)
		<tr>
			<td>{{ $count }}</td>
			<td><img src="{{ ($event->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/events/small/' . $event->image) }}" width="60px" height="60px"></td>
			<td>{{ $event->name }}</td>
			<td>{!! str_limit($event->description,100) !!} </td>
			<td>{{ $event->start_date }}</td>
			<td>{{ $event->end_date }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ url('event').'/'.$event->slug }}" target="_blank">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('events.edit', $event->id) }}">Edit</a>
			    {!! Form::model($event ,['route' => ['events.destroy', $event->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Description</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>
{{ $events->links() }}