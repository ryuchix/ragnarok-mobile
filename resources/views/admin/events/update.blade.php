@extends('layouts.master')
@section('header')
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Add Event<small></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <!-- left column -->
          <div class="col-md-6">
                <div class="box box-primary">
                  <!-- form start -->
                      {!! Form::model($event,['route' => ['events.update', $event->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}
                      <div class="box-body">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" name="name" placeholder="Enter name" required value="{{ $event->name }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Sub Name</label>
                          <input type="text" class="form-control" name="sub_name" placeholder="Enter sub name" value="{{ $event->sub_name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Description</label>
                  <textarea name="description" class="textarea" placeholder="Place some text here"
                                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $event->description }}</textarea>
                    </div>
                        <div class="form-group">
                          <label for="name">Start Date</label>
                  <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="start_date" name="start_date" value="{{ $event->start_date }}">
                          </div>
                      </div>
                        <div class="form-group">
                          <label for="name">End Date</label>
                  <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="end_date" name="end_date" value="{{ $event->end_date }}">
                          </div>
                      </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="exampleInputFile" name="image">
                            <p class="help-block">Thumbnail</p>
                        </div>
                      </div>
                </div>
        </div>
        <div class="col-md-6">
                <div class="box box-primary">
                  <!-- form start -->
                      <div class="box-body">
                        <div class="form-group">
                          <label for="name">Location</label>
                          <input type="text" class="form-control" name="location" placeholder="Enter location"  value="{{ $event->location }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Type</label>
                          <input type="text" class="form-control" name="type" placeholder="Enter type"  value="{{ $event->type }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Parent Event</label>
                          <input type="text" class="form-control" name="parent_event" placeholder="Enter parent_event" value="{{ $event->parent_event }}" >
                        </div>
                      </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  {!! Form::close() !!}
                </div>
              @include('admin.errors.error')
              @include('admin.messages.message')
        </div>
        </div>
    </section>
@endsection

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    $('.select2').select2()
    CKEDITOR.replace('description')

    $('#end_date').datetimepicker({format: 'YYYY-MM-DD h:mm:00'});
    $('#start_date').datetimepicker({format: 'YYYY-MM-DD h:mm:00'});
  })
</script>
@endsection