@extends('layouts.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<h1>Add Map<small></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      	<div class="row">
        <!-- left column -->
        	<div class="col-md-6">
	          	<div class="box box-primary">
		            <!-- form start -->
		            {!! Form::open([ 'route' => 'maps.store', 'enctype' => 'multipart/form-data'])!!}
	              	<div class="box-body">
		                <div class="form-group">
		                  <label for="name">Name</label>
		                  <input type="text" class="form-control" name="name" placeholder="Enter name" required>
		                </div>
                    <div class="form-group">
                      <label for="email">Description</label>
                      <textarea name="content" class="textarea" placeholder="Place some text here"
                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="name">Type</label>
                      <input type="text" class="form-control" name="type" placeholder="Enter type" >
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile" name="image">
                      <p class="help-block">Thumbnail</p>
                    </div>
	              	</div>                 
                 
	          	</div>
		      </div>
          <div class="col-md-6">
                <div class="box box-primary">
                  <!-- form start -->
                      <div class="box-body">
                        <div class="form-group">
                          <label>Monsters</label>
                          <select name="monsters[]" class="form-control select2" multiple="multiple" data-placeholder="Select a monster" style="width: 100%;">
                            @foreach ($monsters as $monster)
                            <option value="{{ $monster->id }}">{{ $monster->name }}</option>
                            @endforeach
                          </select>
                      </div>

                      </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  {!! Form::close() !!}
                </div>
        </div>
      	</div>
    </section>
    @include('admin.errors.error')
    @include('admin.messages.message')
@endsection

@section('footer')
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    $('.select2').select2()
  })
</script>
@endsection