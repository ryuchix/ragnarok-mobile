@extends('layouts.master')
@section('content')
      <!-- Content Header (Page header) -->
      <section class="content-header">
            <h1>Update Character<small></small></h1>
      </section>

      <!-- Main content -->
      <section class="content">
            <div class="row">
                  <!-- left column -->
                  <div class="col-md-6">
                        <div class="box box-primary">
                            <!-- form start -->
                            {!! Form::model($character,['route' => ['characters.update', $character->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ $character->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="name">Class</label>
                                    <input type="text" class="form-control" name="class" placeholder="Enter name" value="{{ $character->class }}">
                                </div>
                            </div>                    
                            <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                  </div>
            </div>
      </section>
              @include('admin.errors.error')
              @include('admin.messages.message')
      @endsection

      @section('footer')
      <script>
      $(function () {
      //bootstrap WYSIHTML5 - text editor
      $('.textarea').wysihtml5();
      $('.select2').select2()
      })
      </script>
@endsection