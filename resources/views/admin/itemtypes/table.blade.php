<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($itemtypes as $itemtype)
		<tr>
			<td>{{ $count }}</td>
			<td>{{ $itemtype->name }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ route('item-types.show', $itemtype->id) }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('item-types.edit', $itemtype->id) }}">Edit</a>
			    {!! Form::model($itemtype ,['route' => ['item-types.destroy', $itemtype->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>