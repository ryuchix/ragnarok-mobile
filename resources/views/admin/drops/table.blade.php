<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($drops as $drop)
		<tr>
			<td>{{ $count }}</td>
			<td><img src="{{ ($drop->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/drops/small/' . $drop->image) }}" width="60px" height="60px"></td>
			<td>{{ $drop->name }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ url('/access/').'/'.$drop->slug }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('drops.edit', $drop->id) }}">Edit</a>
			    {!! Form::model($drop ,['route' => ['drops.destroy', $drop->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>
{{ $drops->links() }}