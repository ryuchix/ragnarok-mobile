@extends('layouts.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<h1>Add Monster<small></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      	<div class="row">
        <!-- left column -->
        	<div class="col-md-6">
		          	<div class="box box-primary">
			            <!-- form start -->
			            {!! Form::open([ 'route' => 'monsters.store', 'enctype' => 'multipart/form-data'])!!}
			              	<div class="box-body">
				                <div class="form-group">
				                  <label for="name">Name</label>
				                  <input type="text" class="form-control" name="name" placeholder="Enter name" required>
				                </div>
				                <div class="form-group">
				                  	<label for="email">Content</label>
									<textarea name="content" class="textarea" placeholder="Place some text here"
						                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
						        </div>
				                <div class="form-group">
				                  <label for="name">Level</label>
				                  <input type="text" class="form-control" name="level" placeholder="Enter level" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">Atk</label>
				                  <input type="text" class="form-control" name="atk" placeholder="Enter atk" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">MAtk</label>
				                  <input type="text" class="form-control" name="matk" placeholder="Enter matk" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">Def</label>
				                  <input type="text" class="form-control" name="def" placeholder="Enter def" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">MDef</label>
				                  <input type="text" class="form-control" name="mdef" placeholder="Enter mdef" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">HP</label>
				                  <input type="text" class="form-control" name="hp" placeholder="Enter hp" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">Hit</label>
				                  <input type="text" class="form-control" name="hit" placeholder="Enter hit" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">Flee</label>
				                  <input type="text" class="form-control" name="flee" placeholder="Enter flee" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">Movement Speed</label>
				                  <input type="text" class="form-control" name="movement_speed" placeholder="Enter movement speed" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">Atk Speed</label>
				                  <input type="text" class="form-control" name="atk_speed" placeholder="Enter atk speed" required>
				                </div>
				                <div class="form-group">
				                  <label for="name">Base exp</label>
				                  <input type="text" class="form-control" name="base_exp" placeholder="Enter base exp" >
				                </div>
				                <div class="form-group">
				                  <label for="name">Job exp</label>
				                  <input type="text" class="form-control" name="job_exp" placeholder="Enter job exp" >
				                </div>
				                <div class="form-group">
				                  	<label for="exampleInputFile">File input</label>
				                  	<input type="file" id="exampleInputFile" name="big_image">
				                  	<p class="help-block">Big Image</p>
				                </div>
			              	</div>
		          	</div>
		    </div>
		    <div class="col-md-6">
		          	<div class="box box-primary">
			            <!-- form start -->
			              	<div class="box-body">
				              	<div class="form-group">
					                <label>Card</label>
					                <select name="card_id" class="form-control select2" style="width: 100%;">
					                	<option value="">Select Card</option>
					                	@foreach ($cards as $card)
						                <option value="{{ $card->id }}">{{ $card->name }}</option>
						                @endforeach
					                </select>
				              	</div>
				              	<div class="form-group">
					                <label>Type</label>
					                <select name="type_id" class="form-control select2" style="width: 100%;">
					                	<option value="">Select Type</option>
					                	@foreach ($types as $type)
						                <option value="{{ $type->id }}">{{ $type->name }}</option>
						                @endforeach
					                </select>
				              	</div>
				              	<div class="form-group">
					                <label>Zone</label>
					                <select name="zone_id" class="form-control select2" style="width: 100%;">
					                	<option value="">Select Zone</option>
					                	@foreach ($zones as $zone)
						                <option value="{{ $zone->id }}">{{ $zone->name }}</option>
						                @endforeach
					                </select>
				              	</div>
				              	<div class="form-group">
					                <label>Race</label>
					                <select name="race_id" class="form-control select2" style="width: 100%;">
					                	<option value="">Select Race</option>
					                	@foreach ($races as $race)
						                <option value="{{ $race->id }}">{{ $race->name }}</option>
						                @endforeach
					                </select>
				              	</div>
				              	<div class="form-group">
					                <label>Element</label>
					                <select name="element_id" class="form-control select2" style="width: 100%;">
					                	<option value="">Select Element</option>
					                	@foreach ($elements as $element)
						                <option value="{{ $element->id }}">{{ $element->name }}</option>
						                @endforeach
					                </select>
				              	</div>
				              	<div class="form-group">
					                <label>Size</label>
					                <select name="size_id" class="form-control select2" style="width: 100%;">
					                	<option value="">Select Size</option>
					                	@foreach ($sizes as $size)
						                <option value="{{ $size->id }}">{{ $size->name }}</option>
						                @endforeach
					                </select>
				              	</div>             
				              	<div class="form-group">
					                <label>Drops</label>
					                <select name="items[]" class="form-control select2" multiple="multiple" data-placeholder="Select a Drops" style="width: 100%;">
					                	@foreach ($items as $item)
					                	<option value="{{ $item->id }}">{{ $item->name }}</option>
					                	@endforeach
					                </select>
					            </div>

			              	</div>
				            <div class="box-footer">
				            	<button type="submit" class="btn btn-primary">Submit</button>
				            </div>
			            {!! Form::close() !!}
		          	</div>
		          @include('admin.errors.error')
		          @include('admin.messages.message')
		    </div>
      	</div>
    </section>
@endsection

@section('footer')
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    $('.select2').select2()
  })
</script>
@endsection