@extends('layouts.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Add Monster<small></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <!-- left column -->
          <div class="col-md-6">
                <div class="box box-primary">
                  <!-- form start -->
                  {!! Form::model($monster,['route' => ['monsters.update', $monster->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}
                      <div class="box-body">
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ $monster->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Content</label>
                  <textarea name="content" class="textarea" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $monster->content }}</textarea>
                    </div>
                        <div class="form-group">
                          <label for="name">Level</label>
                          <input type="text" class="form-control" name="level" placeholder="Enter level" value="{{ $monster->level }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Atk</label>
                          <input type="text" class="form-control" name="atk" placeholder="Enter atk" value="{{ $monster->atk }}">
                        </div>
                        <div class="form-group">
                          <label for="name">MAtk</label>
                          <input type="text" class="form-control" name="matk" placeholder="Enter matk" value="{{ $monster->matk }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Def</label>
                          <input type="text" class="form-control" name="def" placeholder="Enter def" value="{{ $monster->def }}">
                        </div>
                        <div class="form-group">
                          <label for="name">MDef</label>
                          <input type="text" class="form-control" name="mdef" placeholder="Enter mdef" value="{{ $monster->mdef }}">
                        </div>
                        <div class="form-group">
                          <label for="name">HP</label>
                          <input type="text" class="form-control" name="hp" placeholder="Enter hp" value="{{ $monster->hp }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Hit</label>
                          <input type="text" class="form-control" name="hit" placeholder="Enter hit" value="{{ $monster->hit }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Flee</label>
                          <input type="text" class="form-control" name="flee" placeholder="Enter flee" value="{{ $monster->flee }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Movement Speed</label>
                          <input type="text" class="form-control" name="movement_speed" placeholder="Enter movement speed" value="{{ $monster->movement_speed }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Atk Speed</label>
                          <input type="text" class="form-control" name="atk_speed" placeholder="Enter atk speed" value="{{ $monster->atk_speed }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Base exp</label>
                          <input type="text" class="form-control" name="base_exp" placeholder="Enter base exp" value="{{ $monster->base_exp }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Job exp</label>
                          <input type="text" class="form-control" name="job_exp" placeholder="Enter job exp" value="{{ $monster->job_exp }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input type="file" id="exampleInputFile" name="big_image">
                            <p class="help-block">Big Image</p>
                        </div>
                      </div>
                </div>
        </div>
        <div class="col-md-6">
                <div class="box box-primary">
                  <!-- form start -->
                      <div class="box-body">
                        <div class="form-group">
                          <label>Card</label>
                          <select name="card_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select Card</option>
                            @foreach ($cards as $card)
                            <option value="{{ $card->id }}" {{ ($card->id == $monster->card_id) ? 'selected' : '' }}>{{ $card->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Type</label>
                          <select name="type_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select Type</option>
                            @foreach ($types as $type)
                            <option value="{{ $type->id }}" {{ ($type->id == $monster->type_id) ? 'selected' : '' }}>{{ $type->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Zone</label>
                          <select name="zone_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select Zone</option>
                            @foreach ($zones as $zone)
                            <option value="{{ $zone->id }}" {{ ($zone->id == $monster->zone_id) ? 'selected' : '' }}>{{ $zone->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Race</label>
                          <select name="race_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select Race</option>
                            @foreach ($races as $race)
                            <option value="{{ $race->id }}" {{ ($race->id == $monster->race_id) ? 'selected' : '' }}>{{ $race->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Element</label>
                          <select name="element_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select Element</option>
                            @foreach ($elements as $element)
                            <option value="{{ $element->id }}" {{ ($element->id == $monster->element_id) ? 'selected' : '' }}>{{ $element->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Size</label>
                          <select name="size_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select Size</option>
                            @foreach ($sizes as $size)
                            <option value="{{ $size->id }}" {{ ($size->id == $monster->size_id) ? 'selected' : '' }}>{{ $size->name }}</option>
                            @endforeach
                          </select>
                        </div>             
                        <div class="form-group">
                          <label>Drops</label>
                          <select name="items[]" class="form-control select2 slec" multiple="multiple" data-placeholder="Select a Drops" style="width: 100%;">
                            @foreach ($items as $item)
                            <option class="slect" value="{{ $item->id }}" {{ ($item->id == '' ) ? "selected" : "" }}>{{ $item->name }}</option>
                            @endforeach
                          </select>
                          <p>Drops: </p> 
                          @foreach ($drops as $item)
                          {{ $item->name }}, 
                          @endforeach
                      </div>

                      </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  {!! Form::close() !!}
                </div>
              @include('admin.errors.error')
              @include('admin.messages.message')
        </div>
        </div>
    </section>
@endsection

@section('footer')
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    $('.select2').select2({
      tags: true
    });
    @foreach( $drops as $drop )
      $(".slec option[value={{ $drop->id }}]").attr("selected","selected");
    @endforeach
  })
</script>
@endsection