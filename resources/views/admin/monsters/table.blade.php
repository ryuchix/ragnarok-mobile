<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($monsters as $monster)
		<tr>
			<td>{{ $count }}</td>
			<td><img src="{{ ($monster->big_image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/monsters/large/' . $monster->big_image) }}" width="60px" height="60px"></td>
			<td>{{ $monster->name }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ url('/monster/').'/'.$monster->slug }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('monsters.edit', $monster->id) }}">Edit</a>
			    {!! Form::model($monster ,['route' => ['monsters.destroy', $monster->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>
{{ $monsters->links() }}