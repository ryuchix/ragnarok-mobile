<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Title</th>
			<th>Slug</th>
			<th>Category</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($blogs as $blog)
		<tr>
			<td>{{ $count }}</td>
			<td>{{ $blog->title }}</td>
			<td>{{ $blog->slug }}</td>
			<td>{{ $blog->category }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ url('/guide/').'/'.$blog->slug }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('blogs.edit', $blog->id) }}">Edit</a>
			    {!! Form::model($blog ,['route' => ['blogs.destroy', $blog->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Title</th>
			<th>Slug</th>
			<th>Category</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>
{{ $blogs->links() }}