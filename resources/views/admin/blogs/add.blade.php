@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Add Blog<small></small></h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-6">
			<div class="box box-primary">
				<!-- form start -->
				{!! Form::open([ 'route' => 'blogs.store', 'enctype' => 'multipart/form-data'])!!}
				<div class="box-body">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" name="title" placeholder="Enter title" required>
					</div>
					<div class="form-group">
						<label for="email">Content</label>

						<textarea id="content" name="content" rows="10" cols="80">

						</textarea>
					</div>
					<div class="form-group">
						<label for="email">Excerpt</label>

						<textarea id="excerpt" name="excerpt" rows="10" cols="80">

						</textarea>

					</div>
					<div class="form-group">
						<label for="exampleInputFile">File input</label>
						<input type="file" id="exampleInputFile" name="image">
						<p class="help-block">Thumbnail</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-primary">
				<!-- form start -->
				<div class="box-body">
					<div class="form-group">
						<label>Category</label>
						<select name="category" class="form-control select2" style="width: 100%;">
							<option value="Guide">Guide</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				{!! Form::close() !!}
			</div>
			@include('admin.errors.error')
			@include('admin.messages.message')
		</div>
	</div>
</section>
@endsection
<!-- str_replace(url('/'), url('rpmobilenet/public'), $item->thumb) -->
@section('footer')
<script>
	$(function () {
    //bootstrap WYSIHTML5 - text editor
    
    CKEDITOR.replace('excerpt')
    //$('.textarea').wysihtml5();
    $('.select2').select2()

    var options = {
    	filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    	filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    	filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    	filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };

    CKEDITOR.replace('content', options)

})
</script>
@endsection