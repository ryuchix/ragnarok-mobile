<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($jobs as $job)
		<tr>
			<td>{{ $count }}</td>
			<td><img src="{{ ($job->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/jobs/small/' . $job->image) }}" width="60px" height="60px"></td>
			<td>{{ $job->name }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ url($job->slug) }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('jobs.edit', $job->id) }}">Edit</a>
			    {!! Form::model($job ,['route' => ['jobs.destroy', $job->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>
{{ $jobs->links() }}