@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Update Job<small></small></h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <div class="box box-primary">
        <!-- form start -->
        {!! Form::model($job,['route' => ['jobs.update', $job->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}
        <div class="box-body">
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ $job->name }}" required>
          </div>
          <div class="form-group">
            <label for="name">Parent</label>
            <input type="text" class="form-control" name="parent" placeholder="Enter name" value="{{ $job->parent }}" required>
          </div>
          <div class="form-group">
            <label for="name">Tier</label>
            <input type="text" class="form-control" name="tier" placeholder="Enter name" value="{{ $job->tier }}" required>
          </div>
          <div class="form-group">
            <label for="email">Description</label>
            <textarea name="description" class="textarea" placeholder="Place some text here"
            style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $job->description }}</textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile" name="image">
            <p class="help-block">Thumbnail</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <!-- form start -->
        <div class="box-body">
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
      </div>
      @include('admin.errors.error')
      @include('admin.messages.message')
    </div>
  </div>
</section>
@endsection

@section('footer')
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    //CKEDITOR.replace('content')
    $('.select2').select2();
})
</script>
@endsection