<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Id</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($items as $item)
		<tr>
			<td>{{ $count }}</td>
			<td>{{ $item->id }}</td>
			<td><img src="{{ ($item->image == null) ? asset('frontend/img/noimage.png') : asset('uploads/images/items/small/' . $item->image) }}" width="60px" height="60px"></td>
			<td>{{ $item->name }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ url('/item/').'/'.$item->slug }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('items.edit', $item->id) }}">Edit</a>
			    {!! Form::model($item ,['route' => ['items.destroy', $item->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Id</th>
			<th>Image</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>
{{ $items->links() }}