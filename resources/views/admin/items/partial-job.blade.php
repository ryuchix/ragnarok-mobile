	 <div class="box-body col-md-2">
          <div class="form-group">
            <label for="name">Swordman</label>
          </div>
          @foreach ($swordsmen as $swordsman)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="jobs[]" value="{{ $swordsman->id }}" {{ ($item->characters->contains($swordsman->id)) ? 'checked' : '' }}>
              {{ $swordsman->name }}
            </label>
          </div>
          @endforeach
        </div>
        <div class="box-body col-md-2">
          <div class="form-group">
            <label for="name">Thief</label>
          </div>
          @foreach ($thiefs as $thief)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="jobs[]" value="{{ $thief->id }}" {{ ($item->characters->contains($thief->id)) ? 'checked' : '' }}>
              {{ $thief->name }}
            </label>
          </div>
          @endforeach
        </div>
        <div class="box-body col-md-2">
          <div class="form-group">
            <label for="name">Mage</label>
          </div>
          @foreach ($magicians as $magician)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="jobs[]" value="{{ $magician->id }}" {{ ($item->characters->contains($magician->id)) ? 'checked' : '' }}>
              {{ $magician->name }}
            </label>
          </div>
          @endforeach
        </div>
        <div class="box-body col-md-2">
          <div class="form-group">
            <label for="name">Acolyte</label>
          </div>
          @foreach ($acolytes as $acolyte)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="jobs[]" value="{{ $acolyte->id }}" {{ ($item->characters->contains($acolyte->id)) ? 'checked' : '' }}>
              {{ $acolyte->name }}
            </label>
          </div>
          @endforeach
        </div>
        <div class="box-body col-md-2">
          <div class="form-group">
            <label for="name">Merchant</label>
          </div>
          @foreach ($merchants as $merchant)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="jobs[]" value="{{ $merchant->id }}" {{ ($item->characters->contains($merchant->id)) ? 'checked' : '' }}>
              {{ $merchant->name }}
            </label>
          </div>
          @endforeach
        </div>
        <div class="box-body col-md-2">
          <div class="form-group">
            <label for="name">Archer</label>
          </div>
          @foreach ($archers as $archer)
          <div class="checkbox">
            <label>
              <input type="checkbox" name="jobs[]" value="{{ $archer->id }}" {{ ($item->characters->contains($archer->id)) ? 'checked' : '' }}>
              {{ $archer->name }}
            </label>
          </div>
          @endforeach
          <br><br><br>
        </div>