@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Add Tier<small></small></h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-6">
			<div class="box box-primary">
				<form action="{{ url('admin/items/tier/save/'.$id) }}" method="post">
				@csrf
				<input type="hidden" name="id" value="{{ $id }}">	
				<table class="table table-bordered table-striped">
					<tbody>
						<tr>
							<td width="100" rowspan="2"><input type="text" name="tier[]" value="Tier I"></td>
							<td><input type="text" name="effect[]"></td>
						</tr>
						<tr>
							<td>
								<ul>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem1[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity1[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem1[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity1[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem1[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity1[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem1[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity1[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem1[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity1[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem1[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity1[]" autocomplete="off">
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td width="100" rowspan="2"><input type="text" name="tier[]" value="Tier II"></td>
							<td><input type="text" name="effect[]"></td>
						</tr>
						<tr>
							<td>
								<ul>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem2[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity2[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem2[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity2[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem2[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity2[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem2[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity2[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem2[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity2[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem2[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity2[]" autocomplete="off">
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td width="100" rowspan="2"><input type="text" name="tier[]" value="Tier III"></td>
							<td><input type="text" name="effect[]"></td>
						</tr>
						<tr>
							<td>
								<ul>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem3[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity3[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem3[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity3[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem3[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity3[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem3[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity3[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem3[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity3[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem3[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity3[]" autocomplete="off">
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td width="100" rowspan="2"><input type="text" name="tier[]" value="Tier IV"></td>
							<td><input type="text" name="effect[]"></td>
						</tr>
						<tr>
							<td>
								<ul>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem4[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity4[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem4[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity4[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem4[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity4[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem4[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity4[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem4[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity4[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem4[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity4[]" autocomplete="off">
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td width="100" rowspan="2"><input type="text" name="tier[]" value="Tier V"></td>
							<td><input type="text" name="effect[]"></td>
						</tr>
						<tr>
							<td>
								<ul>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem5[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity5[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem5[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity5[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem5[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity5[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem5[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity5[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem5[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity5[]" autocomplete="off">
									</li>
									<li>
						              <select class="form-control select2" style="width: 50%;" name="selecteditem5[]">
						                <option value="0">Select</option>
						                @foreach($items as $item)
						                <option value="{{ $item->id }}">{{ $item->name }}</option>
						                @endforeach
						              </select> x <input type="text" name="quantity5[]" autocomplete="off">
									</li>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>

				<button type="submit">Save</button>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('footer')
<script>
	$(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    //CKEDITOR.replace('content')
    $('.select2').select2();

$("#checkAll").on('click', function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

$('input:radio[name="has_craft"]').change(function(){
	    $('.sleee').show();
		$('.slee').show();
		$('.craft-location').show();
    
});

$('.add_tier').on('click', function(){
	// var inc = '<input type="text" name="tiers[]" placeholder="tier_number"> <input type="text" name="tiers_effect[]" placeholder="tier_effect"><br>';
	$(".destory-sele").find("select").select2('destroy');
	var includethis = $('.dup_this').clone();

	var cnt = $('.dup_this').length + 1;

	$('.dup_this').clone().find('input').attr('name', 'tiercraftinfo' + cnt).appendTo('#copied');

	


	//$('.dup_this').clone().find("input:name").val("").end()appendTo('#copied');
	$('.select2').select2();
});

$(document).on("click", '.closehits',  function(){
	$(this).parent().remove();
});


})
</script>
@endsection