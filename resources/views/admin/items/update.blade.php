@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Add Item<small></small></h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <div class="box box-primary">
        <!-- form start -->
        {!! Form::model($item,['route' => ['items.update', $item->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}
        <div class="box-body">
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{ $item->name }}" required>
          </div>
          <div class="form-group">
            <label for="email">Description</label>
            <textarea name="description" class="textarea" placeholder="Place some text here"
            style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $item->description }}</textarea>
          </div>
          <div class="form-group">
            <label for="email">Effects</label>
            <textarea name="content" class="textarea" placeholder="Place some text here"
            style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $item->content }}</textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile" name="image">
            <p class="help-block">Thumbnail</p>
          </div>
        </div>
      </div>
      <div class="box box-primary">
         <div class="box-body">
          if has tier
          <h2><a href="{{ url('admin/items/tier/'.$item->id) }}" target="_blank">click here</a></h2>
         </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <!-- form start -->
        <div class="box-body">
          <div class="form-group">
            <label for="name">Type</label>
            <select class="form-control select2" style="width: 100%;" name="type">
              @foreach ($item_types as $item_type)
              <option value="{{ $item_type->name }}" {{ ($item_type->name == $item->type) ? 'Selected' : '' }}>{{ $item_type->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="name">Equipable</label>
            <select class="form-control select2" style="width: 100%;" name="equipable">
              <option>Select</option>
              <option value="Weapon" {{ $item->equipable == "Weapon" ? "Selected" : "" }}>Weapon</option>
              <option value="Armor" {{ $item->equipable == "Armor" ? "Selected" : "" }}>Armor</option>
              <option value="Garment" {{ $item->equipable == "Garment" ? "Selected" : "" }}>Garment</option>
              <option value="Accessory" {{ $item->equipable == "Accessory" ? "Selected" : "" }}>Accessory</option>
              <option value="Off-hand" {{ $item->equipable == "Off-hand" ? "Selected" : "" }}>Off-hand</option>
              <option value="Footgear" {{ $item->equipable == "Footgear" ? "Selected" : "" }}>Footgear</option>
              <option value="Headgear" {{ $item->equipable == "Headgear" ? "Selected" : "" }}>Headgear</option>
            </select>
          </div>
          <div class="form-group">
            <label for="name">Upgrade of</label>
            <select class="form-control select2" style="width: 100%;" name="upgrade_of">
              <option value="0">Select</option>
              @foreach($items as $itemss)
              <option value="{{ $itemss->id }}" {{ $itemss->id == $item->upgrade_of ? "Selected" : "" }}>{{ $itemss->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="name">Upgrade to</label>
            <select class="form-control select2" style="width: 100%;" name="upgrade_to">
              <option value="0">Select</option>
              @foreach($items as $itemss)
              <option value="{{ $itemss->id }}" {{ $itemss->id == $item->upgrade_to ? "Selected" : "" }}>{{ $itemss->name }}</option>
              @endforeach
            </select>
          </div>           
            <div class="form-group">
              <label>Drops</label>
              <select name="drops[]" class="form-control select2 slec" multiple="multiple" data-placeholder="Select a Drops" style="width: 100%;">
                @foreach ($drops as $drop)
                <option class="slect" value="{{ $drop->id }}" {{ ($drop->id == '' ) ? "selected" : "" }}>{{ $drop->name }}</option>
                @endforeach
              </select>
              <p>Drops: </p> 
              @foreach ($drop_items as $drop_item)
              {{ $drop_item->name }}, 
              @endforeach
          </div>
          <!-- radio -->
          <div class="form-group">
            <label for="name">Has Tier?</label>
            <div class="radio">
              <label>
                <input type="radio" name="has_tier" id="optionsRadios1" value="1" {{ $item->has_tier == 1 ? 'checked' : '' }}>
                Yes
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="has_tier" id="optionsRadios1" value="0" {{ $item->has_tier == 0 ? 'checked' : '' }}>
                No
              </label>
            </div>
          </div>
          <label for=""><input type="checkbox" id="checkAll">Check All</label>
          
        </div>
        @include('admin.items.partial-job')
        <div class="box-body col-md-12">
          <div class="form-group">
            <label for="name">Has Craft?</label>
            <div class="radio">
              <label>
                <input type="radio" name="has_craft" id="optionsRadios1" value="1" {{ $item->has_craft == 1 ? 'checked' : '' }}>
                Yes
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="has_craft" id="optionsRadios2" value="0" {{ $item->has_craft == 0 ? 'checked' : '' }}>
                No
              </label>
            </div>
          </div>
    <div class="form-group">
            <label for="name">Craft Location</label>
            <select class="form-control select2" style="width: 100%;" name="craft_location">
            <option value="0">Select</option>
            @foreach($maps as $map)
            <option value="{{ $map->id }}" {{ ($map->id == $item->craft_location) ? "selected" : "" }}>{{ $map->name }}</option>
            @endforeach
            </select>
        </div>
          <p id="add-more-craft">add more</p>

        </div>
        
        <div class="box-body col-md-8 slee">
          @foreach($craftitems as $craftitem)
          <div class="form-group">
            <label for="name">Craft Info</label>
            <div class="form-group">
              <select class="form-control select2" style="width: 100%;" name="craftinfo[]">
                <option>Select</option>
                @foreach($items as $item)
                <option tabindex="" value="{{ $item->id }}" {{ ($craftitem->item_id == $item->id) ? "Selected" : "" }}>{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          @endforeach
          <div class="form-group">
            <label for="name">Craft Info</label>
            <div class="form-group">
              <select class="form-control select2" style="width: 100%;" name="craftinfo[]">
                <option>Select</option>
                @foreach($items as $item)
                <option tabindex="" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="name">Craft Info</label>
            <div class="form-group">
              <select class="form-control select2" style="width: 100%;" name="craftinfo[]">
                <option>Select</option>
                @foreach($items as $item)
                <option tabindex="" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="name">Craft Info</label>
            <div class="form-group">
              <select class="form-control select2" style="width: 100%;" name="craftinfo[]">
                <option>Select</option>
                @foreach($items as $item)
                <option tabindex="" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="name">Craft Info</label>
            <div class="form-group">
              <select class="form-control select2" style="width: 100%;" name="craftinfo[]">
                <option>Select</option>
                @foreach($items as $item)
                <option tabindex="" value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="name">Craft Info</label>
            <div class="form-group">
              <select class="form-control select2" style="width: 100%;" name="craftinfo[]">
                <option>Select</option>
                @foreach($items as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="name">Craft Info</label>
            <div class="form-group">
              <select class="form-control select2" style="width: 100%;" name="craftinfo[]">
                <option>Select</option>
                @foreach($items as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>



        <div class="box-body col-md-4 sleee">
          @foreach($craftitems as $craftitem)
          <div class="form-group">
            <label for="name">Quantity</label>
            <input tabindex="" type="text" class="form-control" name="craftinfoquantity[]" value="{{ $craftitem->quantity }}">
          </div>
          @endforeach
          <div class="form-group">
            <label for="name">Quantity</label>
            <input tabindex="" type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="name">Quantity</label>
            <input tabindex="" type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="name">Quantity</label>
            <input tabindex="" type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="name">Quantity</label>
            <input tabindex="" type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="name">Quantity</label>
            <input tabindex="" type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="name">Quantity</label>
            <input tabindex="" type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
          </div>
        </div>
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
      </div>
      @include('admin.errors.error')
      @include('admin.messages.message')
    </div>
  </div>
</section>
@endsection

@section('footer')
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    //CKEDITOR.replace('content')

    $('.select2').select2({
      tags: true
    });
    @foreach( $drop_items as $drop_item )
      $(".slec option[value={{ $drop_item->id }}]").attr("selected","selected");
    @endforeach


    // $("body").on("click", "#add-more-craft", function() {
    // //$('#add-more-craft').bind('click', function(){
    //   var append = '<div class="form-group">' +
    //         '<label for="name">Craft Info</label>'+
    //         '<div class="form-group">'+
    //           '<select class="form-control select2" style="width: 100%;" name="craftinfo[]">'+
    //             '<option>Select</option>'+
    //             @foreach($items as $item)
    //             '<option value="{{ $item->id }}">{{ $item->name }}</option>'+
    //             @endforeach
    //           '</select>'+
    //         '</div>'+
    //       '</div>';

    //   var append2 = '<div class="form-group">'+
    //         '<label for="name">Quantity</label>'+
    //         '<input type="text" class="form-control" name="craftinfoquantity[]">'+
    //       '</div>';
    //   $('.slee').append(append).after();
    //   $('.sleee').append(append2).after();
    // });
$("#checkAll").on('click', function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});



})
</script>
@endsection