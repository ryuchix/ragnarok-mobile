@extends('layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Add Item<small></small></h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-6">
			<div class="box box-primary">
				<!-- form start -->
				{!! Form::open([ 'route' => 'items.store', 'enctype' => 'multipart/form-data'])!!}
				<div class="box-body">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" name="name" placeholder="Enter name" required>
					</div>
					<div class="form-group">
						<label for="email">Description</label>
						<textarea name="description" class="textarea" placeholder="Place some text here"
						style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
					</div>
					<div class="form-group">
						<label for="email">Effects</label>
						<textarea name="content" class="textarea" placeholder="Place some text here"
						style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
					</div>
					<div class="form-group">
						<label for="exampleInputFile">File input</label>
						<input type="file" id="exampleInputFile" name="image">
						<p class="help-block">Thumbnail</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-primary">
				<!-- form start -->
				<div class="box-body">
					<div class="form-group">
						<label for="name">has slot?</label><br>
						<input type="radio" name="slot" value="0" checked> default <br>
						<input type="radio" name="slot" value="1"> 1 slot
						<input type="radio" name="slot" value="2"> 2 slot
					</div>
					<div class="form-group">
						<label for="name">Type</label>
						<select class="form-control select2" style="width: 100%;" name="type">
							@foreach ($item_types as $item_type)
							<option value="{{ $item_type->name }}" {{ ($item_type->name == "Equipment") ? 'Selected' : '' }}>{{ $item_type->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="name">Equipable</label>
						<select class="form-control select2" style="width: 100%;" name="equipable">
							<option>Select</option>
							<option value="Weapon">Weapon</option>
							<option value="Armor">Armor</option>
							<option value="Garment">Garment</option>
							<option value="Accessory">Accessory</option>
							<option value="Off-hand">Off-hand</option>
							<option value="Footgear">Footgear</option>
							<option value="Headgear">Headgear</option>
						</select>
					</div>
					<div class="form-group">
						<label for="name">Upgrade of</label>
						<select class="form-control select2" style="width: 100%;" name="upgrade_of">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="name">Upgrade to</label>
						<select class="form-control select2" style="width: 100%;" name="upgrade_to">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
					</div> 
	              	<div class="form-group">
		                <label>Drops</label>
		                <select name="drops[]" class="form-control select2" multiple="multiple" data-placeholder="Select a Drops" style="width: 100%;">
		                	@foreach ($drops as $drop)
		                	<option value="{{ $drop->id }}">{{ $drop->name }}</option>
		                	@endforeach
		                </select>
		            </div>               
					<!-- radio -->
					<div class="form-group">
						<label for="name">Has Tier?</label>
						<div class="radio">
							<label>
								<input type="radio" name="has_tier" id="optionsRadios1" value="1">
								Yes
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="has_tier" id="optionsRadios1" value="0" checked>
								No
							</label>
						</div>
					</div>
				</div>
				<div class="box-body col-md-2">
					<div class="form-group">
						<label for="name">Swordman</label>
					</div>
					@foreach ($swordsmen as $swordsman)
					<div class="checkbox">
						<label>
							<input type="checkbox" name="jobs[]" value="{{ $swordsman->id }}">
							{{ $swordsman->name }}
						</label>
					</div>
					@endforeach
				</div>
				<div class="box-body col-md-2">
					<div class="form-group">
						<label for="name">Thief</label>
					</div>
					@foreach ($thiefs as $thief)
					<div class="checkbox">
						<label>
							<input type="checkbox" name="jobs[]" value="{{ $thief->id }}">
							{{ $thief->name }}
						</label>
					</div>
					@endforeach
				</div>
				<div class="box-body col-md-2">
					<div class="form-group">
						<label for="name">Mage</label>
					</div>
					@foreach ($magicians as $magician)
					<div class="checkbox">
						<label>
							<input type="checkbox" name="jobs[]" value="{{ $magician->id }}">
							{{ $magician->name }}
						</label>
					</div>
					@endforeach
				</div>
				<div class="box-body col-md-2">
					<div class="form-group">
						<label for="name">Acolyte</label>
					</div>
					@foreach ($acolytes as $acolyte)
					<div class="checkbox">
						<label>
							<input type="checkbox" name="jobs[]" value="{{ $acolyte->id }}">
							{{ $acolyte->name }}
						</label>
					</div>
					@endforeach
				</div>
				<div class="box-body col-md-2">
					<div class="form-group">
						<label for="name">Merchant</label>
					</div>
					@foreach ($merchants as $merchant)
					<div class="checkbox">
						<label>
							<input type="checkbox" name="jobs[]" value="{{ $merchant->id }}">
							{{ $merchant->name }}
						</label>
					</div>
					@endforeach
				</div>
				<div class="box-body col-md-2">
					<div class="form-group">
						<label for="name">Archer</label>
					</div>
					@foreach ($archers as $archer)
					<div class="checkbox">
						<label>
							<input type="checkbox" name="jobs[]" value="{{ $archer->id }}">
							{{ $archer->name }}
						</label>
					</div>
					@endforeach
					<br><br><br>
				</div>

					<input type="checkbox" id="checkAll">Check All

				<div class="box-body col-md-12">
					<div class="form-group">
						<label for="name">Has Craft?</label>
						<div class="radio">
							<label>
								<input type="radio" name="has_craft" id="optionsRadios1" value="1">
								Yes
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="has_craft" id="optionsRadios2" value="0" checked>
								No
							</label>
						</div>
					</div>
				</div>
          <div class="box-body col-md-12 craft-location" style="display: none">
			<div class="form-group">
	            <label for="name">Craft Location</label>
	            <select class="form-control select2" style="width: 100%;" name="craft_location">
	            <option value="0">Select</option>
	            @foreach($maps as $map)
	            <option value="{{ $map->id }}">{{ $map->name }}</option>
	            @endforeach
	            </select>
	        </div>	
        </div>				<div class="box-body col-md-8 slee" style="display: none">		
					<div class="form-group">
						<label for="name">Craft Info</label>
						<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="craftinfo[]">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="name">Craft Info</label>
						<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="craftinfo[]">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="name">Craft Info</label>
						<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="craftinfo[]">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="name">Craft Info</label>
						<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="craftinfo[]">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="name">Craft Info</label>
						<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="craftinfo[]">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
						</div>
					</div>
					<div class="form-group">
						<label for="name">Craft Info</label>
						<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="craftinfo[]">
							<option value="0">Select</option>
							@foreach($items as $item)
							<option value="{{ $item->id }}">{{ $item->name }}</option>
							@endforeach
						</select>
						</div>
					</div>
				</div>
				<div class="box-body col-md-4 sleee" style="display: none">
					<div class="form-group">
						<label for="name">Quantity</label>
						<input type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="name">Quantity</label>
						<input type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="name">Quantity</label>
						<input type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="name">Quantity</label>
						<input type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="name">Quantity</label>
						<input type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="name">Quantity</label>
						<input type="text" class="form-control" name="craftinfoquantity[]" autocomplete="off">
					</div>
				
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				{!! Form::close() !!}
			</div>
			@include('admin.errors.error')
			@include('admin.messages.message')
		</div>
	</div>
</section>
@endsection

@section('footer')
<script>
	$(function () {
    //bootstrap WYSIHTML5 - text editor
    //$('.textarea').wysihtml5();
    //CKEDITOR.replace('content')
    $('.select2').select2();

$("#checkAll").on('click', function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

$('input:radio[name="has_craft"]').change(function(){
	    $('.sleee').show();
		$('.slee').show();
		$('.craft-location').show();
    
});


// $("input[type=radio][checked]").each(
// 	function() {

//  	}
// );

})
</script>
@endsection


