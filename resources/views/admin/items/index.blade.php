@extends('layouts.master')
@section('content')
	<!-- Content Header (Page header) -->
	<section class="content-header">              
		<div class="pull-right">
			<a href="{{ url('admin/items/create') }}" type="button" class="btn bg-purple"><i class="fa fa-plus"></i> Add Item</a>
		</div>
		<h1>Item<small>List of items</small></h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header "></div>
					<div class="box-body">
					<form action="{{ url('/admin-items') }}" method="get">
						<div class="form-group">
						    <input type="text" name="search" placeholder="Search..." class="form-control">
						    <button type="submit" class="btn btn-primary btn-custom btn-rounded pull-right">Submit</button>
				        </div>
					</form>
						@include('admin.items.table')
					</div>
					@include('admin.errors.error')
					@include('admin.messages.message')
				</div>
			</div>
		</div>
	</section>
	@include('admin.modals.modal')
@endsection