<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Email</th>
			<th>Date Created</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@php $count = 1 @endphp
		@foreach ($users as $user)
		<tr>
			<td>{{ $count }}</td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->created_at }}</td>
			<td>
			  <div class="timeline-footer">
			    <a class="btn btn-primary btn-xs" href="{{ route('users.show', $user->id) }}">View</a>
			    <a class="btn btn-primary btn-xs" href="{{ route('users.edit', $user->id) }}">Edit</a>
			    @if(auth()->user()->id != 3)
			    {!! Form::model($user ,['route' => ['users.destroy', $user->id], 'method' => 'DELETE', 'style' => 'display: inline-block', 'class' => 'form-inline form-delete']) !!}
			    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    {!! Form::close() !!}
			    @endif
			  </div>
			</td>
		</tr>
		@php $count++ @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Email</th>
			<th>Date Created</th>
			<th>Action</th>
		</tr>
	</tfoot>
</table>