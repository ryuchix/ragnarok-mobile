@extends('layouts.master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<h1>Add User<small></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      	<div class="row">
	        <div class="col-xs-12 col-md-6">
	          <div class="box box-primary">

	            <!-- form start -->
	            {!! Form::open([ 'route' => 'users.store'])!!}
	              <div class="box-body">
	                <div class="form-group">
	                  <label for="name">Name</label>
	                  <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required>
	                </div>
	                <div class="form-group">
	                  <label for="email">Email address</label>
	                  <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
	                </div>
	                <div class="form-group">
	                  <label for="password">Password</label>
	                  <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
	                </div>
	                <div class="form-group">
	                  <label for="password">Confirm password</label>
	                  <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password" required>
	                </div>
	              </div>
	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Submit</button>
	              </div>
	            {!! Form::close() !!}
	          </div>
	          @include('admin.errors.error')
	          @include('admin.messages.message')
	        </div>
      	</div>
    </section>
@endsection