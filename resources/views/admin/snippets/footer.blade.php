<footer class="main-footer">

	<!-- To the right -->
	<div class="pull-right hidden-xs">
		<b>Version</b> 1.0.0
	</div>
	
	<!-- Default to the left -->
	<strong>Copyright &copy; {{ date('Y') }} <a href="#">Ragnarok Mobile</a>.</strong> All rights reserved.
</footer>