<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}" ></script>
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}" defer></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" defer></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" defer></script>
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}" defer></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('dist/js/adminlte.min.js') }}" defer></script>
<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}" defer></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" defer></script>
@yield('footer')