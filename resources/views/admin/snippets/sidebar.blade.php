<section class="sidebar">

	<!-- Sidebar user panel (optional) -->
	<div class="user-panel">
		<div class="pull-left image">
			<img src="{{ (Auth::user()->image == null) ? asset('image/default-user.png') : asset('image/admins/' . Auth::user()->id . '/'. Auth::user()->image) }}" class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p>{{ Auth::user()->name }}</p>

			<!-- Status -->
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>

	<!-- Sidebar Menu -->
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">NAVIGATION</li>

		<!-- Optionally, you can add icons to the links -->
		<!-- <li class=""><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li> -->
		<li class="treeview {{ active('admin/blogs*') }}">
			<a href="#"><i class="fa fa-file"></i> <span>Blogs</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="{{ active('admin/blogs') }}"><a href="{{ url('admin/blogs') }}">Blogs</a></li>
				<li class="{{ active('admin/blogs/create') }}"><a href="{{ url('admin/blogs/create') }}">Add Blog</a></li>
			</ul>
		</li>
		<li class="treeview {{ active('admin/monsters*') }}">
			<a href="#"><i class="fa fa-odnoklassniki"></i> <span>Monsters</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="{{ active('admin/monsters') }}"><a href="{{ url('admin/monsters') }}">Monsters</a></li>
				<li class="{{ active('admin/monsters/create') }}"><a href="{{ url('admin/monsters/create') }}">Add Monster</a></li>
			</ul>
		</li>
		<li class="treeview {{ active('admin/jobs*') }} {{ active('admin/skills*') }} {{ active('admin/levels*') }}">
			<a href="#"><i class="fa fa-odnoklassniki"></i> <span>Skills</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="{{ active('admin/jobs') }}"><a href="{{ url('admin/jobs') }}">Jobs</a></li>
				<li class="{{ active('admin/jobs/create') }}"><a href="{{ url('admin/jobs/create') }}">Add Job</a></li>
				<li class="{{ active('admin/skills') }}"><a href="{{ url('admin/skills') }}">Skills</a></li>
				<li class="{{ active('admin/skills/create') }}"><a href="{{ url('admin/skills/create') }}">Add Skill</a></li>
				<li class="{{ active('admin/levels') }}"><a href="{{ url('admin/levels') }}">Levels</a></li>
				<li class="{{ active('admin/levels/create') }}"><a href="{{ url('admin/levels/create') }}">Add Level</a></li>
			</ul>
		</li>
		<li class="treeview {{ active('admin/users*') }}">
			<a href="#"><i class="fa fa-users"></i> <span>Users</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="{{ active('admin/users') }}"><a href="{{ url('admin/users') }}">Users</a></li>
				<li class="{{ active('admin/users/create') }}"><a href="{{ url('admin/users/create') }}">Add User</a></li>
			</ul>
		</li>
		<li class="treeview {{ active('admin/items*') }}">
			<a href="#"><i class="fa fa-leaf"></i> <span>Items</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="{{ active('admin/items') }}"><a href="{{ url('admin/items') }}">Items</a></li>
				<li class="{{ active('admin/items/create') }}"><a href="{{ url('admin/items/create') }}">Add Item</a></li>
			</ul>
		</li>
		<li class="treeview {{ active('admin/cards*') }}">
			<a href="#"><i class="fa fa-sticky-note"></i> <span>Cards</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="{{ active('admin/cards') }}"><a href="{{ url('admin/cards') }}">Cards</a></li>
				<li class="{{ active('admin/cards/create') }}"><a href="{{ url('admin/cards/create') }}">Add Card</a></li>
			</ul>
		</li>
		<li class="treeview {{ active('admin/events*') }}">
			<a href="#"><i class="fa fa-trophy"></i> <span>Events</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="{{ active('admin/events') }}"><a href="{{ url('admin/events') }}">Events</a></li>
				<li class="{{ active('admin/events/create') }}"><a href="{{ url('admin/events/create') }}">Add Event</a></li>
			</ul>
		</li>

		<li class="treeview {{ active('admin/zones*') }} {{ active('admin/elements*') }} {{ active('admin/races*') }} {{ active('admin/sizes*') }} {{ active('admin/types*') }} {{ active('admin/item-types*') }} {{ active('admin/characters*') }} {{ active('admin/maps*') }}{{ active('admin/drops*') }}">
			<a href="#">
				<i class="fa fa-share"></i> <span>Miscellaneous</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">


				<li class="treeview {{ active('admin/elements*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Elements</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/elements') }}"><a href="{{ url('admin/elements') }}">Elements</a></li>
						<li class="{{ active('admin/elements/create') }}"><a href="{{ url('admin/elements/create') }}">Add Element</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/zones*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Zones</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/zones') }}"><a href="{{ url('admin/zones') }}">Zones</a></li>
						<li class="{{ active('admin/zones/create') }}"><a href="{{ url('admin/zones/create') }}">Add Zone</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/types*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Types</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/types') }}"><a href="{{ url('admin/types') }}">Types</a></li>
						<li class="{{ active('admin/types/create') }}"><a href="{{ url('admin/types/create') }}">Add Type</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/races*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Races</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/races') }}"><a href="{{ url('admin/races') }}">Races</a></li>
						<li class="{{ active('admin/races/create') }}"><a href="{{ url('admin/races/create') }}">Add Race</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/sizes*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Sizes</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/sizes') }}"><a href="{{ url('admin/sizes') }}">Sizes</a></li>
						<li class="{{ active('admin/sizes/create') }}"><a href="{{ url('admin/sizes/create') }}">Add Size</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/item-types*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Item Types</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/item-types') }}"><a href="{{ url('admin/item-types') }}">Item Types</a></li>
						<li class="{{ active('admin/item-types/create') }}"><a href="{{ url('admin/item-types/create') }}">Add Item Type</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/characters*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Item Characters</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/characters') }}"><a href="{{ url('admin/characters') }}">Characters</a></li>
						<li class="{{ active('admin/characters/create') }}"><a href="{{ url('admin/characters/create') }}">Add Character</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/maps*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Maps</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/maps') }}"><a href="{{ url('admin/maps') }}">Maps</a></li>
						<li class="{{ active('admin/maps/create') }}"><a href="{{ url('admin/maps/create') }}">Add Map</a></li>
					</ul>
				</li>
				<li class="treeview {{ active('admin/drops*') }}">
					<a href="#"><i class="fa fa-tag"></i> <span>Drops</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="{{ active('admin/drops') }}"><a href="{{ url('admin/drops') }}">Drops</a></li>
						<li class="{{ active('admin/drops/create') }}"><a href="{{ url('admin/drops/create') }}">Add Drop</a></li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</section>