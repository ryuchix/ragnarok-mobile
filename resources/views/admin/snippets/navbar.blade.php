<!-- Logo -->
<a href="{{ config('app.url', 'https://www.ragnarokmobile.net') }}/admin" class="logo">
	<!-- mini logo for sidebar mini 50x50 pixels -->
	<span class="logo-mini"><b>RO</b>M</span>
	<!-- logo for regular state and mobile devices -->
	<span class="logo-lg"><b>RO</b>MOBILE</span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    	<span class="sr-only">Toggle navigation</span>
  	</a>
    
  <!-- Navbar Right Menu -->
	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">

			<!-- User Account Menu -->
			<li class="dropdown user user-menu">

				<!-- Menu Toggle Button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">

				  <!-- The user image in the navbar-->
				  <img src="{{ (Auth::user()->image == null) ? asset('image/default-user.png') : asset('image/admins/' . Auth::user()->id . '/'. Auth::user()->image) }}" class="user-image" alt="User Image">

				  <!-- hidden-xs hides the username on small devices so only the image appears. -->
				  <span class="hidden-xs">{{ Auth::user()->name }}</span>
				</a>
				<ul class="dropdown-menu">

				  <!-- The user image in the menu -->
				  	<li class="user-header">
					    <img src="{{ (Auth::user()->image == null) ? asset('image/default-user.png') : asset('image/admins/' . Auth::user()->id . '/'. Auth::user()->image) }}" class="img-circle" alt="User Image">
					    <p>
					      {{ Auth::user()->name }}
					      <small>{{ Auth::user()->email }}</small>
					    </p>
				  	</li>
				  	
				  	<!-- Menu Footer-->
				    <li class="user-footer">
					    <div class="pull-left">
					      <a href="#" class="btn btn-default btn-flat">Profile</a>
					    </div>
					    <div class="pull-right">
					      	<a class="btn btn-default btn-flat" href="{{ route('logout') }}"
					        onclick="event.preventDefault();
					        document.getElementById('logout-form').submit();">
					        {{ __('Logout') }}
					      	</a>
					      	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					        	@csrf
					      	</form>
					    </div>
				    </li>
				</ul>
			</li>
		</ul>
	</div>
</nav>