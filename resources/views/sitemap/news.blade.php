<?= '<'.'?'.'xml version="1.0" encoding="UTF-8"?>'."\n" ?>
	<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"> 
		@foreach ($items as $item)
		<url> 
			<loc>{{ url('guide/'.$item->slug) }}</loc> 
				<news:news> 
					<news:publication> 
						<news:name>{{ $item->title }}</news:name> 
						<news:language>en</news:language> 
					</news:publication> 
					<news:genres>Blog</news:genres> 
					<news:publication_date>{{ \Carbon\Carbon::parse($item->updated_at)->format('Y-m-d') }}</news:publication_date> 
					<news:title>{{ $item->title }}</news:title> 
					<news:keywords>ragnarok, ragnarok mobile, ragnarok eternal love, ragnarok guide, ragnarok quest, ragnarok cards</news:keywords> 
					<news:stock_tickers></news:stock_tickers> 
				</news:news> 
		</url>
		@endforeach
	</urlset>