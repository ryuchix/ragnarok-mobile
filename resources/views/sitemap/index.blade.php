<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ url('sitemap/posts') }}</loc>
        <lastmod>{{ $post->updated_at->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/cards') }}</loc>
        <lastmod>{{ $card->updated_at->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>

    <sitemap>
        <loc>{{ url('sitemap/monsters') }}</loc>
        <lastmod>{{ $monster->updated_at->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>

    <sitemap>
        <loc>{{ url('sitemap/items') }}</loc>
        <lastmod>{{ $item->updated_at->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>

    <sitemap>
        <loc>{{ url('sitemap/pages') }}</loc>
        <lastmod>2019-01-02T09:19:58+00:00</lastmod>
    </sitemap>
    
    <sitemap>
        <loc>{{ url('sitemap/maps') }}</loc>
        <lastmod>{{ $map->updated_at->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>
</sitemapindex>