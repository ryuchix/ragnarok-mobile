<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ url('thief') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('assassin') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('assassin-cross') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('rogue') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('stalker') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('merchant') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('blacksmith') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('whitesmith') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('alchemist') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('creator') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('archer') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('hunter') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('sniper') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('mage') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('wizard') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('high-wizard') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('swordsman') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('knight') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('lord-knight') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('crusader') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    <url>
        <loc>{{ url('paladin') }}</loc>
        <lastmod>2018-11-27T17:58:05+00:00</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
</urlset>