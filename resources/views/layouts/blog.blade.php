<!DOCTYPE html>
<html lang="en">
	@include('frontend.snippets.header')
<body>
	@include('frontend.snippets.menu')
		
	@yield('banner')

	<div class="container wrapper">
		<div class="inner_content">
			<div class="row pad30">
				@yield('content')
				<!-- sidebar -->
				@include('frontend.snippets.sidebar')
			</div>
		</div>
	</div>

	<!-- footer -->
	@include('frontend.snippets.footer')

	@include('frontend.snippets.script')

<script type="text/javascript">
	jQuery(document).ready(function($) {
		 $(window).load(function() {
		 	$('#nslider').nivoSlider({
		 		effect: 'fade', directionNav: true, pauseOnHover: true});
		 });

		 $("#slider_home").carouFredSel({ 
		 	width : "100%", 
		 	height : "auto",
		 	responsive : true,
		 	auto : false,
		 	items : { width : 280, visible: { min: 1, max: 3 }
			 },
			 swipe : { onTouch : true, onMouse : true },
			 scroll: { items: 1, },
			 prev : { button : "#sl-prev", key : "left"},
			 next : { button : "#sl-next", key : "right" }
		});
	});
</script>
</body>
</html>