<!DOCTYPE html>
<html lang="en">
	@include('frontend.snippets.header')
<body>
	@include('frontend.snippets.menu')
		
	@yield('banner')

	<div class="container wrapper">
		<div class="inner_content">
			<div class="row pad30">
				<div class="col-md-9 pad25">
					<div class="row whitebg">
						@yield('content')
					</div>
				</div>
				<!-- sidebar -->
				@include('frontend.snippets.sidebar')
			</div>
		</div>
	</div>

	<!-- footer -->
	@include('frontend.snippets.footer')

	@include('frontend.snippets.script')

</body>
</html>