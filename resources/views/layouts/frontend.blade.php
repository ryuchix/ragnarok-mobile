<!DOCTYPE html>
<html lang="en">
@include('frontend.snippets.header')
<body>
@include('frontend.snippets.menu')
	
@include('frontend.snippets.slider')
	
@include('frontend.snippets.banner')

<div class="container wrapper">
	<div class="inner_content">

		<!--info boxes-->
		<div class="pad25 hidden-xs hidden-sm"></div> 
		<div class="row">
			<!--col 1-->
			<div class="col-md-12">
				@yield('content')
			</div>
		</div>
	</div>
</div>

@include('frontend.snippets.footer')

@include('frontend.snippets.script')

	
</body>
</html>