<!DOCTYPE html>
<html>
<head>
  @include('admin.snippets.header')
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">
		<header class="main-header">
	      @include('admin.snippets.navbar')
	    </header>

	    <aside class="main-sidebar">
	      @include('admin.snippets.sidebar')
	    </aside>

	    <div class="content-wrapper">
	      @yield('content')
	    </div>

	    @include('admin.snippets.footer')

	    @include('admin.snippets.control')

	</div>
	@include('admin.snippets.script')
</body>
</html>