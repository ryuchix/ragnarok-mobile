<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Services\Slug;

class Map extends Model
{
	use SLug;
	
    protected $fillable = ['name', 'slug', 'content', 'image', 'big_image', 'type'];

    protected static function boot() {
        parent::boot();

        static::creating(function($map) {
            $map->slug = $map->createMapSlug($map->name);
        });
    }

    public function monsters()
    {
        return $this->belongsToMany('App\Monster');
    }
}
