<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkillLevel extends Model
{
    protected $fillable = [

        'skill_id',
    	'level',
    	'cast_delay',
    	'sp_cost',
    	'description',
    	'cast_time',
    	'hp_cost',
    	'cooldown',
    	'bt'

    ];

    public function skill()
    {
        return $this->belongsTo('App\skill', 'skill_id');
    }
 
}
