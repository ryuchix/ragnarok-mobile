<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Monster extends Model
{

	use Slug;
	
    protected $fillable = ['name', 'content', 'image', 'big_image', 'level', 'type_id', 'zone_id', 'race_id', 'element_id', 'size_id', 'card_id', 'item_category_id',
    'atk',
    'matk',
    'def',
    'mdef',
    'hit',
    'flee',
    'hp',
    'movement_speed',
    'atk_speed',
    'description',
    'base_exp',
    'job_exp'
    
    
    
    ];

    protected static function boot() {
        parent::boot();

        static::creating(function($monster) {
            $monster->slug = $monster->createSlug($monster->name);
        });
    }

    public function race()
    {
    	return $this->belongsTo('App\Race');
    }

    public function zone()
    {
    	return $this->belongsTo('App\Zone');
    }

    public function card()
    {
        return $this->belongsTo('App\Card');
    }

    public function size()
    {
    	return $this->belongsTo('App\Size');
    }

    public function element()
    {
    	return $this->belongsTo('App\Element');
    }

    public function type()
    {
    	return $this->belongsTo('App\Type');
    }

    public function maps()
    {
        return $this->belongsToMany('App\Map');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item')->orderBy('name');;
    }
}
