<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Blog extends Model
{
	use Slug;
	
    protected $fillable = ['title', 'slug', 'category', 'content', 'excerpt', 'image', 'category'];

    protected static function boot() {
        parent::boot();

        static::creating(function($blog) {
            $blog->slug = $blog->createBlogSlug($blog->title);
        });
    }
}
