<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Job extends Model
{
    use SLug;

    protected $fillable = [

    	'name',
        'slug',
    	'parent',
    	'tier',
    	'description',
    	'image'

    ];

    protected static function boot() {
        parent::boot();

        static::creating(function($job) {
            $job->slug = $job->createJobSlug($job->name);
        });
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill');
    }
}
