<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Item extends Model
{
	use SLug;

    protected $fillable = ['name', 'content', 'image', 'big_image', 'type', 'item_category_id', 'description', 'has_tier', 'upgrade_to', 'upgrade_of', 'equipable', 'has_craft', 'craft_location'];

    protected static function boot() {
        parent::boot();

        static::creating(function($item) {
            $item->slug = $item->createItemSlug($item->name);
        });
    }

    public function monsters()
    {
        return $this->belongsToMany('App\Monster')->orderBy('name');;
    }

    public function characters()
    {
        return $this->belongsToMany('App\Character');
    }

    public function craft()
    {
        return $this->belongsTo('App\ItemCraft', 'id');
    }

    public function drops()
    {
        return $this->belongsToMany('App\Drop');
    }
}
