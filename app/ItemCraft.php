<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCraft extends Model
{
    protected $fillable = ['item', 'item_id', 'quantity'];

	public function item()
	{
	    return $this->belongsTo('App\Item', 'id');
	}
}
