<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTierProcess extends Model
{
    protected $fillable = [
    	'item_tier_id',
    	'item_id',
    	'quantity'
    ];

   	public function itemTier()
	{
	    return $this->belongsTo('App\ItemTier', 'item_tier_id');
	}

	public function item()
	{
	    return $this->belongsTo('App\Item', 'item_id');
	}
}
