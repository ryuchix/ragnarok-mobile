<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DropItem extends Model
{
    protected $fillable = ['drop_id', 'item_id'];
}
