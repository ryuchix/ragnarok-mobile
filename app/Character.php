<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $fillable = ['name', 'class'];

    public function items()
    {
        return $this->belongsToMany('App\Item');
    }
}
