<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Toast;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::paginate();

        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.events.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $event = Event::create($inputs);

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $event->id); 
        }

        Toast::success('Event added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('admin.events.update', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $inputs = $request->all();

        $event->update($request->all());

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $event->id); 
        }

        Toast::success('Event updated', 'Success');

        return redirect('admin/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();

        Toast::success('Event Deleted', 'Success');

        return redirect('admin/events');
    }


    /**
     * Upload Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/events/small';
        $upload = $image->move($destinationPath, $name);
        Event::where('id', $id)->update(['image' => $name]);
    }
}
