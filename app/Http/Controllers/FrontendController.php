<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monster;
use App\Card;
use App\Blog;
use App\Item;
use App\Element;
use App\Race;
use App\Size;
use App\ItemCraft;
use App\Map;
use App\ItemType;
use Input;
use App\Drop;
use App\Type;
use App\Event;
use Carbon\Carbon;
use DB;
use App\ItemTier;
use App\ItemTierProcess;

class FrontendController extends Controller
{
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->limit(15)->get();
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.index', compact('blogs', 'guides', 'events'));
    }

    public function monsterFilter($name)
    {
        $type = Type::where('name', $name)->first();

        $monsters = Monster::where('type_id', $type->id)->paginate();

        $sizes = Size::all();
        $races = Race::all();
        $elements = Element::all();
        $events = Event::all();
        
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();

        return view('frontend.monsters.index', compact('monsters', 'guides', 'sizes', 'races', 'elements', 'events'));
    }

    public function monsters(Request $request)
    {

        $search = $request->input('search');
        $size = $request->input('size');
        $element = $request->input('element');
        $race = $request->input('race');
        $sort = $request->input('sort') != null ? $request->input('sort') : "name";
        $order = $request->input('order') != null ? $request->input('order') : "asc";

        if ($search != "") {
            $monsters = Monster::where('name', 'LIKE', "%$search%")
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
        } else {
            if ($size != "" && $element != "" && $race != "") {
                $monsters = Monster::where('size_id', $request->input('size'))
                            ->where('element_id', $request->input('element'))
                            ->where('race_id', $request->input('race'))
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
            }  elseif ($element != "" && $race != "") {
                $monsters = Monster::where('element_id', $request->input('element'))
                            ->where('race_id', $request->input('race'))
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
            } elseif ($size != "" && $race != "") {
                $monsters = Monster::where('size_id', $request->input('size'))
                            ->where('race_id', $request->input('race'))
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
            } elseif ($size != "" && $element != "") {
                $monsters = Monster::where('size_id', $request->input('size'))
                            ->where('element_id', $request->input('element'))
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
            } elseif ($size != "") {
                $monsters = Monster::where('size_id', $request->input('size'))
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
            } elseif ($element != "") {
                $monsters = Monster::where('element_id', $request->input('element'))
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
            } elseif ($race != "") {
                $monsters = Monster::where('race_id', $request->input('race'))
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy($sort, $order)->paginate();
            } else {
                if($sort == "") {
                    $monsters = Monster::with(['element', 'race', 'zone', 'type', 'size'])->sortBy('name', 'asc')->paginate();
                }else {
                    $monsters = Monster::with(['element', 'race', 'zone', 'type', 'size'])->orderBy($sort, $order)->paginate();
                }
            
            }
        }

        $sizes = Size::all();
        $races = Race::all();
        $elements = Element::all();
        $events = Event::all();
        $events = Event::all();

        
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();

        return view('frontend.monsters.index', compact('monsters', 'guides', 'sizes', 'races', 'elements', 'events'));
    }

    public function showMonster($slug)
    {
        $monster = Monster::where('slug', $slug)->with(['element', 'race', 'zone', 'type', 'size', 'card'])->firstOrFail();

        $maps = $monster->maps;

        $items = $monster->items;
        
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.monsters.show', compact('monster', 'maps', 'items', 'guides', 'events'));
    }

    public function items(Request $request)
    {
        //$items = Item::orderBy('name', 'asc')->paginate();
        
        $search = $request->input('search');
        $type = $request->input('item_type');

        if ($search != "") {
            $items = Item::where('name', 'LIKE', "%$search%")
                            ->orderBy('name', 'asc')->paginate()->onEachSide(5);
        } else {
            if ($type != "") {
                $items = Item::where('type', $type)
                            ->orderBy('name', 'asc')->paginate()->onEachSide(5);
            } else {
                $items = Item::paginate()->onEachSide(5);
            }
        }

        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();

        $item_types = ItemType::all();
        $events = Event::all();

        return view('frontend.items.index', compact('items', 'guides', 'item_types', 'events'));
    } 

    public function showItem($slug)
    {
        $item = Item::where('slug', $slug)->firstOrFail();

        $monsters = $item->monsters;

        $jobs = $item->characters;

        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();

        $item_upgrade_of = Item::find($item->upgrade_of);

        $item_upgrade_to = Item::find($item->upgrade_to);

        $item_crafts = ItemCraft::with('item')->where('item', $item->id)->get();

        $map = Map::find($item->craft_location);

        $path = "https://www.romexchange.com/api?item=" . urlencode($item->name) . "&exact=true&slim=false";

        $file = $path;
        $file_headers = @get_headers($file);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $prices = null;
        }
        else {
            $prices = json_decode(file_get_contents($path), true);
        }

        $item_drops = $item->drops()->get();
        $events = Event::all();

        $itemTiers = ItemTier::where('item_id', $item->id)->get();

        return view('frontend.items.show', compact('events', 'monsters', 'item', 'guides', 'jobs', 'item_upgrade_of', 'item_upgrade_to', 'item_crafts', 'map', 'item_drops', 'prices', 'itemTiers'));
    }

    public function dropItem($slug)
    {
        $drop = Drop::where('slug', $slug)->firstOrFail();

        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();

        $item_drops = $drop->items()->get();
        $events = Event::all();

        return view('frontend.items.drops', compact('drop', 'guides', 'item_drops', 'events'));
    }

    public function allMaps()
    {
        $maps = Map::all();

        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.maps.index', compact('maps', 'guides', 'events'));
    }

    public function showMap($slug)
    {
        $map = Map::where('slug', $slug)->firstOrFail();

        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();

        $map_monsters = $map->monsters()->get();
        $events = Event::all();

        return view('frontend.maps.show', compact('map', 'guides', 'map_monsters', 'events'));
    }

    public function cards(Request $request)
    {
        $search = $request->input('search');
        $slot = $request->input('slot');

        if ($search != "") {
            $cards = Card::where('name', 'LIKE', "%$search%")
                            ->orderBy('name', 'asc')->paginate();
        } else {
            if ($slot != "") {
                $cards = Card::where('equipable', $slot)
                            ->orderBy('name', 'asc')->paginate();
            } else {
                $cards = Card::orderBy('name', 'asc')->paginate();
            }
        }
        
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.cards.index', compact('cards', 'guides', 'events'));
    }

    public function showCard($slug)
    {
        $card = Card::where('slug', $slug)->firstOrFail();

        $monsters = Monster::where('card_id', $card->id)->with(['type'])->get();

        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        $path = "https://www.romexchange.com/api?item=" . urlencode($card->name) . "&exact=true&slim=false";

        $file = $path;
        $file_headers = @get_headers($file);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $prices = null;
        }
        else {
            $prices = json_decode(file_get_contents($path), true);
        }

        return view('frontend.cards.show', compact('card', 'monsters', 'guides', 'events', 'prices'));
    }

    public function guides()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->paginate();
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.guides.index', compact('blogs', 'guides', 'events'));
    }

    public function quests()
    {
        $blogs = Blog::where('category', 'Quest')->orderBy('created_at', 'desc')->paginate();
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.guides.index', compact('blogs', 'guides', 'events'));
    }

    public function showGuide($slug)
    {
        $blog = Blog::where('category', 'Guide')->where('slug', $slug)->firstOrFail();
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.guides.show', compact('blog', 'guides', 'events'));
    }

    public function showQuest($slug)
    {
        $blog = Blog::where('category', 'Quest')->where('slug', $slug)->firstOrFail();
        $events = Event::all();

        return view('frontend.guides.show', compact('blog', 'events'));
    }

    public function search(Request $request) 
    {
        $query = $request->input('q');

        $cards = Card::where('name', 'LIKE', "%$query%")->get();
        $monsters = Monster::where('name', 'LIKE', "%$query%")->get();
        $items = Item::where('name', 'LIKE', "%$query%")->get();
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        $blogs = Blog::orWhere('title', 'LIKE', "%$query%")
                        ->orWhere('content', 'LIKE', "%$query%")
                        ->orWhere('excerpt', 'LIKE', "%$query%")
                        ->orderBy('created_at', 'desc')->get();
        $events = Event::all();

        return view('frontend.search.index', compact('cards', 'monsters', 'items', 'query', 'guides', 'blogs', 'events'));

    }

    public function privacy()
    {
        $guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
        
        $events = Event::all();
        
        return view('frontend.pages.privacy-policy', compact('guides', 'events'));
    }
}
