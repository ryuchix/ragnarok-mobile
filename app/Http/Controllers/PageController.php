<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Blog;
use App\Event;

class PageController extends Controller
{
    public function endlessTower()
    {
    	$guides = Blog::where('category', 'Guide')->orderBy('created_at', 'desc')->get();
    	$events = Event::all();

		return view('frontend.pages.endless-tower', compact('guides', 'events'));
    }
}
