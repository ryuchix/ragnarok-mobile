<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemType;
use App\Character;
use App\ItemCraft;
use App\Map;
use Illuminate\Http\Request;
use Toast;
use DB;
use App\Drop;
use App\ItemTier;
use App\ItemTierProcess;

class ItemController extends Controller
{

    public function search(Request $request)
    {
        $search = $request->input('search');

        $items = Item::where('name', 'LIKE', "%$search%")
                            ->orderBy('name', 'asc')->paginate();

        return view('admin.items.index', compact('items'));                   
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::orderBy('id', 'desc')->paginate();

        return view('admin.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::all();

        $item_types = ItemType::all();
        $maps = Map::all();
        $swordsmen = Character::where('class', 'Swordman')->get();
        $thiefs = Character::where('class', 'Thief')->get();
        $acolytes = Character::where('class', 'Acolyte')->get();
        $merchants = Character::where('class', 'Merchant')->get();
        $archers = Character::where('class', 'Archer')->get();
        $magicians = Character::where('class', 'Magician')->get();
        $drops = Drop::all();

        return view('admin.items.add', compact('items', 'item_types', 'swordsmen', 'thiefs', 'acolytes', 'merchants', 'archers', 'magicians','maps', 'drops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        if($inputs['craft_location'] == 0) {
            $inputs['craft_location'] = NULL;
        }

        if($inputs['equipable'] == "Select") {
            $inputs['equipable'] = NULL;
        }

        if($inputs['upgrade_of'] == 0) {
            $inputs['upgrade_of'] = NULL;
        }

        if($inputs['upgrade_to'] == 0) {
            $inputs['upgrade_to'] = NULL;
        }

        $items = Item::create($inputs);

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $items->id); 
        }

        if ($request->has('jobs')) {
            $jobs = $request->input('jobs');
           
            foreach ($jobs as $job) {
                $items->characters()->attach($job);
            }
        }

        if ($request->has('drops')) {
            $drops = $request->input('drops');
           
            foreach ($drops as $drop) {
                $items->drops()->attach($drop);
            }
        }


        if ($request->has('craftinfo')) {
            $craftitem = $request->input('craftinfo');
            $craftqty = $request->input('craftinfoquantity');
            $count = 0;
            foreach ($craftitem as $craft) {
                if ($craft != 0) {
                    $item_craft = new ItemCraft;
                    $item_craft->item = $items->id;
                    $item_craft->item_id = $craft;
                    $item_craft->quantity = $craftqty[$count];
                    $item_craft->save();

                    $count++;
                } else {
                    break;
                }
            }
        }
        
        if ($request->input('slot') == 1) {
            // Retrieve the first task
            $task = Item::orderBy('id', 'desc')->first();
            
            $newTask = $task->replicate();
            $newTask->name = $request->input("name") . " [1]";
            $newTask->slug = $task->createItemSlug($request->input("name") . " [1]");
            $newTask->save();
            
            if ($request->has('jobs')) {
                $jobs = $request->input('jobs');
               
                foreach ($jobs as $job) {
                    $newTask->characters()->attach($job);
                }
            }
    
            if ($request->has('drops')) {
                $drops = $request->input('drops');
               
                foreach ($drops as $drop) {
                    $newTask->drops()->attach($drop);
                }
            }
            
            
            if ($request->has('craftinfo')) {
                $craftitem = $request->input('craftinfo');
                $craftqty = $request->input('craftinfoquantity');
                $count = 0;
                foreach ($craftitem as $craft) {
                    if ($craft != 0) {
                        $item_craft = new ItemCraft;
                        $item_craft->item = $newTask->id;
                        $item_craft->item_id = $craft;
                        $item_craft->quantity = $craftqty[$count];
                        $item_craft->save();
    
                        $count++;
                    } else {
                        break;
                    }
                }
            }
            
        }
        
        if ($request->input('slot') == 2) {
            // Retrieve the first task
            $task = Item::orderBy('id', 'desc')->first();
            
            $newTask = $task->replicate();
            $newTask->name = str_replace("[1]", "[2]", $request->input("name"));
            $newTask->slug = $task->createItemSlug(str_replace("[1]", "[2]", $request->input("name")));
            $newTask->save();
            
            if ($request->has('jobs')) {
                $jobs = $request->input('jobs');
               
                foreach ($jobs as $job) {
                    $newTask->characters()->attach($job);
                }
            }
    
            if ($request->has('drops')) {
                $drops = $request->input('drops');
               
                foreach ($drops as $drop) {
                    $newTask->drops()->attach($drop);
                }
            }
            
            
            if ($request->has('craftinfo')) {
                $craftitem = $request->input('craftinfo');
                $craftqty = $request->input('craftinfoquantity');
                $count = 0;
                foreach ($craftitem as $craft) {
                    if ($craft != 0) {
                        $item_craft = new ItemCraft;
                        $item_craft->item = $newTask->id;
                        $item_craft->item_id = $craft;
                        $item_craft->quantity = $craftqty[$count];
                        $item_craft->save();
    
                        $count++;
                    } else {
                        break;
                    }
                }
            }
        }
        
        Toast::success('Item added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $items = Item::all();
        $item_types = ItemType::all();
        $jobs = $item->characters();
        $maps = Map::all();
        $swordsmen = Character::where('class', 'Swordman')->get();
        $thiefs = Character::where('class', 'Thief')->get();
        $acolytes = Character::where('class', 'Acolyte')->get();
        $merchants = Character::where('class', 'Merchant')->get();
        $archers = Character::where('class', 'Archer')->get();
        $magicians = Character::where('class', 'Magician')->get();
        $drops = Drop::all();
        $drop_items = $item->drops()->get();

        $craftitems = ItemCraft::with('item')->where('item', $id)->get();

        return view('admin.items.update', compact('item', 'items', 'jobs', 'swordsmen', 'thiefs', 'acolytes', 'merchants', 'archers', 'magicians', 'item_types', 'craftitems', 'maps', 'drops', 'drop_items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $inputs = $request->all();

        if($inputs['craft_location'] == 0) {
            $inputs['craft_location'] = NULL;
        }

        if($inputs['equipable'] == "Select") {
            $inputs['equipable'] = NULL;
        }

        if($inputs['upgrade_of'] == 0) {
            $inputs['upgrade_of'] = NULL;
        }

        if($inputs['upgrade_to'] == 0) {
            $inputs['upgrade_to'] = NULL;
        }

        $item->update($inputs);
 
        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $item->id); 
        }

        if ($request->has('drops')) {
            $drops_items = DB::table('drop_item')->where('item_id', $item->id)->get();

            foreach ($drops_items as $drops_item) {
                $item->drops()->detach($drops_item);
            }

            $drops = $request->input('drops');
            foreach ($drops as $drop) {
                $item->drops()->attach($drop);
            }
        }

        if ($request->has('jobs')) {
            $character_items = DB::table('character_item')->where('item_id', $item->id)->get();

            foreach ($character_items as $character_item) {
                $item->characters()->detach($character_item);
            }

            $jobs = $request->input('jobs');
            foreach ($jobs as $job) {
                $item->characters()->attach($job);
            }
        }
        if ($request->has('craftinfo')) {
            $craftitem = $request->input('craftinfo');
            $craftqty = $request->input('craftinfoquantity');

            $ItemCraft = ItemCraft::where('item', $item->id)->get();
            foreach ($ItemCraft as $craft) {
                $craft->delete();
            }

            $count = 0;
            foreach ($craftitem as $craft) {
                if ($craft != 0) {
                    $item_craft = new ItemCraft;
                    $item_craft->item = $item->id;
                    $item_craft->item_id = $craft;
                    $item_craft->quantity = $craftqty[$count];
                    $item_craft->save();

                    $count++;
                } else {
                    break;
                }
            }
        }

        
        Toast::success('Item updated', 'Success');

        return redirect('admin/items');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();

        Toast::success('Item Deleted', 'Success');

        return redirect('admin/items');
    }

    /**
     * Upload Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/items/small';
        $upload = $image->move($destinationPath, $name);
        Item::where('id', $id)->update(['image' => $name]);
    }

    /**
     * Upload Big Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadBigImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/items/large';
        $upload = $image->move($destinationPath, $name);
        Item::where('id', $id)->update(['big_image' => $name]);
    }

    public function tier(Request $request, $id)
    {
        $items = Item::all();

        return view('admin.items.tier', compact('id', 'items'));
    }

    public function saveTier(Request $request, $id)
    {
        $id = $request->id;

        $has_tier = Item::where('id', $request->id)->update([
            'has_tier' => 1,
        ]);
        
        $item = Item::find($id);
        
        $s = rtrim($item->slug, "1") . 2;
        
        $item2 = Item::where('slug', $s)->first();

        if($request->has('tier')) {
            $tiers = $request->input('tier');
            $effects = $request->input('effect');
            $selectedItems1 = $request->input('selecteditem1');
            $selectedItemQty1 = $request->input('quantity1');

            $selectedItems2 = $request->input('selecteditem2');
            $selectedItemQty2 = $request->input('quantity2');

            $selectedItems3 = $request->input('selecteditem3');
            $selectedItemQty3 = $request->input('quantity3');

            $selectedItems4 = $request->input('selecteditem4');
            $selectedItemQty4 = $request->input('quantity4');

            $selectedItems5 = $request->input('selecteditem5');
            $selectedItemQty5 = $request->input('quantity5');

            $count = 0;
            $tierid = array();
            foreach ($tiers as $tier) {
                if($tier != null && $tier != "") {
                    $itemTier = ItemTier::create([
                        'tier' => $tier,
                        'item_id' => $request->id,
                        'effect' => $effects[$count]
                    ]);

                    $tierid[] = $itemTier->id;

                    $count ++;

                } else {
                    break;
                }
            }

            $count1 = 0;
            foreach ($selectedItems1 as $selectedItem1) {
                if($selectedItem1 != 0) {
                    ItemTierProcess::create([
                        'item_tier_id' => $tierid[0],
                        'item_id' => $selectedItem1,
                        'quantity' => $selectedItemQty1[$count1]
                    ]);

                    $count1 ++;
                }
            }
    
            $count2 = 0;
            foreach ($selectedItems2 as $selectedItem2) {
                if($selectedItem2 != 0) {
                    ItemTierProcess::create([
                        'item_tier_id' => $tierid[1],
                        'item_id' => $selectedItem2,
                        'quantity' => $selectedItemQty2[$count2]
                    ]);

                    $count2 ++;
                }
            }

            $count3 = 0;
            foreach ($selectedItems3 as $selectedItem3) {
                if($selectedItem3 != 0) {
                    ItemTierProcess::create([
                        'item_tier_id' => $tierid[2],
                        'item_id' => $selectedItem3,
                        'quantity' => $selectedItemQty3[$count3]
                    ]);

                    $count3 ++;
                }
            }

            $count4 = 0;
            foreach ($selectedItems4 as $selectedItem4) {
                if($selectedItem4 != 0) {
                    ItemTierProcess::create([
                        'item_tier_id' => $tierid[3],
                        'item_id' => $selectedItem4,
                        'quantity' => $selectedItemQty4[$count4]
                    ]);

                    $count4 ++;
                }
            }

            $count5 = 0;
            foreach ($selectedItems5 as $selectedItem5) {
                if($selectedItem5 != 0) {
                    ItemTierProcess::create([
                        'item_tier_id' => $tierid[4],
                        'item_id' => $selectedItem5,
                        'quantity' => $selectedItemQty5[$count5]
                    ]);

                    $count5 ++;
                }
            }
        }
        
         
        if($item2 != null) {
            if($request->has('tier')) {
                $tiers = $request->input('tier');
                $effects = $request->input('effect');
                $selectedItems1 = $request->input('selecteditem1');
                $selectedItemQty1 = $request->input('quantity1');
    
                $selectedItems2 = $request->input('selecteditem2');
                $selectedItemQty2 = $request->input('quantity2');
    
                $selectedItems3 = $request->input('selecteditem3');
                $selectedItemQty3 = $request->input('quantity3');
    
                $selectedItems4 = $request->input('selecteditem4');
                $selectedItemQty4 = $request->input('quantity4');
    
                $selectedItems5 = $request->input('selecteditem5');
                $selectedItemQty5 = $request->input('quantity5');
    
                $count = 0;
                $tierid = array();
                foreach ($tiers as $tier) {
                    if($tier != null && $tier != "") {
                        $itemTier = ItemTier::create([
                            'tier' => $tier,
                            'item_id' => $item2->id,
                            'effect' => $effects[$count]
                        ]);
    
                        $tierid[] = $itemTier->id;
    
                        $count ++;
    
                    } else {
                        break;
                    }
                }
    
                $count1 = 0;
                foreach ($selectedItems1 as $selectedItem1) {
                    if($selectedItem1 != 0) {
                        ItemTierProcess::create([
                            'item_tier_id' => $tierid[0],
                            'item_id' => $selectedItem1,
                            'quantity' => $selectedItemQty1[$count1]
                        ]);
    
                        $count1 ++;
                    }
                }
        
                $count2 = 0;
                foreach ($selectedItems2 as $selectedItem2) {
                    if($selectedItem2 != 0) {
                        ItemTierProcess::create([
                            'item_tier_id' => $tierid[1],
                            'item_id' => $selectedItem2,
                            'quantity' => $selectedItemQty2[$count2]
                        ]);
    
                        $count2 ++;
                    }
                }
    
                $count3 = 0;
                foreach ($selectedItems3 as $selectedItem3) {
                    if($selectedItem3 != 0) {
                        ItemTierProcess::create([
                            'item_tier_id' => $tierid[2],
                            'item_id' => $selectedItem3,
                            'quantity' => $selectedItemQty3[$count3]
                        ]);
    
                        $count3 ++;
                    }
                }
    
                $count4 = 0;
                foreach ($selectedItems4 as $selectedItem4) {
                    if($selectedItem4 != 0) {
                        ItemTierProcess::create([
                            'item_tier_id' => $tierid[3],
                            'item_id' => $selectedItem4,
                            'quantity' => $selectedItemQty4[$count4]
                        ]);
    
                        $count4 ++;
                    }
                }
    
                $count5 = 0;
                foreach ($selectedItems5 as $selectedItem5) {
                    if($selectedItem5 != 0) {
                        ItemTierProcess::create([
                            'item_tier_id' => $tierid[4],
                            'item_id' => $selectedItem5,
                            'quantity' => $selectedItemQty5[$count5]
                        ]);
    
                        $count5 ++;
                    }
                }
            }
        }

        Toast::success('Item added', 'Success');

        return redirect()->back();
    }
}
