<?php

namespace App\Http\Controllers;

use App\Drop;
use Illuminate\Http\Request;
use Toast;

class DropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drops = Drop::paginate();

        return view('admin.drops.index', compact('drops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.drops.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $drops = Drop::create($inputs);

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $drops->id); 
        }

        Toast::success('Drop added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function show(Drop $drop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Drop  $drops
     * @return \Illuminate\Http\Response
     */
    public function edit(Drop $drop)
    {
        return view('admin.drops.update', compact('drop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Drop $drop)
    {
        $inputs = $request->all();

        $drop->update($request->all());

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $drop->id); 
        }


        Toast::success('Drop updated', 'Success');

        return redirect('admin/drops');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Drop  $drop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Drop $drop)
    {
        $drop->delete();

        Toast::success('Drop Deleted', 'Success');

        return redirect('admin/drops');
    }

    /**
     * Upload Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/drops/small';
        $upload = $image->move($destinationPath, $name);
        Drop::where('id', $id)->update(['image' => $name]);
    }
}