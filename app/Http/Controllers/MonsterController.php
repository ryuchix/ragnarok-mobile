<?php

namespace App\Http\Controllers;

use App\Monster;
use App\Zone;
use App\Type;
use App\Element;
use App\Race;
use App\Size;
use App\Card;
use App\Item;
use Illuminate\Http\Request;
use Toast;
use DB;

class MonsterController extends Controller
{

    public function search(Request $request)
    {
        $search = $request->input('search');

        $monsters = Monster::where('name', 'LIKE', "%$search%")
                            ->with(['element', 'race', 'zone', 'type', 'size'])
                            ->orderBy('name', 'asc')->paginate();

        return view('admin.monsters.index', compact('monsters'));                  
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monsters = Monster::orderBy('id', 'desc')->paginate();

        return view('admin.monsters.index', compact('monsters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cards = Card::all();
        $zones = Zone::all();
        $types = Type::all();
        $elements = Element::all();
        $races = Race::all();
        $sizes = Size::all();
        $items = Item::all();

        return view('admin.monsters.add', compact('zones', 'types', 'elements', 'races', 'sizes', 'cards', 'items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $monster = Monster::create($inputs);

        if ($request->hasFile('big_image')) {
           $this->uploadBigImage($request->big_image, $monster->id); 
        }

        if ($request->has('items')) {
            $items = $request->input('items');
           
            foreach ($items as $item) {
                $monster->items()->attach($item);
            }
        }
        
        Toast::success('Monster added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Monster  $monster
     * @return \Illuminate\Http\Response
     */
    public function show(Monster $monster)
    {
        return view('admin.monsters.show', compact('monster'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Monster  $monster
     * @return \Illuminate\Http\Response
     */
    public function edit(Monster $monster)
    {   
        $cards = Card::all();
        $zones = Zone::all();
        $types = Type::all();
        $elements = Element::all();
        $races = Race::all();
        $sizes = Size::all();
        $items = Item::all();
        $drops = $monster->items()->get();
        return view('admin.monsters.update', compact('monster', 'zones', 'types', 'elements', 'races', 'sizes', 'cards', 'items', 'drops'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Monster  $monster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Monster $monster)
    {
        $inputs = $request->all();

        $monster->update($request->all());

        if ($request->hasFile('big_image')) {
           $this->uploadBigImage($request->big_image, $monster->id); 
        }
        if ($request->has('items')) {
            $item_monsters = DB::table('item_monster')->where('monster_id', $monster->id)->get();

            foreach ($item_monsters as $item_monster) {
                $monster->items()->detach($item_monster);
            }

            $items = $request->input('items');
            foreach ($items as $item) {
                $monster->items()->attach($item);
            }
        }

        Toast::success('Monster updated', 'Success');

        return redirect('admin/monsters');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Monster  $monster
     * @return \Illuminate\Http\Response
     */
    public function destroy(Monster $monster)
    {
        $monster->delete();

        Toast::success('User Deleted', 'Success');

        return redirect('admin/monsters');
    }

    /**
     * Upload Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/monsters/small';
        $upload = $image->move($destinationPath, $name);
        Monster::where('id', $id)->update(['image' => $name]);
    }

    /**
     * Upload Big Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadBigImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/monsters/large';
        $upload = $image->move($destinationPath, $name);
        Monster::where('id', $id)->update(['big_image' => $name]);
    }
}
