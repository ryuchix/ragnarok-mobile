<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Toast;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{

    public function search(Request $request)
    {
        $search = $request->input('search');

        $blogs = Blog::where('title', 'LIKE', "%$search%")
                            ->orderBy('id', 'desc')->paginate();

        return view('admin.blogs.index', compact('blogs'));                
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderBy('id', 'desc')->paginate();

        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $blog = Blog::create($inputs);

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $blog->id); 
        }

        Toast::success('Blog added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('admin.blogs.update', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $inputs = $request->all();

        $blog->update($request->all());

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $blog->id); 
        }

        Toast::success('Blog updated', 'Success');

        return redirect('admin/blogs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $blog->delete();

        Toast::success('Blog Deleted', 'Success');

        return redirect('admin/blogs');
    }

    /**
     * Upload Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        
        $destinationPath = 'uploads/images/blogs';
        $upload = $image->move($destinationPath, $name);
        Blog::where('id', $id)->update(['image' => $name]);
    }
}
