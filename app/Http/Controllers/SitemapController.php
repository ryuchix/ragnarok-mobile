<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Card;
use App\Item;
use App\Monster;
use App\Map;

class SitemapController extends Controller
{
	public function index()
	{
	  $post = Blog::orderBy('updated_at', 'desc')->first();
	  $card = Card::orderBy('updated_at', 'desc')->first();
	  $monster = Monster::orderBy('updated_at', 'desc')->first();
	  $item = Item::orderBy('updated_at', 'desc')->first();
	  $quest = Blog::where('category', 'Quest')->orderBy('updated_at', 'desc')->first();
	  $guide = Blog::where('category', 'Guide')->orderBy('updated_at', 'desc')->first();
	  $map = Map::orderBy('updated_at', 'desc')->first();

	  return response()->view('sitemap.index', [
	      'post' => $post,
	      'card' => $card,
	      'monster' => $monster,
	      'item' => $item,
	      'quest' => $quest,
	      'guide' => $guide,
	      'map' => $map
	  ])->header('Content-Type', 'text/xml');
	}

	public function blogs()
	{
	    $posts = Blog::orderBy('updated_at', 'desc')->get();

	    return response()->view('sitemap.posts', [
	        'posts' => $posts,
	    ])->header('Content-Type', 'text/xml');
	}

	public function cards()
	{
	    $cards = Card::all();
	    return response()->view('sitemap.cards', [
	        'cards' => $cards,
	    ])->header('Content-Type', 'text/xml');
	}

	public function monsters()
	{
	    $monsters = Monster::all();
	    return response()->view('sitemap.monsters', [
	        'monsters' => $monsters,
	    ])->header('Content-Type', 'text/xml');
	}

	public function items()
	{
	    $items = Item::all();
	    return response()->view('sitemap.items', [
	        'items' => $items,
	    ])->header('Content-Type', 'text/xml');
	}

	public function guides()
	{
	    $guides = Blog::where('category', 'Guide')->get();
	    return response()->view('sitemap.guides', [
	        'guides' => $guides,
	    ])->header('Content-Type', 'text/xml');
	}

	public function quests()
	{
	    $quests = Blog::where('category', 'Quest')->get();
	    return response()->view('sitemap.quests', [
	        'quests' => $quests,
	    ])->header('Content-Type', 'text/xml');
	}
	
	public function maps()
	{
	    $maps = Map::all();
	    return response()->view('sitemap.maps', [
	        'maps' => $maps,
	    ])->header('Content-Type', 'text/xml');
	}

	public function news()
	{
	    $items = Blog::orderBy('updated_at', 'desc')->get();

	    return response()->view('sitemap.news', [
	        'items' => $items,
	    ])->header('Content-Type', 'text/xml');
	}

	public function pages()
	{
	    return response()->view('sitemap.page')->header('Content-Type', 'text/xml');
	}

}
