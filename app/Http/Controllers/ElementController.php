<?php

namespace App\Http\Controllers;

use App\Element;
use Illuminate\Http\Request;
use Toast;

class ElementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $elements = Element::all();

        return view('admin.elements.index', compact('elements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $elements = Element::create($inputs);

        Toast::success('Element added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Element $element
     * @return \Illuminate\Http\Response
     */
    public function show(Element $elements)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Element $element
     * @return \Illuminate\Http\Response
     */
    public function edit(Element $element)
    {
        return view('admin.elements.update', compact('element'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Element $element
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Element $element)
    {
        $inputs = $request->all();

        $element->update($request->all());

        Toast::success('Element updated', 'Success');

        return redirect('admin/elements');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Element $element
     * @return \Illuminate\Http\Response
     */
    public function destroy(Element $element)
    {
        $element->delete();

        Toast::success('Element Deleted', 'Success');

        return redirect('admin/elements');
    }
}