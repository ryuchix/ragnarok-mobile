<?php

namespace App\Http\Controllers;

use App\ItemType;
use Illuminate\Http\Request;
use Toast;

class ItemTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemtypes = ItemType::all();

        return view('admin.itemtypes.index', compact('itemtypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.itemtypes.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $races = ItemType::create($inputs);

        Toast::success('Item Type added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Race  $itemtypes
     * @return \Illuminate\Http\Response
     */
    public function show(ItemType $itemtypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Race  $itemtypes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemtype = ItemType::find($id);
        
        return view('admin.itemtypes.update', compact('itemtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemType $race)
    {
        $inputs = $request->all();

        $race->update($request->all());

        Toast::success('Item Type updated', 'Success');

        return redirect('admin/itemtypes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Race  $race
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemType $itemtypes)
    {
        $itemtypes->delete();

        Toast::success('Item Type Deleted', 'Success');

        return redirect('admin/itemtypes');
    }
}
