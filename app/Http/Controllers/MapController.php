<?php

namespace App\Http\Controllers;

use App\Map;
use Illuminate\Http\Request;
use Toast;
use App\Monster;
use DB;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maps = Map::all();

        return view('admin.maps.index', compact('maps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $monsters = Monster::all();

        return view('admin.maps.add', compact('monsters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $maps = Map::create($inputs);

        if ($request->has('monsters')) {
            $monsters = $request->input('monsters');
           
            foreach ($monsters as $monster) {
                $maps->monsters()->attach($monster);
            }
        }

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $maps->id); 
        }

        Toast::success('Map added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Map  $map
     * @return \Illuminate\Http\Response
     */
    public function show(Map $map)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Map  $map
     * @return \Illuminate\Http\Response
     */
    public function edit(Map $map)
    {
        $monsters = Monster::all();

        $map_monsters = $map->monsters()->get();

        return view('admin.maps.update', compact('map', 'monsters', 'map_monsters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Map  $map
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Map $map)
    {
        $inputs = $request->all();

        $map->update($request->all());

        if ($request->has('monsters')) {
            $item_monsters = DB::table('map_monster')->where('map_id', $map->id)->get();

            foreach ($item_monsters as $item_monster) {
                $map->monsters()->detach($item_monster);
            }

            $monsters = $request->input('monsters');
            foreach ($monsters as $monster) {
                $map->monsters()->attach($monster);
            }
        }

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $map->id); 
        }

        Toast::success('Map updated', 'Success');

        return redirect('admin/maps');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Map  $map
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $map = Map::find($id);

        $map->delete();

        Toast::success('Map Deleted', 'Success');

        return redirect('admin/maps');
    }

    /**
     * Upload Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/maps/small';
        $upload = $image->move($destinationPath, $name);
        Map::where('id', $id)->update(['image' => $name]);
    }
}