<?php

namespace App\Http\Controllers;

use App\Card;
use App\Monster;
use Illuminate\Http\Request;
use Toast;

class CardController extends Controller
{

    public function search(Request $request)
    {
        $search = $request->input('search');

        $cards = Card::where('name', 'LIKE', "%$search%")
                            ->orderBy('name', 'asc')->paginate();

        return view('admin.cards.index', compact('cards'));                 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = Card::orderBy('id', 'desc')->paginate();

        return view('admin.cards.index', compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $monsters = Monster::all();

        return view('admin.cards.add', compact('monsters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $cards = Card::create($inputs);

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $cards->id); 
        }

        if ($request->hasFile('big_image')) {
           $this->uploadBigImage($request->big_image, $cards->id); 
        }

        Toast::success('Item added', 'Success');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function edit(Card $card)
    {
        $monsters = Monster::all();

        return view('admin.cards.update', compact('card', 'monsters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        $inputs = $request->all();

        $card->update($request->all());

        if ($request->hasFile('image')) {
           $this->uploadImage($request->image, $card->id); 
        }
 
        if ($request->hasFile('big_image')) {
           $this->uploadBigImage($request->big_image, $card->id); 
        }

        Toast::success('Card updated', 'Success');

        return redirect('admin/cards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        $card->delete();

        Toast::success('Card Deleted', 'Success');

        return redirect('admin/cards');
    }

    /**
     * Upload Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/cards/small';
        $upload = $image->move($destinationPath, $name);
        Card::where('id', $id)->update(['image' => $name]);
    }

    /**
     * Upload Big Image
     *
     * @param Request $request
     * @param image image, big_image
     * @return void
     */
    public function uploadBigImage($image, $id)
    {
        $name = time() . '.' . $image->getClientOriginalName();
        $destinationPath = 'uploads/images/cards/large';
        $upload = $image->move($destinationPath, $name);
        Card::where('id', $id)->update(['big_image' => $name]);
    }
}
