<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTier extends Model
{
    protected $fillable = [
    	'item_id',
    	'tier',
    	'effect'
    ];

	public function item()
	{
	    return $this->belongsTo('App\Item', 'item_id');
	}

	public function process()
	{
		return $this->hasMany('App\ItemTierProcess', 'item_tier_id');
	}
}

