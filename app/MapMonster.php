<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapMonster extends Model
{
    protected $fillable = ['map_id', 'monster_id'];
}
