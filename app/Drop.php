<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Services\Slug;

class Drop extends Model
{
	use SLug;

    protected $fillable = ['name', 'slug', 'image', 'type', 'description'];

    protected static function boot() {
        parent::boot();

        static::creating(function($drop) {
            $drop->slug = $drop->createDropSlug($drop->name);
        });
    }

    public function items()
    {
        return $this->belongsToMany('App\Item');
    }
}
