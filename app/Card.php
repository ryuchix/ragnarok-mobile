<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Card extends Model
{
	use Slug;
	
    protected $fillable = ['name', 'content', 'image', 'big_image', 'equipable', 'type', 'dropped_by', 'monster_id', 'buff', 'deposit'];

    protected static function boot() {
        parent::boot();

        static::creating(function($card) {
            $card->slug = $card->createCardSlug($card->name);
        });
    }

    public function monster()
    {
    	return $this->belongsTo('App\Monster');
    }
}
