<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = [

        'job_id',
    	'name',
    	'image',
    	'type',
    	'casting_range',
    	'breakthrough',
    	'special_cost',
    	'item',
    	'qty',
    	'requirement',
    	'skill',
    	'skill_level',
    	'cast_time',
    	'pre_requisite',
    	'level'

    ];

    public function jobs()
    {
        return $this->belongsTo('App\Job', 'job_id');
    }

    public function levels()
    {
        return $this->hasMany('App\SkillLevel', 'skill_id');
    }

}
