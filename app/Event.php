<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Slug;

class Event extends Model
{
    use Slug;
    
    protected $fillable = [
    	'name',
    	'slug',
    	'sub_name',
    	'description',
    	'start_date',
    	'end_date',
    	'image',
    	'location',
    	'type',
    	'parent_event'
    ];

    protected static function boot() {
        parent::boot();

        static::creating(function($event) {
            $event->slug = $event->createEventSlug($event->name);
        });
    }
}
