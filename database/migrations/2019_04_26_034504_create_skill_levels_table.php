<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('skill_id')->unsigned()->index();
            $table->integer('level');
            $table->string('cast_delay')->nullable();
            $table->string('sp_cost')->nullable();
            $table->string('description')->nullable();
            $table->string('cast_time')->nullable();
            $table->string('hp_cost')->nullable();
            $table->string('cooldown')->nullable();
            $table->boolean('bt')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_levels');
    }
}
