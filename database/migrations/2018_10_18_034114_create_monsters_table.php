<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonstersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monsters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug', 150)->unique();
            $table->string('description')->nullable();
            $table->string('content')->nullable();
            $table->string('image')->nullable();
            $table->string('big_image')->nullable();
            $table->string('level')->nullable();
            $table->string('atk')->nullable();
            $table->string('matk')->nullable();
            $table->string('def')->nullable();
            $table->string('mdef')->nullable();
            $table->string('hit')->nullable();
            $table->string('flee')->nullable();
            $table->string('hp')->nullable();
            $table->string('movement_speed')->nullable();
            $table->string('atk_speed')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('zone_id')->nullable();
            $table->integer('race_id')->nullable();
            $table->integer('element_id')->nullable();
            $table->integer('size_id')->nullable();
            $table->integer('card_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monsters');
    }
}
