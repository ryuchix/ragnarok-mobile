<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('content')->nullable();
            $table->string('buff')->nullable();
            $table->string('image')->nullable();
            $table->string('big_image')->nullable();
            $table->string('equipable')->nullable();
            $table->string('type')->nullable();
            $table->string('dropped_by')->nullable();
            $table->integer('monster_id')->unsigned()->index();
            $table->foreign('monster_id')->references('id')->on('monsters')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
