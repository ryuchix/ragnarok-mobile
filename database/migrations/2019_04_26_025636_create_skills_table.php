<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned()->index();
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('type');
            $table->string('casting_range')->nullable();
            $table->boolean('breakthrough')->default(false);
            $table->boolean('special_cost')->default(false);
            $table->integer('item')->nullable();
            $table->integer('qty')->nullable();
            $table->boolean('requirement')->default(false);
            $table->integer('skill')->nullable();
            $table->integer('skill_level')->nullable();
            $table->string('cast_time')->nullable();
            $table->boolean('pre_requisite')->default(false);
            $table->integer('level')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skills');
    }
}
