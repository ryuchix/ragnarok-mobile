$( document ).ready(function() {

    $('body').on('click', '.mvps', function() {
        $('.mvp').show();
        $('.mini').prop('style', 'display: none');

        $(this).addClass('selected');
        $('.minis').removeClass('selected');
    });

    $('body').on('click','.minis', function() {
        $('.mini').show();
        $('.mvp').prop('style', 'display: none');

        $('.table-responsive').last().show();

        $(this).addClass('selected');
        $('.mvps').removeClass('selected');

    });

        $('.table').first().addClass("mvp");
        $('.table').last().addClass("mini");

        $('.table-responsive').last().prop('style', 'display:none');
        
        var con =   '<div id="options">'+
                        '<ul id="filters" class="option-set" data-option-key="filter">'+
                            '<li><a href="#filters" class="mvps selected">MVP</a></li>'+
                            '<li><a href="#filters" class="minis">Mini Boss</a></li>   '+
                        '</ul>        '+                                   
                        '<div class="clear"></div>'+
                    '</div>';
        
        $('#put-here').append(con);

        $('.align-middle').find('a').removeAttr('title');
        $('.align-middle').removeAttr('style');

        $('.align-middle > a').each(function(){
            // drake
            this.href = this.href.replace('/db/monsters/72004/', '/monster/drake');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/drake_mvp.png", "uploads/images/monsters/large/1544075501.drake_mvp.png");
                $(el).attr("src", newSrc);
                var link = $(el).parent().data('content');
                var href = link.replace("<a href='", '');
                var hreff = href.split("'>")[0];
                $(el).parent().attr('href', hreff);
            });

            // angeling
            this.href = this.href.replace('/db/monsters/72001/', '/monster/angeling');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/angeling_mvp.png", "uploads/images/monsters/large/1543915670.angeling_mvp.png");
                $(el).attr("src", newSrc);
            });

            // gtb
            this.href = this.href.replace('/db/monsters/72002/', '/monster/golden-thief-bug');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/golden_bug_mvp.png", "uploads/images/monsters/large/1543993695.golden_bug_mvp.png");
                $(el).attr("src", newSrc);
            });

            // devling
            this.href = this.href.replace('/db/monsters/72003/', '/monster/deviling');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/deviling_mvp.png", " uploads/images/monsters/large/1544001204.deviling_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // mistress
            this.href = this.href.replace('/db/monsters/72007/', '/monster/mistress');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/bee_queen_mvp.png", "uploads/images/monsters/large/1544081250.bee_queen_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // phreeoni
            this.href = this.href.replace('/db/monsters/72009/', '/monster/phreeoni');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/phreeoni_mvp.png", "uploads/images/monsters/large/1544083917.phreeoni_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // maya
            this.href = this.href.replace('/db/monsters/72008/', '/monster/maya');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/maya_mvp.png", "uploads/images/monsters/large/1544082180.maya_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // goblin leader
            this.href = this.href.replace('/db/monsters/72006/', '/monster/goblin-leader');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/goblin_king_mvp.png", "uploads/images/monsters/large/1544080138.goblin_king_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // strouf
            this.href = this.href.replace('/db/monsters/72005/', '/monster/strouf');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/strouf_mvp.png", "uploads/images/monsters/large/1544077110.strouf_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // atroce_mvp
            this.href = this.href.replace('/db/monsters/72023/', '/monster/atroce');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/atroce_mvp.png", "uploads/images/monsters/large/1544148085.atroce_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // orc hero
            this.href = this.href.replace('/db/monsters/72014/', '/monster/orc-hero');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/orc_hero_mvp.png", "uploads/images/monsters/large/1544145985.orc_hero_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // osiris
            this.href = this.href.replace('/db/monsters/72012/', '/monster/osiris');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/osiris_mvp.png", "uploads/images/monsters/large/1544086481.osiris_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // eddga
            this.href = this.href.replace('/db/monsters/72011/', '/monster/eddga');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/eddga_mvp.png", "uploads/images/monsters/large/1544085843.eddga_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // moonlight-flower
            this.href = this.href.replace('/db/monsters/72013/', '/monster/moonlight-flower');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/moonlight_mvp.png", "uploads/images/monsters/large/1544145202.moonlight_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // kobold-leader
            this.href = this.href.replace('/db/monsters/72024/', '/monster/kobold-leader');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/kobold_leader_mvp.png", "uploads/images/monsters/large/1544146278.kobold_leader_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // baphomet
            this.href = this.href.replace('/db/monsters/72021/', '/monster/baphomet');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/baphomet_mvp.png", "uploads/images/monsters/large/1544152683.baphomet_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // spashire
            this.href = this.href.replace('/db/monsters/72029/', '/monster/spashire');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/specialre_mvp.png", "uploads/images/monsters/large/1544156836.specialre_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // bloody-knight
            this.href = this.href.replace('/db/monsters/72020/', '/monster/bloody-knight');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/bloody_knight_mvp.png", "uploads/images/monsters/large/1544152206.bloody_knight_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // time-holder
            this.href = this.href.replace('/db/monsters/72028/', '/monster/time-holder');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/timeholder_mvp.png", "uploads/images/monsters/large/1544156139.timeholder_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // doppelganger
            this.href = this.href.replace('/db/monsters/72015/', '/monster/doppelganger');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/doppelganger_mvp.png", "uploads/images/monsters/large/1544147608.doppelganger_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // dark-lord
            this.href = this.href.replace('/db/monsters/72025/', '/monster/dark-lord');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/dark_lord_mvp.png", "uploads/images/monsters/large/1544152819.dark_lord_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // owl-baron
            this.href = this.href.replace('/db/monsters/72018/', '/monster/owl-baron');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/owl_baron_mvp.png", "uploads/images/monsters/large/1544151193.owl_baron_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // orc-lord
            this.href = this.href.replace('/db/monsters/72016/', '/monster/orc-lord');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/orc_lord_mvp.png", "uploads/images/monsters/large/1544148686.orc_lord_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // detarderous
            this.href = this.href.replace('/db/monsters/72017/', '/monster/detarderous');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/detale_mvp.png", "uploads/images/monsters/large/1544150652.detale_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // chimera
            this.href = this.href.replace('/db/monsters/30019/', '/monster/chimera');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/chimera_mvp.png", "uploads/images/monsters/large/1544151762.chimera_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // huge-deviling
            this.href = this.href.replace('/db/monsters/30030/', '/monster/huge-deviling');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/deviling_mvp.png", "uploads/images/monsters/large/1544152930.deviling_mvp%20(1).png");
                $(el).attr("src", newSrc);
            });
            
            // dracula
            this.href = this.href.replace('/db/monsters/30031/', '/monster/dracula');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/dracula_mvp.png", "uploads/images/monsters/large/1544153064.dracula_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // randgris
            this.href = this.href.replace('/db/monsters/30032/', '/monster/randgris');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/valkyrie_mvp.png", "uploads/images/monsters/large/1544153171.valkyrie_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // stormy-knight
            this.href = this.href.replace('/db/monsters/30040/', '/monster/stormy-knight');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/knight_of_windstorm_mvp.png", "uploads/images/monsters/large/1544159087.knight_of_windstorm_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // hatii
            this.href = this.href.replace('/db/monsters/30039/', '/monster/hatii');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/garm_mvp.png", "uploads/images/monsters/large/1544167562.garm_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // firelord-kaho
            this.href = this.href.replace('/db/monsters/30044/', '/monster/firelord-kaho');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/lordkaho_mvp.png", "uploads/images/monsters/large/1544169843.lordkaho_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // arc-angeling
            this.href = this.href.replace('/db/monsters/30022/', '/monster/arc-angeling');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/archangeling_mvp.png", "uploads/images/monsters/large/1544171713.archangeling_mvp.png");
                $(el).attr("src", newSrc);
            });
            
            // arc-angeling
            this.href = this.href.replace('/db/monsters/30045/', '/monster/arc-angeling');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/archangeling_mvp.png", "uploads/images/monsters/large/1544171713.archangeling_mvp.png");
                $(el).attr("src", newSrc);
            });

            // smokie
            this.href = this.href.replace('/db/monsters/71001/', '/monster/smokie');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/smokie_mini.png", "uploads/images/monsters/large/1543571195.smokie_mini.png");
                $(el).attr("src", newSrc);
            });

            // ghostring
            this.href = this.href.replace('/db/monsters/71004/', '/monster/ghosting');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/ghostring_mini.png", "uploads/images/monsters/large/1543999831.ghostring_mini.png");
                $(el).attr("src", newSrc);
            });

            // dragon fly
            this.href = this.href.replace('/db/monsters/71007/', '/monster/dragon-fly');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/dragonfly_mini.png", "uploads/images/monsters/large/1544079953.dragonfly_mini.png");
                $(el).attr("src", newSrc);
            });

            // eclipse
            this.href = this.href.replace('/db/monsters/71002/', '/monster/eclipse');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/eclipse_mini.png", "uploads/images/monsters/large/1543571398.eclipse_mini.png");
                $(el).attr("src", newSrc);
            });

            // vocal
            this.href = this.href.replace('/db/monsters/71003/', '/monster/vocal');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/rocker_mini.png", "uploads/images/monsters/large/1543913363.rocker_mini.png");
                $(el).attr("src", newSrc);
            });

            // rotar zairo
            this.href = this.href.replace('/db/monsters/71006/', '/monster/rotar-zairo');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/goblin_helicopter_mini.png", "uploads/images/monsters/large/1544076824.goblin_helicopter_mini.png");
                $(el).attr("src", newSrc);
            });

            // toad
            this.href = this.href.replace('/db/monsters/71005/', '/monster/toad');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("http://s01.cdn.roguard.net/content/img/icons/monsters/rodafrog_mini.png", "uploads/images/monsters/large/1544075381.rodafrog_mini.png");
                $(el).attr("src", newSrc);
            });

            // mastering
            this.href = this.href.replace('/db/monsters/71017/', '/monster/mastering');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/masterring_mini.png", "uploads/images/monsters/large/1543625686.masterring_mini.png");
                $(el).attr("src", newSrc);
            });

            // orc baby
            this.href = this.href.replace('/db/monsters/71010/', '/monster/orc-baby');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/orc_baby_mini.png", "uploads/images/monsters/large/1544145478.orc_baby_mini.png");
                $(el).attr("src", newSrc);
            });

            // anubis
            this.href = this.href.replace('/db/monsters/71018/', '/monster/anubis');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/anubis_mini.png", "uploads/images/monsters/large/1544085668.anubis_mini.png");
                $(el).attr("src", newSrc);
            });

            // wolf
            this.href = this.href.replace('/db/monsters/71008/', '/monster/vagabond-wolf');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("http://s01.cdn.roguard.net/content/img/icons/monsters/vagabondwolf_mini.png", "uploads/images/monsters/large/1544081098.vagabondwolf_mini.png");
                $(el).attr("src", newSrc);
            });

            // wood goblin
            this.href = this.href.replace('/db/monsters/72010/', '/monster/wood-goblin');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/wilow_huge1_mini.png", "uploads/images/monsters/large/1544083800.wilow_huge1_mini.png");
                $(el).attr("src", newSrc);
            });

            // hyuegen
            this.href = this.href.replace('/db/monsters/71019/', '/monster/hyegun');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/hyegun_mini.png", "uploads/images/monsters/large/1544144266.hyegun_mini.png");
                $(el).attr("src", newSrc);
            });

            // mutant-dragon
            this.href = this.href.replace('/db/monsters/71012/', '/monster/mutant-dragon');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/mutant_dragon_mini.png", "uploads/images/monsters/large/1544148211.mutant_dragon_mini.png");
                $(el).attr("src", newSrc);
            });

            // gryphon
            this.href = this.href.replace('/db/monsters/71009/', '/monster/gryphon');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/grypjon_mini.png", "uploads/images/monsters/large/1544085559.grypjon_mini.png");
                $(el).attr("src", newSrc);
            });

            // jakk
            this.href = this.href.replace('/db/monsters/71011/', '/monster/jakk');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/nanguaxiansheng_mini.png", "uploads/images/monsters/large/1544145800.nanguaxiansheng_mini.png");
                $(el).attr("src", newSrc);
            });

            // zherlthsh
            this.href = this.href.replace('/db/monsters/71016/', '/monster/zherlthsh');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/no_icon_mini_boss.png", "uploads/images/monsters/large/1544151492.zherlthsh-204.png");
                $(el).attr("src", newSrc);
            });

            // alice
            this.href = this.href.replace('/db/monsters/71015/', '/monster/alice');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/alice_mini.png", "uploads/images/monsters/large/1544150872.alice_mini.png");
                $(el).attr("src", newSrc);
            });

            // mysteltainn
            this.href = this.href.replace('/db/monsters/71014/', '/monster/mysteltainn');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/mysteltainn_mini_mini.png", "uploads/images/monsters/large/1544151662.mysteltainn_mini_mini.png");
                $(el).attr("src", newSrc);
            });

            // rafflesia
            this.href = this.href.replace('/db/monsters/71023/', '/monster/rafflesia');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/raffliesia_mini.png", "uploads/images/monsters/large/1544148498.raffliesia_mini.png");
                $(el).attr("src", newSrc);
            });

            // owl-duke
            this.href = this.href.replace('/db/monsters/71013/', '/monster/owl-duke');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/owl_duke_mini.png", "uploads/images/monsters/large/1544150547.owl_duke_mini.png");
                $(el).attr("src", newSrc);
            });

            // clock
            this.href = this.href.replace('/db/monsters/71024/', '/monster/clock');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/bigben_mini.png", "uploads/images/monsters/large/1544154803.bigben_mini.png");
                $(el).attr("src", newSrc);
            });

            // chepet
            this.href = this.href.replace('/db/monsters/71026/', '/monster/chepet');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/chepet_mini.png", "uploads/images/monsters/large/1544161428.chepet_mini.png");
                $(el).attr("src", newSrc);
            });

            // clock-tower-manager
            this.href = this.href.replace('/db/monsters/71025/', '/monster/clock-tower-manager');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/tower_manager_mini.png", "uploads/images/monsters/large/1544155196.tower_manager_mini.png");
                $(el).attr("src", newSrc);
            });

            // dark-illusion
            this.href = this.href.replace('/db/monsters/71020/', '/monster/dark-illusion');
            $(this).find("img").each(function(k, el) {
                var newSrc = $(el).attr("src").replace("//s01.cdn.roguard.net/content/img/icons/monsters/dark_illusion_mini.png", "uploads/images/monsters/large/1544152089.dark_illusion_mini.png");
                $(el).attr("src", newSrc);
            });

        });


        var head1 = "<thead> <tr> <th></th> <th>3F</th> <th>6F</th> <th>13F</th> <th>16F</th> <th>23F</th> <th>26F</th> <th>33F</th> <th>36F</th> <th>43F</th> <th>46F</th> <th>53F</th> <th>56F</th> <th>63F</th> <th>66F</th> <th>73F</th> <th>76F</th> <th>83F</th> <th>86F</th> <th>93F</th> <th>96F</th> </tr> </thead>";

        var head2 = "<thead><tr style='font-weight: bold;'><th></th><th>10F</th><th>20F</th><th>30F</th><th>40F</th><th>50F</th><th>60F</th><th>70F</th><th>80F</th><th>90F</th><th>100F</th></tr></thead>";

        $('.mvp > tbody' ).after( head2 );

        $('.mini > tbody' ).after( head1 );

        
    });
