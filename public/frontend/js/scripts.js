$(document).ready(function(){	


	    var x = setInterval(function() {

      $('.checkDate').each(function(){
          var countDownDate = new Date($(this).data('countdown')).getTime();
          var startDate = new Date($(this).data('start')).getTime();

          // Get todays date and time
          var now = new Date().getTime();

          // Find the distance between now and the count down date
          var distance = countDownDate - now;
          var distance2 = startDate - now;

          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          var days2 = Math.floor(distance2 / (1000 * 60 * 60 * 24));
          var hours2 = Math.floor((distance2 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes2 = Math.floor((distance2 % (1000 * 60 * 60)) / (1000 * 60));
          var seconds2 = Math.floor((distance2 % (1000 * 60)) / 1000);
        
          if(now < startDate) {
            $(this).html("<small style='letter-spacing: 0em;'>"+"starts on " + days2 + "d " + hours2 + "h " + minutes2 + "m " + seconds2 + "s "+"</small>");
          } else {
            $(this).html("<small style='letter-spacing: 0em;'>"+"ends on " + days + "d " + hours + "h " + minutes + "m " + seconds + "s "+"</small>");
          }

          // If the count down is finished
          if (distance < 0) {
            clearInterval(x);
            $(this).parent().css({"text-decoration": "line-through", "color": "#ff0000"}).html("<small style='letter-spacing: 0em;'>event ended</small>");
          }
        });

      }, 1000);

/***************************************************
	MENU
***************************************************/
jQuery('ul.nav li.dropdown').hover(function (){
    jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    jQuery('a').hover(function() {
        var height = jQuery(this).offset();
        jQuery(this).parent().find('.sub-menu').css('top', height.top - +106);
    });
}, function (){
    jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();      
});

jQuery('.btn-navbar').on('click',function(){
    jQuery('nav#main_menu > .nav-collapse > ul.navbar-nav').slideDown();
});

/***************************************************
		TOOLTIP & POPOVER
***************************************************/
$("[rel=tooltip]").tooltip();
$("[data-rel=tooltip]").tooltip();


/***************************************************
		HOVERS
***************************************************/
	$(".hover_img, .hover_colour").on('mouseover',function(){
			var info=$(this).find("img");
			info.stop().animate({opacity:0.1},500);
		}
	);
	$(".hover_img, .hover_colour").on('mouseout',function(){
			var info=$(this).find("img");
			info.stop().animate({opacity:1},800);
		}
	);
	
/***************************************************
		BACK TO TOP LINK
***************************************************/
			$(window).scroll(function() {
				if ($(this).scrollTop() > 200) {
					$('.go-top').fadeIn(200);
				} else {
					$('.go-top').fadeOut(200);
				}
			});
			
			// Animate the scroll to top
			$('.go-top').click(function(event) {
				event.preventDefault();
				
				$('html, body').animate({scrollTop: 0}, 300);
			})
		});	

/***************************************************
	IFRAME
***************************************************/
	$("iframe").each(function(){
		var ifr_source = $(this).attr('src');
		var wmode = "wmode=transparent";
		if(ifr_source.indexOf('?') != -1) {
		var getQString = ifr_source.split('?');
		var oldString = getQString[1];
		var newString = getQString[0];
		$(this).attr('src',newString+'?'+wmode+'&'+oldString);
		}
		else $(this).attr('src',ifr_source+'?'+wmode);
	});
	


