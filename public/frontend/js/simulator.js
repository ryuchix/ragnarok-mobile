	var job1 = 0;
	var job2 = 0;
	var job3 = 0;
	var count1 = 0;
	var count2 = 0;
	var count3 = 0;
 	$(function(){

  		initRejob();

    	$('.skill-icon img').click(function(){
    		// stops adding when t reaches 0
    		var rej = $('#rejob').text();
				var int_rej = parseInt(rej);
				if(int_rej == 0) {
					return;
			}
			count1 = count1+1;
			count2 = count2+1;
			count3 = count3+1;
			var now = $(this).parent().parent('.skill').attr('now');
			var max = $(this).parent().parent('.skill').attr('max');
			var job = $(this).parent().parent('.skill').attr('job');
			var preNum = $(this).parent().parent('.skill').attr('preNum');

      		if(now*1 < max*1 && checkAdd(job)){
		       	if(preNum){
			       	var preLevel = $(this).parent().parent('.skill').attr('preLevel');
				    if(preLevel*1 > $('.skill').eq(preNum).attr('now')*1){
				        return;
				    }
				}

			    $(this).parent().parent('.skill').find('em').html((now*1+1)+'/'+max);
			    $(this).parent().parent('.skill').attr('now', now*1+1);
			    $(this).parent().parent('.skill').find('b').show();
			    $(this).parent().parent('.skill').find('p').hide();
			    $(this).parent().parent('.skill').find('p').eq(now*1+1).show();
			    changeRejob(job, 'add');

				if(job==1){
			 		var skillpoint = $('#smax1').find('#re').text()
			 		var int_skillpoint = parseInt(skillpoint);
					$('#smax1').find('#re').text(int_skillpoint+1);
				}
				if(job==2){
			 		var skillpoint = $('#smax2').find('#re').text()
			 		var int_skillpoint = parseInt(skillpoint);
					$('#smax2').find('#re').text(int_skillpoint+1);
				}
				if(job==3){
			 		var skillpoint = $('#smax3').find('#re').text()
			 		var int_skillpoint = parseInt(skillpoint);
					$('#smax3').find('#re').text(int_skillpoint+1);
				}

			}
  		});

  		$('.skill b').click(function(){
			count1 = count1-1;
			count2 = count2-1;
			count3 = count3-1;
			var now = $(this).parent('.skill-icon').parent('.skill').attr('now');
			var max = $(this).parent('.skill-icon').parent('.skill').attr('max');
			var job = $(this).parent('.skill-icon').parent('.skill').attr('job');
			var aftNum = $(this).parent('.skill-icon').parent('.skill').attr('aftNum');

		    if(now*1 >0 && checkDel(job)){

		   		if(aftNum){
		        	var aftLevel = $(this).parent('.skill-icon').parent('.skill').attr('aftLevel');
		    		if($('.skill').eq(aftNum).attr('now')*1 > 0 && aftLevel*1 >= now*1){
		         		return;
			    	}
			    }

			    $(this).parent('.skill-icon').parent('.skill').find('em').html((now*1-1)+'/'+max);
			    $(this).parent('.skill-icon').parent('.skill').attr('now', now*1-1);
			    $(this).parent('.skill-icon').parent('.skill').find('p').hide();
			    $(this).parent('.skill-icon').parent('.skill').find('p').eq(now*1-1).show();

			    changeRejob(job, 'del');

			    if(now*1-1==0){
			        $(this).hide();
			    }

				if(job==1){
			 		var skillpoint = $('#smax1').find('#re').text()
			 		var int_skillpoint = parseInt(skillpoint);
					$('#smax1').find('#re').text(int_skillpoint-1);
				}
				if(job==2){
			 		var skillpoint = $('#smax2').find('#re').text()
			 		var int_skillpoint = parseInt(skillpoint);
					$('#smax2').find('#re').text(int_skillpoint-1);
				}
				if(job==3){
			 		var skillpoint = $('#smax3').find('#re').text()
			 		var int_skillpoint = parseInt(skillpoint);
					$('#smax3').find('#re').text(int_skillpoint-1);
				}	





		   	}
		});
 	});

 	function checkAdd(job){
		var jobval = job;
		var level = 120;

		if(jobval==1 && job==2){
			return false;
		}

		if(jobval==1 && job==3){
			return false;
		}

		if(jobval==2 && job==3){
			return false;
		}

		var sum1 = 0;
		$('#job1 .skill').each(function(){
			sum1 += $(this).attr('now')*1;
		})

		var sum2 = 0;
		$('#job2 .skill').each(function(){
			sum2 += $(this).attr('now')*1;
		})

		var sum3 = 0;
		$('#job3 .skill').each(function(){
			sum3 += $(this).attr('now')*1;
		})

		if(job==1){
			if(jobval==1 && sum1>=120){
				return false;
			}else if(jobval==2 && sum1>=40+level*1){
				return false;
			}else if(jobval==3 && sum2>=40+level*1){
				return false;
			}
		}
		if(job==2){
			if(sum1<40){
				return false;
			}
			if(jobval==2 && sum1+sum2>=40+level*1){
				return false;
			}
		}

		if(job==3){
			if(sum1<40){
				return false;
			}
			if(sum2<40){
				return false;
			}
			if(jobval==3 && sum1+sum2+sum3>=80+level*1){
				return false;
			}
		}

		return true;
 	}

	function checkDel(job){
		var jobval = 3;

		if(jobval==2 && job==1){
			var sum1 = 0;
			$('#job1 .skill').each(function(){
				sum1 += $(this).attr('now')*1;
			});

			var sum2 = 0;
			$('#job2 .skill').each(function(){
				sum2 += $(this).attr('now')*1;
			});

			if(sum2>0 && sum1<=40){
				return false;
			}
		}

		if(jobval==3 && job==1){
			var sum1 = 0;
			$('#job1 .skill').each(function(){
				sum1 += $(this).attr('now')*1;
			});

			var sum2 = 0;
			$('#job2 .skill').each(function(){
				sum2 += $(this).attr('now')*1;
			});

			if(sum2>0 && sum1<=40){
				return false;
			}
		}

		if(jobval==3 && job==2){
			var sum2 = 0;
			$('#job2 .skill').each(function(){
				sum2 += $(this).attr('now')*1;
			});

			var sum3 = 0;
			$('#job3 .skill').each(function(){
				sum3 += $(this).attr('now')*1;
			});

			if(sum3>0 && sum2<=40){
				return false;
			}
		} 

		return true;
 	}

	function changeJob(){
		var job = $('#job').val();
		var html='';
		var end=0;

		if(job==1){
			end = 40;
		} else if(job==2){
			end = 40;
		} else if(job==3){
			end = 40;
		}

		for(var i=1; i<=end; i++){
			html += '<option value ="'+i+'">'+i+'</option>';
		}

		$('#level').html(html);
 	}

 	function initRejob(){
		var jobval = 3;
		var level = 40;
		var rejob1 = 40;
		var rejob2 = 40;
		var rejob3 = 40;
		var rejob= rejob1+rejob2+rejob3;
		job1 = rejob1;
		job2 = rejob2;
		job3 = rejob3;
		$('#rejob1').html(rejob1);
		$('#rejob2').html(rejob2);
		$('#rejob3').html(rejob3);  
		$('.rejob').html(rejob);
 	}

 	function changeRejob(job, change){
		var job = job;
		if(change=='add'){
			if(job==1 ){
				job1 = job1-1;
			}else if(job==2 ){
				job2 = job2-1;
			}else if(job==3){
				job3 = job3-1;
			}
		} else if(change=='del'){
			if(job==1 ){
				job1 = job1+1;
			} else if(job==2 ){
				job2 = job2+1;
			} else if(job==3){
				job3 = job3+1;
			}
		}

		$('.rejob').html(job1+job2+job3);
 	}

 	function clear(){
		$('.skill').attr('now',0);
		$('.skill .skill-icon b').hide();

		$('.skill').each(function(){
			var max = $(this).attr('max');
			$(this).find('.skill-icon').find('em').html('0/'+max);
		});
	}

 	document.onselectstart = new Function("return false");

 		$('button#open-modal').click(function(){

 		var smax1 = $('.smax1').text();
 		var smax2 = $('.smax2').text();
 		var smax3 = $('.smax3').text();

		$('#myskill_1').empty();
		$('#myskill_2').empty();
		$('#myskill_3').empty();
		$('#screen').empty();
		$('#download_image').hide();
		$('#screen_this').show();

		if(parseInt(smax1) > 0) {
			$('.first_job').css('display', 'block');
			$('.first_job').find('span').text(smax1);
		}

		if(parseInt(smax2) > 0) {
			$('.second_job').css('display', 'block');
			$('.second_job').find('span').text(smax2);
		}

		if(parseInt(smax3) > 0) {
			$('.third_job').css('display', 'block');
			$('.third_job').find('span').text(smax3);
		}

		var totals = 0;
		$('#job1 .skill').each(function(){
			var title = $(this).find('span').text();
			var image = $(this).find('.skill-image').attr('src');
			var now = $(this).attr('now');
			var max = $(this).attr('max');

			if(now != 0) {
			var html = '<div class="skills text-sm col-xs-6 col-sm-3">'+
							'<div class="skill">'+
								'<img crossorigin="anonymous" class="skill-image " src="'+image+'" width="88px">'+
								'<div class="info">'+
									'<div class="name">'+
										'<font style="vertical-align: inherit;">'+
											'<font style="vertical-align: inherit;">'+title+'</font>'+
										'</font>'+
									'</div>'+
									'<div class="level text-info">'+
										'<font style="vertical-align: inherit;">'+
											'<font style="vertical-align: inherit;">Lv. </font>'+
											'<span>'+
												'<font style="vertical-align: inherit;">'+now+'</font>'+
											'</span>'+
										'</font>'+
										'<span>'+
											'<font style="vertical-align: inherit;"></font>'+
										'</span>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>';
			}

			$('#myskill_1').append(html);

			totals += now++;	
		});

		$('#job2 .skill').each(function(){
			var title = $(this).find('span').text();
			var image = $(this).find('.skill-image').attr('src');
			var now = $(this).attr('now');
			var max = $(this).attr('max');

			if(now != 0) {
			var html = '<div class="skills text-sm col-xs-6 col-sm-3">'+
							'<div class="skill">'+
								'<img crossorigin="anonymous" class="skill-image " src="'+image+'" width="88px">'+
								'<div class="info">'+
									'<div class="name">'+
										'<font style="vertical-align: inherit;">'+
											'<font style="vertical-align: inherit;">'+title+'</font>'+
										'</font>'+
									'</div>'+
									'<div class="level text-info">'+
										'<font style="vertical-align: inherit;">'+
											'<font style="vertical-align: inherit;">Lv. </font>'+
											'<span>'+
												'<font style="vertical-align: inherit;">'+now+'</font>'+
											'</span>'+
										'</font>'+
										'<span>'+
											'<font style="vertical-align: inherit;"></font>'+
										'</span>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>';
			}

			$('#myskill_2').append(html);

			totals += now++;	
		});

		$('#job3 .skill').each(function(){
			var title = $(this).find('span').text();
			var image = $(this).find('.skill-image').attr('src');
			var now = $(this).attr('now');
			var max = $(this).attr('max');

			if(now != 0) {
			var html = '<div class="skills text-sm col-xs-6 col-sm-3">'+
							'<div class="skill">'+
								'<img crossorigin="anonymous" class="skill-image " src="'+image+'" width="88px">'+
								'<div class="info">'+
									'<div class="name">'+
										'<font style="vertical-align: inherit;">'+
											'<font style="vertical-align: inherit;">'+title+'</font>'+
										'</font>'+
									'</div>'+
									'<div class="level text-info">'+
										'<font style="vertical-align: inherit;">'+
											'<font style="vertical-align: inherit;">Lv. </font>'+
											'<span>'+
												'<font style="vertical-align: inherit;">'+now+'</font>'+
											'</span>'+
										'</font>'+
										'<span>'+
											'<font style="vertical-align: inherit;"></font>'+
										'</span>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>';
			}

			$('#myskill_3').append(html);

			totals += now++;	
		});

			$('#used-points').text(totals);
			$('#remaining-points').text($('#rejob').text());

	});

	$('#screen_this').on('click', function(){
		$(this).hide();
	    $('#screen_thiss').show();
		html2canvas(document.querySelector("#capture")).then(canvas => {
		    var append = $('#screen').append(canvas);
			if(append) {
				$('#screen_thiss').hide();
				$('#download_image').show();
			}

		});



	});

	$('#download_image').on('click', function(){

	    html2canvas(document.querySelector('#capture')).then(function(canvas) {

	        saveAs(canvas.toDataURL(), 'ragnarokmobile.net.png');

		});
		
	});

	function saveAs(uri, filename) {

	    var link = document.createElement('a');

	    if (typeof link.download === 'string') {

	        link.href = uri;
	        link.download = filename;

	        //Firefox requires the link to be in the body
	        document.body.appendChild(link);

	        //simulate click
	        link.click();

	        //remove the link when done
	        document.body.removeChild(link);

	    } else {

	        window.open(uri);

	    }
	}